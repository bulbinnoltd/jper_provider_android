package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.component.HttpsService

class ForgotPasswordActivity : AppCompatActivity() {
    var lang:String? = "en"
    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pw)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })


        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")

        val forgot_pw_value = findViewById<TextInputEditText>(R.id.forgot_pw_value)

        val forgot_pw_submit_button = findViewById<Button>(R.id.forgot_pw_submit_button)
        // set on-click listener
        forgot_pw_submit_button.setOnClickListener {
            sendinfo(forgot_pw_value.text)

        }
        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            super.onBackPressed()
        }

    }
    fun sendinfo(email: Editable? ){
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE

        val msg = HttpsService.ForgotPassword(this,"https://uat.jper.com.hk/api/forget/password",email.toString(),lang)
        if (msg!=null){
            Loading!!.visibility = View.GONE
            progressOverlay!!.visibility = View.GONE
            showDialog(this,msg)
        }
    }
    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
            val intent = Intent (this, FirstPageActivity::class.java)
            startActivity(intent)
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
}
