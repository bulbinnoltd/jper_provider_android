package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.myproject.PaymentPlanNotice
import com.koushikdutta.ion.Ion
import java.text.DecimalFormat
import java.util.*


class PaymentNoticeActivity : AppCompatActivity() {
//
//    val posts: ArrayList<ProgressPhotoStyle> = ArrayList()
//    var style_adapter = ProgressPhotoAdapter(posts,this)
//

    private lateinit var paymentIntentClientSecret: String
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var plan_id :Int? = null
    var project_id :Int? = null
    var notice_id :Int? = null
    var is_final :Boolean = false
    var header : String? = null
    var lang : String? = null
    var owner_name : String? = null
    var owner_email : String? = null
    var company_address : String? = null
    var company_name_zh : String? = null
    var company_name_en : String? = null
    var due_date : String=""
    var paymentplan_data: PaymentPlanNotice?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_payment_notice)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        try {
            plan_id = intent.getIntExtra("plan_id",0)
            notice_id = intent.getIntExtra("notice_id",0)
            project_id = intent.getIntExtra("project_id",0)
            owner_name = intent.getStringExtra("owner_name")
            owner_email = intent.getStringExtra("owner_email")
            company_address = intent.getStringExtra("company_address")
            company_name_en = intent.getStringExtra("company_name_en")
            company_name_zh = intent.getStringExtra("company_name_zh")
            due_date = intent.getStringExtra("due_date")
            is_final = intent.getBooleanExtra("is_final",false)


        }
        catch (e:java.lang.Exception){
            is_final = false
            Log.d("project_id_get",e.toString())
        }

        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
//        style_listview.adapter = style_adapter
//        style_listview.layoutManager = LinearLayoutManager (this, RecyclerView.HORIZONTAL,false)
//        // set on-click listener

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val not_final_view = findViewById(R.id.not_final_view) as LinearLayout
        val not_final_space = findViewById(R.id.not_final_space) as View
        val pn_provider_company_name = findViewById(R.id.pn_provider_company_name) as TextView
        val pn_provider_address = findViewById(R.id.pn_provider_address) as TextView
        val pn_owner_username = findViewById(R.id.pn_owner_username) as TextView
        val pn_owener_email = findViewById(R.id.pn_owener_email) as TextView
        val pn_project_title = findViewById(R.id.pn_project_title) as TextView
        val pn_payment_name_value = findViewById(R.id.pn_payment_name_value) as TextView
        val pn_invoice_amount_value =findViewById(R.id.pn_invoice_amount_value) as TextView
        val pn_amount_due_value = findViewById(R.id.pn_amount_due_value) as TextView
        val pn_due_by = findViewById(R.id.pn_due_by) as TextInputEditText
        val pn_retention_applied_value = findViewById(R.id.pn_retention_applied_value) as TextView
        val pn_create_button = findViewById(R.id.pn_create_button) as Button

        pn_due_by.setText(due_date)
        pn_owner_username.setText(owner_name)
        pn_owener_email.setText(owner_email)
        pn_provider_address.text = company_address
        if(lang.equals("en")){
            pn_provider_company_name.setText(company_name_en)
        }
        else{
            pn_provider_company_name.setText(company_name_zh)

        }
        Thread(Runnable {
            Log.d("planid_project_id",plan_id.toString() +"  "+ project_id.toString())
            paymentplan_data = HttpsService.GetPaymentPlanNotice(
                this,
                "https://uat.jper.com.hk/api/company/project/".plus(project_id).plus("/payment/")
                    .plus(plan_id).plus("/notice"),
                header,
                lang
            )
            val staffprofile = HttpsService.GetUserProfile(this,"https://uat.jper.com.hk/api/company/staff/profile",header,lang)
            val companyfile = HttpsService.GetCompanyInfo(this,"https://uat.jper.com.hk/api/company/profile",header,lang)
            // try to touch View of UI thread
            runOnUiThread(java.lang.Runnable {

                if(paymentplan_data!=null){
                    if(lang.equals("zh")) pn_provider_company_name.text = paymentplan_data!!.company_name_zh else pn_provider_company_name.text =  paymentplan_data!!.company_name_en

                    pn_project_title.text = getString(R.string.project_title).plus(" # ").plus(project_id)
                    pn_payment_name_value.text = paymentplan_data!!.payment_title
                    try {
                        val number: String = paymentplan_data!!.plan_amount
                        val amount = number.toDouble()
                        val formatter = DecimalFormat("#,###.##")
                        var formatted = formatter.format(amount)
                        if (formatted.contains(".00")) formatted =
                            formatted.substring(0, formatted.length - 3)
                        pn_invoice_amount_value.text = "HK $" + formatted
                    } catch (e: Exception) {
                        e.printStackTrace()
                        pn_invoice_amount_value.text = "HK$ ".plus(paymentplan_data!!.plan_amount)
                    }
                    try {
                        val number: String = paymentplan_data!!.retention_amount
                        val amount = number.toDouble()
                        val formatter = DecimalFormat("#,###.##")
                        var formatted = formatter.format(amount)
                        if (formatted.contains(".00")) formatted =
                            formatted.substring(0, formatted.length - 3)
                        pn_retention_applied_value.text = "HK $" + formatted
                    } catch (e: Exception) {
                        e.printStackTrace()
                        pn_retention_applied_value.text = "HK$ ".plus(paymentplan_data!!.retention_amount)
                    }
                    try {
                        val number: String = paymentplan_data!!.plan_amount
                        val amount = number.toDouble()
                        val formatter = DecimalFormat("#,###.##")
                        var formatted = formatter.format(amount)
                        if (formatted.contains(".00")) formatted =
                            formatted.substring(0, formatted.length - 3)
                        pn_amount_due_value.text = "HK $" + formatted
                    } catch (e: Exception) {
                        e.printStackTrace()
                        pn_amount_due_value.text = "HK$ ".plus(paymentplan_data!!.plan_amount)
                    }


                    if(is_final!! && paymentplan_data!!.scope ==1){
                        not_final_view.visibility = View.GONE
                        not_final_space.visibility = View.GONE
                    }
                }
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()


        pn_create_button.setOnClickListener{
            var result:Boolean?=null
            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE
            Thread(Runnable {
                result = HttpsService.CreatePaymentNotice(
                    this,
                    "https://uat.jper.com.hk/api/company/project/payment/notice/create",
                    header,
                    plan_id!!,
                    project_id!!,
                    pn_due_by.text.toString(),
                    lang
                )
                runOnUiThread(java.lang.Runnable {

                    if (result != null) {

                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        val dialog = Dialog(this)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        dialog.setCanceledOnTouchOutside(false)
                        dialog.setContentView(R.layout.dialog_message)
                        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
                        dialog_message.text = getString(R.string.Success_msg)
                        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

                        val close_button = dialog.findViewById(R.id.close_button) as ImageView
                        close_button.visibility = View.INVISIBLE
                        confirm_button.setOnClickListener {
                            dialog.dismiss()
                            val editor = getSharedPreferences(
                                "MyPreferences",
                                Context.MODE_PRIVATE
                            ).edit()
                            editor.putBoolean("refresh_paymentplan", true)
                            editor.apply()
                            finish()
                        }

                        close_button.setOnClickListener {
                            dialog.dismiss()
                        }

                        dialog.setCancelable(true)
                        dialog.show()

                    }
                })
            }).start()
        }
        pn_due_by.setOnClickListener{
            val dpd = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    pn_due_by.setText(""+mYear+"-"+(mMonth+1)+"-"+mDay)
                },year,month,day)
            dpd.datePicker.minDate = System.currentTimeMillis() -1000
            if(lang.equals("en")) {
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancle_button), dpd)
            }
            else{
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE,  getString(R.string.cancle_button), dpd)
            }
            dpd.show()
        }
        val back_button = findViewById(R.id.back_button) as ImageView
        back_button.setOnClickListener {
            onBackPressed()
        }
    }

    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }

}
//    override fun onNothingSelected(parent: AdapterView<*>?) {
//        Toast.makeText(applicationContext,"Please Select", Toast.LENGTH_LONG).show()
//    }
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        var items:String = parent?.getItemAtPosition(position) as String
//        Toast.makeText(applicationContext,"$items",Toast.LENGTH_LONG).show()
//    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == Activity.RESULT_OK) {
//            //Image Uri will not be null for RESULT_OK
//            val fileUri = data?.data
//            Log.d("fileUri",fileUri.toString())
//            //You can get File object from intent
//            ImagePicker.getFile(data)
//            val photo_position = this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).getInt("progress_photo_position",999)
//            posts.get(photo_position).fileUri = fileUri
//            for(i in 0..9){
//                Log.d("arraylist",posts.get(i).fileUri.toString() )
//            }
//
//            style_adapter.refreshDataset()
//            //You can also get File Path from intent
//            val filePath: String? = ImagePicker.getFilePath(data)
//        } else if (resultCode == ImagePicker.RESULT_ERROR) {
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
//        } else {
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
//        }
//    }
