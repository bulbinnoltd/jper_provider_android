package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonObject
import com.koushikdutta.ion.Ion
import java.util.*


class AddContractItemActivity : AppCompatActivity() {
//
//    val posts: ArrayList<ProgressPhotoStyle> = ArrayList()
//    var style_adapter = ProgressPhotoAdapter(posts,this)
//

    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    var project_id:Int?=null
    private var lang : String? = "en"
    private var header : String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_contract_item)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        try {
            project_id = intent.getIntExtra("project_id",0)
            Log.d("project_id_get","ok")
        }
        catch (e:java.lang.Exception){
            Log.d("project_id_get",e.toString())
        }
        val back_button = findViewById(R.id.back_button) as ImageView
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
//        style_listview.adapter = style_adapter
//        style_listview.layoutManager = LinearLayoutManager (this, RecyclerView.HORIZONTAL,false)
//        // set on-click listener
        back_button.setOnClickListener {
            super.onBackPressed()
        }

        if (!lang!!.equals("")){
            val locale = Locale(lang)
            val config = resources.configuration
            Locale.setDefault(locale)
            config.locale = locale
            resources.updateConfiguration(config, resources.displayMetrics)
        }

        val add_contract_item_title = findViewById<TextInputEditText>(R.id.add_contract_item_title)
        val add_contract_item_unit_price = findViewById<TextInputEditText>(R.id.add_contract_item_unit_price)
        val add_contract_item_qty = findViewById<TextInputEditText>(R.id.add_contract_item_qty)
        val add_contract_item_amount = findViewById<TextView>(R.id.add_contract_item_amount)
        val add_contract_completion_date = findViewById<TextInputEditText>(R.id.add_contract_completion_date)
        val add_contract_remarks = findViewById<TextInputEditText>(R.id.add_contract_remarks)

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        add_contract_item_qty.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    s!!
                }
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    s!!
                    try{
                        add_contract_item_amount.text =
                            (s.toString().toDouble() * add_contract_item_unit_price.text.toString().toDouble()).toInt().toString()
                    }catch (e:Exception){
                        add_contract_item_amount.text ="0"
                        Log.d("text_changed","failed")
                    }
                }
                override fun afterTextChanged(s: Editable?) {
                    s!!
                }
            })
        add_contract_item_unit_price.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                s!!
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s!!
                try{
                    add_contract_item_amount.text =
                        (s.toString().toDouble() * add_contract_item_qty.text.toString().toDouble()).toInt().toString()
                }catch (e:Exception){
                    add_contract_item_amount.text ="0"
                    Log.d("text_changed",e.toString())
                }
            }
            override fun afterTextChanged(s: Editable?) {
                s!!
            }
        })


        add_contract_completion_date.setOnClickListener{
            val dpd = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    add_contract_completion_date.setText(""+mYear+"-"+(mMonth+1)+"-"+mDay)
                },year,month,day)
            dpd.datePicker.minDate = System.currentTimeMillis() -1000
            if(lang.equals("en")) {
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancle_button), dpd)
            }
            else{
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE,  getString(R.string.cancle_button), dpd)
            }
            dpd.show()
        }

        var contract_title:String? =null
        var contract_unit_price:String? =null
        var contract_qty:String? =null
        var contract_amount:String? =null
        var contract_completion_date:String? =null
        var contract_remarks:String? =null

        val add_contract_submit_button = findViewById<Button>(R.id.add_contract_submit_button)
        add_contract_submit_button.setOnClickListener {

            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE

            try { contract_title=  add_contract_item_title.text.toString()}catch (x: Exception) { }
            try { contract_unit_price=  add_contract_item_unit_price.text.toString()}catch (x: Exception) { }
            try { contract_qty=  add_contract_item_qty.text.toString()}catch (x: Exception) { }
            try { contract_amount=  add_contract_item_amount.text.toString()}catch (x: Exception) { }
            try {
                contract_completion_date=   add_contract_completion_date.text.toString()

            }catch (x: Exception) { }
            try { contract_remarks=  add_contract_remarks.text.toString()}catch (x: Exception) { }

            if(contract_title.equals("") ||contract_unit_price.equals("") || contract_qty.equals("") || contract_amount.equals(""))
            {
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
                showDialog(this,getString(R.string.Without_info))
            }
            else{

                Thread(Runnable {

                    try {
                        val json = JsonObject()
                        json.addProperty("project_id", project_id)
                        json.addProperty("item", contract_title)
                        json.addProperty("u_price", contract_unit_price!!.toFloat())
                        json.addProperty("quantity", contract_qty!!.toFloat())
                        json.addProperty("complete_date", contract_completion_date!!)
                        json.addProperty("remark", contract_remarks)

                        Ion.with(baseContext)
                            .load("https://uat.jper.com.hk/api/company/project/budget/task/add")
                            .setHeader("Authorization", "Bearer " +header)
                            .setHeader("Content-Language", lang)
                            .setJsonObjectBody(json)
                            .asJsonObject()
                            .setCallback { e, result ->
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE
                                if (e != null)
                                    Log.d("result_error", e.toString())
                                if (result["result"].asString.equals("success")) {

                                    Log.d("result_updated", "result_updated")

                                    val dialog = Dialog(this)
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                    dialog.setCanceledOnTouchOutside(false)
                                    dialog.setContentView(R.layout.dialog_message)

                                    val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
                                    dialog_message.text =  getString(R.string.Success_msg)
                                    val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

                                    val close_button = dialog.findViewById(R.id.close_button) as ImageView
                                    close_button.visibility=View.INVISIBLE
                                    confirm_button.setOnClickListener {
                                        dialog.dismiss()
                                        val editor = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
                                        editor.putBoolean("refresh_contract",true)
                                        editor.apply()
                                        finish()
                                    }
                                    close_button.setOnClickListener {
                                        dialog.dismiss()
                                    }

                                    dialog.setCancelable(true)
                                    dialog.show()


                                }
                                else if (result["result"].asString.equals("error")) {

                                    showDialog(this, result["msg"].asString)
                                }
                                Log.d("result_okok", result.toString())
                            }
                    }
                    catch (x: Exception) {
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        Log.d("result_error", x.toString())
                    }

                }).start()
            }

        }
//        add_contract_item_title.
//        posts.add(ProgressPhotoStyle("1", null, 0))
//        posts.add(ProgressPhotoStyle("1", null, 1))
//        posts.add(ProgressPhotoStyle("1", null, 2))
//        posts.add(ProgressPhotoStyle("1", null, 0))
//        posts.add(ProgressPhotoStyle("1", null, 0))
//        posts.add(ProgressPhotoStyle("1", null, 0))
//        posts.add(ProgressPhotoStyle("1", null, 0))
//        posts.add(ProgressPhotoStyle("1", null, 0))
//        posts.add(ProgressPhotoStyle("1", null, 0))
//        posts.add(ProgressPhotoStyle("1", null, 0))
//



//        val items = arrayOf("Material", "Design", "Components", "Android")
//        val adapter = ArrayAdapter(this, R.layout.item_text_option, items)
//        val project_type_value = findViewById<AutoCompleteTextView>(R.id.spinner)
//        project_type_value?.setAdapter(adapter)
//

//        style_adapter = ProgressPhotoAdapter(posts,this)
//        val style_listview = findViewById<View>(R.id.progress_photo_listview) as RecyclerView

    }

    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }

}
//    override fun onNothingSelected(parent: AdapterView<*>?) {
//        Toast.makeText(applicationContext,"Please Select", Toast.LENGTH_LONG).show()
//    }
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        var items:String = parent?.getItemAtPosition(position) as String
//        Toast.makeText(applicationContext,"$items",Toast.LENGTH_LONG).show()
//    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode == Activity.RESULT_OK) {
//            //Image Uri will not be null for RESULT_OK
//            val fileUri = data?.data
//            Log.d("fileUri",fileUri.toString())
//            //You can get File object from intent
//            ImagePicker.getFile(data)
//            val photo_position = this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).getInt("progress_photo_position",999)
//            posts.get(photo_position).fileUri = fileUri
//            for(i in 0..9){
//                Log.d("arraylist",posts.get(i).fileUri.toString() )
//            }
//
//            style_adapter.refreshDataset()
//            //You can also get File Path from intent
//            val filePath: String? = ImagePicker.getFilePath(data)
//        } else if (resultCode == ImagePicker.RESULT_ERROR) {
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
//        } else {
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
//        }
//    }
