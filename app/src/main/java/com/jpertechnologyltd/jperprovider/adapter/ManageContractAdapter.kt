package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.ManageContract
import com.jpertechnologyltd.jperprovider.ui.myproject.ManageContractFragment


class ManageContractAdapter (private val mManageContract: List<ManageContract>) : RecyclerView.Adapter<ManageContractAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val manage_contract_title = itemView.findViewById<TextView>(R.id.manage_contract_title)
        val manage_contract_edit = itemView.findViewById<ImageView>(R.id.manage_contract_edit_icon)
        val manage_contract_view = itemView.findViewById<ImageView>(R.id.manage_contract_view_icon)
        val manage_contract_download = itemView.findViewById<ImageView>(R.id.manage_contract_download_icon)
        val manage_contract_delete = itemView.findViewById<ImageView>(R.id.manage_contract_delete_icon)
        val manage_contract_layout = itemView.findViewById<LinearLayout>(R.id.manage_contract_layout)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val manageContractView = inflater.inflate(R.layout.item_manage_contract, parent, false)
        // Return a new holder instance
        return ViewHolder(manageContractView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val ManageContract: ManageContract = mManageContract.get(position)
        // Set item views based on your views and data model
        val manage_contract_title = viewHolder.manage_contract_title
        val manage_contract_edit = viewHolder.manage_contract_edit
        val manage_contract_view = viewHolder.manage_contract_view
        val manage_contract_download = viewHolder.manage_contract_download
        val manage_contract_delete = viewHolder.manage_contract_delete
        val manage_contract_layout = viewHolder.manage_contract_layout


        manage_contract_title.setText(ManageContract.title)
        manage_contract_edit.setImageURI(ManageContract.view)
        manage_contract_view.setImageURI(ManageContract.download)
        manage_contract_download.setImageURI(ManageContract.view)
        manage_contract_delete.setImageURI(ManageContract.delete)


        manage_contract_layout.setOnClickListener(View.OnClickListener { arg0 ->

            val activity = arg0.getContext() as AppCompatActivity
            var fr = activity.supportFragmentManager?.beginTransaction()
            fr?.replace(R.id.nav_host_fragment,
                ManageContractFragment()
            )
            fr?.commit()
        })
    }//quote_subtitle.setText(quote.subtitle)
//        val res: Int =
//            context?.getResources()!!.getIdentifier(quote.image, "drawable", context!!.getPackageName())
//        imageView.setImageResource(res)

//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mManageContract.size
    }
}

//fun ImageView.setImageURI(view: String) {

//}



