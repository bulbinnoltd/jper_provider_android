package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.QuoteProject
import com.jpertechnologyltd.jperprovider.ui.quoteproject.QuoteProjectDetailFragment


class QuoteProjectBackupAdapter (private val mQuoteProjects: List<QuoteProject>) : RecyclerView.Adapter<QuoteProjectBackupAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val quote_title = itemView.findViewById<TextView>(R.id.quote_title)
        val quote_status = itemView.findViewById<TextView>(R.id.projects_status)
        val quote_district = itemView.findViewById<TextView>(R.id.projects_district)
        val quote_price = itemView.findViewById<TextView>(R.id.projects_price)
        val quote_style = itemView.findViewById<TextView>(R.id.projects_style)
        val quote_type = itemView.findViewById<TextView>(R.id.projects_type)
        val quote_date = itemView.findViewById<TextView>(R.id.projects_postedDate_title)
        val quote_layout = itemView.findViewById<LinearLayout>(R.id.quote_layout)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val quoteView = inflater.inflate(R.layout.item_partialbidding, parent, false)
        // Return a new holder instance
        return ViewHolder(quoteView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val quote: QuoteProject = mQuoteProjects.get(position)
        // Set item views based on your views and data model
        val quote_title = viewHolder.quote_title
        val quote_status = viewHolder.quote_status
        val quote_district = viewHolder.quote_district
        val quote_price = viewHolder.quote_price
        val quote_style = viewHolder.quote_style
        val quote_type = viewHolder.quote_type
        val quote_date = viewHolder.quote_date
        val quote_layout = viewHolder.quote_layout

//        quote_title.setText(quote.title)
//        if(quote.status.equals("Urgent")){
//            quote_status.visibility= View.VISIBLE
//            val drawable =    quote_status.getBackground() as GradientDrawable
//            drawable.setColor(Color.rgb(224,67,30))
//           quote_status.setText(quote.status)
//        }
//        else{
//            quote_status.visibility= View.GONE
//        }
//        quote_district.setText(quote.district)
//        quote_price.setText(quote.price)
//        quote_style.setText(quote.style)
//        quote_type.setText(quote.type)
//        quote_date.setText(quote.date)

        quote_layout.setOnClickListener(View.OnClickListener { arg0 ->

            val activity = arg0.getContext() as AppCompatActivity
            var fr = activity.supportFragmentManager?.beginTransaction()

            fr?.replace(R.id.nav_host_fragment, QuoteProjectDetailFragment())
            fr?.commit()
        })
}//quote_subtitle.setText(quote.subtitle)
//        val res: Int =
//            context?.getResources()!!.getIdentifier(quote.image, "drawable", context!!.getPackageName())
//        imageView.setImageResource(res)

//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mQuoteProjects.size
    }
}