package com.jpertechnologyltd.jperprovider.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.jpertechnologyltd.jperprovider.EditProgressImageActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.ProgressPhotoStyle
import com.jpertechnologyltd.jperprovider.ui.profile.ProfileFragment
import com.stfalcon.imageviewer.StfalconImageViewer
import java.io.File

class ProgressPhotoAdapter(
    private val mStyles: ArrayList<ProgressPhotoStyle>,
    private val activity: Activity,
    private val project_id:Int
) : RecyclerView.Adapter<ProgressPhotoAdapter.ViewHolder>()
{

    var context:Context?=null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val progress_photo_view = itemView.findViewById<CardView>(R.id.image_cardview)
        val progress_photo_imageview = itemView.findViewById<ImageView>(R.id.imageView)
        val progress_photo_date = itemView.findViewById<TextView>(R.id.progress_photo_item_date)
        val delete_image_button = itemView.findViewById<Button>(R.id.delete_image_button)
        val delete_image_button_view = itemView.findViewById<ConstraintLayout>(R.id.delete_image_button_view)


    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val styleView = inflater.inflate(R.layout.item_progress_image, parent, false)
        // Return a new holder instance
        return ViewHolder(styleView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val style: ProgressPhotoStyle = mStyles.get(position)
        // Set item views based on your views and data model
        val progress_photo_view = viewHolder.progress_photo_view
        val progress_photo_imageview = viewHolder.progress_photo_imageview
        val progress_photo_date = viewHolder.progress_photo_date
        val delete_image_button = viewHolder.delete_image_button
        val delete_image_button_view = viewHolder.delete_image_button_view
        progress_photo_date.text= style.date

        if(style.fromServer) {

            delete_image_button.setOnClickListener {
                var save_result: Boolean? = null
//                val dialog = Dialog(activity.applicationContext)
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//                dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//                dialog.setCancelable(false)
//                dialog.setContentView(R.layout.dialog_confirm)
//
//                val dialog_message = dialog.findViewById(R.id.message) as TextView
//                dialog_message.text = context!!.getString(R.string.dialog_delete_image_msg)
//                val confirm_button = dialog.findViewById(R.id.save_button) as Button
//
//                val close_button = dialog.findViewById(R.id.close_button) as ImageView
//                val cancel_button = dialog.findViewById(R.id.cancel_button) as Button
//                close_button.visibility = View.INVISIBLE
////                confirm_button.setOnClickListener {
////                    dialog.dismiss()
////
                    Thread(Runnable {

                        var header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                            ?.getString("user_token","")
                        var lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                            ?.getString("lang","en")
                        save_result = HttpsService.DeleteProgressImage(
                            context!!,
                            "https://uat.jper.com.hk/api/company/project/budget/image/remove",
                            header,
                            style.id,
                            project_id,
                            lang
                        )
                        activity?.runOnUiThread(Runnable {


                            if (save_result!=null) {

//
//
//                                val dialog = Dialog(context!!)
//                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//                                dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//                                dialog.setCancelable(false)
//                                dialog.setContentView(R.layout.dialog_message)
//
//                                val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
//                                if(save_result!!)dialog_message.text = context!!.getString(R.string.Success_msg)
//                                else dialog_message.text = "Unknown Error"
//                                val confirm_button = dialog.findViewById(R.id.confirm_button) as Button
//
//                                val close_button = dialog.findViewById(R.id.close_button) as ImageView
//                                close_button.visibility=View.INVISIBLE
//                                confirm_button.setOnClickListener {
//                                    dialog.dismiss()
                                    val editor = activity.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
                                    editor.putBoolean("refresh_contract",true)
                                    editor.apply()
                                    (activity as EditProgressImageActivity).finish()
//                                }
//                                close_button.setOnClickListener {
//                                    dialog.dismiss()
//                                    val editor = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
//                                    editor.putBoolean("refresh_contract",true)
//                                    editor.apply()
//                                    (activity as EditProgressImageActivity).finish()
//
//                                }
//
//                                dialog.setCancelable(true)
//                                dialog.show()

                            }
                        })
                    }).start()
                }



            try {
//                val glideUrl = GlideUrl(
//                    style.image_link,
//                    LazyHeaders.Builder()
//                        .addHeader("Authorization", "Bearer $header")
//                        .build()
//                )
//                Glide.with(context!!).load(glideUrl)
//                    .into(progress_photo_imageview)
                val header =
                    context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                        .getString("user_token", "")

                val glideUrl = GlideUrl(
                    style.image_link,
                    LazyHeaders.Builder()
                        .addHeader("Authorization", "Bearer ".plus(header))
                        .build()
                )


                Glide.with(context!!).load(glideUrl).placeholder(R.drawable.image_placeholder).into(progress_photo_imageview)
                progress_photo_imageview.setOnClickListener{

                    val profileimage =
                        arrayOf<String?>(style.image_link)

                    StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->
                        val glideUrl = GlideUrl(
                            image,
                            LazyHeaders.Builder()
                                .addHeader("Authorization", "Bearer ".plus(header))
                                .build()
                        )

                        Glide.with(context!!).load(glideUrl).placeholder(R.drawable.image_placeholder).into(view)

                    }.show()
                }
            } catch (e: Exception) {
                Glide.with(context!!).load(R.drawable.image_placeholder)
                    .into(progress_photo_imageview)
            }

        }
        else{
            delete_image_button.setOnClickListener{
                removeAt(position)
            }

            try {
                progress_photo_imageview.setImageURI(Uri.parse(style.fileUri))

                progress_photo_imageview.setOnClickListener{

                    val profileimage =
                        arrayOf<String?>(style.fileUri)

                    StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->
                        view.setImageURI(Uri.parse(image))

                    }.show()
                }
            }
            catch (e: Exception) {
                Glide.with(context!!).load(R.drawable.image_placeholder)
                    .into(progress_photo_imageview)
            }
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mStyles.size
    }

    fun refreshDataset() {
        notifyDataSetChanged()
    }
    fun removeAt(position: Int) {
        this.mStyles.removeAt(position)
//        (activity as RetentionDepositNoticeActivity).UpdatePosition(count)
        notifyItemRemoved(position)
        notifyDataSetChanged()
    }
    fun getFiles(): ArrayList<File>?{
        val Files = ArrayList<File>()
        for(index in mStyles.indices) {
            if (mStyles.get(index).fileUri!=null && !mStyles.get(index).fromServer){
                Files.add(File(mStyles.get(index).fileUri.toString()))
            }
        }
        if (Files.size>0) return Files
        else return null
    }

}