package com.jpertechnologyltd.jperprovider.adapter

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.MyProjects
import com.jpertechnologyltd.jperprovider.ui.myproject.MyProjectsQuotedDetailFragment


class MyProjectsQuotedAdapter (private val  mMyProjects: ArrayList<MyProjects>, private val parentFragment: Fragment) : RecyclerView.Adapter<MyProjectsQuotedAdapter.ViewHolder>()
{
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val myprojects_title = itemView.findViewById<TextView>(R.id.myprojects_title)
        val myprojects_status = itemView.findViewById<TextView>(R.id.myprojects_status)
        val myprojects_district = itemView.findViewById<TextView>(R.id.myprojects_district)
        val myprojects_price = itemView.findViewById<TextView>(R.id.myprojects_price)
        val myprojects_style = itemView.findViewById<TextView>(R.id.myprojects_style)
        val myprojects_type = itemView.findViewById<TextView>(R.id.myprojects_type)
        val chipgroup = itemView.findViewById<ChipGroup>(R.id.chipgroup)
        val myprojects_submitDate = itemView.findViewById<TextView>(R.id.myprojects_submitDate)
        val projectView = itemView.findViewById<CardView>(R.id.projectView)
        val myprojects_imageView = itemView.findViewById<CardView>(R.id.myprojects_imageView)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_myprojects, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val myprojects: MyProjects = mMyProjects.get(position)
        // Set item views based on your views and data model
        val myprojects_title = viewHolder.myprojects_title
        val myprojects_district = viewHolder.myprojects_district
        val myprojects_price = viewHolder.myprojects_price
        val myprojects_style = viewHolder.myprojects_style
        val myprojects_type = viewHolder.myprojects_type
        val chipgroup = viewHolder.chipgroup
        val myprojects_status = viewHolder.myprojects_status
        val projectView = viewHolder.projectView
        val myprojects_submitDate = viewHolder.myprojects_submitDate
        val myprojects_imageView = viewHolder.myprojects_imageView

        myprojects_title.setText(myprojects.title)
        myprojects_district.setText(myprojects.district)
        myprojects_price.setText(myprojects.price)
        myprojects_style.setText(myprojects.style)
        myprojects_type.setText(myprojects.type)
        myprojects_imageView.visibility=View.INVISIBLE

        val chipCategory = arrayOf(
            "Full Kitchen","Flooring","test"
        )

        for (index in chipCategory.indices) {
            val chip = Chip(chipgroup.context)
            chip.text= chipCategory.get(index)
            chipgroup.addView(chip)
        }

        myprojects_submitDate.setText(myprojects.date)
//        val drawable2 = myprojects_submitDate.getBackground() as GradientDrawable
//        drawable2.setColor(Color.rgb(45,56,68))

        myprojects_status.setText("Pending")
        val drawable = myprojects_status.getBackground() as GradientDrawable
        drawable.setColor(Color.rgb(237,176,173))
        projectView.setOnClickListener(View.OnClickListener { arg0 ->

            var fr =(parentFragment.activity as MainActivity?)?.supportFragmentManager?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.add(R.id.nav_host_fragment, MyProjectsQuotedDetailFragment())
            fr?.commit()
        })
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mMyProjects.size
    }
}