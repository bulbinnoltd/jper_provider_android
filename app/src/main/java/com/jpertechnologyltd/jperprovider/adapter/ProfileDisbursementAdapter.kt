package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Disbursement
import com.jpertechnologyltd.jperprovider.item.DisbursementStatus


class ProfileDisbursementAdapter (private val  mDisbursements: ArrayList<Disbursement>, private val parentFragment: Fragment) : RecyclerView.Adapter<ProfileDisbursementAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val dis_project_title = itemView.findViewById<TextView>(R.id.dis_project_title)
        val dis_after_proj_retention_title = itemView.findViewById<TextView>(R.id.dis_after_proj_retention_title)
        val dis_after_proj_retention_amount = itemView.findViewById<TextView>(R.id.dis_after_proj_retention_amount)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
         context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_disbursement_short, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        viewHolder.setIsRecyclable(false)
        val disbursementitem: Disbursement = mDisbursements.get(position)
        // Set item views based on your views and data model

        val dis_project_title = viewHolder.dis_project_title
        val dis_after_proj_retention_title = viewHolder.dis_after_proj_retention_title
        val dis_after_proj_retention_amount = viewHolder.dis_after_proj_retention_amount

        dis_project_title.text = "Project #" + disbursementitem.project_id
        dis_after_proj_retention_amount.text = "HK$"+disbursementitem.release_amount
        Log.d("profile_dis","Project #" + disbursementitem.project_id + "; HK$"+disbursementitem.release_amount )

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mDisbursements.size
    }
}