package com.jpertechnologyltd.jperprovider.adapter;


import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener;
import com.jpertechnologyltd.jperprovider.item.ProjectCategory;
import com.jpertechnologyltd.jperprovider.item.ProjectStatus;
import com.jpertechnologyltd.jperprovider.item.QuoteStatus;
import com.jpertechnologyltd.jperprovider.item.mybids.MyProjects;
import com.jpertechnologyltd.jperprovider.ui.mybids.MybidsQuoteFragment;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MyBiddingAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    private ArrayList<MyProjects> myProjectsList;
    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;



    public MyBiddingAdapter(ArrayList<MyProjects> myProjects, RecyclerView recyclerView) {
        myProjectsList = myProjects;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
//        ////////////////////////////////ad_mob///////////////////////////
//        if (position % 5 == 0)
//            return AD_TYPE;
//        ////////////////////////////////ad_mob///////////////////////////
        return myProjectsList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_mybids, parent, false);

            vh = new BiddingViewHolder(v);
        }
//        ////////////////////////////////ad_mob///////////////////////////
//        else if(viewType == AD_TYPE) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.list_item_native_ad, parent, false);
//            vh = new UnifiedNativeAdViewHolder(v);
//
//        }
//        ////////////////////////////////ad_mob///////////////////////////
        else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        if (holder instanceof BiddingViewHolder) {

            final MyProjects singleMyProject= (MyProjects) myProjectsList.get(position);


            ((BiddingViewHolder) holder).myprojects_title.setText(context.getResources().getString(R.string.project_title)+" #"+singleMyProject.getProject_id());
            ((BiddingViewHolder) holder).myprojects_status.setText(String.valueOf(singleMyProject.getStatus_id()));
            Log.d("mybidstatus",String.valueOf(singleMyProject.getStatus_id()));
            ((BiddingViewHolder) holder).myprojects_district.setText(singleMyProject.getDistrict());
            ((BiddingViewHolder) holder).myprojects_style.setText(singleMyProject.getType());
            if(singleMyProject.getArea().equals("")){
                ((BiddingViewHolder) holder).myprojects_style_space.setVisibility(View.GONE);
                ((BiddingViewHolder) holder).myprojects_area_unit.setVisibility(View.GONE);
                ((BiddingViewHolder) holder).myprojects_area.setVisibility(View.GONE);
            }
            else {
                ((BiddingViewHolder) holder).myprojects_area.setText((singleMyProject.getArea()));
            }
            ((BiddingViewHolder) holder).myprojects_type.setText(singleMyProject.getScope());
            try {
                double amount_min = Double.parseDouble(singleMyProject.getMin());
                double amount_max = Double.parseDouble(singleMyProject.getMax());
                DecimalFormat formatter = new DecimalFormat("#,###.##");
                String formatted_min = formatter.format(amount_min);
                String formatted_max = formatter.format(amount_max);
                if(formatted_min.contains(".00"))  formatted_min = formatted_min.substring(0, formatted_min.length() - 3);
                if(formatted_max.contains(".00"))  formatted_max = formatted_max.substring(0, formatted_max.length() - 3);
                ((BiddingViewHolder) holder).myprojects_price.setText("HK $" + formatted_min + " - "+formatted_max );
            }
            catch (Exception e){
                e.printStackTrace();
                ((BiddingViewHolder) holder).myprojects_price.setText("HK$ "+ singleMyProject.getMin() + " - "+singleMyProject.getMax());
            }

            ((BiddingViewHolder) holder).myprojects_submitDate.setText(singleMyProject.getPost_date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'");
            try {
                Date newDate = format.parse(singleMyProject.getPost_date());
                format = new SimpleDateFormat("yyyy-MM-dd");
                String date = format.format(newDate);
                ((BiddingViewHolder) holder).myprojects_submitDate.setText(date);

            } catch (ParseException e) {
                e.printStackTrace();
                ((BiddingViewHolder) holder).myprojects_submitDate.setText(singleMyProject.getPost_date());
            }

            ChipGroup chipGroup = new ChipGroup(((BiddingViewHolder) holder).chipgroup.getContext());
            String status = new QuoteStatus(context, singleMyProject.getStatus_id()).getName();
            Log.d("status_bidding_2",status);

            Integer status_rgb  = new QuoteStatus(context, singleMyProject.getStatus_id()).getRgb();
            ((BiddingViewHolder) holder).myprojects_status.setText(status);
            GradientDrawable drawable = (GradientDrawable) ((BiddingViewHolder) holder).myprojects_status.getBackground();
            drawable.setColor(status_rgb);

            ArrayList<String> project_category_array = new ArrayList<String>();
            if(singleMyProject.getCategory()!=null ){
                if(singleMyProject.getCategory().size()>0) {
                    for (int index : singleMyProject.getCategory())
                    {
                        // only changes num, not the array element
                        Log.d("cat_proj",new ProjectCategory(context, (index)).getName());
                        Log.d("cat_proj2",String.valueOf(index));
                        project_category_array.add(new ProjectCategory(context, index).getName());
                    }
                    for(String genre : project_category_array) {
                        Chip chip = new Chip(((MyBiddingAdapter.BiddingViewHolder) holder).chipgroup.getContext());
                        chip.setText(genre);
                        chip.setTextSize(12);
                        chip.setChipStartPadding(2);
                        chip.setChipEndPadding(0);
                        chip.setIconEndPadding(0);
                        chip.setTextEndPadding(0);
                        chip.setCloseIconEndPadding(0);
                        chip.setEnsureMinTouchTargetSize(false);
                        chip.setChipBackgroundColorResource(R.color.colorChip);
                        ((MyBiddingAdapter.BiddingViewHolder) holder).chipgroup.addView(chip);
                    }
//                    for (index in singlePartialBidding.getCategory().indices) {
//                        project_category_array.add(ProjectCategory(context
//                        !!, QuoteProjectSummary.category_array[index]).name)
//                    }

//                    for (index in project_category_array.indices) {
//                        val chip = Chip(chipgroup.context)
//                        chip.text = project_category_array.get(index)
//                        chipgroup.addView(chip)
                }
            }

            ((BiddingViewHolder) holder).projectView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Fragment nextFrag= new MybidsQuoteFragment();

                    Bundle bundle = new Bundle();

                    bundle.putInt("quote_id", singleMyProject.getQuote_id());
                    bundle.putInt("status_id", singleMyProject.getStatus_id());
                    bundle.putInt("project_id", singleMyProject.getProject_id());
                    bundle.putString("post_date", singleMyProject.getPost_date());
                    nextFrag.setArguments(bundle);

                    ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.nav_host_fragment, nextFrag)
                            .addToBackStack(null)
                            .commit();

                }
            });

        }
//
        else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return myProjectsList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    //
    public static class BiddingViewHolder extends RecyclerView.ViewHolder {
        public TextView myprojects_title;
        public TextView myprojects_district;
        public TextView myprojects_style;
        public TextView myprojects_area;
        public TextView myprojects_type;
        public TextView myprojects_status;
        public TextView myprojects_price;
        public TextView myprojects_style_space;
        public TextView myprojects_area_unit;
        public TextView myprojects_submitDate;
        public CardView projectView;
        public ChipGroup chipgroup;

        public BiddingViewHolder(View v) {
            super(v);
            myprojects_title = (TextView)v.findViewById(R.id.myprojects_title);
            myprojects_district = (TextView)v.findViewById(R.id.myprojects_district);
            myprojects_style = (TextView)v.findViewById(R.id.myprojects_style);
            myprojects_area = (TextView)v.findViewById(R.id.myprojects_area);
            myprojects_type = (TextView)v.findViewById(R.id.myprojects_type);
            myprojects_status = (TextView)v.findViewById(R.id.myprojects_status);
            myprojects_price = (TextView)v.findViewById(R.id.myprojects_price);
            myprojects_style_space = (TextView)v.findViewById(R.id.myprojects_style_space);
            myprojects_area_unit = (TextView)v.findViewById(R.id.myprojects_area_unit);
            myprojects_submitDate = (TextView)v.findViewById(R.id.myprojects_submitDate);
            projectView = (CardView) v.findViewById(R.id.projectView);
            chipgroup = (ChipGroup) v.findViewById(R.id.chipgroup);


        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
    public void update(ArrayList<MyProjects> MyProjects){
        myProjectsList.addAll(MyProjects);
        notifyDataSetChanged();
    }

    // This method is used to remove ProgressBar when data is loaded
    public void removeLastItem(){
        myProjectsList.remove(myProjectsList.size() - 1);
        notifyDataSetChanged();
    }

}