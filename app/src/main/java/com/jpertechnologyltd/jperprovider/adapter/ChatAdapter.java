package com.jpertechnologyltd.jperprovider.adapter;


import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener;
import com.jpertechnologyltd.jperprovider.item.Chat;
import com.jpertechnologyltd.jperprovider.item.ChatImage;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.List;


public class ChatAdapter extends RecyclerView.Adapter {
    private final int LOADING = 2;
    private final int OWNER_ITEM = 1;
    private final int PROVIDER_ITEM = 0;
    private final int OWNER_ITEM_IMAGE = 3;
    private final int PROVIDER_ITEM_IMAGE = 4;
    public Context context;
    private List<Chat> ChatList;
    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 30;
    private int lastVisibleItem, totalItemCount, visibleItemCount,pastVisiblesItems;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private RecyclerView mrecyclerView;


    public ChatAdapter(List<Chat> ChatLists, RecyclerView recyclerView) {
        ChatList = ChatLists;
        mrecyclerView =recyclerView;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            visibleItemCount = linearLayoutManager.getChildCount();
                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                            Log.d("totalItemCount", String.valueOf(totalItemCount));
                            Log.d("visibleItemCount", String.valueOf(visibleItemCount));
                            Log.d("pastVisiblesItems", String.valueOf(pastVisiblesItems));
//                            if (!loading
//                                    && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (!loading
                                    && pastVisiblesItems==0) {
                                // End has been reached
                                // Do something
                                loading = false;
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                    Log.d("totalItemCount2", String.valueOf(totalItemCount));
                                    Log.d("visibleItemCount2", String.valueOf(visibleItemCount));
                                    Log.d("pastVisiblesItems2", String.valueOf(pastVisiblesItems));
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (ChatList.get(position) ==null)
            return LOADING;
        else
        {
            if ( ChatList.get(position).getType()==0)
                return ChatList.get(position).getSendfromOwener() ? OWNER_ITEM : PROVIDER_ITEM;

            else
                return ChatList.get(position).getSendfromOwener() ? OWNER_ITEM_IMAGE : PROVIDER_ITEM_IMAGE;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        context = parent.getContext();
        if (viewType == OWNER_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.chat_mymessage, parent, false);

            vh = new OwnerViewHolder(v);
        }
        else if(viewType == LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        else if(viewType == OWNER_ITEM_IMAGE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.chat_mymessage_image, parent, false);

            vh = new OwnerImageViewHolder(v);
        }
        else if(viewType == PROVIDER_ITEM_IMAGE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.chat_oppositemessage_image, parent, false);

            vh = new ProviderImageViewHolder(v);
        }
        else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.chat_oppositemessage, parent, false);

            vh = new ProviderViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        if (holder instanceof OwnerViewHolder) {

            final Chat singleChat= (Chat) ChatList.get(position);
            ((OwnerViewHolder) holder).message.setText(String.valueOf(singleChat.getContent()));
            ((OwnerViewHolder) holder).timestamp.setText(String.valueOf(singleChat.getSend_date()));

        }
//
        else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
//        else if (holder instanceof OwnerImageViewHolder) {
//
//            final Chat singleChat= (Chat) ChatList.get(position);
//            ChatImageAdapter ChatImageAdapter = new ChatImageAdapter(singleChat.getImages(),context);
//            LinearLayoutManager  LinearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
//            ((OwnerImageViewHolder) holder).image_listview.setLayoutManager(LinearLayoutManager);
//            ((OwnerImageViewHolder) holder).image_listview.setAdapter(ChatImageAdapter);
//            ((OwnerImageViewHolder) holder).timestamp.setText(String.valueOf(singleChat.getSend_date()));
//
//        }
        else if (holder instanceof OwnerImageViewHolder) {
            String header = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).getString("user_token","");
            final Chat singleChat= (Chat) ChatList.get(position);
            final ChatImage singleChatImage= (ChatImage) singleChat.getImage();
            if(singleChatImage.getType()==0){
                ((OwnerImageViewHolder) holder).chat_imageView.setImageURI(singleChatImage.getFileUri());

                Log.d("upload_imageinAdaptera", singleChatImage.getFileUri().toString());
                Uri[] images = new Uri[1];
                images[0] = singleChatImage.getFileUri();
                ((OwnerImageViewHolder) holder).chat_imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View arg0) {
                         new StfalconImageViewer.Builder<Uri>(context, images, new ImageLoader<Uri>() {
                             @Override
                             public void loadImage(ImageView imageView, Uri image) {
                                 imageView.setImageURI(image);
                             }
                         }).show();
                     
                    }
                });
            }
            else {
                GlideUrl glideUrl = new GlideUrl(
                        singleChatImage.getPath(),
                        new LazyHeaders.Builder().addHeader("Authorization", "Bearer "+header).build()
                    );
                Glide.with(context).load(glideUrl)
                        .placeholder(R.drawable.image_placeholder).into( ((OwnerImageViewHolder) holder).chat_imageView);

                String[] images = new String[1];
                images[0] = singleChatImage.getPath();
                ((OwnerImageViewHolder) holder).chat_imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View arg0) {
                        new StfalconImageViewer.Builder<String>(context, images, new ImageLoader<String>() {
                            @Override
                            public void loadImage(ImageView imageView, String res) {
                                GlideUrl glideUrl = new GlideUrl(
                                        singleChatImage.getPath(),
                                        new LazyHeaders.Builder().addHeader("Authorization", "Bearer "+header).build()
                                );
                                Glide.with(context).load(glideUrl)
                                        .placeholder(R.drawable.image_placeholder).into(imageView);

                            }
                        }).show();

                    }
                });
            }
            ((OwnerImageViewHolder) holder).timestamp.setText(String.valueOf(singleChat.getSend_date()));


        }
//        else if (holder instanceof ProviderImageViewHolder) {
//
//            final Chat singleChat= (Chat) ChatList.get(position);
//            ChatImageAdapter ChatImageAdapter = new ChatImageAdapter(singleChat.getImages(),context);
//
//            LinearLayoutManager  LinearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
//
//            LinearLayoutManager.setStackFromEnd(true);
//            LinearLayoutManager.setStackFromEnd(true);
//            LinearLayoutManager.setSmoothScrollbarEnabled(true);
//            ((ProviderImageViewHolder) holder).image_listview.setLayoutManager(LinearLayoutManager);
//            ((ProviderImageViewHolder) holder).image_listview.setAdapter(ChatImageAdapter);
//            ((ProviderImageViewHolder) holder).timestamp.setText(String.valueOf(singleChat.getSend_date()));
//
//        }
        else if (holder instanceof ProviderImageViewHolder) {

            String header = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).getString("user_token","");
            final Chat singleChat= (Chat) ChatList.get(position);
            final ChatImage singleChatImage= (ChatImage) singleChat.getImage();
            Log.d("singleChatImage.getPath()",singleChatImage.getPath());
            GlideUrl glideUrl = new GlideUrl(
                    singleChatImage.getPath(),
                    new LazyHeaders.Builder().addHeader("Authorization", "Bearer "+header).build()
            );
            try {
                Glide.with(context).load(glideUrl)
                        .placeholder(R.drawable.image_placeholder).into(((ProviderImageViewHolder) holder).chat_imageView);
            }
            catch (Exception e) {
                ((ProviderImageViewHolder) holder).chat_imageView.setImageResource(R.drawable.image_placeholder);
            }

            String[] images = new String[1];
            images[0] = singleChatImage.getPath();
            ((ProviderImageViewHolder) holder).chat_imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    new StfalconImageViewer.Builder<String>(context, images, new ImageLoader<String>() {
                        @Override
                        public void loadImage(ImageView imageView, String res) {
                            GlideUrl glideUrl = new GlideUrl(
                                    singleChatImage.getPath(),
                                    new LazyHeaders.Builder().addHeader("Authorization", "Bearer "+header).build()
                            );
                            Glide.with(context).load(glideUrl)
                                    .placeholder(R.drawable.image_placeholder).into(imageView);

                        }
                    }).show();

                }
            });

            ((ProviderImageViewHolder) holder).timestamp.setText(String.valueOf(singleChat.getSend_date()));

        }
        else {

            final Chat singleChat= (Chat) ChatList.get(position);

            ((ProviderViewHolder) holder).message.setText(String.valueOf(singleChat.getContent()));
            ((ProviderViewHolder) holder).timestamp.setText(String.valueOf(singleChat.getSend_date()));
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return ChatList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    //
    public static class OwnerViewHolder extends RecyclerView.ViewHolder {
        public TextView message;
        public TextView timestamp;

        public OwnerViewHolder(View v) {
            super(v);

            message = (TextView)v.findViewById(R.id.message);
            timestamp = (TextView)v.findViewById(R.id.timestamp);
        }
    }

    public static class ProviderViewHolder extends RecyclerView.ViewHolder {
        public TextView message;
        public TextView timestamp;

        public ProviderViewHolder(View v) {
            super(v);
            message = (TextView)v.findViewById(R.id.message);
            timestamp = (TextView)v.findViewById(R.id.timestamp);

        }
    }
    public static class OwnerImageViewHolder extends RecyclerView.ViewHolder {
        public ImageView chat_imageView;
        public TextView timestamp;

        public OwnerImageViewHolder(View v) {
            super(v);

            chat_imageView = (ImageView)v.findViewById(R.id.chat_imageView);
            timestamp = (TextView)v.findViewById(R.id.timestamp);
        }
    }

    public static class ProviderImageViewHolder extends RecyclerView.ViewHolder {
        public ImageView chat_imageView;
        public TextView timestamp;

        public ProviderImageViewHolder(View v) {
            super(v);

            chat_imageView = (ImageView)v.findViewById(R.id.chat_imageView);
            timestamp = (TextView)v.findViewById(R.id.timestamp);
        }
    }
    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
    public void update(List<Chat> ChatList){
        ChatList.addAll(ChatList);
        notifyDataSetChanged();
    }
    public void refresh(){
        notifyDataSetChanged();
    }

    // This method is used to remove ProgressBar when data is loaded
    public void removeLastItem(){
        ChatList.remove(ChatList.size() - 1);
        notifyDataSetChanged();
    }

}