package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Disbursement
import com.jpertechnologyltd.jperprovider.item.DisbursementStatus


class DisbursementAdapter (private val  mDisbursements: ArrayList<Disbursement>, private val parentFragment: Fragment) : RecyclerView.Adapter<DisbursementAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val dis_deposit_date_title = itemView.findViewById<TextView>(R.id.dis_deposit_date_title)
        val dis_deposit_date = itemView.findViewById<TextView>(R.id.dis_deposit_date)
        val dis_after_proj_retention_title = itemView.findViewById<TextView>(R.id.dis_after_proj_retention_title)
        val dis_after_proj_retention = itemView.findViewById<TextView>(R.id.dis_after_proj_retention)
        val dis_after_transaction_fee_title = itemView.findViewById<TextView>(R.id.dis_after_transaction_fee_title)
        val dis_after_transaction_fee = itemView.findViewById<TextView>(R.id.dis_after_transaction_fee)
        val dis_after_commision_fee_title = itemView.findViewById<TextView>(R.id.dis_after_commision_fee_title)
        val dis_after_commision_fee = itemView.findViewById<TextView>(R.id.dis_after_commision_fee)
        val dis_after_retention_disbursed_title = itemView.findViewById<TextView>(R.id.dis_after_retention_disbursed_title)
        val dis_after_retention_disbursed = itemView.findViewById<TextView>(R.id.dis_after_retention_disbursed)
        val dis_after_disbursed_on_title = itemView.findViewById<TextView>(R.id.dis_after_disbursed_on_title)
        val dis_after_disbursed_on = itemView.findViewById<TextView>(R.id.dis_after_disbursed_on)
        val dis_b4_retention_note = itemView.findViewById<TextView>(R.id.dis_b4_retention_note)
        val dis_after_retention_status = itemView.findViewById<TextView>(R.id.dis_after_retention_status)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
         context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_disbursement, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        viewHolder.setIsRecyclable(false)
        val disbursementitem: Disbursement = mDisbursements.get(position)
        // Set item views based on your views and data model


        val dis_deposit_date_title = viewHolder.dis_deposit_date_title
        val dis_deposit_date = viewHolder.dis_deposit_date
        val dis_after_proj_retention_title = viewHolder.dis_after_proj_retention_title
        val dis_after_proj_retention = viewHolder.dis_after_proj_retention
        val dis_after_transaction_fee_title = viewHolder.dis_after_transaction_fee_title
        val dis_after_transaction_fee = viewHolder.dis_after_transaction_fee
        val dis_after_commision_fee_title = viewHolder.dis_after_commision_fee_title
        val dis_after_commision_fee = viewHolder.dis_after_commision_fee
        val dis_after_retention_disbursed_title = viewHolder.dis_after_retention_disbursed_title
        val dis_after_retention_disbursed = viewHolder.dis_after_retention_disbursed
        val dis_after_disbursed_on_title = viewHolder.dis_after_disbursed_on_title
        val dis_after_disbursed_on = viewHolder.dis_after_disbursed_on
        val dis_b4_retention_note = viewHolder.dis_b4_retention_note
        val dis_after_retention_status = viewHolder.dis_after_retention_status

//        //Retention disbursement and no more payment disbursement
//        if(mDisbursements.size==1 && disbursementitem.type != 1){
//            dis_deposit_date_title.text = context!!.getResources().getString(R.string.dis_after_deposit_date)
//            dis_deposit_date.text = disbursementitem.init_date
//            dis_after_proj_retention.text = disbursementitem.init_amount
//            dis_b4_retention_note.visibility = View.VISIBLE
//            dis_after_transaction_fee_title.visibility = View.GONE
//            dis_after_transaction_fee.visibility = View.GONE
//            dis_after_commision_fee_title.visibility = View.GONE
//            dis_after_commision_fee.visibility = View.GONE
//            dis_after_retention_disbursed_title.visibility = View.GONE
//            dis_after_retention_disbursed.visibility = View.GONE
//            dis_after_disbursed_on_title.visibility = View.GONE
//            dis_after_disbursed_on.visibility = View.GONE
//        }
        //Retention disbursement
        if(disbursementitem.type != 1 && (disbursementitem.status_id==3 ||disbursementitem.status_id==5)){
            dis_deposit_date_title.text = context!!.getResources().getString(R.string.dis_after_deposit_date)
            dis_deposit_date.text = disbursementitem.init_date
            dis_after_proj_retention.text = "$"+disbursementitem.init_amount
            dis_after_transaction_fee.text = "($"+disbursementitem.transaction_fee+")"
            dis_after_commision_fee.text = "($"+disbursementitem.commission_amount+")"
            dis_after_retention_disbursed.text = "HK$"+disbursementitem.release_amount
            dis_after_disbursed_on.text = disbursementitem.released_date
            dis_after_retention_status.text= DisbursementStatus(context!!, disbursementitem.status_id).name
            Log.d("temp","disbursementitem: " + disbursementitem.init_date )
        }

        //Retention disbursement and hv other payment disbursements
        else if(disbursementitem.type == 1 && (disbursementitem.status_id==2 ||disbursementitem.status_id==5)){
            dis_deposit_date_title.text = context!!.getResources().getString(R.string.dis_after_online_payment_date)
            dis_deposit_date.text = disbursementitem.init_date
            dis_after_proj_retention_title.text = disbursementitem.title
            dis_after_proj_retention.text = "HK$"+disbursementitem.init_amount
            dis_after_transaction_fee.text = disbursementitem.transaction_fee
            dis_after_commision_fee_title.visibility = View.GONE
            dis_after_commision_fee.visibility = View.GONE
            dis_after_retention_disbursed.text = "HK$"+disbursementitem.release_amount
            dis_after_disbursed_on.text = disbursementitem.released_date
            dis_after_retention_status.text= DisbursementStatus(context!!, disbursementitem.status_id).name
        }

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mDisbursements.size
    }
}