package com.jpertechnologyltd.jperprovider.adapter

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.component.HttpsService.EditProjectEvent
import com.jpertechnologyltd.jperprovider.item.Event
import com.jpertechnologyltd.jperprovider.ui.calendar.CalendarFragment
import java.util.*

class EventAdapter(
    private var mEvents: List<Event>,
    private val parentFragment: Fragment,
    private val project_id_fake: Int,
    private val header: String,
    private val Loading: ImageView,
    private val progressOverlay: FrameLayout
) : RecyclerView.Adapter<EventAdapter.ViewHolder>()
{

    var context: Context? =null
    var lang:String? ="en"
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val event_project_title = itemView.findViewById<TextView>(R.id.event_project_title)
        val event_task_title = itemView.findViewById<TextView>(R.id.event_task_title)
        val event_day = itemView.findViewById<TextView>(R.id.event_day_value)
        val event_month = itemView.findViewById<TextView>(R.id.event_month_value)
        val event_card = itemView.findViewById<CardView>(R.id.event_card)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_calendar_event, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        viewHolder.setIsRecyclable(false)
        val event: Event = mEvents.get(position)
        // Set item views based on your views and data model
        val event_project_title = viewHolder.event_project_title
        val event_task_title = viewHolder.event_task_title
        val event_day = viewHolder.event_day
        val event_month = viewHolder.event_month
        val event_card = viewHolder.event_card

        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")

        event_project_title.setText("Project #".plus(event.project_id.toString()))
        event_task_title.setText(event.item)
        event_day.setText(event.day)
        event_month.setText(event.month)
        val event_date = event.day.plus(" - ").plus(event.month)
        if(event.is_editable){
            event_card.setOnClickListener(View.OnClickListener { arg0 ->
                val activity = parentFragment.activity as Activity?
                if (activity != null) {
                    showDialog(activity, event.task_title, event.fulldate, event.id, position,event.project_id)
                }
            })
        }

        Log.d("event_date", event_date)
//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mEvents.size
    }
    private fun showDialog(
        activity: Activity,
        task_title: String,
        task_date: String,
        id: Int,
        position: Int,
        project_id: Int
    ) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)

        dialog.setContentView(R.layout.dialog_calendar_create)
        val dialog_title = dialog.findViewById(R.id.dialog_title) as TextView
        dialog_title.text = "Edit Task"
        val edit_title = dialog.findViewById(R.id.edit_title) as TextView
        edit_title.text = task_title
        val edit_date = dialog.findViewById(R.id.edit_date) as TextView
        edit_date.text = task_date

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        edit_date.setOnClickListener{
            val dpd = DatePickerDialog(context!!,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    edit_date.setText("" + mYear + "-" + (mMonth + 1) + "-" + mDay)
                }, year, month, day
            )

            dpd.datePicker.minDate = System.currentTimeMillis() -1000
            if(lang.equals("en")) {
                dpd.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    context!!.getString(R.string.confirm_button),
                    dpd
                )
                dpd.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    context!!.getString(R.string.cancle_button),
                    dpd
                )
            }
            else{
                dpd.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    context!!.getString(R.string.confirm_button),
                    dpd
                )
                dpd.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    context!!.getString(R.string.cancle_button),
                    dpd
                )
            }
            dpd.show()
        }

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
//        val noBtn = dialog.findViewById(R.id.noBtn) as TextView
        val delete_button_view = dialog.findViewById(R.id.delete_button_view) as LinearLayout
        delete_button_view.visibility = View.VISIBLE

        val save_button_view = dialog.findViewById(R.id.save_button_view) as LinearLayout
        save_button_view.visibility = View.GONE

        val save_button_2 = dialog.findViewById(R.id.save_button2) as Button

        save_button_2.setOnClickListener {
            Loading.visibility=View.VISIBLE
            progressOverlay.visibility=View.VISIBLE
            var result = EditProjectEvent(
                context,
                "https://uat.jper.com.hk/api/company/project/event/task/update",
                header,
                id,
                project_id,
                edit_title.text.toString(),
                edit_date.text.toString(),
                lang
            )
                if (result!!.get(0).toString().equals("success")
                ) {
                    Loading.visibility=View.GONE
                    progressOverlay.visibility=View.GONE
                    dialog.dismiss()

                    val dialog = Dialog(context!!)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.setCanceledOnTouchOutside(false)
                    dialog.setContentView(R.layout.dialog_message)

                    val dialog_message =
                        dialog.findViewById(R.id.dialog_message) as TextView
                    dialog_message.text = context!!.getString(R.string.Success_msg)
                    val confirm_button =
                        dialog.findViewById(R.id.confirm_button) as Button

                    val close_button =
                        dialog.findViewById(R.id.close_button) as ImageView
                    close_button.visibility = View.INVISIBLE
                    confirm_button.setOnClickListener {
                        dialog.dismiss()
                        (parentFragment as CalendarFragment).refreshItems()
                    }
                    close_button.setOnClickListener {
                        dialog.dismiss()
                    }

                    dialog.setCancelable(true)
                    dialog.show()




                    Log.d("success_add", "ok")
                }
                else{

                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE
                    val dialog = Dialog(context!!)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    dialog.setCanceledOnTouchOutside(false)
                    dialog.setContentView(R.layout.dialog_message)

                    val dialog_message =
                        dialog.findViewById(R.id.dialog_message) as TextView
                    dialog_message.text = result!!.get(1)
                    val confirm_button =
                        dialog.findViewById(R.id.confirm_button) as Button

                    val close_button =
                        dialog.findViewById(R.id.close_button) as ImageView
                    close_button.visibility = View.INVISIBLE
                    confirm_button.setOnClickListener {
                        dialog.dismiss()
                    }
                    close_button.setOnClickListener {
                        dialog.dismiss()
                    }

                    dialog.setCancelable(true)
                    dialog.show()
                }
            }


        close_button.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setCancelable(true)
        dialog.show()
    }
    fun refreshDataset(){
        notifyDataSetChanged()
    }
    private fun showWarningDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
}