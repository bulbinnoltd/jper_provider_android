package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Activity
import com.jpertechnologyltd.jperprovider.ui.activity.ActivityFragment
import de.hdodenhof.circleimageview.CircleImageView


class ActivityAdapter (private val mActivities: List<Activity>) : RecyclerView.Adapter<ActivityAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val activity_title = itemView.findViewById<TextView>(R.id.review_title)
        val activity_subtitle = itemView.findViewById<TextView>(R.id.activity_desc)
        val activity_post_time = itemView.findViewById<TextView>(R.id.activity_post_time)
        val activity_icon = itemView.findViewById<ImageView>(R.id.activity_icon)
        val unread_dot = itemView.findViewById<CircleImageView>(R.id.unread_dot)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_activity, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val activity: Activity = mActivities.get(position)
        // Set item views based on your views and data model
        val activity_title = viewHolder.activity_title
        val activity_subtitle = viewHolder.activity_subtitle
        val activity_post_time = viewHolder.activity_post_time
        val activity_icon = viewHolder.activity_icon
        val unread_dot = viewHolder.unread_dot

        activity_title.setText(activity.content)
        activity_subtitle.setText(activity.date)
        activity_post_time.setText(activity.time)

        val res: Int =
            context?.getResources()!!.getIdentifier(activity.icon, "drawable", context!!.getPackageName())
        activity_icon.setImageResource(res)

        if(activity.read){
            unread_dot.visibility =View.GONE
        }
//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mActivities.size
    }
}