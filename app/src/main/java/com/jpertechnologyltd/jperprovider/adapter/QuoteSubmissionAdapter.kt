package com.jpertechnologyltd.jperprovider.adapter

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Add
import com.jpertechnologyltd.jperprovider.ui.quoteproject.EditItemFragment
import java.text.DecimalFormat
import java.util.*


class QuoteSubmissionAdapter(
    private var mQuotes: ArrayList<Add>,
    private val num_item_shown: Int,
    private val lang: String,
    private val project_id: Int,
    private val post_date: String
) : RecyclerView.Adapter<QuoteSubmissionAdapter.ViewHolder>()
{
    private var header : String? = null
    val ITEM_TYPE = 0
    val TIME_TYPE = 1
    val AMOUNT_TYPE = 2

    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val item_title = itemView.findViewById<TextView>(R.id.item_title)
        val item_min = itemView.findViewById<TextView>(R.id.item_minvalue)
        val item_max = itemView.findViewById<TextView>(R.id.item_maxvalue)
        val item_edit = itemView.findViewById<ImageView>(R.id.item_edit_icon)
        val time_title = itemView.findViewById<TextView>(R.id.time_title)
//        val time_min = itemView.findViewById<TextView>(R.id.time_minvalue)
//        val time_max = itemView.findViewById<TextView>(R.id.time_maxvalue)
//        val time_edit = itemView.findViewById<ImageView>(R.id.time_edit_icon)
        val time_edited_text = itemView.findViewById<TextInputEditText>(R.id.time_edited_text)
//        val min_amount_edititext = itemView.findViewById<TextInputEditText>(R.id.min_amount_edititext)
//        val max_amount_edititext = itemView.findViewById<TextInputEditText>(R.id.max_amount_edititext)
        val amount_title = itemView.findViewById<TextView>(R.id.amount_title)
        val amount_min = itemView.findViewById<TextView>(R.id.amount_minvalue)
        val amount_max = itemView.findViewById<TextView>(R.id.amount_maxvalue)
//        val amount_edit = itemView.findViewById<ImageView>(R.id.amount_edit_icon)
//        val amount_textview = itemView.findViewById<LinearLayout>(R.id.textview)
//        val amount_edittextview = itemView.findViewById<LinearLayout>(R.id.edittextview)
//        val time_textview = itemView.findViewById<LinearLayout>(R.id.time_textview)
        val time_edittextview = itemView.findViewById<CardView>(R.id.edit_time_textview)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun getItemViewType(position: Int): Int {

        if (position == mQuotes.size-2)
            return AMOUNT_TYPE
        else if (position  == mQuotes.size-1)
            return TIME_TYPE
        else return ITEM_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout
        if (viewType == AMOUNT_TYPE){
            val quoteSubmissionView = inflater.inflate(R.layout.item_amount, parent, false)
            // Return a new holder instance
            return ViewHolder(quoteSubmissionView)
        }
        else if (viewType == TIME_TYPE){
            val quoteSubmissionView = inflater.inflate(R.layout.item_time, parent, false)
            // Return a new holder instance
            return ViewHolder(quoteSubmissionView)}

        else {
            val quoteSubmissionView = inflater.inflate(R.layout.item_item, parent, false)
            // Return a new holder instance
            return ViewHolder(quoteSubmissionView)}
        }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val quote: Add = mQuotes[position]
        // Set item views based on your views and data model
        if (position == mQuotes.size-2){
            val amount_title = viewHolder.amount_title
            val amount_minvalue = viewHolder.amount_min
            val amount_maxvalue = viewHolder.amount_max
//            val amount_edit = viewHolder.amount_edit
//            val amount_textview = viewHolder.amount_textview
//            val amount_edittextview = viewHolder.amount_edittextview
//            val min_amount_edititext = viewHolder.min_amount_edititext
//            val max_amount_edititext = viewHolder.max_amount_edititext

            amount_title.setText(quote.description)

            try {
                val number: String = quote.min_amount.toDouble().toInt().toString()
                val amount = number.toDouble()
                val formatter = DecimalFormat("#,###")
                val formatted = formatter.format(amount)
                amount_minvalue.text = "HK $"+formatted
            } catch (e: Exception) {
                Log.d("quote_error",e.toString())
                amount_minvalue.setText("HK $" + quote.min_amount.toDouble().toInt().toString())
            }


            try {
                val number: String = quote.max_amount.toDouble().toInt().toString()
                val amount = number.toDouble()
                val formatter = DecimalFormat("#,###")
                val formatted = formatter.format(amount)
                amount_maxvalue.text = "HK $"+formatted
            } catch (e: Exception) {
                Log.d("quote_error",e.toString())
                amount_maxvalue.setText("HK $" +quote.max_amount.toDouble().toInt().toString())
            }
//
//            amount_edit.setOnClickListener(View.OnClickListener { arg0 ->
//
//                if (amount_textview.visibility== View.GONE)
//                {
//                    amount_edittextview.visibility = View.GONE
//                    amount_textview.visibility= View.VISIBLE
//                    notifyDataSetChanged()
//                    mQuotes[position].min_amount= min_amount_edititext.getText().toString().trim().toFloat()
//                    mQuotes[position].max_amount= max_amount_edititext.getText().toString().trim().toFloat()
//
//                    //sendinfoamount(min_amount_edititext.text,max_amount_edititext.text)
//
//                }
//                else if (amount_textview.visibility== View.VISIBLE)
//                {
//                    amount_edittextview.visibility = View.VISIBLE
//                    amount_textview.visibility= View.GONE
//                    notifyDataSetChanged()
//                }
//
//            })
        }
        else if (position  == mQuotes.size-1){
            val time_title = viewHolder.time_title
//            val time_minvalue = viewHolder.time_min
//            val time_maxvalue = viewHolder.time_max
//            val time_edit = viewHolder.time_edit
            time_title.text = context!!.getString(R.string.Create_bid_Earliest_Availability_with_star )
            val time_edited_text = viewHolder.time_edited_text
            time_edited_text.setText(quote.remarks)

//            val time_textview = viewHolder.time_textview
            val time_edittextview = viewHolder.time_edittextview
            time_edited_text.setOnClickListener{

                val calendar = Calendar.getInstance()
                val year = calendar.get(Calendar.YEAR)
                val month = calendar.get(Calendar.MONTH)
                val day = calendar.get(Calendar.DAY_OF_MONTH)
                val dpd = DatePickerDialog(context!!,
                    DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                        time_edited_text.setText("" + mYear + "-" + (mMonth + 1) + "-" + mDay)
                        mQuotes[position].remarks = "" + mYear + "-" + (mMonth + 1) + "-" + mDay
                        notifyDataSetChanged()
                    }, year, month, day
                )
                dpd.datePicker.minDate = System.currentTimeMillis() -1000
                if(lang.equals("en")) {
                    dpd.setButton(
                        DialogInterface.BUTTON_POSITIVE,
                        context!!.getString(R.string.confirm_button),
                        dpd
                    )
                    dpd.setButton(
                        DialogInterface.BUTTON_NEGATIVE,
                        context!!.getString(R.string.cancle_button),
                        dpd
                    )
                }
                else{
                    dpd.setButton(
                        DialogInterface.BUTTON_POSITIVE,
                        context!!.getString(R.string.confirm_button),
                        dpd
                    )
                    dpd.setButton(
                        DialogInterface.BUTTON_NEGATIVE,
                        context!!.getString(R.string.cancle_button),
                        dpd
                    )
                }
                dpd.show()
            }
//            time_title.setText(quote.description)
//            time_minvalue.setText(quote.min_amount.toString())
//            time_maxvalue.setText(quote.max_amount.toString())
//
//            time_edit.setOnClickListener(View.OnClickListener { arg0 ->
//
//                if (time_textview.visibility== View.GONE)
//                {
//
//                    time_edittextview.visibility = View.GONE
//                    time_textview.visibility= View.VISIBLE
//                    notifyDataSetChanged()
////                    time_edited_text.setText(mQuotes!![position!!].min_amount.toString())
//                    //val time_edited_text_float: Float = java.lang.Float.valueOf(time_edited_text.getText().toString())
//                    mQuotes[position].min_amount= time_edited_text.getText().toString().trim().toFloat()
//                    //sendinfotime(time_edited_text.text)
//                }
//                else if (time_textview.visibility== View.VISIBLE)
//                {
//                    time_edittextview.visibility = View.VISIBLE
//                    time_textview.visibility= View.GONE
//                    notifyDataSetChanged()
//
//                }
//
//            })
        }

        else {
            val item_title = viewHolder.item_title
            val item_minvalue = viewHolder.item_min
            val item_maxvalue = viewHolder.item_max
            val item_edit = viewHolder.item_edit

            item_title.setText(quote.description + " " + quote.remarks)

            try {
                val number: String = quote.min_amount.toDouble().toInt().toString()
                val amount = number.toDouble()
                val formatter = DecimalFormat("#,###")
                val formatted = formatter.format(amount)
                item_minvalue.text = "HK $"+formatted
            } catch (e: Exception) {
                e.printStackTrace()
                item_minvalue.setText(quote.min_amount.toDouble().toInt().toString())
            }
            try {
                val number: String = quote.max_amount.toDouble().toInt().toString()
                val amount = number.toDouble()
                val formatter = DecimalFormat("#,###")
                val formatted = formatter.format(amount)
                item_maxvalue.text = "HK $"+formatted
            } catch (e: Exception) {
                e.printStackTrace()
                item_maxvalue  .setText(quote.max_amount.toDouble().toInt().toString())
            }

            item_edit.setOnClickListener(View.OnClickListener { arg0 ->

                val EditItemFragment: Fragment = EditItemFragment()
                val bundle = Bundle()
                bundle.putParcelableArrayList("data", mQuotes)
                bundle.putString("post_date", post_date!!)
                bundle.putInt("project_id", project_id!!)
                bundle.putInt("itemPosition", position)
                EditItemFragment.setArguments(bundle)

                Log.d("adapter item get", mQuotes!!.get(position).description)

                val activity = arg0.getContext() as AppCompatActivity
                var fr = activity.supportFragmentManager?.beginTransaction()
                fr?.addToBackStack(null)
                fr?.add(R.id.nav_host_fragment, EditItemFragment)
                fr?.commit()
            })

        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mQuotes.size
    }

//
//    fun sendinfotime(time: Editable? ){
//
//        val msg = HttpsService.UpdateTime(this,"https://uat.jper.com.hk/api/owner/change/password",password.toString(),header)
//        if (msg!=null){
//            showDialog(this ,msg)
//        }}
//
//
//    fun sendinfoamount(minAmount: Editable?, maxAmount: Editable? ){
//
//        val msg = HttpsService.UpdateTime(this,"https://uat.jper.com.hk/api/owner/change/password",password.toString(),header)
//        if (msg!=null){
//            showDialog(this ,msg)
//        }}




}


