package com.jpertechnologyltd.jperprovider.adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener;
import com.jpertechnologyltd.jperprovider.item.QuoteProject;
import com.jpertechnologyltd.jperprovider.ui.quoteproject.QuoteProjectDetailFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class QuoteProjectAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    private List<QuoteProject> QuoteProjectsList;
    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;



    public QuoteProjectAdapter(List<QuoteProject> mQuoteProjects, RecyclerView recyclerView) {
        QuoteProjectsList = mQuoteProjects;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
//        ////////////////////////////////ad_mob///////////////////////////
//        if (position % 5 == 0)
//            return AD_TYPE;
//        ////////////////////////////////ad_mob///////////////////////////
        return QuoteProjectsList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_partialbidding, parent, false);

            vh = new QuoteProjectViewHolder(v);
        }
//        ////////////////////////////////ad_mob///////////////////////////
//        else if(viewType == AD_TYPE) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.list_item_native_ad, parent, false);
//            vh = new UnifiedNativeAdViewHolder(v);
//
//        }
//        ////////////////////////////////ad_mob///////////////////////////
        else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof QuoteProjectViewHolder) {

            final QuoteProject singleBlog= (QuoteProject) QuoteProjectsList.get(position);
            ((QuoteProjectViewHolder) holder).quote_title.setText(String.valueOf(singleBlog.getId()));
            ((QuoteProjectViewHolder) holder).quote_district.setText(String.valueOf(singleBlog.getDistrict()));
            ((QuoteProjectViewHolder) holder).quote_price.setText(String.valueOf(singleBlog.getBudget_max()));
            ((QuoteProjectViewHolder) holder).quote_style.setText(String.valueOf(singleBlog.getType()));
            ((QuoteProjectViewHolder) holder).quote_type.setText(String.valueOf(singleBlog.getType()));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'");
             try {
                Date newDate = format.parse(singleBlog.getCreated_at());
                format = new SimpleDateFormat("yyyy-MM-dd");
                String date = format.format(newDate);
                 ((QuoteProjectViewHolder) holder).quote_date.setText(date);

             } catch (ParseException e) {
                e.printStackTrace();
            }

             ((QuoteProjectViewHolder) holder).quote_area.setText(String.valueOf(singleBlog.getArea()));

            Log.d("singleBlog.getStyle_id",String.valueOf(singleBlog.getStyle_id()));
            if (singleBlog.getStyle_id() ==1 ||singleBlog.getStyle_id() ==2){
                GradientDrawable drawable = (GradientDrawable) ((QuoteProjectViewHolder) holder).quote_status.getBackground();
                drawable.setColor(Color.rgb(224,67,30));
                ((QuoteProjectViewHolder) holder).quote_status.setText("Urgent");
            }

            ((QuoteProjectViewHolder) holder).quote_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Fragment nextFrag= new QuoteProjectDetailFragment();

                    Bundle bundle = new Bundle();

                    final QuoteProject singleProject= (QuoteProject) QuoteProjectsList.get(position);
                    bundle.putInt("project_id", singleProject.getId());
                    nextFrag.setArguments(bundle);

                    ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.nav_host_fragment, nextFrag)
                            .addToBackStack(null)
                            .commit();

                }
            });
//            ((BlogsViewHolder) holder).blog_subtitle.setText(String.valueOf(singleBlog.getDesc()));
//            Glide.with(context).load("https://www.hypesphere.com/files/news/5f27eacc34b81.jpg").into( ((BlogsViewHolder) holder).imageView);
//            Ion.with(context)
//                    .load(singleBlog.getImg_name())
//                    .withBitmap()
//                    .centerCrop()
//                    .resize(200,200)
//                    .animateLoad(null)
//                    .animateIn(null)
//                    .intoImageView(((BlogsViewHolder) holder).imageView);

//            (BlogsViewHolder) holder).listitem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(final View arg0) {
//                    final Intent viewIntent =
//                            new Intent(arg0.getContext(), ArticleActivity.class);
//                    viewIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    viewIntent.putExtra("Article_id",Long.toString(signleNews.getArticle_id()));
//                    viewIntent.putExtra("Url",signleNews.geturl());
//                    viewIntent.putExtra("Image",signleNews.getimage());
//                    viewIntent.putExtra("Source_icon",signleNews.getsource_icon());
//                    viewIntent.putExtra("Title",signleNews.gettitle());
//                    viewIntent.putExtra("Provider_name",signleNews.getprovider_name());
//                }
//            });

        }
//
        else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return QuoteProjectsList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    //
    public static class QuoteProjectViewHolder extends RecyclerView.ViewHolder {
        public TextView quote_title;
        public TextView quote_status;
        public TextView quote_district;
        public TextView quote_price;
        public TextView quote_style;
        public TextView quote_type;
        public TextView quote_date;
        public TextView quote_area;
        public LinearLayout quote_layout;
        public QuoteProject quoteprojects;

        public QuoteProjectViewHolder(View v) {
            super(v);
             quote_title = (TextView)v.findViewById(R.id.quote_title);
                     quote_status = (TextView)v.findViewById(R.id.projects_status);
                     quote_district = (TextView)v.findViewById(R.id.projects_district);
                     quote_price = (TextView)v.findViewById(R.id.projects_price);
                     quote_style = (TextView)v.findViewById(R.id.projects_style);
                     quote_type = (TextView)v.findViewById(R.id.projects_type);
                     quote_area = (TextView)v.findViewById(R.id.projects_area);
                     quote_date = (TextView)v.findViewById(R.id.projects_postedDate_title);
                     quote_layout =(LinearLayout) v.findViewById(R.id.quote_layout);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
    public void update(List<QuoteProject> BlogItems){
        QuoteProjectsList.addAll(BlogItems);
        notifyDataSetChanged();
    }

    // This method is used to remove ProgressBar when data is loaded
    public void removeLastItem(){
        QuoteProjectsList.remove(QuoteProjectsList.size() - 1);
        notifyDataSetChanged();
    }

}