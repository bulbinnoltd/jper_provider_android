package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Quotation
import com.jpertechnologyltd.jperprovider.item.QuotationItem


class QuoteQuoteAdapter (private val context: Context, val mQuotation: List<Quotation>) : RecyclerView.Adapter<QuoteQuoteAdapter.ViewHolder>()
{

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row

        val provider_name = itemView.findViewById<TextView>(R.id.review_title)
        val quotation_rating = itemView.findViewById<RatingBar>(R.id.rating)
        val quotation_rating_num = itemView.findViewById<TextView>(R.id.review_username)
        val quoted_date_value = itemView.findViewById<TextView>(R.id.review_date)
        val quoted_amount_value = itemView.findViewById<TextView>(R.id.quoted_amount_value)
        val available_date_value = itemView.findViewById<TextView>(R.id.available_date_value)
        val quotation_itemlist = itemView.findViewById<RecyclerView>(R.id.quotation_item_listview)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_quote_quotation,parent,false))
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val quotation: Quotation = mQuotation.get(position)
        // Set item views based on your views and data model
        val provider_name = viewHolder.provider_name
        val quotation_rating = viewHolder.quotation_rating
        val quotation_rating_num = viewHolder.quotation_rating_num
        val earliest_date_value = viewHolder.quoted_date_value
        val quoted_amount_value = viewHolder.quoted_amount_value
        val submission_date_value = viewHolder.available_date_value

        provider_name.setText(quotation.name)
        //quotation_rating.setText(quotation.rating)
        quotation_rating_num.setText(quotation.rating)
        earliest_date_value.setText(quotation.earliest_date)
        submission_date_value.setText(quotation.submission_date)
        quoted_amount_value.setText(quotation.amount)

        setQuotationItem(viewHolder.quotation_itemlist, mQuotation[position].item)

//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mQuotation.size
    }

    private fun setQuotationItem(recyclerView: RecyclerView, quotationItem: List<QuotationItem>){
       val itemRecyclerAdapter = QuoteQuoteQuotationItemAdapter(context, quotationItem)
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclerView.adapter = itemRecyclerAdapter
    }
}