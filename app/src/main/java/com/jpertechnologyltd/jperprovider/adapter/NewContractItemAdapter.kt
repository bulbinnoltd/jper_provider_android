package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.EditContractItemActivity
import com.jpertechnologyltd.jperprovider.EditProgressImageActivity
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.ContractItem
import com.jpertechnologyltd.jperprovider.item.ContractItemStatus
import com.jpertechnologyltd.jperprovider.item.NewContractItem
import com.jpertechnologyltd.jperprovider.ui.myproject.ViewContractItemFragment
import java.text.DecimalFormat


class NewContractItemAdapter(
    private val mContractItems: List<NewContractItem>,
    private val parentFragment: Fragment,
    private val project_id: Int
) : RecyclerView.Adapter<NewContractItemAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val contract_title = itemView.findViewById<TextView>(R.id.contract_title)
        val contract_status = itemView.findViewById<TextView>(R.id.contract_status)
        val contract_desc = itemView.findViewById<TextView>(R.id.contract_desc)
        val contract_time_value = itemView.findViewById<TextView>(R.id.contract_time_value)
        val contract_amount_value = itemView.findViewById<TextView>(R.id.contract_amount_value)
        val contract_price_value = itemView.findViewById<TextView>(R.id.contract_price_value)
        val contract_qty_value = itemView.findViewById<TextView>(R.id.contract_qty_value)
        val contract_card = itemView.findViewById<CardView>(R.id.contract_card)
        val contract_item_number = itemView.findViewById<TextView>(R.id.contract_item_number)
        val provider_address_icon = itemView.findViewById<ImageView>(R.id.provider_address_icon)
        val add_image_button = itemView.findViewById<Button>(R.id.add_image_button)
        val edit_icon_button = itemView.findViewById<Button>(R.id.edit_icon_button)
        val edit_icon_button_view =  itemView.findViewById<View>(R.id.edit_icon_button_view)
        val add_icon_button_view =  itemView.findViewById<View>(R.id.add_icon_button_view)


    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
         context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_contract_item, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val contractitem: NewContractItem = mContractItems.get(position)
        // Set item views based on your views and data model
        val contract_title = viewHolder.contract_title
        val contract_desc = viewHolder.contract_desc
        val contract_time_value = viewHolder.contract_time_value
        val contract_amount_value = viewHolder.contract_amount_value
        val contract_price_value = viewHolder.contract_price_value
        val contract_qty_value = viewHolder.contract_qty_value
        val contract_status = viewHolder.contract_status
        val contract_card = viewHolder.contract_card
        val contract_item_number = viewHolder.contract_item_number
        val provider_address_icon = viewHolder.provider_address_icon
        val add_image_button = viewHolder.add_image_button
        val edit_icon_button = viewHolder.edit_icon_button
        val edit_icon_button_view = viewHolder.edit_icon_button_view
        val add_icon_button_view =  viewHolder.add_icon_button_view

        val contract_item_number_value = position +1
        contract_item_number.setText(contract_item_number_value.toString())
        val drawable2 = contract_item_number.getBackground() as GradientDrawable
        drawable2.setColor(Color.rgb(45,56,68))

        contract_title.setText(contractitem.item)
        contract_desc.setText(contractitem.remark)
        contract_time_value.setText(contractitem.complete_date)
        try {
            val number: String = contractitem.amount
            val amount = number.toDouble()
            val formatter = DecimalFormat("#,###.##")
            var formatted = formatter.format(amount)

            if (formatted.contains(".00")) formatted =
                formatted.substring(0, formatted.length - 3)
            contract_amount_value.text = "HK $"+formatted
        } catch (e: Exception) {
            e.printStackTrace()

            contract_amount_value.setText("HK $" + contractitem.amount)
        }
        try {
            val number: String = contractitem.u_price
            val amount = number.toDouble()
            val formatter = DecimalFormat("#,###.##")
            var formatted = formatter.format(amount)
            if (formatted.contains(".00")) formatted =
                formatted.substring(0, formatted.length - 3)
            contract_price_value.text = "HK $"+formatted
        } catch (e: Exception) {
            e.printStackTrace()
            contract_price_value.setText("HK $" + contractitem.u_price)
        }


        contract_qty_value.setText(contractitem.qty)

        provider_address_icon.setOnClickListener{
            var fr =(parentFragment.activity as MainActivity?)?.supportFragmentManager?.beginTransaction()
            fr?.replace(
                R.id.nav_host_fragment,
                ViewContractItemFragment()
            )
            fr?.commit()
        }

        contract_status.visibility = View.GONE
        edit_icon_button_view.visibility = View.GONE
        edit_icon_button.visibility = View.INVISIBLE
        add_icon_button_view.visibility = View.INVISIBLE
        //#635

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mContractItems.size
    }
}