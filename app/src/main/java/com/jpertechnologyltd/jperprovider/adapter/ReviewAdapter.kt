package com.jpertechnologyltd.jperprovider.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Comment

class ReviewAdapter (private val mComments: ArrayList<Comment>, private val num_item_shown: Int) : RecyclerView.Adapter<ReviewAdapter.ViewHolder>()
{
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val review_desc = itemView.findViewById<TextView>(R.id.review_desc)
        val review_date = itemView.findViewById<TextView>(R.id.review_date)
        val review_username = itemView.findViewById<TextView>(R.id.review_username)
        val rating = itemView.findViewById<RatingBar>(R.id.rating)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_review, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.setIsRecyclable(false)
        // Get the data model based on position
        val comment: Comment = mComments.get(position)
        // Set item views based on your views and data mode
        val review_desc = viewHolder.review_desc
        val review_username = viewHolder.review_username
        val review_date = viewHolder.review_date
        val rating = viewHolder.rating

        review_desc.setText(comment.comment)
        review_username.setText(comment.username)
        review_date.setText(comment.created_at)
        rating.rating = comment.rate

//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        if (mComments.size<num_item_shown)
            return mComments.size
        return num_item_shown //only show three items
    }
}