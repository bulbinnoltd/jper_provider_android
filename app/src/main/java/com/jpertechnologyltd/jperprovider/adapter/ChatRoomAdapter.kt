package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.chat.ChatRoom
import com.jpertechnologyltd.jperprovider.item.chat.ChatRoomList
import com.jpertechnologyltd.jperprovider.item.chat.ChatRoom_Provider
import com.jpertechnologyltd.jperprovider.ui.chat.ChatActivity
import com.jpertechnologyltd.jperprovider.ui.chat.ChatRoomActivity

class ChatRoomAdapter (private val mChatRoomList: ArrayList<ChatRoomList>) : RecyclerView.Adapter<ChatRoomAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val chatroom_provider_company = itemView.findViewById<TextView>(R.id.chatroom_provider_username2)
        val chatroom_provider_username = itemView.findViewById<TextView>(R.id.chatroom_provider_username)
        val chatroom_project = itemView.findViewById<TextView>(R.id.chatroom_project)

        val profile_image = itemView.findViewById<ImageView>(R.id.profile_image)
        val unread_number = itemView.findViewById<TextView>(R.id.unread_number)
        val chatroom_date = itemView.findViewById<TextView>(R.id.chatroom_date)
        val chatroom_view = itemView.findViewById<ConstraintLayout>(R.id.chatroom_view)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_chatroom, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val chatroomlist: ChatRoomList = mChatRoomList.get(position)
        val chatroom: ChatRoom = mChatRoomList.get(position).ChatRoom
        val provider: ChatRoom_Provider = mChatRoomList.get(position).ChatRooom_Provider

        // Set item views based on your views and data model
        val chatroom_project = viewHolder.chatroom_project
        val chatroom_provider_company = viewHolder.chatroom_provider_company
        val chatroom_provider_username = viewHolder.chatroom_provider_username
        val profile_image=  viewHolder.profile_image
        val unread_number = viewHolder.unread_number
        val chatroom_view = viewHolder.chatroom_view
        val chatroom_date = viewHolder.chatroom_date

        chatroom_date.text = chatroom.update_date
        chatroom_project.setText("Project # ".plus(chatroom.project_id))
        val lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        if(lang.equals("zh")) chatroom_provider_company.setText(chatroomlist.company_name_zh) else chatroom_provider_company.setText(chatroomlist.company_name_en)
        chatroom_provider_username.setText(provider.username)
        try{
            val header =
                context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                    .getString("user_token", "")

            val glideUrl = GlideUrl(
                provider.icon,
                LazyHeaders.Builder()
                    .addHeader("Authorization", "Bearer ".plus(header))
                    .build()
            )
            Log.d("chatroomlist.company_logo",chatroomlist.company_logo)
            Glide.with(context!!).load(glideUrl).placeholder(R.drawable.profile_pic_placeholder).into(profile_image)
        }

        catch(e:Exception){
            Log.d("chatroomlist.company_logo2",chatroomlist.company_logo)
            Glide.with(context!!).load(R.drawable.profile_pic_placeholder).into(profile_image)
        }
        if(chatroom.unread>0) {
            unread_number.setText(chatroom.unread.toString())
        }
        else {
            unread_number.visibility=View.GONE
        }
        chatroom_view.setOnClickListener(View.OnClickListener {
            Log.d("quote_id2",chatroom.quote_id.toString())

            Intent(context, ChatActivity::class.java).also{
                it.putExtra("company_id",chatroomlist.company_id)
                Log.d("company_id",chatroomlist.company_id.toString())
                it.putExtra("company_name_en",provider.username)
                it.putExtra("company_name_zh",provider.username)
                it.putExtra("company_logo", provider.icon)
                it.putExtra("company_avg_score", chatroomlist.company_avg_score.toFloat())
                it.putExtra("key", chatroom.key)
                it.putExtra("quotation_id", chatroom.quote_id)
                it.putExtra("project_id", chatroom.project_id)
                context?.startActivity(it) } }
        )
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mChatRoomList.size
    }
}