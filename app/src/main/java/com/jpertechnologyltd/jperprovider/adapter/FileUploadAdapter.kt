package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.FileUpload

class FileUploadAdapter (private val mFileUpload: ArrayList<FileUpload>) : RecyclerView.Adapter<FileUploadAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val file_title = itemView.findViewById<TextView>(R.id.file_title)
        val delete_button = itemView.findViewById<ImageView>(R.id.delete_button)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_fileupload, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.setIsRecyclable(false)
        // Get the data model based on position
        val fileupload: FileUpload = mFileUpload.get(position)
        // Set item views based on your views and data model
        val file_title = viewHolder.file_title
        val delete_button = viewHolder.delete_button


        file_title.setText(fileupload.title)
        delete_button.setOnClickListener(View.OnClickListener { arg0 ->

            Log.d("title1",mFileUpload.get(position).title.toString())
            removeAt(position)


        })
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mFileUpload.size
    }
    fun refreshDataset(){
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        this.mFileUpload.removeAt(position)
        notifyItemRemoved(position)
        notifyDataSetChanged()
    }
    fun getFileUpload(): ArrayList<FileUpload>? {
        return if (mFileUpload.size > 0)
            mFileUpload
        else
            null
    }

}