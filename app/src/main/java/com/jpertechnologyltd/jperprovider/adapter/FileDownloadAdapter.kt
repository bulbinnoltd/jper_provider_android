package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.FileDownload
import com.jpertechnologyltd.jperprovider.WebviewActivity

class FileDownloadAdapter (private val mFileDownload: ArrayList<FileDownload>) : RecyclerView.Adapter<FileDownloadAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val file_title = itemView.findViewById<TextView>(R.id.file_title)
        val delete_button = itemView.findViewById<ImageView>(R.id.delete_button)
        val file_layout = itemView.findViewById<ConstraintLayout>(R.id.setting_layout)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_fileupload, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val filedownload: FileDownload = mFileDownload.get(position)
        // Set item views based on your views and data model
        val file_title = viewHolder.file_title
        val delete_button = viewHolder.delete_button
        val file_layout=  viewHolder.file_layout

//        file_title.setText(filedownload.title)
        file_title.setText(context!!.getString(R.string.Attachment_Title).plus(" ").plus(position+1))
        file_title.setPaintFlags(file_title.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        delete_button.visibility=View.GONE
        file_layout.setOnClickListener{ arg0 ->
            val activity = arg0.getContext() as AppCompatActivity
            val intent = Intent(activity, WebviewActivity::class.java)
            intent.putExtra("url",filedownload.url)
            context!!.startActivity(intent)
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mFileDownload.size
    }
    fun refreshDataset(){
        notifyDataSetChanged()
    }




}