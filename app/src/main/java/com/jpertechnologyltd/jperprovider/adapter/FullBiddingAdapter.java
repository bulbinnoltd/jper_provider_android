package com.jpertechnologyltd.jperprovider.adapter;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener;
import com.jpertechnologyltd.jperprovider.item.Bidding;
import com.jpertechnologyltd.jperprovider.ui.quoteproject.QuoteProjectDetailFragment;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class FullBiddingAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    private List<Bidding> mBiddingList;
    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private boolean full_loading;
    private OnLoadMoreListener onLoadMoreListener;



    public FullBiddingAdapter(List<Bidding> mBiddings, RecyclerView recyclerView) {
        mBiddingList = mBiddings;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            Log.d("full_totalItemCount",String.valueOf( totalItemCount));
                            Log.d("full_lastVisibleItem",String.valueOf( lastVisibleItem));
                            Log.d("full_visibleThreshold",String.valueOf( visibleThreshold));
                            if (!full_loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                full_loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
//        ////////////////////////////////ad_mob///////////////////////////
//        if (position % 5 == 0)
//            return AD_TYPE;
//        ////////////////////////////////ad_mob///////////////////////////
        return mBiddingList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_fullbidding, parent, false);

            vh = new BiddingViewHolder(v);
        }
//        ////////////////////////////////ad_mob///////////////////////////
//        else if(viewType == AD_TYPE) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.list_item_native_ad, parent, false);
//            vh = new UnifiedNativeAdViewHolder(v);
//
//        }
//        ////////////////////////////////ad_mob///////////////////////////
        else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        //Recycler view item data would not be messed up
        holder.setIsRecyclable(false);
        if (holder instanceof BiddingViewHolder) {

            final Bidding Bidding= (Bidding) mBiddingList.get(position);
            ((BiddingViewHolder) holder).fullbidding_district.setText(String.valueOf(Bidding.getDistrict()));
            ((BiddingViewHolder) holder).fullbidding_project_id.setText("#"+String.valueOf(Bidding.getId()));
            //New amount format
            try {
                String number = Bidding.getBudget_max();
                double amount = Double.parseDouble(number);
                DecimalFormat formatter = new DecimalFormat("#,###.##");
                String formatted = formatter.format(amount);

                if(formatted.contains(".00"))  formatted = formatted.substring(0, formatted.length() - 3);
                ((BiddingViewHolder) holder).fullbidding_price.setText("HK $" + formatted);
            }
            catch (Exception e){
                e.printStackTrace();
                ((BiddingViewHolder) holder).fullbidding_price.setText("HK $" +  Bidding.getBudget_max());
            }
//            ((BiddingViewHolder) holder).quote_style.setText(String.valueOf(singlePartialBidding.getStyle_id()));
            ((BiddingViewHolder) holder).fullbidding_type.setText(String.valueOf(Bidding.getType()));

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date newDate = format.parse(Bidding.getPost_date());
                format = new SimpleDateFormat("yyyy-MM-dd");
                String date = format.format(newDate);
                ((BiddingViewHolder) holder).fullbidding_submitDate.setText(date);

            } catch (ParseException e) {
                e.printStackTrace();
                ((BiddingViewHolder) holder).fullbidding_submitDate.setText(Bidding.getPost_date());
            }
            if(Bidding.getArea().equals("")){
                ((BiddingViewHolder) holder).fullbidding_area.setVisibility(View.GONE);
                ((BiddingViewHolder) holder).fullbidding_area_unit.setVisibility(View.GONE);
                ((BiddingViewHolder) holder).fullbidding_slash.setVisibility(View.GONE);
            }
            else{
                ((BiddingViewHolder) holder).fullbidding_area.setText(Bidding.getArea());
            }
            if(Bidding.getNominated())
                ((BiddingViewHolder) holder).fullbidding_nominated_icon.setVisibility(View.VISIBLE);
            if(!Bidding.getStyle().getImg().equals("")){
                try{

                    String header = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                            .getString("user_token","");

                    GlideUrl glideUrl = new GlideUrl(
                            Bidding.getStyle().getImg(),
                            new LazyHeaders.Builder()
                                    .addHeader("Authorization", "Bearer "+header)
                                    .build());

                    Glide.with(context).load(glideUrl).into( ( (BiddingViewHolder) holder).fullbidding_image);


                }
                catch (Exception e){
                    Glide.with(context).load(R.drawable.image_placeholder).into( ( (BiddingViewHolder) holder).fullbidding_image);
                }
            }
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'");
//             try {
//                Date newDate = format.parse(singlePartialBidding.getCreated_at());
//                format = new SimpleDateFormat("yyyy-MM-dd");
//                String date = format.format(newDate);
//                 ((BiddingViewHolder) holder).bidding_create_date.setText(date);
//
//             } catch (ParseException e) {
//                e.printStackTrace();
//            }


            ((BiddingViewHolder) holder).fullbidding_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Fragment nextFrag= new QuoteProjectDetailFragment();

                    Bundle bundle = new Bundle();

                    bundle.putInt("project_id", Bidding.getId());
                    bundle.putString("post_date", Bidding.getPost_date());
                    bundle.putString("project_image", Bidding.getStyle().getImg());
                    nextFrag.setArguments(bundle);

                    ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.nav_host_fragment, nextFrag)
                            .addToBackStack(null)
                            .commit();

                }
            });

        }
//
        else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        full_loading = false;
    }

    @Override
    public int getItemCount() {
        return mBiddingList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    //
    public static class BiddingViewHolder extends RecyclerView.ViewHolder {
        public TextView fullbidding_district;
        public TextView fullbidding_price;
        public TextView fullbidding_type;
        public TextView fullbidding_area;
        public TextView fullbidding_area_unit;
        public TextView fullbidding_slash;
        public TextView fullbidding_submitDate;
        public TextView fullbidding_project_id;
        public ImageView fullbidding_image;
        public ImageView fullbidding_nominated_icon;
        public LinearLayout fullbidding_layout;

        public BiddingViewHolder(View v) {
            super(v);
            fullbidding_district = (TextView)v.findViewById(R.id.fullbidding_district);
            fullbidding_price = (TextView)v.findViewById(R.id.fullbidding_price);
            fullbidding_type = (TextView)v.findViewById(R.id.fullbidding_type);
            fullbidding_area = (TextView)v.findViewById(R.id.fullbidding_area);
            fullbidding_area_unit = (TextView)v.findViewById(R.id.fullbidding_area_unit);
            fullbidding_slash = (TextView)v.findViewById(R.id.fullbidding_slash);
            fullbidding_type = (TextView)v.findViewById(R.id.fullbidding_type);
            fullbidding_submitDate = (TextView)v.findViewById(R.id.fullbidding_submitDate);
            fullbidding_project_id = (TextView)v.findViewById(R.id.fullbidding_project_id);
            fullbidding_image = (ImageView) v.findViewById(R.id.fullbidding_image);
            fullbidding_nominated_icon = (ImageView) v.findViewById(R.id.bidding_nominate_icon);
            fullbidding_layout = (LinearLayout) v.findViewById(R.id.fullbidding_layout);


        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
    public void update(List<Bidding> BiddingItems){
        mBiddingList.addAll(BiddingItems);
        notifyDataSetChanged();
    }

    // This method is used to remove ProgressBar when data is loaded
    public void removeLastItem(){
        mBiddingList.remove(mBiddingList.size() - 1);
        notifyDataSetChanged();
    }

}