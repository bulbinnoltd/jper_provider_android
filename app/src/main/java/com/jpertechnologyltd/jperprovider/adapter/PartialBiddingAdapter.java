package com.jpertechnologyltd.jperprovider.adapter;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener;
import com.jpertechnologyltd.jperprovider.item.Bidding;
import com.jpertechnologyltd.jperprovider.item.ProjectCategory;
import com.jpertechnologyltd.jperprovider.ui.quoteproject.QuoteProjectDetailFragment;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PartialBiddingAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    private List<Bidding> mPartialBiddingList;
    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    public boolean loading;
    private OnLoadMoreListener onLoadMoreListener;



    public PartialBiddingAdapter(List<Bidding> mPartialBiddings, RecyclerView recyclerView) {
        mPartialBiddingList = mPartialBiddings;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            Log.d("part_totalItemCount",String.valueOf( totalItemCount));
                            Log.d("part_lastVisibleItem",String.valueOf( lastVisibleItem));
                            Log.d("part_visibleThreshold",String.valueOf( visibleThreshold));
                            Log.d("part_loading",String.valueOf(loading));
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
//        ////////////////////////////////ad_mob///////////////////////////
//        if (position % 5 == 0)
//            return AD_TYPE;
//        ////////////////////////////////ad_mob///////////////////////////
        return mPartialBiddingList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_partialbidding, parent, false);

            vh = new BiddingViewHolder(v);
        }
//        ////////////////////////////////ad_mob///////////////////////////
//        else if(viewType == AD_TYPE) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.list_item_native_ad, parent, false);
//            vh = new UnifiedNativeAdViewHolder(v);
//
//        }
//        ////////////////////////////////ad_mob///////////////////////////
        else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        if (holder instanceof BiddingViewHolder) {

            final Bidding singlePartialBidding= (Bidding) mPartialBiddingList.get(position);
            ((BiddingViewHolder) holder).bidding_district.setText(singlePartialBidding.getDistrict());
            ((BiddingViewHolder) holder).bidding_project_id.setText("#"+String.valueOf(singlePartialBidding.getId()));
            try {
                String number = singlePartialBidding.getBudget_max();
                double amount = Double.parseDouble(number);
                DecimalFormat formatter = new DecimalFormat("#,###.##");
                String formatted = formatter.format(amount);

                if(formatted.contains(".00"))  formatted = formatted.substring(0, formatted.length() - 3);
                ((BiddingViewHolder) holder).bidding_price.setText("HK $" + formatted);
            }
            catch (Exception e){
                e.printStackTrace();
                ((BiddingViewHolder) holder).bidding_price.setText("HK $" +  singlePartialBidding.getBudget_max());
            }

//            ((BiddingViewHolder) holder).quote_style.setText(String.valueOf(singlePartialBidding.getStyle_id()));
            ((BiddingViewHolder) holder).bidding_style.setText(singlePartialBidding.getType());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date newDate = format.parse(singlePartialBidding.getPost_date());
                format = new SimpleDateFormat("yyyy-MM-dd");
                String date = format.format(newDate);
                ((BiddingViewHolder) holder).bidding_create_date.setText(date);

            } catch (ParseException e) {
                e.printStackTrace();
                ((BiddingViewHolder) holder).bidding_create_date.setText(singlePartialBidding.getPost_date());
            }
            if(!singlePartialBidding.getCat_cover().equals("")){
                    try{

                        String header = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                                .getString("user_token","");

                        GlideUrl glideUrl = new GlideUrl(
                                singlePartialBidding.getCat_cover(),
                                new LazyHeaders.Builder()
                                        .addHeader("Authorization", "Bearer "+header)
                                        .build());

                        Glide.with(context).load(glideUrl).into( ((BiddingViewHolder) holder).partialbidding_image);


                    }
                    catch (Exception e){
                        Glide.with(context).load(R.drawable.image_placeholder).into( ((BiddingViewHolder) holder).partialbidding_image);
                    }
                }

//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'");
//             try {
//                Date newDate = format.parse(singlePartialBidding.getCreated_at());
//                format = new SimpleDateFormat("yyyy-MM-dd");
//                String date = format.format(newDate);
//                 ((BiddingViewHolder) holder).bidding_create_date.setText(date);
//
//             } catch (ParseException e) {
//                e.printStackTrace();
//            }


            ChipGroup chipGroup = new ChipGroup(((BiddingViewHolder) holder).chipgroup.getContext());

            String[] genres = {"Thriller", "Comedy", "Adventure"};
//            for(String genre : genres) {
//                Chip chip = new Chip(((BiddingViewHolder) holder).chipgroup.getContext());
//                chip.setText(genre);
//                chip.setTextSize(12);
//                chip.setChipBackgroundColorResource(R.color.colorChip);
//                ((BiddingViewHolder) holder).chipgroup.addView(chip);
//            }

            ArrayList<String> project_category_array = new ArrayList<String>();
            if(singlePartialBidding.getCategory()!=null ){
                if(singlePartialBidding.getCategory().size()>0) {
                    for (int index : singlePartialBidding.getCategory())
                    {
                        // only changes num, not the array element
                        Log.d("cat_proj",new ProjectCategory(context, (index)).getName());
                        Log.d("cat_proj2",String.valueOf(index));
                        project_category_array.add(new ProjectCategory(context, index).getName());
                    }
                    for(String genre : project_category_array) {
                        Chip chip = new Chip(((BiddingViewHolder) holder).chipgroup.getContext());
                        chip.setText(genre);
                        chip.setTextSize(12);
                        chip.setChipStartPadding(2);
                        chip.setChipEndPadding(0);
                        chip.setIconEndPadding(0);
                        chip.setTextEndPadding(0);
                        chip.setCloseIconEndPadding(0);
                        chip.setEnsureMinTouchTargetSize(false);
                        chip.setChipBackgroundColorResource(R.color.colorChip);
                        ((BiddingViewHolder) holder).chipgroup.addView(chip);
                    }
//                    for (index in singlePartialBidding.getCategory().indices) {
//                        project_category_array.add(ProjectCategory(context
//                        !!, QuoteProjectSummary.category_array[index]).name)
//                    }

//                    for (index in project_category_array.indices) {
//                        val chip = Chip(chipgroup.context)
//                        chip.text = project_category_array.get(index)
//                        chipgroup.addView(chip)
                    }
            }
            ((BiddingViewHolder) holder).bidding_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Fragment nextFrag= new QuoteProjectDetailFragment();

                    Bundle bundle = new Bundle();

                    bundle.putInt("project_id", singlePartialBidding.getId());
                    bundle.putString("post_date", singlePartialBidding.getPost_date());
                    bundle.putString("project_image", singlePartialBidding.getCat_cover());
                    nextFrag.setArguments(bundle);

                    ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.nav_host_fragment, nextFrag)
                            .addToBackStack(null)
                            .commit();

                }
            });
        }
//
        else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return mPartialBiddingList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    //
    public static class BiddingViewHolder extends RecyclerView.ViewHolder {
        public TextView bidding_district;
        public TextView bidding_style;
        public TextView bidding_price;
        public TextView bidding_create_date;
        public TextView bidding_project_id;
        public LinearLayout bidding_layout;
        public ImageView partialbidding_image;
        public com.google.android.material.chip.ChipGroup chipgroup;

        public BiddingViewHolder(View v) {
            super(v);
            partialbidding_image = (ImageView)v.findViewById(R.id.partialbidding_image);
            bidding_district = (TextView)v.findViewById(R.id.bidding_district);
            bidding_style = (TextView)v.findViewById(R.id.bidding_style);
            bidding_price = (TextView)v.findViewById(R.id.bidding_price);
            bidding_create_date = (TextView)v.findViewById(R.id.bidding_create_date);
            bidding_project_id = (TextView)v.findViewById(R.id.bidding_project_id);
            bidding_layout = (LinearLayout)v.findViewById(R.id.bidding_layout);
            chipgroup = (ChipGroup) v.findViewById(R.id.chipgroup);


        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
    public void update(List<Bidding> PartialBiddingItems){
        mPartialBiddingList.addAll(PartialBiddingItems);
        notifyDataSetChanged();
    }

    // This method is used to remove ProgressBar when data is loaded
    public void removeLastItem(){
        mPartialBiddingList.remove(mPartialBiddingList.size() - 1);
        notifyDataSetChanged();
    }

}