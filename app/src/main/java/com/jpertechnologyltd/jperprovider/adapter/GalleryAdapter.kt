package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.item.Activity
import com.jpertechnologyltd.jperprovider.item.Portfolio
import com.jpertechnologyltd.jperprovider.item.Style
import com.stfalcon.imageviewer.StfalconImageViewer

class GalleryAdapter (private val mPortfolios: List<Portfolio>, private val mactivity: FragmentActivity) : RecyclerView.Adapter<GalleryAdapter.ViewHolder>()
{
    var context : Context?=null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row

        val imageView = itemView.findViewById<ImageView>(R.id.imageView)
        val cardView = itemView.findViewById<CardView>(R.id.cardView)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val styleView = inflater.inflate(R.layout.item_gallery, parent, false)
        // Return a new holder instance


        return ViewHolder(styleView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val portfolio: Portfolio = mPortfolios.get(position)
        // Set item views based on your views and data model

        val imageView= viewHolder.imageView
        val cardView = viewHolder.cardView
//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline
        cardView.setOnClickListener{
//            val listimages =
//                arrayOf<String?>("https://uat.jper.com.hk/style/image/7","https://uat.jper.com.hk/style/image/7")
//            StfalconImageViewer.Builder<String>(context, listimages) { view, image ->
//                Glide.with(context!!).load(image).into(view)
//                //Picasso.get().load(image).into(view)
//            }.show()
            val intent = Intent(context, WebviewActivity::class.java)
            intent.putExtra("url",portfolio.url)
            Log.d("url", "https://uat.jper.com.hk/join/us")
            context!!.startActivity(intent)
        }
        try{
            val header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                ?.getString("user_token","")
            Log.d("image_cover",portfolio.image)
            val glideUrl = GlideUrl(
                portfolio.image,
                LazyHeaders.Builder()
                    .addHeader("Authorization", "Bearer ".plus(header))
                    .build()
            )

            Glide.with(context!!).load(glideUrl).placeholder(R.drawable.image_placeholder).into(imageView)
        }
        catch (e: Exception ){
            Log.d("error_load_image",e.toString())
            Glide.with(context!!).load(R.drawable.image_placeholder).into(imageView)
        }
        try{
            cardView.setOnClickListener{arg0 ->
                val activity = arg0.getContext() as AppCompatActivity

                val intent = Intent(activity, WebviewActivity::class.java)
                intent.putExtra("url",portfolio.url!!)
                Log.d("url",portfolio.url)
                context!!.startActivity(intent)
            }
        }
        catch (e: Exception ){
            Log.d("error",e.toString())
        }

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mPortfolios.size
    }
}