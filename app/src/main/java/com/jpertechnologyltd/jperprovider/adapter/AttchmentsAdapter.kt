package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Attachments
import com.jpertechnologyltd.jperprovider.ui.quoteproject.AttachmentsFragment


class AttachmentsAdapter (private val mAttachments: List<Attachments>) : RecyclerView.Adapter<AttachmentsAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val attachment_title = itemView.findViewById<TextView>(R.id.attachments_title)
        val attachment_view = itemView.findViewById<ImageView>(R.id.attachments_view_icon)
        val attachment_download = itemView.findViewById<ImageView>(R.id.attachments_download_icon)
        val attachments_layout = itemView.findViewById<LinearLayout>(R.id.attachments_layout)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val attachmentView = inflater.inflate(R.layout.item_attachment, parent, false)
        // Return a new holder instance
        return ViewHolder(attachmentView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val Attachments: Attachments = mAttachments.get(position)
        // Set item views based on your views and data model
        val attachment_title = viewHolder.attachment_title
        val attachment_view = viewHolder.attachment_view
        val attachment_dowload = viewHolder.attachment_download
        val attachment_layout = viewHolder.attachments_layout


        attachment_title.setText(Attachments.title)
        attachment_view.setImageURI(Attachments.view)
        attachment_dowload.setImageURI(Attachments.download)


        attachment_layout.setOnClickListener(View.OnClickListener { arg0 ->

            val activity = arg0.getContext() as AppCompatActivity
            var fr = activity.supportFragmentManager?.beginTransaction()
            fr?.replace(R.id.nav_host_fragment,
                AttachmentsFragment()
            )
            fr?.commit()
        })
    }//quote_subtitle.setText(quote.subtitle)
//        val res: Int =
//            context?.getResources()!!.getIdentifier(quote.image, "drawable", context!!.getPackageName())
//        imageView.setImageResource(res)

//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mAttachments.size
    }
}

fun ImageView.setImageURI(view: String) {

}



