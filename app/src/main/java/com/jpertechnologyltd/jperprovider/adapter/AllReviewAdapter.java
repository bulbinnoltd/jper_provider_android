package com.jpertechnologyltd.jperprovider.adapter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener;
import com.jpertechnologyltd.jperprovider.item.Comment;

import java.util.ArrayList;
import java.util.List;


public class AllReviewAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    private ArrayList<Comment> commentsList;
    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;



    public AllReviewAdapter(ArrayList<Comment> comments, RecyclerView recyclerView) {
        commentsList = comments;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                Log.d("review_totalItemCount", String.valueOf(totalItemCount));
                                Log.d("review_lastVisibleItem", String.valueOf(lastVisibleItem));
                                Log.d("review_visibleThreshold", String.valueOf(visibleThreshold));
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
//        ////////////////////////////////ad_mob///////////////////////////
//        if (position % 5 == 0)
//            return AD_TYPE;
//        ////////////////////////////////ad_mob///////////////////////////
        return commentsList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_review, parent, false);

            vh = new ProjectsViewHolder(v);
        }
//        ////////////////////////////////ad_mob///////////////////////////
//        else if(viewType == AD_TYPE) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.list_item_native_ad, parent, false);
//            vh = new UnifiedNativeAdViewHolder(v);
//
//        }
//        ////////////////////////////////ad_mob///////////////////////////
        else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProjectsViewHolder) {

            final Comment singleComment= (Comment) commentsList.get(position);
            ((ProjectsViewHolder) holder).review_desc.setText(String.valueOf(singleComment.getComment()));
            ((ProjectsViewHolder) holder).review_date.setText(singleComment.getCreated_at());
            ((ProjectsViewHolder) holder).review_username.setText(singleComment.getUsername());
            ((ProjectsViewHolder) holder).rating.setRating(singleComment.getRate());

//
//            ((ProjectsViewHolder) holder).projectView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//                }
//            });
//            Ion.with(context)
//                    .load(singleBlog.getImg_name())
//                    .withBitmap()
//                    .centerCrop()
//                    .resize(200,200)
//                    .animateLoad(null)
//                    .animateIn(null)
//                    .intoImageView(((BlogsViewHolder) holder).imageView);

//            (BlogsViewHolder) holder).listitem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(final View arg0) {
//                    final Intent viewIntent =
//                            new Intent(arg0.getContext(), ArticleActivity.class);
//                    viewIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                    viewIntent.putExtra("Article_id",Long.toString(signleNews.getArticle_id()));
//                    viewIntent.putExtra("Url",signleNews.geturl());
//                    viewIntent.putExtra("Image",signleNews.getimage());
//                    viewIntent.putExtra("Source_icon",signleNews.getsource_icon());
//                    viewIntent.putExtra("Title",signleNews.gettitle());
//                    viewIntent.putExtra("Provider_name",signleNews.getprovider_name());
//                }
//            });

        }
//
        else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    //
    public static class ProjectsViewHolder extends RecyclerView.ViewHolder {
        public TextView review_desc;
        public TextView review_date;
        public TextView review_username;
        public RatingBar rating;


        public Comment comments;

        public ProjectsViewHolder(View v) {
            super(v);

            review_desc = (TextView)v.findViewById(R.id.review_desc);
            review_date = (TextView)v.findViewById(R.id.review_date);
            review_username = (TextView)v.findViewById(R.id.review_username);
            rating = (RatingBar) v.findViewById(R.id.rating);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
    public void update(List<Comment> CommentsItems){
        commentsList.addAll(CommentsItems);
        notifyDataSetChanged();
    }

    // This method is used to remove ProgressBar when data is loaded
    public void removeLastItem(){
        commentsList.remove(commentsList.size() - 1);
        notifyDataSetChanged();
    }
    public void clear() {
        commentsList.clear();
        notifyDataSetChanged();
    }


}