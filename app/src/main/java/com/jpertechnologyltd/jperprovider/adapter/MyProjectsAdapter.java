package com.jpertechnologyltd.jperprovider.adapter;


import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener;
import com.jpertechnologyltd.jperprovider.item.ProjectCategory;
import com.jpertechnologyltd.jperprovider.item.ProjectStatus;
import com.jpertechnologyltd.jperprovider.item.myproject.MyProjects;
import com.jpertechnologyltd.jperprovider.ui.myproject.MyProjectsQuotedDetailFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MyProjectsAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private Context context;
    private ArrayList<MyProjects> myProjectsList;
    // The minimum amount of items to have below your current scroll position
// before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;



    public MyProjectsAdapter(ArrayList<MyProjects> myProjects, RecyclerView recyclerView) {
        myProjectsList = myProjects;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
//        ////////////////////////////////ad_mob///////////////////////////
//        if (position % 5 == 0)
//            return AD_TYPE;
//        ////////////////////////////////ad_mob///////////////////////////
        return myProjectsList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;
        context = parent.getContext();
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_myprojects, parent, false);

            vh = new BiddingViewHolder(v);
        }
//        ////////////////////////////////ad_mob///////////////////////////
//        else if(viewType == AD_TYPE) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(
//                    R.layout.list_item_native_ad, parent, false);
//            vh = new UnifiedNativeAdViewHolder(v);
//
//        }
//        ////////////////////////////////ad_mob///////////////////////////
        else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        if (holder instanceof BiddingViewHolder) {

            final MyProjects singleMyProject= (MyProjects) myProjectsList.get(position);


            ((BiddingViewHolder) holder).myprojects_title.setText(context.getResources().getString(R.string.project_title)+" #"+singleMyProject.getProject_id());
            ((BiddingViewHolder) holder).myprojects_status.setText(String.valueOf(singleMyProject.getStatus_id()));
            ((BiddingViewHolder) holder).myprojects_district.setText(singleMyProject.getDistrict());
            ((BiddingViewHolder) holder).myprojects_style.setText(singleMyProject.getType()+" "+singleMyProject.getScope());
//
//            ((BiddingViewHolder) holder).myprojects_submitDate.setText(singleMyProject.getMatched_date());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date newDate = format.parse(singleMyProject.getMatched_date());
                format = new SimpleDateFormat("yyyy-MM-dd");
                String date = format.format(newDate);
                ((BiddingViewHolder) holder).myprojects_submitDate.setText(date);

            } catch (ParseException e) {
                e.printStackTrace();
                ((BiddingViewHolder) holder).myprojects_submitDate.setText(singleMyProject.getMatched_date());
            }

            ((BiddingViewHolder) holder).myprojects_address.setText(singleMyProject.getAddress());
            ((BiddingViewHolder) holder).owner_username.setText(singleMyProject.getOwner_name());


            ChipGroup chipGroup = new ChipGroup(((BiddingViewHolder) holder).chipgroup.getContext());
            String status = new ProjectStatus(context, singleMyProject.getStatus_id()).getName();
            ArrayList<String> project_category_array = new ArrayList<String>();
            if(singleMyProject.getCategory()!=null ){
                if(singleMyProject.getCategory().size()>0) {
                    for (int index : singleMyProject.getCategory())
                    {
                        // only changes num, not the array element
                        Log.d("cat_myproj",new ProjectCategory(context, (index)).getName());
                        Log.d("cat_myproj2",String.valueOf(index));
                        project_category_array.add(new ProjectCategory(context, index).getName());
                    }
                    for(String genre : project_category_array) {
                        Chip chip = new Chip(((BiddingViewHolder) holder).chipgroup.getContext());
                        chip.setText(genre);
                        chip.setTextSize(12);

                        chip.setChipStartPadding(2);
                        chip.setChipEndPadding(0);
                        chip.setIconEndPadding(0);
                        chip.setTextEndPadding(0);
                        chip.setCloseIconEndPadding(0);
                        chip.setEnsureMinTouchTargetSize(false);
                        chip.setChipBackgroundColorResource(R.color.colorChip);
                        ((BiddingViewHolder) holder).chipgroup.addView(chip);
                    }
//                    for (index in singlePartialBidding.getCategory().indices) {
//                        project_category_array.add(ProjectCategory(context
//                        !!, QuoteProjectSummary.category_array[index]).name)
//                    }

//                    for (index in project_category_array.indices) {
//                        val chip = Chip(chipgroup.context)
//                        chip.text = project_category_array.get(index)
//                        chipgroup.addView(chip)
                }
            }

            Integer status_rgb  = new ProjectStatus(context, singleMyProject.getStatus_id()).getRgb();
            ((BiddingViewHolder) holder).myprojects_status.setText(status);
            GradientDrawable drawable = (GradientDrawable) ((BiddingViewHolder) holder).myprojects_status.getBackground();
            drawable.setColor(status_rgb);


            ((BiddingViewHolder) holder).projectView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {

//                    Activity activity = (AppCompatActivity) arg0.getContext();
//
//                    Intent intent = new Intent(activity, MyProjectDetailActivity.class);
//                    intent.putExtra("project_id",singleMyProject.getProject_id());
//                    intent.putExtra("status_id",singleMyProject.getStatus_id());
//
//                    context.startActivity(intent);
                    Fragment nextFrag= new MyProjectsQuotedDetailFragment();

                    Bundle bundle = new Bundle();

                    bundle.putInt("bg_accepted",singleMyProject.getBg_accepted());
                    bundle.putInt("project_id",singleMyProject.getProject_id());
                    bundle.putInt("status_id",singleMyProject.getStatus_id());
                    bundle.putString("contract_sum",singleMyProject.getContract_sum());
                    bundle.putString("post_date",singleMyProject.getPost_date());
                    bundle.putString("contract_m",singleMyProject.getContract_m());
                    bundle.putInt("scope_id",singleMyProject.getScope_id());
                    Log.d("scope_id_in_proj",String.valueOf(singleMyProject.getScope_id()));
//
                    nextFrag.setArguments(bundle);

                    ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
                            .add(R.id.nav_host_fragment, nextFrag)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
//
        else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return myProjectsList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    //
    public static class BiddingViewHolder extends RecyclerView.ViewHolder {
        public TextView myprojects_title;
        public TextView myprojects_district;
        public TextView myprojects_address;
        public TextView myprojects_style;
        public TextView myprojects_status;
        public TextView myprojects_submitDate;
        public TextView owner_username;
        public CardView projectView;
        public ChipGroup chipgroup;

        public BiddingViewHolder(View v) {
            super(v);
            myprojects_title = (TextView)v.findViewById(R.id.myprojects_title);
            myprojects_district = (TextView)v.findViewById(R.id.myprojects_district);
            myprojects_style = (TextView)v.findViewById(R.id.myprojects_style);
            myprojects_address = (TextView)v.findViewById(R.id.myprojects_address);

            myprojects_status = (TextView)v.findViewById(R.id.myprojects_status);

            owner_username = (TextView)v.findViewById(R.id.owner_username);
            myprojects_submitDate = (TextView)v.findViewById(R.id.myprojects_submitDate);
            projectView = (CardView) v.findViewById(R.id.projectView);
            chipgroup = (ChipGroup) v.findViewById(R.id.chipgroup);


        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
    public void update(ArrayList<MyProjects> MyProjects){
        myProjectsList.addAll(MyProjects);
        notifyDataSetChanged();
    }

    // This method is used to remove ProgressBar when data is loaded
    public void removeLastItem(){
        myProjectsList.remove(myProjectsList.size() - 1);
        notifyDataSetChanged();
    }

}