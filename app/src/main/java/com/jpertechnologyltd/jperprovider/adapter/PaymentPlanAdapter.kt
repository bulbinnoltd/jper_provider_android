package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.*
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.PaymentPlan
import com.jpertechnologyltd.jperprovider.ui.myproject.MyProjectsQuotedPaymentPlanFragment
import com.jpertechnologyltd.jperprovider.ui.myproject.PaymentNoticeFragment
import com.jpertechnologyltd.jperprovider.ui.myproject.notice.DepositReceiptFragment
import com.jpertechnologyltd.jperprovider.ui.myproject.notice.PaymentReceiptFragment
import java.text.DecimalFormat
import java.util.ArrayList


class PaymentPlanAdapter(
    private val mPaymentPlans: ArrayList<PaymentPlan>,
    private val parentFragment: Fragment,
    private val thisFragment: MyProjectsQuotedPaymentPlanFragment
) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    val Retention = 0
    val PaymentPlan = 1
    var context: Context? =null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class RetentionViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val payment_plan_title = itemView.findViewById<TextView>(R.id.payment_plan_title)
        val payment_plan_status = itemView.findViewById<TextView>(R.id.payment_plan_status)
        val payment_plan_date_value = itemView.findViewById<TextView>(R.id.payment_plan_date_value)
        val payment_plan_date = itemView.findViewById<TextView>(R.id.payment_plan_date)
        val payment_plan_date_icon = itemView.findViewById<ImageView>(R.id.payment_plan_date_icon)
        val payment_plan_amount_value = itemView.findViewById<TextView>(R.id.payment_plan_amount_value)
        val payment_notice_button = itemView.findViewById<ImageView>(R.id.payment_notice_button)
        val pay_receipt_button = itemView.findViewById<ImageView>(R.id.pay_receipt_button)
        val payment_notice_button_title = itemView.findViewById<TextView>(R.id.payment_notice_button_title)
        val pay_receipt_button_title = itemView.findViewById<TextView>(R.id.pay_receipt_button_title)
        val payment_plan_view = itemView.findViewById<ConstraintLayout>(R.id.payment_plan_view)
        val payment_plan_remarks = itemView.findViewById<TextView>(R.id.payment_plan_remarks)
        val separator =  itemView.findViewById<View>(R.id.separator)
    }
    inner class PaymentPlanViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val payment_plan_title = itemView.findViewById<TextView>(R.id.payment_plan_title)
        val payment_plan_status = itemView.findViewById<TextView>(R.id.payment_plan_status)
        val payment_plan_date_value = itemView.findViewById<TextView>(R.id.payment_plan_date_value)
        val payment_plan_date = itemView.findViewById<TextView>(R.id.payment_plan_date)
        val payment_plan_date_icon = itemView.findViewById<ImageView>(R.id.payment_plan_date_icon)
        val payment_plan_amount_value = itemView.findViewById<TextView>(R.id.payment_plan_amount_value)
        val issue_notice_button = itemView.findViewById<Button>(R.id.issue_notice_button)
        val payment_notice_button = itemView.findViewById<ImageView>(R.id.payment_notice_button)
        val pay_receipt_button = itemView.findViewById<ImageView>(R.id.pay_receipt_button)
        val payment_notice_button_title = itemView.findViewById<TextView>(R.id.payment_notice_button_title)
        val pay_receipt_button_title = itemView.findViewById<TextView>(R.id.pay_receipt_button_title)
        val payment_plan_view = itemView.findViewById<ConstraintLayout>(R.id.payment_plan_view)
        val payment_plan_remarks = itemView.findViewById<TextView>(R.id.payment_plan_remarks)
        val view_payment_record_buttonView = itemView.findViewById<LinearLayout>(R.id.view_payment_record_buttonView)
        val view_payment_record_button = itemView.findViewById<Button>(R.id.view_payment_record_button)
        val separator =  itemView.findViewById<View>(R.id.separator)

        val edit_icon_button = itemView.findViewById<Button>(R.id.edit_icon_button)
        val edit_icon_button_view = itemView.findViewById<ConstraintLayout>(R.id.edit_icon_button_view)
    }

    override fun getItemViewType(position: Int): Int {
        if  (mPaymentPlans.get(position).is_retention)
            return Retention
        else
            return PaymentPlan
    }
    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout

        // Return a new holder instance
        if (viewType == Retention) {

            val blogView = inflater.inflate(R.layout.item_payment_plan, parent, false)
            return RetentionViewHolder(blogView)
        } else  {

            val blogView = inflater.inflate(R.layout.item_payment_plan, parent, false)
            return PaymentPlanViewHolder(blogView)
        }

    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder( viewHolder: RecyclerView.ViewHolder, position: Int) {
        viewHolder.setIsRecyclable(false)
        // override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        if (viewHolder is RetentionViewHolder) {
            val payment_planitem: PaymentPlan = mPaymentPlans.get(position)
            // Set item views based on your views and data model
            val payment_plan_title = viewHolder.payment_plan_title
            val payment_plan_date_icon = viewHolder.payment_plan_date_icon
            val payment_plan_date = viewHolder.payment_plan_date
            val payment_plan_date_value = viewHolder.payment_plan_date_value
            val payment_plan_amount_value = viewHolder.payment_plan_amount_value
            val payment_plan_status = viewHolder.payment_plan_status
            val payment_notice_button=viewHolder.payment_notice_button
            val pay_receipt_button = viewHolder.pay_receipt_button
            val payment_notice_button_title=viewHolder.payment_notice_button_title
            val pay_receipt_button_title = viewHolder.pay_receipt_button_title
            val payment_plan_remarks = viewHolder.payment_plan_remarks
            val payment_plan_view = viewHolder.payment_plan_view
            val separator = viewHolder.separator

            when (payment_planitem.status_id) {

                1 -> {

                    payment_notice_button_title.visibility = View.VISIBLE
                }

                2 -> {
                    payment_plan_status.visibility = View.VISIBLE
                    separator.visibility = View.GONE
                    payment_plan_date.visibility = View.GONE
                    payment_plan_date_value.visibility = View.GONE
                    payment_plan_date_icon.visibility = View.GONE
                }

                3 -> {
                    payment_plan_status.visibility = View.VISIBLE
                    separator.visibility = View.VISIBLE
                    payment_plan_date.visibility = View.GONE
                    payment_plan_date_value.visibility = View.GONE
                    payment_plan_date_icon.visibility = View.GONE
                    pay_receipt_button_title.visibility = View.VISIBLE

                }
                else ->{
                    payment_plan_date.visibility = View.GONE
                    payment_plan_date_value.visibility = View.GONE
                    payment_plan_date_icon.visibility = View.GONE
                    pay_receipt_button_title.visibility = View.VISIBLE

                }
            }

            val bundle1 = Bundle()

            bundle1.putInt("project_id", payment_planitem.project_id)
            bundle1.putInt("retention_id", payment_planitem.id)
            bundle1.putString("amount", payment_planitem.amount)
            bundle1.putString("company_name_en", payment_planitem.company_name_en)
            bundle1.putString("company_name_zh", payment_planitem.company_name_zh)

            bundle1.putString("owner_name", payment_planitem.owner_name!!)
            bundle1.putString("owner_email", payment_planitem.owner_email!!)
//            val DepositNoticeFragment= DepositNoticeFragment()
//            DepositNoticeFragment.setArguments(bundle1)


            val bundle2 = Bundle()

            bundle2.putInt("project_id", payment_planitem.project_id)
            val DepositReceiptFragment= DepositReceiptFragment()
            DepositReceiptFragment.setArguments(bundle2)

            Log.d(
                "is_retention",
                mPaymentPlans.get(position).is_retention.toString() + "   " + position.toString()
            )
            payment_plan_view.setBackgroundResource(R.drawable.project_retention_payment_plan)
            payment_notice_button.setOnClickListener(View.OnClickListener {
//                val activity = parentFragment.activity as MainActivity
//                var fr = activity.supportFragmentManager?.beginTransaction()
//                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                fr?.addToBackStack(null)
//                fr?.add(
//                    R.id.nav_host_fragment,
//                    DepositNoticeFragment
//                )
//                fr?.commit()
            })
            payment_notice_button_title.setOnClickListener(View.OnClickListener {
//                val activity = parentFragment.activity as MainActivity
//                var fr = activity.supportFragmentManager?.beginTransaction()
//                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                fr?.addToBackStack(null)
//                fr?.add(
//                    R.id.nav_host_fragment,
//                    DepositNoticeFragment
//                )
//                fr?.commit()
            })
            pay_receipt_button.setOnClickListener(View.OnClickListener {
                val activity = parentFragment.activity as MainActivity
                var fr = activity.supportFragmentManager?.beginTransaction()
                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())

                fr?.addToBackStack(null)
                fr?.add(
                    R.id.nav_host_fragment,
                    DepositReceiptFragment
                )
                fr?.commit()
            })
            pay_receipt_button_title.setOnClickListener(View.OnClickListener {
                val activity = parentFragment.activity as MainActivity
                var fr = activity.supportFragmentManager?.beginTransaction()
                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())

                fr?.addToBackStack(null)
                fr?.add(
                    R.id.nav_host_fragment,
                    DepositReceiptFragment
                )
                fr?.commit()
            })

            payment_plan_title.setText(payment_planitem.title)
            payment_plan_date_value.setText(context!!.getString(R.string.PaymentItem_immediate))
//            payment_plan_amount_value.setText(
//                "HK$ ".plus(
//                    NumberFormat.getNumberInstance(Locale.US).format(
//                        payment_planitem.amount.toInt()
//                    )
//                )
//            )
            payment_plan_amount_value.setText(
                "HK$ ".plus(payment_planitem.amount)
            )

            payment_plan_status.setText(payment_planitem.status)
            val drawable = payment_plan_status.getBackground() as GradientDrawable
            drawable.setColor(payment_planitem.status_rgb)
        }
        else if (viewHolder is PaymentPlanViewHolder)  {
            val payment_planitem: PaymentPlan = mPaymentPlans.get(position)
            // Set item views based on your views and data model
            val payment_plan_title = viewHolder.payment_plan_title
            val payment_plan_date_icon = viewHolder.payment_plan_date_icon
            val payment_plan_date = viewHolder.payment_plan_date
            val payment_plan_date_value = viewHolder.payment_plan_date_value
            val payment_plan_amount_value = viewHolder.payment_plan_amount_value
            val payment_plan_status = viewHolder.payment_plan_status
            val payment_notice_button=viewHolder.payment_notice_button
            val issue_notice_button=viewHolder.issue_notice_button
            val pay_receipt_button = viewHolder.pay_receipt_button
            val payment_notice_button_title=viewHolder.payment_notice_button_title
            val pay_receipt_button_title = viewHolder.pay_receipt_button_title
            val payment_plan_remarks = viewHolder.payment_plan_remarks
            val payment_plan_view = viewHolder.payment_plan_view
            val view_payment_record_buttonView = viewHolder.view_payment_record_buttonView
            val view_payment_record_button = viewHolder.view_payment_record_button
            val separator = viewHolder.separator
            val edit_icon_button = viewHolder.edit_icon_button
            val edit_icon_button_view = viewHolder.edit_icon_button_view

            Log.d("payment_plan_title", payment_planitem.title.toString())
            Log.d("payment_status_id", payment_planitem.payment_status_id.toString())
            Log.d("payment_status", payment_planitem.status_id.toString())
            if(payment_planitem.remarks.equals(""))
                payment_plan_remarks.visibility=View.GONE
            else
                payment_plan_remarks.text= payment_planitem.remarks
            Log.d("payment_planitem.status_id",payment_planitem.status_id.toString())
            Log.d("payment_planitem.payment_status_id",payment_planitem.payment_status_id.toString())
            if(payment_planitem.payment_status_id ==1){
                edit_icon_button_view.visibility=View.VISIBLE
                edit_icon_button.setOnClickListener(View.OnClickListener {
                    val intent = Intent((parentFragment.activity as MainActivity?), EditPaymentPlanActivity::class.java)
                    intent.putExtra("title",payment_planitem.title)
                    intent.putExtra("plan_id",payment_planitem.id)
                    intent.putExtra("project_id",payment_planitem.project_id)
                    intent.putExtra("is_final",payment_planitem.is_final)
                    intent.putExtra("amount",payment_planitem.amount)
                    intent.putExtra("remarks",payment_planitem.remarks)
                    intent.putExtra("created_at",payment_planitem.created_at)

                    context!!.startActivity(intent)
                })
                payment_plan_status.visibility = View.VISIBLE
                issue_notice_button.visibility = View.VISIBLE
            }
            else if(payment_planitem.payment_status_id ==4){

                    payment_plan_date.visibility = View.GONE
                    payment_plan_date_value.visibility = View.GONE
                    payment_plan_date_icon.visibility = View.GONE
                    issue_notice_button.visibility = View.GONE
                    pay_receipt_button_title.visibility = View.VISIBLE
                    view_payment_record_buttonView.visibility = View.VISIBLE

            }
            else if(payment_planitem.payment_status_id ==2) {
                when (payment_planitem.status_id) {

                    1 -> {
                        payment_plan_status.visibility = View.VISIBLE
                        separator.visibility = View.GONE
                        issue_notice_button.visibility = View.GONE

                    }
                    3 -> {
                        payment_plan_date.visibility = View.GONE
                        payment_plan_date_value.visibility = View.GONE
                        payment_plan_date_icon.visibility = View.GONE
                        pay_receipt_button_title.visibility = View.VISIBLE
                        view_payment_record_buttonView.visibility = View.VISIBLE
                    }

                    4 -> {
                        payment_plan_status.visibility = View.VISIBLE
                        separator.visibility = View.GONE
                        issue_notice_button.visibility = View.GONE
                    }
                    else -> {
                        payment_plan_status.visibility = View.GONE
                        separator.visibility = View.GONE
                        issue_notice_button.visibility = View.GONE
                    }
                }
            }
            else{
                payment_plan_status.visibility = View.GONE
                separator.visibility = View.GONE
                payment_notice_button_title.visibility = View.GONE
            }

            //!!!
            if(payment_planitem.phase == 1 || payment_planitem.phase == 2|| payment_planitem.phase == 3|| payment_planitem.phase == 4) {
                edit_icon_button_view.visibility = View.GONE

            }//!!!

            val bundle3 = Bundle()

            bundle3.putInt("project_id", payment_planitem.project_id)
            bundle3.putInt("plan_id", payment_planitem.id)
            bundle3.putInt("notice_id", payment_planitem.notice_id!!)
            bundle3.putBoolean("is_final", payment_planitem.is_final!!)
            bundle3.putString("owner_name", payment_planitem.owner_name!!)
            bundle3.putString("owner_email", payment_planitem.owner_email!!)
            bundle3.putString("company_address", payment_planitem.company_address!!)
            bundle3.putString("company_name_en", payment_planitem.company_name_en!!)
            bundle3.putString("company_name_zh", payment_planitem.company_name_zh!!)

            val PaymentNoticeFragment = PaymentNoticeFragment()
            PaymentNoticeFragment.setArguments(bundle3)


            val bundle4 = Bundle()

            bundle4.putInt("project_id", payment_planitem.project_id)
            bundle4.putInt("notice_id", payment_planitem.notice_id!!)
            bundle4.putInt("plan_id", payment_planitem.id)
            val PaymentReceiptFragment = PaymentReceiptFragment()
            PaymentReceiptFragment.setArguments(bundle4)

            issue_notice_button.setOnClickListener(View.OnClickListener {
                val intent = Intent((parentFragment.activity as MainActivity?), PaymentNoticeActivity::class.java)

                intent.putExtra("project_id", payment_planitem.project_id)
                intent.putExtra("plan_id", payment_planitem.id)
                intent.putExtra("notice_id", payment_planitem.notice_id!!)
                intent.putExtra("is_final", payment_planitem.is_final!!)
                intent.putExtra("owner_name", payment_planitem.owner_name!!)
                intent.putExtra("owner_email", payment_planitem.owner_email!!)
                intent.putExtra("company_address", payment_planitem.company_address!!)
                intent.putExtra("company_name_en", payment_planitem.company_name_en!!)
                intent.putExtra("company_name_zh", payment_planitem.company_name_zh!!)
                intent.putExtra("due_date", payment_planitem.created_at!!)
                context!!.startActivity(intent)

            })
//            payment_notice_button_title.setOnClickListener(View.OnClickListener {
//                val activity = parentFragment.activity as MainActivity
//                var fr = activity.supportFragmentManager?.beginTransaction()
//                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//                if (payment_planitem.is_final)
////                    showDialog(activity, fr, PaymentNoticeFragment)
//                    Log.d("is_final","is_final")
//                else {
////                    fr?.addToBackStack(null)
////                    fr?.add(R.id.nav_host_fragment, PaymentNoticeFragment)
////                    fr?.commit()
//                }
//            })


            view_payment_record_button.setOnClickListener(View.OnClickListener {
                val activity = parentFragment.activity as MainActivity
                var fr = activity.supportFragmentManager?.beginTransaction()
                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())

                fr?.addToBackStack(null)
                fr?.add(R.id.nav_host_fragment, PaymentReceiptFragment)
                fr?.commit()
            })

            pay_receipt_button_title.setOnClickListener(View.OnClickListener {
                val activity = parentFragment.activity as MainActivity
                var fr = activity.supportFragmentManager?.beginTransaction()
                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())

                fr?.addToBackStack(null)
                fr?.add(R.id.nav_host_fragment, PaymentReceiptFragment)
                fr?.commit()
            })

            if (payment_planitem.is_final) {
                payment_plan_title.setText(
                    HtmlCompat.fromHtml(
                        "<a><font color=#2D3844>" + payment_planitem.title
                                + "</font></a>" + "  " + "<a><font color=#DF431E>" + context!!.getString(
                            R.string.PaymentItem_final
                        ) + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY
                    ), TextView.BufferType.SPANNABLE
                )
            }
            else payment_plan_title.setText(payment_planitem.title)
            payment_plan_date_value.setText(payment_planitem.created_at)
            try {
                val number: String = payment_planitem.amount
                val amount = number.toDouble()
                val formatter = DecimalFormat("#,###.##")
                var formatted = formatter.format(amount)
                if (formatted.contains(".00")) formatted =
                    formatted.substring(0, formatted.length - 3)
                payment_plan_amount_value.text = "HK $"+formatted
            } catch (e: Exception) {
                e.printStackTrace()
                payment_plan_amount_value.setText("HK $" + payment_planitem.amount)
            }


            payment_plan_status.setText(payment_planitem.status)
            val drawable = payment_plan_status.getBackground() as GradientDrawable
            drawable.setColor(payment_planitem.status_rgb)
        }
    }


//        payment_notice_button_title.setPaintFlags(payment_notice_button_title.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
//        pay_receipt_button_title.setPaintFlags(pay_receipt_button_title.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
//
//        if(thisFragment.get_contain_retention()){
//            if(!mPaymentPlans.get(position).is_retention) {
    //paymentplan
//                Log.d("payment_planitem.remarks", payment_planitem.remarks.toString())
//                if(payment_planitem.remarks.equals(""))
//                    payment_plan_remarks.visibility=View.GONE
//                else
//                    payment_plan_remarks.text= payment_planitem.remarks
//
//                if(payment_planitem.payment_status_id ==2) {
//                    when (payment_planitem.status_id) {
//
//                        1 -> {
//
//                            payment_plan_status.visibility = View.GONE
//                            payment_notice_button_title.visibility = View.VISIBLE
//
//                        }
//                        3 -> {
//                            payment_plan_date.visibility = View.GONE
//                            payment_plan_date_value.visibility = View.GONE
//                            payment_plan_date_icon.visibility = View.GONE
//                            pay_receipt_button_title.visibility = View.VISIBLE
//                        }
//
//                        else -> {
//                            payment_plan_status.visibility = View.GONE
//                            separator.visibility = View.GONE
//                        }
//                    }
//                }
//                else{
//                    payment_plan_status.visibility = View.GONE
//                    separator.visibility = View.GONE
//                }
//            }
//            else{
//                //retention
//                when (payment_planitem.status_id) {
//
//                    1 -> {
//
//                        payment_notice_button_title.visibility = View.VISIBLE
//                    }
//
//                    2 -> {
//                        payment_plan_status.visibility = View.VISIBLE
//                        separator.visibility = View.GONE
//                        payment_plan_date.visibility = View.GONE
//                        payment_plan_date_value.visibility = View.GONE
//                        payment_plan_date_icon.visibility = View.GONE
//                    }
//
//                    3 -> {
//                        payment_plan_status.visibility = View.VISIBLE
//                        separator.visibility = View.VISIBLE
//                        payment_plan_date.visibility = View.GONE
//                        payment_plan_date_value.visibility = View.GONE
//                        payment_plan_date_icon.visibility = View.GONE
//                        pay_receipt_button_title.visibility = View.VISIBLE
//
//                    }
//                    else ->{
//                        payment_plan_date.visibility = View.GONE
//                        payment_plan_date_value.visibility = View.GONE
//                        payment_plan_date_icon.visibility = View.GONE
//                        pay_receipt_button_title.visibility = View.VISIBLE
//
//                    }
//            }
//        }
//        }
//        else{
//            Log.d("payment_planitem.remarks", payment_planitem.remarks.toString())
//            if(payment_planitem.remarks.equals(""))
//                payment_plan_remarks.visibility=View.GONE
//            else
//                payment_plan_remarks.text= payment_planitem.remarks
//
//            if(payment_planitem.payment_status_id ==2) {
//                when (payment_planitem.status_id) {
//
//                    1 -> {
//
//                        payment_plan_status.visibility = View.GONE
//                        payment_notice_button_title.visibility = View.VISIBLE
//
//                    }
//                    3 -> {
//                        payment_plan_date.visibility = View.GONE
//                        payment_plan_date_value.visibility = View.GONE
//                        payment_plan_date_icon.visibility = View.GONE
//                        pay_receipt_button_title.visibility = View.VISIBLE
//                    }
//
//                    else -> {
//                        payment_plan_status.visibility = View.GONE
//                        separator.visibility = View.GONE
//                    }
//                }
//            }
//            else{
//                payment_plan_status.visibility = View.GONE
//                separator.visibility = View.GONE
//            }
//        }
//        val bundle1 = Bundle()
//
//        bundle1.putInt("project_id", payment_planitem.project_id)
//        bundle1.putInt("retention_id", payment_planitem.id)
//        bundle1.putString("amount", payment_planitem.amount)
//        bundle1.putString("company_name_en", payment_planitem.company_name_en)
//        bundle1.putString("company_name_zh", payment_planitem.company_name_zh)
//        val DepositNoticeFragment= DepositNoticeFragment()
//        DepositNoticeFragment.setArguments(bundle1)
//
//
//        val bundle2 = Bundle()
//
//        bundle2.putInt("project_id", payment_planitem.project_id)
//        val DepositReceiptFragment= DepositReceiptFragment()
//        DepositReceiptFragment.setArguments(bundle2)

    //Retention Item
//        if(thisFragment.get_contain_retention()) {
//            if (mPaymentPlans.get(position).is_retention) {
//                Log.d(
//                    "is_retention",
//                    mPaymentPlans.get(position).is_retention.toString() + "   " + position.toString()
//                )
//                payment_plan_view.setBackgroundResource(R.drawable.project_retention_payment_plan)
//                payment_notice_button.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, DepositNoticeFragment)
//                    fr?.commit()
//                })
//                payment_notice_button_title.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, DepositNoticeFragment)
//                    fr?.commit()
//                })
//                pay_receipt_button.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, DepositReceiptFragment)
//                    fr?.commit()
//                })
//                pay_receipt_button_title.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, DepositReceiptFragment)
//                    fr?.commit()
//                })
    //           }
    //Payment Plan Item
//            else {
//
//                val bundle3 = Bundle()
//
//                bundle3.putInt("project_id", payment_planitem.project_id)
//                bundle3.putInt("plan_id", payment_planitem.id)
//                bundle3.putInt("notice_id", payment_planitem.notice_id!!)
//                bundle3.putBoolean("is_final", payment_planitem.is_final!!)
//                val PaymentNoticeFragment = PaymentNoticeFragment()
//                PaymentNoticeFragment.setArguments(bundle3)
//
//                val bundle4 = Bundle()
//
//                bundle4.putInt("project_id", payment_planitem.project_id)
//                bundle4.putInt("notice_id", payment_planitem.notice_id!!)
//                bundle4.putInt("plan_id", payment_planitem.id)
//                val PaymentReceiptFragment = PaymentReceiptFragment()
//                PaymentReceiptFragment.setArguments(bundle4)
//
//                payment_notice_button.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, PaymentNoticeFragment)
//                    fr?.commit()
//                })
//                payment_notice_button_title.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, PaymentNoticeFragment)
//                    fr?.commit()
//                })
//                pay_receipt_button.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, PaymentReceiptFragment)
//                    fr?.commit()
//                })
//
//                pay_receipt_button_title.setOnClickListener(View.OnClickListener {
//                    val activity = parentFragment.activity as MainActivity
//                    var fr = activity.supportFragmentManager?.beginTransaction()
//                    //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                    fr?.addToBackStack(null)
//                    fr?.add(R.id.nav_host_fragment, PaymentReceiptFragment)
//                    fr?.commit()
//                })
//
//            }
//        }
//        else{
//            val bundle3 = Bundle()
//
//            bundle3.putInt("project_id", payment_planitem.project_id)
//            bundle3.putInt("plan_id", payment_planitem.id)
//            bundle3.putInt("notice_id", payment_planitem.notice_id!!)
//            bundle3.putBoolean("is_final", payment_planitem.is_final!!)
//            val PaymentNoticeFragment = PaymentNoticeFragment()
//            PaymentNoticeFragment.setArguments(bundle3)
//
//            val bundle4 = Bundle()
//
//            bundle4.putInt("project_id", payment_planitem.project_id)
//            bundle4.putInt("notice_id", payment_planitem.notice_id!!)
//            bundle4.putInt("plan_id", payment_planitem.id)
//            val PaymentReceiptFragment = PaymentReceiptFragment()
//            PaymentReceiptFragment.setArguments(bundle4)
//
//            payment_notice_button.setOnClickListener(View.OnClickListener {
//                val activity = parentFragment.activity as MainActivity
//                var fr = activity.supportFragmentManager?.beginTransaction()
//                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                fr?.addToBackStack(null)
//                fr?.add(R.id.nav_host_fragment, PaymentNoticeFragment)
//                fr?.commit()
//            })
//            payment_notice_button_title.setOnClickListener(View.OnClickListener {
//                val activity = parentFragment.activity as MainActivity
//                var fr = activity.supportFragmentManager?.beginTransaction()
//                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                fr?.addToBackStack(null)
//                fr?.add(R.id.nav_host_fragment, PaymentNoticeFragment)
//                fr?.commit()
//            })
//            pay_receipt_button.setOnClickListener(View.OnClickListener {
//                val activity = parentFragment.activity as MainActivity
//                var fr = activity.supportFragmentManager?.beginTransaction()
//                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                fr?.addToBackStack(null)
//                fr?.add(R.id.nav_host_fragment, PaymentReceiptFragment)
//                fr?.commit()
//            })
//
//            pay_receipt_button_title.setOnClickListener(View.OnClickListener {
//                val activity = parentFragment.activity as MainActivity
//                var fr = activity.supportFragmentManager?.beginTransaction()
//                //fr?.replace(R.id.nav_host_fragment, QuotationDetailFragment())
//
//                fr?.addToBackStack(null)
//                fr?.add(R.id.nav_host_fragment, PaymentReceiptFragment)
//                fr?.commit()
//            })
//
//        }
//        payment_plan_title.setText(payment_planitem.title)
//        payment_plan_date_value.setText(payment_planitem.created_at)
//        if(position==0) {payment_plan_date_value.setText(context!!.getString(R.string.PaymentItem_immediate))}
//        payment_plan_amount_value.setText(
//            "HK$ ".plus(
//                NumberFormat.getNumberInstance(Locale.US).format(
//                    payment_planitem.amount.toInt()
//                )
//            )
//        )
//
//        payment_plan_status.setText(payment_planitem.status)
//        val drawable = payment_plan_status.getBackground() as GradientDrawable
//        drawable.setColor(payment_planitem.status_rgb)



    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mPaymentPlans.size
    }

//    private fun showDialog(
//        activity: Activity,
//        fr: FragmentTransaction,
//        PaymentNoticeFragment: Fragment
//    ) {
//        val dialog = Dialog(context!!)
//        val pn_final_warning= activity.getString(R.string.pn_final_warning)
//        val pn_final_warning_1= activity.getString(R.string.pn_final_warning_1)
//        val pn_final_warning_2= activity.getString(R.string.pn_final_warning_2)
//        val pn_final_warning_3= activity.getString(R.string.pn_final_warning_3)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog.setCanceledOnTouchOutside(false)
//        dialog.setContentView(R.layout.dialog_final_payment)
//
//        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
//        dialog_message.setMovementMethod(LinkMovementMethod.getInstance());
////        dialog_message.setText(
////            HtmlCompat.fromHtml(
////                pn_final_warning_1 + "\n\n" + "<a href=\"https://uat.jper.com.hk/help\"><font color=\"#8FAD7B\">" + pn_final_warning_2
////                        + "</font></a>" + pn_final_warning_3, HtmlCompat.FROM_HTML_MODE_LEGACY
////            ), TextView.BufferType.SPANNABLE
////        )
//
//        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button
//
//        val close_button = dialog.findViewById(R.id.close_button) as ImageView
//        confirm_button.setOnClickListener {
//            dialog.dismiss()
//
//            fr?.addToBackStack(null)
//            fr?.add(R.id.nav_host_fragment, PaymentNoticeFragment)
//            fr?.commit()
//        }
//        close_button.setOnClickListener {
//            dialog.dismiss()
//        }
//
//        dialog.setCancelable(true)
//        dialog.show()
//
//    }
}