package com.jpertechnologyltd.jperprovider.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.QuotationItem


class QuoteQuoteQuotationItemAdapter (private val context: Context, private val mQuotationItem: List<QuotationItem>) : RecyclerView.Adapter<QuoteQuoteQuotationItemAdapter.ViewHolder>()
{
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row

        val quotation_item_title = itemView.findViewById<TextView>(R.id.quotation_item_title)
        val quotation_item_min = itemView.findViewById<TextView>(R.id.quotation_item_min)
        val quotation_item_max = itemView.findViewById<TextView>(R.id.quotation_item_max)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_quote_quotation_item,parent,false))
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val quotation: QuotationItem = mQuotationItem.get(position)
        // Set item views based on your views and data model
        val quotation_item_title = viewHolder.quotation_item_title
        val quotation_item_min = viewHolder.quotation_item_min
        val quotation_item_max = viewHolder.quotation_item_max


        quotation_item_title.setText(quotation.name)
        quotation_item_min.setText(quotation.min)
        quotation_item_max.setText(quotation.max)



//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mQuotationItem.size
    }
}