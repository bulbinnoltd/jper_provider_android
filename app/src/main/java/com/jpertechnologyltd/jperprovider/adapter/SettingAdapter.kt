package com.jpertechnologyltd.jperprovider.adapter

//import com.jpertechnologyltd.jperprovider.EditProfileActivity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.*
import com.jpertechnologyltd.jperprovider.item.Setting
import com.jpertechnologyltd.jperprovider.ui.myproject.MyProjectsQuotedDetailFragment
import com.jpertechnologyltd.jperprovider.ui.myproject.ProfileDisbursementFragment
import com.jpertechnologyltd.jperprovider.ui.profile.ProfileFragment
import com.jpertechnologyltd.jperprovider.ui.profile.UpdatePasswordActivity


class SettingAdapter (private val mSettings: List<Setting>, private val fragment: Fragment) : RecyclerView.Adapter<SettingAdapter.ViewHolder>()
{
    var context: Context? = null
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val setting_title = itemView.findViewById<TextView>(R.id.setting_title)
        val setting_icon = itemView.findViewById<ImageView>(R.id.setting_icon)
        val setting_layout = itemView.findViewById<ConstraintLayout>(R.id.setting_layout)

    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val blogView = inflater.inflate(R.layout.item_setting, parent, false)
        // Return a new holder instance
        return ViewHolder(blogView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val setting: Setting = mSettings.get(position)
        // Set item views based on your views and data model
        val setting_title = viewHolder.setting_title
        val setting_icon = viewHolder.setting_icon
        val setting_layout = viewHolder.setting_layout
        if(setting.icon.equals("profile_edit_icon")) {
            setting_layout.setOnClickListener(View.OnClickListener {

                val profile = (fragment as ProfileFragment).getProfie()

                val intent = Intent(context, UpdateProfileActivity::class.java)
                intent.putExtra("icon", profile?.icon)
                intent.putExtra("phone", profile?.phone)
                intent.putExtra("username", profile?.username)

                context?.startActivity(intent)
            })
        }
        else if(setting.icon.equals("profile_change_pw_icon")) {
            setting_layout.setOnClickListener(View.OnClickListener {


                context?.startActivity(Intent(context, UpdatePasswordActivity::class.java))
            })
        }

        else if(setting.icon.equals("profile_lang_icon")) {
            setting_layout.setOnClickListener(View.OnClickListener {


                context?.startActivity(Intent(context, UpdateLangActivity::class.java))
            })
        }
        else if(setting.icon.equals("notification_icon")) {
            setting_layout.setOnClickListener(View.OnClickListener {


                context?.startActivity(Intent(context, UpdateLangActivity::class.java))
            })
        }
        else if(setting.icon.equals("navbar_activity")) {
            setting_layout.setOnClickListener(View.OnClickListener { arg0 ->
                context?.startActivity(Intent(context, UpdatePasswordActivity::class.java))

            })
        }//!!!
        else if(setting.icon.equals("profile_disbursement_icon")) {
            setting_layout.setOnClickListener(View.OnClickListener { arg0 ->

                var fr =(fragment.activity as MainActivity?)?.supportFragmentManager?.beginTransaction()
                fr?.addToBackStack(null)
                fr?.add(R.id.nav_host_fragment, ProfileDisbursementFragment())
                fr?.commit()

            })
        }//!!!
        else {
            setting_layout.setOnClickListener(View.OnClickListener { arg0 ->
                val activity = arg0.getContext() as AppCompatActivity

                val intent = Intent(activity, WebviewActivity::class.java)
                intent.putExtra("url", setting.link)
                Log.d("url", setting.link)
                context!!.startActivity(intent)
            })
        }
        setting_title.setText(setting.title)
        val res: Int =
            context?.getResources()!!.getIdentifier(
                setting.icon,
                "drawable",
                context!!.getPackageName()
            )
        setting_icon.setImageResource(res)

//        val button = viewHolder.messageButton
//        button.text = if (contact.isOnline) "Message" else "Offline"
//        button.isEnabled = contact.isOnline
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mSettings.size
    }
}