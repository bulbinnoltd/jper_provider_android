package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonObject
import com.koushikdutta.ion.Ion
import java.text.DecimalFormat
import java.util.*


class EditContractItemActivity : AppCompatActivity() {

    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    var project_id:Int?=null
    var status_id:Int?=null
    var contract_id:Int?=null
    var title=""
    var amount=""
    var remarks=""
    var created_at=""
    var u_price=""
    var qty=""
    var selected_status = 0
    private var lang : String? = "en"
    private var header : String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_contract_item)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        val contact_status_list = listOf(
            getString(R.string.contract_item_status1),
//            getString(R.string.contract_item_status2),
            getString(R.string.contract_item_status3),
            getString(R.string.contract_item_status4)
        )

        try {
            project_id = intent.getIntExtra("project_id", 0)
            status_id = intent.getIntExtra("status_id", 0)
            selected_status = intent.getIntExtra("status_id", 0)
            title = intent.getStringExtra("title")
            amount = intent.getStringExtra("amount")
            remarks = intent.getStringExtra("remark")
            created_at = intent.getStringExtra("complete_date")
            contract_id = intent.getIntExtra("contract_id", 0)
            u_price = intent.getStringExtra("u_price")
            qty = intent.getStringExtra("qty")


            Log.d("project_id_get", "ok" + project_id + "  " + status_id + "   " + contract_id)
        } catch (e: java.lang.Exception) {
            Log.d("project_id_get", e.toString())
        }
        val back_button = findViewById(R.id.back_button) as ImageView
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
//        style_listview.adapter = style_adapter
//        style_listview.layoutManager = LinearLayoutManager (this, RecyclerView.HORIZONTAL,false)
//        // set on-click listener
        back_button.setOnClickListener {
            super.onBackPressed()
        }

        val edit_contract_item_title =
            findViewById<TextInputEditText>(R.id.edit_contract_item_title)
        val edit_contract_item_unit_price =
            findViewById<TextInputEditText>(R.id.edit_contract_item_unit_price)
        val edit_contract_item_qty = findViewById<TextInputEditText>(R.id.edit_contract_item_qty)
        val edit_contract_item_amount = findViewById<TextView>(R.id.edit_contract_item_amount)
        val edit_contract_completion_date =
            findViewById<TextInputEditText>(R.id.edit_contract_completion_date)
        val edit_contract_remarks = findViewById<TextInputEditText>(R.id.edit_contract_remarks)
        val edit_contract_item_status =
                findViewById<AutoCompleteTextView>(R.id.edit_contract_item_status)
        val edit_contract_save_button = findViewById<Button>(R.id.edit_contract_save_button)
        val edit_contract_delete_button = findViewById<Button>(R.id.edit_contract_delete_button)

        val adapter_gender = ArrayAdapter(this, R.layout.item_text_option, contact_status_list)
        val available_status: IntArray = intArrayOf(1, 3, 4)

        edit_contract_item_status?.setAdapter(adapter_gender)
        edit_contract_item_status.setText(
            contact_status_list.get(available_status.indexOf(status_id!!)),
            false
        )


        edit_contract_item_title.setText(title)
        edit_contract_item_unit_price.setText(u_price.toFloat().toInt().toString())
        edit_contract_item_qty.setText(qty)
        edit_contract_item_amount.setText(amount.toFloat().toInt().toString())
        if(!created_at.equals("N/A"))edit_contract_completion_date.setText(created_at)
        edit_contract_remarks.setText(remarks)

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)


        if(status_id!=1)
            edit_contract_delete_button.visibility=View.GONE

        edit_contract_item_status.setOnItemClickListener { parent, view, position, id ->
            Log.d("position", position.toString())
//            selected_status = position+1
            selected_status = available_status[position]
            Log.d("selected_status", selected_status.toString())
        }
        edit_contract_item_qty.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                s!!
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s!!
                try {
                    edit_contract_item_amount.text =
                        (s.toString().toDouble()
                            .toInt() * edit_contract_item_unit_price.text.toString()
                            .toDouble().toInt()).toString()
                } catch (e: Exception) {
                    edit_contract_item_amount.text = "0.0"
                    Log.d("text_changed", "failed")
                }
            }

            override fun afterTextChanged(s: Editable?) {
                s!!
            }
        })
        edit_contract_item_unit_price.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                s!!
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s!!
                try {
                    edit_contract_item_amount.text =
                        (s.toString().toDouble().toInt() * edit_contract_item_qty.text.toString()
                            .toDouble().toInt()).toString()


                } catch (e: Exception) {
                    edit_contract_item_amount.text = "0.0"
                    Log.d("text_changed", e.toString())
                }
            }

            override fun afterTextChanged(s: Editable?) {
                s!!

            }
        })
        edit_contract_completion_date.setOnClickListener {
            val dpd = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    edit_contract_completion_date.setText("" + mYear + "-" + (mMonth + 1) + "-" + mDay)
                }, year, month, day
            )
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            if (lang.equals("en")) {
                dpd.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.confirm_button),
                    dpd
                )
                dpd.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.cancle_button),
                    dpd
                )
            } else {
                dpd.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.confirm_button),
                    dpd
                )
                dpd.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.cancle_button),
                    dpd
                )
            }
            dpd.show()
        }

        var contract_title: String? = null
        var contract_status_id: Int? = null
        var contract_unit_price: String? = null
        var contract_qty: String? = null
        var contract_amount: String? = null
        var contract_completion_date: String? = null
        var contract_remarks: String? = null

        edit_contract_save_button.setOnClickListener {

            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE

            try {
                contract_title = edit_contract_item_title.text.toString()
            } catch (x: Exception) {
            }
            try {
                contract_status_id = selected_status
            } catch (x: Exception) {
                contract_status_id =status_id
            }
            try {
                contract_unit_price = edit_contract_item_unit_price.text.toString()
            } catch (x: Exception) {
            }
            try {
                contract_qty = edit_contract_item_qty.text.toString()
            } catch (x: Exception) {
            }
            try {
                contract_amount = edit_contract_item_amount.text.toString()
            } catch (x: Exception) {
            }
            try {
                contract_completion_date = edit_contract_completion_date.text.toString()

            } catch (x: Exception) {
            }
            try {
                contract_remarks = edit_contract_remarks.text.toString()
            } catch (x: Exception) {
            }

            if (contract_title.equals("") || contract_unit_price.equals("") || contract_qty.equals("") || contract_amount.equals("")
            ) {
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
                showDialog(this, getString(R.string.Without_info))
            } else {
                Thread(Runnable {

                    try {
                        val json = JsonObject()
                        json.addProperty("project_id", project_id)
                        json.addProperty("id", contract_id)
                        json.addProperty("status_id", contract_status_id)
                        json.addProperty("item", contract_title)
                        json.addProperty("u_price", contract_unit_price!!.toFloat())
                        json.addProperty("quantity", contract_qty!!.toFloat())
                        json.addProperty("complete_date", contract_completion_date!!)
                        json.addProperty("remark", contract_remarks)

                        Ion.with(baseContext)
                            .load("https://uat.jper.com.hk/api/company/project/budget/task/update")
                            .setHeader("Authorization", "Bearer " + header)
                            .setHeader("Content-Language", lang)
                            .setJsonObjectBody(json)
                            .asJsonObject()
                            .setCallback { e, result ->
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE
                                if (e != null)
                                    Log.d("result_error", e.toString())
                                if (result["result"].asString.equals("success")) {

                                    Log.d("result_updated", "result_updated")
                                    Log.d(
                                        "result_contract_status_id",
                                        contract_status_id.toString()
                                    )

                                    val dialog = Dialog(this)
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                    dialog.setCanceledOnTouchOutside(false)
                                    dialog.setContentView(R.layout.dialog_message)

                                    val dialog_message =
                                        dialog.findViewById(R.id.dialog_message) as TextView
                                    dialog_message.text = getString(R.string.Success_msg)
                                    val confirm_button =
                                        dialog.findViewById(R.id.confirm_button) as Button

                                    val close_button =
                                        dialog.findViewById(R.id.close_button) as ImageView
                                    close_button.visibility = View.INVISIBLE
                                    confirm_button.setOnClickListener {
                                        dialog.dismiss()
                                        val editor = getSharedPreferences(
                                            "MyPreferences",
                                            Context.MODE_PRIVATE
                                        ).edit()
                                        editor.putBoolean("refresh_contract", true)
                                        editor.apply()
                                        finish()
                                    }
                                    close_button.setOnClickListener {
                                        dialog.dismiss()
                                    }

                                    dialog.setCancelable(true)
                                    dialog.show()


                                } else if (result["result"].asString.equals("error")) {

                                    showDialog(this, result["msg"].asString)
                                }
                                Log.d("result_okok", result.toString())
                            }
                    } catch (x: Exception) {
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        Log.d("result_error", x.toString())
                    }

                }).start()
            }
        }
        edit_contract_delete_button.setOnClickListener {

            val dialog = Dialog(this)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.setCancelable(false)
                dialog.setContentView(R.layout.dialog_confirm)

                val dialog_message = dialog.findViewById(R.id.message) as TextView
                dialog_message.text = getString(R.string.dialog_delete_info)
                val confirm_button = dialog.findViewById(R.id.save_button) as Button

                val close_button = dialog.findViewById(R.id.close_button) as ImageView
                val cancel_button = dialog.findViewById(R.id.cancel_button) as Button
                cancel_button.setOnClickListener{
                    dialog.dismiss()
                }
                close_button.visibility = View.INVISIBLE
                confirm_button.setOnClickListener {
                    dialog.dismiss()

                    var delete_result :String?= null


                    Thread(Runnable {

                        try {
                            val json = JsonObject()
                            json.addProperty("project_id", project_id)
                            json.addProperty("id", contract_id)

                            Ion.with(this)
                                .load("https://uat.jper.com.hk/api/company/project/budget/task/remove")
                                .setHeader("Authorization", "Bearer " + header)
                                .setHeader("Content-Language", lang)
                                .setJsonObjectBody(json)
                                .asJsonObject()
                                .setCallback { e, result ->
                                    Loading!!.visibility = View.GONE
                                    progressOverlay!!.visibility = View.GONE
                                    if (e != null)
                                        Log.d("result_error", e.toString())
                                    if (result["result"].asString.equals("success")) {

                                        Log.d("result_updated1", "result_updated1")


                                        delete_result = result["msg"].asString
                                        runOnUiThread(java.lang.Runnable {
                                            if (delete_result == null) {

                                            } else {

                                                dialog.dismiss()
                                                val editor = getSharedPreferences(
                                                    "MyPreferences",
                                                    Context.MODE_PRIVATE
                                                ).edit()
                                                editor.putBoolean("refresh_contract", true)
                                                editor.apply()
                                                finish()
                                                Log.d("result_updated3", "res3ult_updated")

                                            }
                                        })
                                    } else if (result["result"].asString.equals("error")) {

                                        Log.d("result_updated2", "result_updated2")
                                        runOnUiThread(java.lang.Runnable {


                                            dialog.dismiss()
                                            showDialog(this, result["msg"].asString)

                                        })
                                    }
                                    Log.d("result_okok", result.toString())
                                }
                        } catch (x: Exception) {
                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE
                            Log.d("result_error", x.toString())
                        }

                    }).start()
        }
            dialog.setCancelable(true)
            dialog.show()
        }
    }
    private fun showDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
}