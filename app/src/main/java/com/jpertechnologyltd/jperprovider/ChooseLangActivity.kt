package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_choose_lang.*
import java.util.*


class ChooseLangActivity : AppCompatActivity() {
    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    private var header : String? = null
    private var first_time : Boolean = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_lang)
        // Create a new Locale object

        val editor = this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()

        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        first_time = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            .getBoolean("first_time",true)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        val en_button = findViewById<Button>(R.id.en_button)
        // set on-click listener
        en_button.setOnClickListener {

            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE
//            Loading!!.visibility = View.GONE
//            progressOverlay!!.visibility = View.GONE
            en_button.isClickable =false
            zh_button.isClickable=false
            var changelang = false
            Thread(Runnable {
                val languageCode = "en"
                val config = resources.configuration
                val locale = Locale(languageCode)

                editor.putString("lang", "en")
                editor.apply()

                Locale.setDefault(locale)
                config.locale = locale
                resources.updateConfiguration(config, resources.displayMetrics)
//
//                if (first_time) {
//                    changelang = true
//                    val intent = Intent(this, OnBoardingActivity::class.java)
//                    startActivity(intent)
//                } else {
                    changelang = true
                    val intent = Intent(this, FirstPageActivity::class.java)
                    startActivity(intent)

//                }

                this.runOnUiThread(java.lang.Runnable {
                    if(changelang){
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                    }
                })
            }).start()
        }

        val zh_button = findViewById<Button>(R.id.zh_button)

        // set on-click listener
        zh_button.setOnClickListener {
            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE
            en_button.isClickable =false
            zh_button.isClickable=false
            var changelang = false
            Thread(Runnable {
                val languageCode = "zh"
                val config = resources.configuration
                val locale = Locale(languageCode)

                editor.putString("lang", "zh")
                editor.apply()

                Locale.setDefault(locale)
                config.locale = locale
                resources.updateConfiguration(config, resources.displayMetrics)

//
//                if (first_time) {
//                    changelang = true
//                    val intent = Intent(this, OnBoardingActivity::class.java)
//                    startActivity(intent)
//                } else {
                    changelang = true
                    val intent = Intent(this, FirstPageActivity::class.java)
                    startActivity(intent)
//
//                }

                this.runOnUiThread(java.lang.Runnable {
                    if(changelang){
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                    }
                })
            }).start()
        }

    }


}
