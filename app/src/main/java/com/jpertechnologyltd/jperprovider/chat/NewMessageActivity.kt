package com.jpertechnologyltd.jperprovider.chat
//
//import android.content.Intent
//import android.os.Bundle
//import androidx.appcompat.app.AppCompatActivity
//import androidx.core.content.ContextCompat.startActivity
//import androidx.recyclerview.widget.LinearLayoutManager
//import com.google.firebase.database.DataSnapshot
//import com.google.firebase.database.FirebaseDatabase
//import com.google.firebase.database.ValueEventListener
//import com.jpertechnologyltd.jperprovider.R
//import com.jpertechnologyltd.jperprovider.`class`.User
//import com.squareup.picasso.Picasso
//import com.xwray.groupie.GroupAdapter
//import com.xwray.groupie.Item
//import com.xwray.groupie.ViewHolder
//import kotlinx.android.synthetic.main.activity_chat_new_message.*
//import kotlinx.android.synthetic.main.chat_user_row_new_message.view.*
//import androidx.navigation.ui.navigateUp
//
//class NewMessageActivity : AppCompatActivity() {
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_chat_new_message)
//
//        supportActionBar?.title = "Select User"
//
////        val adapter = GroupAdapter<ViewHolder>()
////        adapter.add(UserItem())
////        adapter.add(UserItem())
////        adapter.add(UserItem())
////
////        newMessage_listview.adapter = adapter
//        newMessage_listview.layoutManager = LinearLayoutManager(this)
//
//        fetchUsers()
//
//    }
//
//    companion object{
//        val USER_KEY = "USER_KEY"
//    }
//
//
//    private fun fetchUsers() {
//        val ref = FirebaseDatabase.getInstance().getReference("/users")
//        ref.addListenerForSingleValueEvent(object : ValueEventListener {
//            override fun onDataChange(p0: DataSnapshot) {
//                val adapter = GroupAdapter<ViewHolder>()
//                p0.children.forEach {
//                    val user = it.getValue(User::class.java)
//                    if (user != null) {
//                        adapter.add(UserItem(user))
//                    }
//                }
//
//                adapter.setOnItemClickListener { item, view ->
//                    val userItem = item as UserItem
//                    val intent = Intent(view.context, ChatLogActivity::class.java)
//                    //intent.putExtra(USER_KEY, userItem.user.username)
//                    intent.putExtra(USER_KEY, userItem.user)
//                    startActivity(intent)
//                    finish()
//                }
//                newMessage_listview.adapter = adapter
//            }
//        })
//    }
//}
//
//
//class UserItem(val user: User): Item<ViewHolder>(){
//    override fun bind(viewHolder: ViewHolder, position: Int) {
//        Picasso.get().load(user.profileImageUrl).into(viewHolder.itemView.chat_new_image)
//        viewHolder.itemView.chat_new_username.text = user.username
//    }
//    override fun getLayout(): Int {
//        return R.layout.chat_user_row_new_message
//    }
//}
