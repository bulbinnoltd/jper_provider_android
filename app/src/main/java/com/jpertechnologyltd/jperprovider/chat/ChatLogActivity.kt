package com.jpertechnologyltd.jperprovider.chat
//
//import android.os.Bundle
//import androidx.appcompat.app.AppCompatActivity
//import com.google.firebase.auth.FirebaseAuth
//import com.google.firebase.database.ChildEventListener
//import com.google.firebase.database.DataSnapshot
//import com.google.firebase.database.DatabaseError
//import com.google.firebase.database.FirebaseDatabase
//import com.jpertechnologyltd.jperprovider.R
//import com.jpertechnologyltd.jperprovider.`class`.ChatMessage
//import com.jpertechnologyltd.jperprovider.`class`.User
//import com.xwray.groupie.GroupAdapter
//import kotlinx.android.synthetic.main.activity_chat.*
//import kotlinx.android.synthetic.main.chat_oppositemessage.view.*
//
//class ChatLogActivity : AppCompatActivity() {
//    companion object{
//        val TAG = "ChatLog"
//    }
//
//    val adapter = GroupAdapter<ViewHolder>()
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_chat)
//
//        messages_view.adapter = adapter
//
//        //val username = intent.getStringExtra(NewMessageActivity.USER_KEY)
//        val user = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)
//        //setupDummyData()
//        listenForMessages()
//
//        messages_sent_button.setOnClickListener{
//            performSendMessage()
//        }
//    }
//
//    private fun listenForMessages(){
//        val fromId = FirebaseAuth.getInstance().uid
//        val toId = toUser?.uid
//        val ref = FirebaseDatabase.getInstance().getReference("/user-messages/$fromId/$toId")
//        ref.addChildEventListener(object: ChildEventListener {
//            override fun onChildAdded(p0: DataSnapshot, p1: String?){
//                val chatMessage = p0.getValue(ChatMessage::class.java)
//                if(chatMessage != null){
//
//                    if (chatMessage.fromId == FirebaseAuth.getInstance().uid){
//                        adapter.add(ChatFromItem(chatMessage.text))
//                    } else {
//                    adapter.add(ChatToItem(chatMessage.text))
//                    }
//                }
//
//                messages_view.scrollToPosition(adapter.itemCount - 1)
//            }
//            override fun onCancelled(p0: DatabaseError){
//
//            }
//            override fun onChildChanged(p0: DataSnapshot, p1: String?){
//
//            }
//            override fun onChildMoved(p0: DataSnapshot, p1: String?){
//
//            }
//            override fun onChildRemoved(p0: DataSnapshot){
//
//            }
//        })
//    }
//
//
//
//    private fun performSendMessage(){
//        val text = messages_edittextfield.text.toString()
//        val fromId = FirebaseAuth.getInstance().uid
//        val user = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)
//        val toId = user.uid
//
//        if (fromId == null) return
//
//        //val reference = FirebaseDatabase.getInstance().getReference("/messages").push()
//
//        val reference = FirebaseDatabase.getInstance().getReference("/user-messages/$fromId/$toId").push()
//
//        val toReference = FirebaseDatabase.getInstance().getReference("/user-messages/$toId/$fromId").push()
//
//        val chatMessage = ChatMessage(reference.key, text, fromId, toId, System.currentTimeMillis()/1000)
//        reference.setValue(chatMessage)
//            .addOnSuccessListener {
//                messages_edittextfield.text?.clear()
//                messages_view.scrollToPosition(adapter.itemCount-1)
//            }
//        toReference.setValue(chatMessage)
//
//        val latestMessageRef = FirebaseDatabase.getInstance().getReference("/latest-messages/$fromId/$toId")
//        latestMessageRef.setValue(chatMessage)
//        val latestMessageToRef = FirebaseDatabase.getInstance().getReference("/latest-messages/$toId/$fromId")
//        latestMessageToRef.setValue(chatMessage)
//
//    }
//    private fun setupDummyData(){
//        val adapter = GroupAapter<ViewHolder>()
//        adapter.add(ChatFromItem("eeee"))
//        adapter.add(ChatToItem("sds"))
//        messages_view.adapter = adapter
//    }
//}
//
//class ChatFromItem(val text: String): Item<ViewHolder>(){
//    override fun bind(viewHolder: ViewHolder, position: Int){
//        viewHolder.itemView.opposite_message_body = "From message....."
//
//    }
//    override fun getLayout(): Int{
//        return R.layout.chat_oppositemessage
//    }
//}
//class ChatToItem(val text: String): Item<ViewHolder>(){
//    override fun bind(viewHolder: ViewHolder, position: Int){
//        viewHolder.itemView.my_message_body = "To message....."
//
//    }
//    override fun getLayout(): Int{
//        return R.layout.chat_mymessage
//    }
//}