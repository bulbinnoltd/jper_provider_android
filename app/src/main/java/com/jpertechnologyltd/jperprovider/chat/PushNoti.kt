package com.jpertechnologyltd.jperprovider.chat

data class PushNoti(
    var to : String? = null,
    var notification : Notification = Notification()
){
    data class Notification(
        var body : String? = null,
        var title : String? = null
    )
}