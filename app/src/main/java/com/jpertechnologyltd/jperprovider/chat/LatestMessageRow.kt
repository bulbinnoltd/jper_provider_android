package com.jpertechnologyltd.jperprovider.chat
//
//import com.google.firebase.auth.FirebaseAuth
//import com.google.firebase.database.DataSnapshot
//import com.google.firebase.database.DatabaseError
//import com.google.firebase.database.FirebaseDatabase
//import com.google.firebase.database.ValueEventListener
//import com.jpertechnologyltd.jperprovider.R
//import com.jpertechnologyltd.jperprovider.`class`.ChatMessage
//import com.jpertechnologyltd.jperprovider.`class`.User
//import com.squareup.picasso.Picasso
//
//class LatestMessageRow(val chatMessage: ChatMessage): Item<ViewHolder>(){
//    var chatPartnerUser: User? = null
//
//    override fun bind(viewHolder: ViewHolder, position: Int){
//        viewHolder.itemView.chat_latest_message.text = chatMessage.text
//
//        val chatPartnerId: String
//        if(chatMessage.fromId == FirebaseAuth.getInstance().uid){
//            chatPartnerId = chatMessage.toId
//        } else {
//            chatPartnerId = chatMessage.fromId
//        }
//
//        val ref = FirebaseDatabase.getInstance().getReference("/users/$chatPartnerId")
//        ref.addListenerForSingleValueEvent(object : ValueEventListener {
//            override fun onDataChange(p0: DataSnapshot){
//                chatPartnerUser = p0.getValue(User::class.java)
//                viewHolder.itemView.chat_latest_username.text = chatPartnerUser?.username
//                val targetImageView = viewHolder.itemView.chat_latest_image
//                Picasso.get().load(chatPartnerUser?.profileImageUrl).into(targetImageView)
//            }
//            override fun onCancelled(p0: DatabaseError){
//
//            }
//        })
//
//    }
//
//    override fun getLayout(): Int{
//        return R.layout.chat_user_row_latest_message
//    }
//}