package com.jpertechnologyltd.jperprovider.chat
//
//import android.content.Intent
//import android.os.Bundle
//import android.view.Menu
//import android.view.MenuItem
//import androidx.appcompat.app.AppCompatActivity
//import androidx.recyclerview.widget.DividerItemDecoration
//import com.google.firebase.auth.FirebaseAuth
//import com.google.firebase.database.ChildEventListener
//import com.google.firebase.database.DataSnapshot
//import com.google.firebase.database.DatabaseError
//import com.google.firebase.database.FirebaseDatabase
//import com.jpertechnologyltd.jperprovider.R
//import com.jpertechnologyltd.jperprovider.RegisterActivity
//import com.jpertechnologyltd.jperprovider.`class`.ChatMessage
//import com.jpertechnologyltd.jperprovider.`class`.User
//import com.jpertechnologyltd.jperprovider.chat.NewMessageActivity.Companion.USER_KEY
//import com.squareup.picasso.Picasso
//import kotlinx.android.synthetic.main.activity_chat_latest_message.*
//
//
//class LatestMessagesActivity : AppCompatActivity() {
//
//    companion object{
//        var currentUser: User? = null
//        val TAG = "LatestMessages"
//    }
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_chat_latest_message)
//        latest_message_listview.adapter = adapter
//        latest_message_listview.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
//        //set item click listener on adapter
//        adapter.setOnItemClickListener{item, view ->
//
//            val intent = Intent(this, ChatLogActivity::class.java)
//            val row = item as LatestMessageRow
//            intent.putExtra(NewMessageActivity.USER_KEY, row.chatPartnerUser)
//            startActivity(intent)
//        }
//
//        //setupDummyRows()
//        listenForLatestMessages()
//
//        verifyUserIsLoggedIn()
//    }
//
//
//
//    val latestMessagesMap = HashMap<String, ChatMessage>()
//
//    fun refreshRecyclerViewMessages(){
//        adapter.clear()
//        latestMessagesMap.values.forEach{
//            adapter.add(LatestMessageRow(it))
//        }
//    }
//
//    private fun listenForLatestMessages(){
//        val formId = FirebaseAuth.getInstance().uid
//        val ref = FirebaseDatabase.getInstance().getReference("/latest-messages/$formId")
//        ref.addChildEventListener(object: ChildEventListener {
//            override fun onChildAdded(p0: DataSnapshot, p1: String?){
//                val chatMessage = p0.getValue(ChatMessage::class.java)
//                latestMessagesMap[p0.key] = chatMessage
//                val notiMessage = chatMessage?.text.toString()
//                val user = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)
//                val toId = user.uid
//                if (formId != null) {
//                    FcmPush.instance.sendMessage(FcmPush(),toId,"Message",notiMessage)
//                }
//                refreshRecyclerViewMessages()
//            }
//            override fun onChildChanged(p0: DataSnapshot, p1: String?){
//                val chatMessage = p0.getValue(ChatMessage::class.java)
//                latestMessagesMap[p0.key] = chatMessage
//                val notiMessage = chatMessage?.text.toString()
//                val user = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)
//                val toId = user.uid
//                if (formId != null) {
//                    FcmPush.instance.sendMessage(FcmPush(),toId,"Message",notiMessage)
//                }
//                refreshRecyclerViewMessages()
//            }
//            override fun onCancelled(p0: DatabaseError){
//
//            }
//            override fun onChildMoved(p0: DataSnapshot, p1: String?){
//
//            }
//            override fun onChildRemoved(p0: DataSnapshot){
//
//            }
//        })
//    }
//    val adapter = GroupAdapter<ViewHolder>()
////    private fun setupDummyRows(){
////        adapter.add(LatestMessageRow())
////    }
//
//    private fun verifyUserIsLoggedIn(){
//        val uid = FirebaseAuth.getInstance().uid
//        if(uid == null){
//            val intent = Intent(this, RegisterActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
//            startActivity(intent)
//        }
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        when (item?.itemId){
//            R.id.menu_new_message ->{
//            val intent = Intent(this, NewMessageActivity::class.java)
//                startActivity(intent)
//            }
//            R.id.menu_back ->{
//
//            }
//        }
//        return super.onOptionsItemSelected(item)
//    }
//
//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.chat_nav_menu, menu)
//        return super.onCreateOptionsMenu(menu)
//    }
//
//    override fun onStop() {
//        super.onStop()
//        FcmPush.instance.sendMessage(FcmPush(),"","hi","hi")
//    }
//}