package com.jpertechnologyltd.jperprovider.component

interface OnLoadMoreListener {
    fun onLoadMore()
}
