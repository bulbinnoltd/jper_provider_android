package com.jpertechnologyltd.jperprovider.component

import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.*
import com.jpertechnologyltd.jperprovider.item.chat.ChatRoom
import com.jpertechnologyltd.jperprovider.item.chat.ChatRoomList
import com.jpertechnologyltd.jperprovider.item.chat.ChatRoom_Provider
import com.jpertechnologyltd.jperprovider.item.myproject.*
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.builder.Builders
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

object HttpsService {

    fun GetUserProfile(
        context: Context?,
        API_url: String?,
        header: String?,
        lang: String?
    ): Profile?{
        var profile : Profile? =null
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("Profile",result.toString())

                val id =  result["id"].asInt
                val username =  result["username"].asString
                val email =  result["email"].asString
                val phone= if(result["phone"].isJsonNull)"" else result["phone"].asString
                val default_lang= if(result["default_lang"].isJsonNull)"" else result["default_lang"].asString
                val icon= if(result["icon"].isJsonNull)"" else result["icon"].asString

                profile = Profile(id, username,email,phone, default_lang,icon)

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
        }
        return profile
    }
    fun PostFCMtoken(
        context: Context?,
        API_url: String?,
        header: String?,
        token:String,
        lang: String?
    ): Boolean {
        val json = JsonObject()
        json.addProperty("token", token)

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " + header)
                .setHeader("Content-Language", lang)
                //    .setTimeout(5000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("result", result.toString())

                val success = result["result"].asString.equals("success")
                if (success) {
                    val device_id =
                        if (result.get("device_id").isJsonNull) "" else result.get("device_id").asString
                    val editor =
                        context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
                    editor.putString("device_id", device_id)
                    editor.apply()
                }
                return success

            } catch (e: Exception) {
                Log.d("error_jper", e.toString())

                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper", e.toString())
            return false
        }
    }
    fun GetAdvertisment(
    context: Context?,
    API_url: String?,
    header:String?,
    lang:String?

    ): ArrayList<Advertisment>? {
        val All_ad = ArrayList<Advertisment>()
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //   .setTimeout(5000)
                .asJsonArray()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("Advertisment_all",result.toString())

                for (i in 0 until result.size()) {

                    val Advertisment_data:JsonObject = result[i].asJsonObject

                    val id = Advertisment_data["id"].asInt
                    val ads_id = Advertisment_data["ads_id"].asInt
                    val url = Advertisment_data["url"].asString
                    val destination = Advertisment_data["destination"].asString

                    All_ad.add(Advertisment(id,ads_id,url,destination))

                }
                return All_ad
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }
        fun Logout(
        context: Context?,
        API_url: String?,
        header: String?,
        lang: String?,
        device_id: String?
    ): Boolean{
        Log.d("device_id_logout",device_id.toString())
        val json = JsonObject()
        json.addProperty("device_id",device_id)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("Profile",result.toString())

                val success =  result["result"].asString.equals("success")

                return success

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }

    }

//    fun ForgotPassword(
//        context: Context?,
//        API_url: String?,
//        email: String?
//    ): String?{
//        val json = JsonObject()
//        json.addProperty("email", email.toString())
//
//        try {
//            val result = Ion.with(context)
//                .load(API_url)
//                .setTimeout(15000)
//                .setJsonObjectBody(json)
//                .asJsonObject()
//                .get()
//            //  Log.d("success_book",result.get("success").getAsString());
//
//            try {
//                Log.d("ForgotPassword",result.toString())
//
//                //val success =  result["result"].asString.equals("success")
//
//                return result["msg"].asString
//
//            }
//            catch (e: Exception) {
//                Log.d("error_jper",e.toString())
//
//                return "Unknown Error"
//            }
//
//        } catch (e: Exception) {
//            Log.d("error_jper",e.toString())
//            return "Unknown Error"
//        }
//
//    }
//    fun UpdatePassword(
//        context: Context?,
//        API_url: String?,
//        password: String?,
//        header: String?
//    ): String?{
//        val json = JsonObject()
//        json.addProperty("password", password.toString())
//        json.addProperty("password_confirmation", password.toString())
//
//        try {
//            val result = Ion.with(context)
//                .load(API_url)
//                .setTimeout(10000)
//                .setHeader("Authorization", "Bearer " +header)
//                .setJsonObjectBody(json)
//                .asJsonObject()
//                .get()
//            //  Log.d("success_book",result.get("success").getAsString());
//
//            try {
//                Log.d("password_updated",result.toString())
//
//                val success =  result["result"].asString.equals("success")
//                if(success)
//                    return result["msg"].asString
//                else
//                    return "Unknown Error"
//            }
//            catch (e: Exception) {
//                Log.d("error_jper",e.toString())
//
//                return "Unknown Error"
//            }
//
//        } catch (e: Exception) {
//            Log.d("error_jper",e.toString())
//            return "Unknown Error"
//        }
//
//    }
    fun GetCompanyInfo(
        context: Context?,
        mAPI_url: String?,
        header: String?,
        lang: String?
    ): CompanyBriefProfile?{
        val API_url = mAPI_url!!
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(10000)
                .asJsonObject()
                .get()

            try {
                Log.d("company",result.toString())
                val id= if(result.get("id").isJsonNull)0 else result.get("id").asInt
                val district_id= if(result.get("district_id").isJsonNull)0 else result.get("district_id").asInt
                val company_name_en= if(result.get("company_name_en").isJsonNull)"" else result.get("company_name_en").asString
                val company_name_zh= if(result.get("company_name_zh").isJsonNull)"" else result.get("company_name_zh").asString
                val logo= if(result.get("logo").isJsonNull)"" else result.get("logo").asString
                val company_address= if(result.get("company_address").isJsonNull)"" else result.get("company_address").asString
                val avg_score= if(result.get("avg_score").isJsonNull)0.toFloat() else result.get("avg_score").asFloat
                //val rating_value= if(result.get("avg_score").isJsonNull)0 else result.get("avg_score").asString

                val item = CompanyBriefProfile(id,district_id,company_name_en,company_name_zh,company_address,logo,avg_score)
                return item
                //return null
            }

            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }

    }
    fun GetMyCompanyInfo(
        context: Context?,
        mAPI_url: String?,
        company_id: Int,
        lang: String?
    ): CompanyProfile?{
        Log.d("company_id.toString()",company_id.toString())
        val API_url = mAPI_url!!.plus(company_id.toString().plus("/summary"))
        val PortfolioFeatured = ArrayList<Portfolio>()
        val CommentList = ArrayList<Comment>()
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Content-Language", lang)
                //   .setTimeout(10000)
                .asJsonObject()
                .get()

            try {
                Log.d("companyresult",result.toString())
                val companydata= if(result.get("company").isJsonNull)null else result.get("company").asJsonObject
                val ratedata= if(result.get("rate").isJsonNull)null else result.get("rate").asJsonObject
                val portfoliodata= if(result.get("portfolio").isJsonNull)null else result.get("portfolio").asJsonArray
                val commentdata = if(result.get("comments").isJsonNull)null else result.get("comments").asJsonArray

                if(companydata!=null && ratedata!=null)
                {
                    Log.d ("companydata",companydata.toString())
                    Log.d ("ratedata",ratedata.toString())
                    val company_name_en = if(companydata.get("company_name_en").isJsonNull)"N/A" else companydata.get("company_name_en").asString
                    val company_name_zh = if(companydata.get("company_name_zh").isJsonNull)"N/A" else companydata.get("company_name_zh").asString
                    val jper_comment = if(companydata.get("jper_comment").isJsonNull)"N/A" else companydata.get("jper_comment").asString
                    val logo= if(companydata.get("logo").isJsonNull) null else companydata.get("logo").asString
                    val about = if(companydata.get("about").isJsonNull)"N/A" else companydata.get("about").asString
                    val company_address = if(companydata.get("company_address").isJsonNull)"N/A" else companydata.get("company_address").asString
                    val company_website = if(companydata.get("company_website").isJsonNull)"N/A" else companydata.get("company_website").asString
                    val rate_count = ratedata.get("count").asInt
                    val avg_rate = if(ratedata.get("average").isJsonNull)0f else ratedata.get("average").asString.toFloat()
                    val communication_rate = if( ratedata.get("communication").isJsonNull)0f else ratedata.get("communication").asString.toFloat()
                    val punctuality_rate =  if( ratedata.get("punctuality").isJsonNull)0f else ratedata.get("punctuality").asString.toFloat()
                    val quality_rate =if( ratedata.get("quality").isJsonNull)0f else ratedata.get("quality").asString.toFloat()


                    if (portfoliodata != null) {
                        if(portfoliodata.size()>0) {
                            for (i in 0 until portfoliodata.size()) {
                                Log.d ("portfoliodata",portfoliodata.toString())
                                val PortfolioFeatured_list = portfoliodata[i].asJsonObject
                                val id = PortfolioFeatured_list["id"].asInt
                                val district_id =
                                    if (PortfolioFeatured_list["district_id"].isJsonNull) 0 else PortfolioFeatured_list["district_id"].asInt
                                val district = if (district_id != 0) District(
                                    context!!,
                                    district_id
                                ).name else "N/A"
                                val building =
                                    if (PortfolioFeatured_list["building"].isJsonNull) "" else PortfolioFeatured_list["building"].asString
                                val area =
                                    if (PortfolioFeatured_list["area"].isJsonNull) "" else PortfolioFeatured_list["area"].asString.dropLast(3)
                                Log.d("proj_area",area)
                                val amount =
                                    if (PortfolioFeatured_list["amount"].isJsonNull) "" else PortfolioFeatured_list["amount"].asString
                                val type_id =
                                    if (PortfolioFeatured_list["type"].isJsonNull) 0 else PortfolioFeatured_list["type"].asInt
                                val type = if (type_id != 0) ProjectType(
                                    context!!,
                                    type_id
                                ).name else "N/A"
                                val cover = if (PortfolioFeatured_list["cover"].isJsonNull) "" else PortfolioFeatured_list["cover"].asString
                                val url =
                                    if (PortfolioFeatured_list["url"].isJsonNull) "" else PortfolioFeatured_list["url"].asString
                                val item =
                                    Portfolio(id, district, building, area, amount, type, url, cover)
                                PortfolioFeatured.add(item)
                            }
                        }
                    }
                    if (commentdata != null ) {
                        if(commentdata.size()>0){
                            for (i in 0 until commentdata.size()) {

                                Log.d ("commentdata",commentdata.toString())
                                val Commentdata_list = commentdata[i].asJsonObject
                                val id = Commentdata_list["id"].asInt
                                val owner= Commentdata_list["owner"].asJsonObject
                                val username= owner["username"].asString
                                val commentdetail= if(Commentdata_list["comment"].isJsonNull) null else Commentdata_list["comment"].asJsonObject
                                var content="N/A"
                                if (commentdetail != null)
                                    content= commentdetail["comment"].asString
                                val rate = Commentdata_list["average"].asString.toFloat()

                                val originalFormat: DateFormat =
                                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                                val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                                val date: Date = originalFormat.parse(Commentdata_list["created_at"].asString)
                                val created_at: String = targetFormat.format(date)

                                val item = Comment(id,username,rate,content, created_at)
                                CommentList.add(item)
//                            Log.d("InsideComment1",id.toString())
//                            Log.d("InsideComment2",created_at.toString())
                            }
                        }
                    }
                    val item = CompanyProfile(company_id,company_name_en,company_name_zh,jper_comment,logo, about,company_address,company_website,rate_count, avg_rate, communication_rate,punctuality_rate,quality_rate,PortfolioFeatured,CommentList)
                    Log.d("communication",communication_rate.toString())
                    return item
                }
                return null
            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())


                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }

    }

    fun GetProjectDetail(
        context: Context?,
        mAPI_url: String?,
        header: String?,
        lang: String?
    ): QuoteProjectSummary? {
        //val quotesummarylists = ArrayList<QuoteProjectSummary>()
        val API_url = mAPI_url!!
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //       .setTimeout(10000)
                .asJsonObject()
                .get()

            try {

                var detail = if (result.get("detail").isJsonNull) null else result.getAsJsonObject("detail")

                val style= if (result.get("style").isJsonNull) null else result.getAsJsonObject("style")

                val category = if (result.get("category").isJsonNull) null else result.get("category").asJsonArray

                val notes = if (result.get("notes").isJsonNull) null else result.get("notes").asJsonArray

                val fixed_items = if (result.get("fixed_items").isJsonNull) null else result.get("fixed_items").asJsonArray

                val docs = if (result.get("docs").isJsonNull) null else result.get("docs").asJsonArray

//                val docs: JsonObject? =
//                    if (result["docs"].isJsonNull) null else result["docs"].asJsonObject


                //   Toast.makeText(getContext(),data.get("title").getAsString() , Toast.LENGTH_SHORT).show();


                val id = if (result.get("id").isJsonNull) 0 else result.get("id").asInt
                val district_id =
                    if (result.get("district_id").isJsonNull) 0 else result.get("district_id").asInt
                val district = District(context!!, district_id).name
                val scope_id =
                    if (result.get("scope").isJsonNull) 0 else result.get("scope").asInt
                val scope=ProjectScope(context,scope_id).name
                val type_id =
                    if (result.get("type").isJsonNull) 0 else result.get("type").asInt
                val type=ProjectType(context,type_id).name
                val start_at =
                    if (result.get("start_at").isJsonNull) "" else result.get("start_at").asString
                val completed_at =
                    if (result.get("completed_at").isJsonNull) "" else result.get("completed_at").asString

                var style_id = 0
                var style_name_en = ""
                var style_name_zh = ""
                var style_img = ""

                if (style != null) {
                    style_id =
                        if (style.get("id").isJsonNull) 0 else style.get("id").asInt
                    style_name_en =
                        if (style.get("name_en").isJsonNull) "" else style.get("name_en").asString
                    style_name_zh =
                        if (style.get("name_zh").isJsonNull) "" else style.get("name_zh").asString
                    style_img =
                        if (style.get("img").isJsonNull) "" else style.get("img").asString
                }

                var description = ""
                var detail_address = ""
                var detail_area = ""
                var detail_budget_max = ""

                if (detail != null) {
                    description =
                        if (detail["description"].isJsonNull) "" else detail["description"].asString
                    detail_address = if (detail["address"].isJsonNull) "" else detail["address"].asString
                    detail_area = if (detail["area"].isJsonNull) "" else detail["area"].asString.dropLast(3)
                    detail_budget_max =
                        if (detail["budget_max"].isJsonNull) "" else detail["budget_max"].asString.toDouble().toInt().toString()
                }
//                var docs_id = 0
//                var docs_project_id = 0
//                var docs_status_id = 0
                var url = ""

//                        if (docs != null) {
//                            docs_id = docs["id"].asInt
//                            docs_project_id = docs["project_id"].asInt
//                            docs_status_id = docs["status_id"].asInt
//                            url = docs["url"].asString
//                        }
                var category_array =ArrayList<Int> ()
                if(category!=null){
                    if(category.size()>0){
                        for(x in 0 until category.size()) {
                            category_array.add(category.get(x).asJsonObject.get("id").asInt)
                        }
                    }
                }
                var note_array =ArrayList<String> ()
                if(notes!=null){
                    if(notes.size()>0){
                        for(x in 0 until notes.size()) {
                            note_array.add(notes.get(x).asJsonObject.get("item").asString)
                            Log.d("note_item", x.toString()+".  "+notes.get(x).asJsonObject.get("item").asString)
                        }
                        Log.d("Note_length",notes.size().toString())
                    }
                }
                var docs_array =ArrayList<FileDownload> ()
                if(docs!=null){
                    if(docs.size()>0){
                        for(x in 0 until docs.size()) {
                            val docs_object = docs.get(x).asJsonObject
                            val docs_title= if (docs_object.get("doc_name").isJsonNull) "N/A" else docs_object.get("doc_name").asString
                            val docs_url= if (docs_object.get("url").isJsonNull) "" else docs_object.get("url").asString
                            docs_array.add(FileDownload(docs_title,docs_url))
                        }
                    }
                }
                var fixed_items_array =ArrayList<Add> ()
                if(fixed_items!=null){
                    if(fixed_items.size()>0){
                        for(x in 0 until fixed_items.size()) {
                            val fixed_items_object = fixed_items.get(x).asJsonObject
                            var fixed_items_id =  if (fixed_items_object.get("id").isJsonNull) 0 else fixed_items_object.get("id").asInt
                            var name =  if (fixed_items_object.get("name").isJsonNull) "" else fixed_items_object.get("name").asString
                            fixed_items_array.add(Add(fixed_items_id,name,0F,0F,""))
                            Log.d("fixed_items", x.toString()+".  "+name)
                        }
                        Log.d("fixed_items_length",fixed_items.size().toString())
                    }
                }
                val quotesummarylist = QuoteProjectSummary(

                    id,
                    district,
                    type,
                    scope,
                    detail_budget_max,
                    detail_area,
                    start_at,
                    completed_at,
                    detail_address,
                    description,
                    style_id,
                    style_name_en,
                    style_name_zh,
                    style_img,
                    url,
                    category_array,
                    note_array,
                    docs_array,
                    fixed_items_array
                )

                return quotesummarylist
            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }

    }
    fun ConfirmBudget(
        context: Context?,
        API_url: String?,
        header: String?,
        project_id:Int,
        lang: String?
    ): Boolean{
        val json = JsonObject()
        json.addProperty("project_id", project_id)

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //    .setTimeout(5000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("result",result.toString())

                val success =  result["result"].asString.equals("success")

                return success

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }

    }
    fun ForgotPassword(
        context: Context?,
        API_url: String?,
        email: String?,
        lang: String?
    ): String?{
        val json = JsonObject()
        json.addProperty("email", email.toString())

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Content-Language", lang)
                // .setTimeout(15000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("ForgotPassword",result.toString())

                //val success =  result["result"].asString.equals("success")

                return result["msg"].asString

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return "Unknown Error"
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return "Unknown Error"
        }

    }
    fun SubmitQuote(
        context: Context?,
        API_url: String?,
        project_id: Int?,
        add: ArrayList<Add>?,
        post_date: String?,
        upload_file: File?,
        file_format: String,
        token: String?,
        lang: String?
    ): String{
//        val Fixed_Jsonarray = JsonArray()
//        val General_Jsonarray = JsonArray()
//        var Fixed=""
//        var General=""
//
//        val json_all = JsonObject()
//        json_all.addProperty("project_id",project_id)
//        json_all.addProperty("available_at",post_date)
//
//
//        if(add!!.size>2) {
//            for (i in 0 until add!!.size - 2) {
//                Log.d("add_array_index", i.toString())
//                if (add.get(i).id == 0) //general item
//                {
//                    val json = JsonObject()
//                    json.addProperty("description", add.get(i).description.toString())
//                    json.addProperty("min_amount", add.get(i).min_amount.toString())
//                    json.addProperty("max_amount", add.get(i).max_amount.toString())
//                    json.addProperty("remarks", add.get(i).remarks.toString())
//                    General_Jsonarray.add(json)
//
//                }
//                else //fixed item
//                {
//                    val json = JsonObject()
//                    json.addProperty("id", add.get(i).id.toString())
//                    json.addProperty("description", add.get(i).description.toString())
//                    json.addProperty("min_amount", add.get(i).min_amount.toString())
//                    json.addProperty("max_amount", add.get(i).max_amount.toString())
//                    json.addProperty("remarks", add.get(i).remarks.toString())
//                    Fixed_Jsonarray.add(json)
//                }
//
//            }
//            Fixed = Fixed_Jsonarray.toString()
//            General = General_Jsonarray.toString()
//
//
//
////            Log.d("Fixed",Fixed.toString())
////
////            if(Fixed_Jsonarray.size()>0)json_all.addProperty("fixed_items",Fixed)
////            if(General_Jsonarray.size()>0)json_all.addProperty("general_items",General)
//        }
//        Log.d("json_all",json_all.toString())
        try {
            var builder: Builders.Any.M = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " + token)
                .setHeader("Content-Language", lang)
                // .setTimeout(15000)
                .setMultipartParameter("project_id",project_id.toString())
                .setMultipartParameter("available_at",post_date.toString())
            val Fixed_Jsonarray = JsonArray()
            val General_Jsonarray = JsonArray()
            var Fixed=""
            var General=""

            val json_all = JsonObject()
            json_all.addProperty("project_id",project_id)
            json_all.addProperty("available_at",post_date)


            if(add!!.size>2) {
                for (i in 0 until add!!.size - 2) {
                    Log.d("add_array_index", i.toString())
                    if (add.get(i).id == 0) //general item
                    {
                        val json = JsonObject()
                        json.addProperty("description", add.get(i).description.toString())
                        json.addProperty("min_amount", add.get(i).min_amount.toString())
                        json.addProperty("max_amount", add.get(i).max_amount.toString())
                        json.addProperty("remarks", add.get(i).remarks.toString())
                        General_Jsonarray.add(json)

                    }
                    else //fixed item
                    {
                        val json = JsonObject()
                        json.addProperty("id", add.get(i).id.toString())
                        json.addProperty("description", add.get(i).description.toString())
                        json.addProperty("min_amount", add.get(i).min_amount.toString())
                        json.addProperty("max_amount", add.get(i).max_amount.toString())
                        json.addProperty("remarks", add.get(i).remarks.toString())
                        Fixed_Jsonarray.add(json)
                    }

                }
                Fixed = Fixed_Jsonarray.toString()
                General = General_Jsonarray.toString()



//            Log.d("Fixed",Fixed.toString())

                if(Fixed_Jsonarray.size()>0) builder.setMultipartParameter("fixed_items",Fixed)
                if(General_Jsonarray.size()>0)  builder.setMultipartParameter("general_items",General)
                if (upload_file!=null && !file_format.equals("")) builder =builder.setMultipartFile("attachment", file_format, upload_file)

            }
            Log.d("json_all",json_all.toString())

            //  Log.d("success_book",result.get("success").getAsString());

            try {
                var result_msg= "Unknown Error"
                    ////////////////////////Send_data_to_server//////////////////////////
                val result = builder
                    .asJsonObject()
                    .get()

                try {
                    Log.d("result",result.toString())

                    result_msg= result["msg"].asString
                    if (result["result"].asString.equals("success")) {
                        result_msg= context!!.getString(R.string.Success_msg)
                        Log.d("msg", result["msg"].asString)

                        //save token

//                            val intent = Intent(this,ThankyouActivity::class.java)
//                            intent.putExtra("navigation_id",R.id.navigation_myprojects)
//                            intent.putExtra("desc1",getString(R.string.Thankyou_submit_project_desc1))
//                            intent.putExtra("desc2",getString(R.string.Thankyou_submit_project_desc2))
//
//                            startActivity(intent)

                    }
                    else if (result["result"].asString.equals("error")) {
                        result_msg= result["msg"].asString
                        Log.d("sendinfo_msg", result["msg"].asString)
                    }
                    Log.d("msg", result["msg"].asString)

                    return result_msg

                }
                catch (e: Exception) {
                    Log.d("error_jper",e.toString())

                    return result_msg
                }

            } catch (x: Exception) {
                Log.d("result_error", x.toString())
                return "Unknown Error"
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return "Unknown Error"
        }

    }

    fun UpdateQuoteAttachment(
        context: Context?,
        API_url: String?,
        quote_id: Int?,
        upload_file: File?,
        file_format: String,
        token: String?,
        lang: String?
    ): String{

        try {
            var builder: Builders.Any.M = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " + token)
                .setHeader("Content-Language", lang)

                // .setTimeout(15000)
                .setMultipartParameter("quote_id",quote_id.toString())

            if (upload_file!=null && !file_format.equals("")) builder =builder.setMultipartFile("attachment", file_format, upload_file)

            //  Log.d("success_book",result.get("success").getAsString());

            try {
                var result_msg= context!!.getString(R.string.Success_msg)
                ////////////////////////Send_data_to_server//////////////////////////
                builder
                    .asJsonObject()
                    .setCallback { e, result ->
                        if (e != null)
                            Log.d("result_error", e.toString())
                        if (result["result"].asString.equals("success")) {
                            result_msg= result["msg"].asString
                            Log.d("msg", result["msg"].asString)
                            //save token

//                            val intent = Intent(this,ThankyouActivity::class.java)
//                            intent.putExtra("navigation_id",R.id.navigation_myprojects)
//                            intent.putExtra("desc1",getString(R.string.Thankyou_submit_project_desc1))
//                            intent.putExtra("desc2",getString(R.string.Thankyou_submit_project_desc2))
//
//                            startActivity(intent)

                        }
                        else if (result["result"].asString.equals("error")) {
                            result_msg= result["msg"].asString
                            Log.d("sendinfo_msg", result["msg"].asString)
                        }
                        Log.d("result_okok", result.toString())
                        result_msg= result["msg"].asString
                    }
                return result_msg
            } catch (x: Exception) {
                Log.d("result_error", x.toString())
                return "Unknown Error"
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return "Unknown Error"
        }

    }
    fun UpdateContract(
        context: Context?,
        API_url: String?,
        quote_id: Int?,
        upload_file: File?,
        file_format: String,
        token: String?,
        lang:String?
    ): String{

        try {
            var builder: Builders.Any.M = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " + token)
                .setHeader("Content-Language", lang)

                // .setTimeout(15000)
                .setMultipartParameter("project_id",quote_id.toString())
            Log.d("UpdateContract_API_url",API_url)
            Log.d("UpdateContract_quote_id",quote_id.toString())
            Log.d("UpdateContract_upload_file",upload_file.toString())
            Log.d("UpdateContract_file_format",file_format.toString())
            Log.d("UpdateContract_token",token.toString())
            Log.d("UpdateContract_lang",lang.toString())
            if (upload_file!=null && !file_format.equals("")) builder =builder.setMultipartFile("contract", file_format, upload_file)

            //  Log.d("success_book",result.get("success").getAsString());

            try {
                var result_msg= context!!.getString(R.string.Success_msg)
                ////////////////////////Send_data_to_server//////////////////////////
                val result = builder
                    .asJsonObject()
                    .get()
                try {
                    if (result["result"].asString.equals("success")) {
                        result_msg = result["msg"].asString
                        Log.d("result_msg", result["msg"].asString)
                        //save token

//                            val intent = Intent(this,ThankyouActivity::class.java)
//                            intent.putExtra("navigation_id",R.id.navigation_myprojects)
//                            intent.putExtra("desc1",getString(R.string.Thankyou_submit_project_desc1))
//                            intent.putExtra("desc2",getString(R.string.Thankyou_submit_project_desc2))
//
//                            startActivity(intent)
                        return result_msg
                    } else if (result["result"].asString.equals("error")) {
                        result_msg = result["msg"].asString
                        Log.d("sendinfo_msg", result["msg"].asString)

                        return result_msg
                    }
                    Log.d("result_okok", result.toString())
                    result_msg = result["msg"].asString
                }
                catch (x: Exception) {
                    Log.d("result_error", x.toString())
                    return "Unknown Error"
                }
                return result_msg
            } catch (x: Exception) {
                Log.d("result_error", x.toString())
                return "Unknown Error"
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return "Unknown Error"
        }

    }
    fun GetPaymentPlanNotice(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ): PaymentPlanNotice? {
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //     .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("noticeresult", result.toString())
                val notice_data = if(result["notice"].isJsonNull) null else result["notice"].asJsonObject
                val project_data = if(result["project"].isJsonNull) null else result["project"].asJsonObject
                val company_data = if(result["company"].isJsonNull) null else result["company"].asJsonObject
                val payment_title = if(result["payment_title"].isJsonNull) "" else result["payment_title"].asString
                val due_date = if(result["due_date"].isJsonNull) "" else result["due_date"].asString
                val plan_amount = if(result["plan_amount"].isJsonNull) "0" else result["plan_amount"].asString
                val owner_data = if(result["owner"].isJsonNull) null else result["owner"].asJsonObject
                val retention_data = if(result["retention"].isJsonNull) null else result["retention"].asJsonObject
                var scope =1
                if(project_data!=null){
                    scope = if(project_data["scope"].isJsonNull) 1 else project_data["scope"].asInt
                }
                var id = 0
                var amount="0"
                var paid_to_date=""
                var issued_date=""
                var ref_no = "0"

                if(notice_data != null ){
                    val id = notice_data["id"].asInt
                    val amount = notice_data["amount"].asString
                    val paid_to_date = if(notice_data["paid_to_date"].isJsonNull) "N/A" else notice_data["paid_to_date"].asString
                    val ref_no = if(notice_data["ref_no"].isJsonNull) "N/A" else notice_data["ref_no"].asString

                    val originalFormat: java.text.DateFormat =
                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                    val targetFormat: java.text.DateFormat = SimpleDateFormat("yyyy-MM-dd")
                    val date: java.util.Date = originalFormat.parse(notice_data["created_at"].asString)
                    val issued_date: String = targetFormat.format(date)
                }
                if(company_data!=null && owner_data!=null ) {


                    val company_name_en =if( company_data["company_name_en"].isJsonNull) "" else company_data["company_name_en"].asString
                    val company_name_zh =if( company_data["company_name_zh"].isJsonNull) "" else company_data["company_name_zh"].asString
                    val company_tel =if( company_data["company_tel"].isJsonNull) "N/A" else company_data["company_tel"].asString
                    val company_email =if( company_data["company_email"].isJsonNull) "N/A" else company_data["company_email"].asString
                    val company_website =if( company_data["company_website"].isJsonNull) "N/A" else company_data["company_website"].asString
                    val company_address =if( company_data["company_address"].isJsonNull) "N/A" else company_data["company_address"].asString
                    val bank_code =if( company_data["bank_code"].isJsonNull) "N/A" else company_data["bank_code"].asString
                    val bank_ac =if( company_data["bank_ac"].isJsonNull) "N/A" else company_data["bank_ac"].asString

                    val owner_name  =if( owner_data["username"].isJsonNull) "N/A" else owner_data["username"].asString
                    val owner_email  =if( owner_data["email"].isJsonNull) "N/A" else owner_data["email"].asString
                    var retention_amount = "0"
                    if(retention_data != null) {
                        retention_amount =
                            if (retention_data["amount"]!!.isJsonNull) "0" else retention_data["amount"].asString
                    }

                    Log.d("plan_amount_string", plan_amount.toString())
                    val item = PaymentPlanNotice(id,scope,payment_title,amount,retention_amount,plan_amount,paid_to_date,due_date,ref_no,issued_date,company_name_en,company_name_zh,company_tel,company_email,company_website,company_address,bank_code,bank_ac,owner_name,owner_email)

                    return item
                }
                return null
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }
    fun CancelQuote(
        context: Context?,
        API_url: String?,
        quote_id: Int?,
        token: String?,
        lang:String?
    ): String{


        val json_all = JsonObject()

        json_all.addProperty("quote_id",quote_id)

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " + token)
                .setHeader("Content-Language", lang)
                // .setTimeout(15000)
                .setJsonObjectBody(json_all)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("jper_okok",result.toString())

                //val success =  result["result"].asString.equals("success")

                return result["msg"].asString

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return "Unknown Error"
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return "Unknown Error"
        }

    }
    fun GetMyProjectSummary(
        context: Context?,
        mAPI_url: String?,
        header: String?,
        lang: String?
    ): MyProjectSummary?{
        val API_url = mAPI_url!!
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(10000)
                .asJsonObject()
                .get()

            try {
                Log.d("my_project_summary",result.toString())
//
                val project_obj: JsonObject? = if(result["project"].isJsonNull) null else result["project"].asJsonObject
                val quote_obj: JsonObject? = if(result["quote"].isJsonNull) null else result["quote"].asJsonObject
                if(project_obj!=null && quote_obj!=null){

                    val style_obj: JsonObject? = if(project_obj["style"].isJsonNull) null else project_obj["style"].asJsonObject
                    val category_obj: JsonArray? = if(project_obj["category"].isJsonNull) null else project_obj["category"].asJsonArray
                    val contacts_obj: JsonArray? = if(project_obj["contacts"].isJsonNull) null else project_obj["contacts"].asJsonArray
                    val docs_obj: JsonArray? = if(project_obj["docs"].isJsonNull) null else project_obj["docs"].asJsonArray
                    val detail_obj: JsonObject? = if(project_obj["detail"].isJsonNull) null else project_obj["detail"].asJsonObject
                    val user_obj: JsonObject? = if(project_obj["user"].isJsonNull) null else project_obj["user"].asJsonObject
                    val notes_obj: JsonArray? = if(project_obj["notes"].isJsonNull) null else project_obj["notes"].asJsonArray


                    val id =   if(project_obj["id"].isJsonNull) 0 else project_obj["id"].asInt
                    val user_id =   if(project_obj["user_id"].isJsonNull) 0 else project_obj["user_id"].asInt
                    val district_id =   if(project_obj["district_id"].isJsonNull) 0 else project_obj["district_id"].asInt
                    val district =  District(context!!, district_id).name
                    val status_id =   if(project_obj["status_id"].isJsonNull) 0 else project_obj["status_id"].asInt
                    val status =  ProjectStatus(context!!, status_id).name
                    val scope_id =   if(project_obj["scope"].isJsonNull) 0 else project_obj["scope"].asInt
                    val scope =  ProjectScope(context!!, scope_id).name
                    val type_id =   if(project_obj["type"].isJsonNull) 0 else project_obj["type"].asInt
                    val type =  ProjectType(context!!, type_id).name
                    val style_id =   if(project_obj["style_id"].isJsonNull) 0 else project_obj["style_id"].asInt
                    val complete_date =   if(project_obj["complete_date"].isJsonNull) "" else project_obj["complete_date"].asString
                    val bg_accepted =   if(project_obj["bg_accepted"].isJsonNull) 0 else project_obj["bg_accepted"].asInt
                    val start_at =   if(project_obj["start_at"].isJsonNull) "" else project_obj["start_at"].asString
                    val completed_at =   if(project_obj["completed_at"].isJsonNull) "" else project_obj["completed_at"].asString
                    val matched_date =   if(project_obj["matched_date"].isJsonNull) "" else project_obj["matched_date"].asString
                    val post_date =   if(project_obj["post_date"].isJsonNull) "" else project_obj["post_date"].asString





                    val id_in_quote = if (quote_obj["id"].isJsonNull) 0 else quote_obj["id"].asInt
                    val project_id_in_quote = if (quote_obj["project_id"].isJsonNull) 0 else quote_obj["project_id"].asInt
                    val company_id_in_quote = if (quote_obj["company_id"].isJsonNull) 0 else quote_obj["company_id"].asInt
                    val last_update_by_in_quote = if (quote_obj["last_update_by"].isJsonNull) "" else quote_obj["last_update_by"].asString
                    val status_id_in_quote = if (quote_obj["status_id"].isJsonNull) 0 else quote_obj["status_id"].asInt
                    val available_at_in_quote = if (quote_obj["available_at"].isJsonNull) "" else quote_obj["available_at"].asString
                    val contract_sum_in_quote = if (quote_obj["contract_sum"].isJsonNull) "" else quote_obj["contract_sum"].asString
                    val max_in_quote = if (quote_obj["max"].isJsonNull) "" else quote_obj["max"].asString
                    val min_in_quote = if (quote_obj["min"].isJsonNull) "" else quote_obj["min"].asString


                    var style_name_en=""
                    var style_name_zh=""
                    var style_img=""
                    if(style_obj!=null){
                         style_name_en =   if(style_obj["name_en"].isJsonNull) "" else style_obj["name_en"].asString
                         style_name_zh =   if(style_obj["name_zh"].isJsonNull) "" else style_obj["name_zh"].asString
                         style_img =   if(style_obj["img"].isJsonNull) "" else style_obj["img"].asString
                    }
                    var category_array =ArrayList<Int> ()
                    if(category_obj!=null && category_obj.size()>0){
                            for(x in 0 until category_obj.size()) {
                                category_array.add(category_obj.get(x).asJsonObject.get("id").asInt)
                            }

                    }
                    var contact_array =ArrayList<MyProjectContacts> ()
                    if(contacts_obj!=null && contacts_obj.size()>0){
                        for(x in 0 until contacts_obj.size()) {
                            val contact = contacts_obj.get(x).asJsonObject
                            val user_id =
                                if (contact["id"].isJsonNull) 0 else contact["id"].asInt
                            val contact_name =
                                if (contact["name"].isJsonNull) "" else contact["name"].asString
                            val contact_no =
                                if (contact["tel"].isJsonNull) "" else contact["tel"].asString
                            val contact_method =
                                if (contact["method"].isJsonNull) 0 else contact["method"].asInt
                            contact_array.add(MyProjectContacts(user_id,contact_name,contact_no,contact_method))
                        }
                    }

                    var description=""
                    var address=""
                    var area=""
                    var budget_max=""
                    var contract_m=""
                    if(detail_obj!=null){
                         description =   if(detail_obj["description"].isJsonNull) "" else detail_obj["description"].asString
                         address =   if(detail_obj["address"].isJsonNull) "" else detail_obj["address"].asString
                         area =   if(detail_obj["area"].isJsonNull) "" else detail_obj["area"].asString.dropLast(3)
                         budget_max =   if(detail_obj["budget_max"].isJsonNull) "" else detail_obj["budget_max"].asString
                         contract_m =   if(detail_obj["contract_m"].isJsonNull) "" else detail_obj["contract_m"].asString
                    }

                    var note_array =ArrayList<String> ()
                    if(notes_obj!=null && notes_obj.size()>0){
                        for(x in 0 until notes_obj.size()) {
                            note_array.add(notes_obj.get(x).asJsonObject.get("item").asString)
                        }
                    }

                    var docs_array =ArrayList<FileDownload> ()
                    if(docs_obj!=null && docs_obj.size()>0){
                        for(x in 0 until docs_obj.size()) {
                            val docs_object = docs_obj.get(x).asJsonObject
                            val docs_title= if (docs_object.get("doc_name").isJsonNull) "N/A" else docs_object.get("doc_name").asString
                            val docs_url= if (docs_object.get("url").isJsonNull) "" else docs_object.get("url").asString
                            docs_array.add(FileDownload(docs_title,docs_url))
                        }
                    }


                    val item = MyProjectSummary(
                        id,
                        user_id,
                        status_id,
                        district_id,
                        district,
                        scope_id,
                        scope,
                        type_id,
                        type,
                        style_id,
                        style_name_en,
                        style_name_zh,
                        style_img,
                        start_at,
                        completed_at,
                        matched_date,
                        complete_date,
                        post_date,
                        bg_accepted,
                        description,
                        address,
                        area,
                        budget_max,
                        max_in_quote,
                        min_in_quote,
                        contract_m,
                        category_array,
                        docs_array,
                        note_array,
                        contact_array,
                        id_in_quote,
                        project_id_in_quote,
                        status_id_in_quote
                    )
                    return item
                }
                return null
            }

            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }

    }
    fun GetMyProjectBgAccepted(
        context: Context?,
        mAPI_url: String?,
        header: String?,
        lang: String?
    ): Int{
        val API_url = mAPI_url!!
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(10000)
                .asJsonObject()
                .get()

            try {
                Log.d("my_project_summary",result.toString())
//
                val project_obj: JsonObject? = if(result["project"].isJsonNull) null else result["project"].asJsonObject
                val quote_obj: JsonObject? = if(result["quote"].isJsonNull) null else result["quote"].asJsonObject
                if(project_obj!=null && quote_obj!=null){


                    val bg_accepted =   if(project_obj["bg_accepted"].isJsonNull) 0 else project_obj["bg_accepted"].asInt
                    Log.d("bg_accepted_inhttps",bg_accepted.toString())
                    return bg_accepted
                }
                return 0
            }

            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return 0
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return 0
        }

    }
    fun GetMyProjectBudget(
        context: Context?,
        mAPI_url: String?,
        header: String?,
        lang: String?
    ): ArrayList<ContractItem>?{
        val API_url = mAPI_url!!
        var contractitem_arraylist :ArrayList<ContractItem>? = null
        var contractimage_arraylist =ArrayList<ContractImage>()
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(10000)
                .asJsonArray()
                .get()
            try {
                if(result!=null && result.size()>0){
                    Log.d("result_quote", result.toString())

                    contractitem_arraylist = ArrayList<ContractItem>()
                    Log.d("result.size()", result.size().toString())
                    for(x in 0 until result.size()) {
                        var result_obj = result.get(x).asJsonObject

                        val id = if (result_obj["id"].isJsonNull) 0 else result_obj["id"].asInt
                        val title = if (result_obj["item"].isJsonNull) "N/A" else result_obj["item"].asString
                        val status_id = if (result_obj["status_id"].isJsonNull) 0 else result_obj["status_id"].asInt
                        val u_price = if (result_obj["u_price"].isJsonNull) "N/A" else result_obj["u_price"].asString
                        val quantity = if (result_obj["quantity"].isJsonNull) "N/A" else result_obj["quantity"].asString
                        val amount = if (result_obj["amount"].isJsonNull) "N/A" else result_obj["amount"].asString
                        val complete_date = if (result_obj["complete_date"].isJsonNull) "N/A" else result_obj["complete_date"].asString
                        val remarks = if (result_obj["remarks"].isJsonNull) "N/A" else result_obj["remarks"].asString
                        val images =if(result_obj["images"].isJsonNull) null else result_obj["images"].asJsonArray

                        if(images!=null && images.size()>0){
                            for (index in 0 until images.size()) {
                                val images_obj =if(images.get(index).isJsonNull) null else images.get(index).asJsonObject

                                if(images_obj!=null){

                                    val item_id = if(images_obj["id"].isJsonNull) 0 else images_obj["id"].asInt
                                    val item_budget_id = if(images_obj["budget_id"].isJsonNull) 0 else images_obj["budget_id"].asInt
                                    val urlm = if(images_obj["urlm"].isJsonNull) "N/A" else images_obj["urlm"].asString
                                    val created_at = if(images_obj["created_at"].isJsonNull) "N/A" else images_obj["created_at"].asString
                                    val originalFormat: DateFormat =
                                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                                    val targetYearFormat: DateFormat = SimpleDateFormat("yyyy")
                                    val targetMonthFormat: DateFormat = SimpleDateFormat("MM")
                                    val targetDayFormat: DateFormat = SimpleDateFormat("dd")
                                    val targetTimeFormat: DateFormat = SimpleDateFormat("HH:mm")
                                    val date: Date = originalFormat.parse(created_at)
                                    val year = (targetYearFormat.format(date).toInt()).toString()
                                    val month = (targetMonthFormat.format(date).toInt()).toString()
                                    val day = targetDayFormat.format(date).toString()
                                    val time = targetTimeFormat.format(date).toString()
                                    val created_date = year.plus("-").plus(month).plus("-").plus(day)
                                    contractimage_arraylist.add(ContractImage(item_id,item_budget_id,urlm,created_date))
                                }
                            }
                        }
                        var contractItem = ContractItem(id, title, status_id, u_price,amount, quantity, remarks, complete_date,contractimage_arraylist)
                        contractitem_arraylist!!.add(contractItem)

                        Log.d("contractitem_arraylist",title.toString())
                    }
                    return contractitem_arraylist
                }
                return null
            }
            catch (e:Exception){
                Log.d("error_jper_budget",e.toString())
                return null
            }

        }
        catch (e:Exception){
            Log.d("error_jper_budget",e.toString())
            return null
        }

    }

    fun GetMyNewProjectBudget(
        context: Context?,
        API_url: String?,
        header:String?,
        project_id:Int

    ): ArrayList<NewContractItem>? {

        val json = JsonObject()
        json.addProperty("project_id", project_id)
//        val NewContractItems = ArrayList<NewContractItem>()
        var NewContractItems=ArrayList<NewContractItem>()

        Log.d("project_id.toString()",project_id.toString())
        Log.d("API_url",API_url)

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
//                .setJsonObjectBody(json)
                //     .setTimeout(5000)
                .asJsonArray()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());
            Log.d("NewContractItems_array",result.toString())

            try {
                Log.d("NewContractItems",result.toString())
                Log.d("result_size",result.size().toString())
                for (i in 0 until result.size()) {
                    val NewContractItem_data: JsonObject = result[i].asJsonObject
                    //val NewContractItem_data:JsonObject = result

                    val id = if (NewContractItem_data.get("id").isJsonNull) null else NewContractItem_data["id"].asInt
//                    val project_id = NewContractItem_data["project_id"].asInt
//                    val project_id =
//                        if (NewContractItem_data.get("project_id").isJsonNull) null else NewContractItem_data.get(
//                            "project_id"
//                        ).asInt
                    val item =
                        if (NewContractItem_data["item"].isJsonNull) "N/A" else NewContractItem_data["item"].asString
                    val unit_price =
                        if (NewContractItem_data["u_price"].isJsonNull) "N/A" else NewContractItem_data["u_price"].asString
                    val quantity =
                        if (NewContractItem_data["quantity"].isJsonNull) "N/A" else NewContractItem_data["quantity"].asString
                    val amount =
                        if (NewContractItem_data["amount"].isJsonNull) "N/A" else NewContractItem_data["amount"].asString
                    val complete_date =
                        if (NewContractItem_data["complete_date"].isJsonNull) "N/A" else NewContractItem_data["complete_date"].asString
                    val remarks =
                        if (NewContractItem_data["remarks"].isJsonNull) "N/A" else NewContractItem_data["remarks"].asString
                    val created_at =
                        if (NewContractItem_data["created_at"].isJsonNull) "N/A" else NewContractItem_data["created_at"].asString
                    val updated_at =
                        if (NewContractItem_data["updated_at"].isJsonNull) "N/A" else NewContractItem_data["updated_at"].asString
                    Log.d("GetNewContractItems", item)

                    val content = NewContractItem(
                        id!!,
                        project_id!!,
                        item,
                        unit_price,
                        quantity,
                        amount,
                        complete_date,
                        remarks,
                        created_at,
                        updated_at
                    )
                    NewContractItems?.add(content)
                    Log.d("GetNewContractItems", NewContractItems?.size.toString())
                }
                return NewContractItems
            } catch (e: Exception) {
                Log.d("error_jper1",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper2",e.toString())
            return null
        }
    }

    fun GetContractSum(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ): String? {
        var contract_sum=""
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //     .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());


            try {
                if(result.get("result").asString.equals("success")){

                    contract_sum = result.get("data").asJsonObject.get("contract_sum").asString
                    return contract_sum
                }
                else if (result.get("result").asString.equals("error"))
                    return "0"
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return "0"
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return "0"
        }
        return "0"
    }
//    fun GetRetention(
//        context: Context?,
//        API_url: String?,
//        header:String?,
//        lang:String?
//
//    ): PaymentPlan? {
//
//        try {
//            val result = Ion.with(context)
//                .load(API_url)
//                .setHeader("Authorization", "Bearer " +header)
//                .setHeader("Content-Language", lang)
//                //    .setTimeout(5000)
//                .asJsonObject()
//                .get()
//            //  Log.d("success_book",result.get("success").getAsString());
//
//            try {
//                Log.d("Retention",result.toString())
//
//                val Retention_data:JsonObject? = if (result["retention"].isJsonNull) null else result["retention"].asJsonObject
//                val company_data:JsonObject? = if (result["company"].isJsonNull) null else result["company"].asJsonObject
//                val owner_data:JsonObject? = if (result["owner"].isJsonNull) null else result["owner"].asJsonObject
//
//                if(Retention_data!=null && company_data!=null) {
//                    val id = Retention_data["id"].asInt
//                    val project_id = Retention_data["project_id"].asInt
//
//                    val amount = Retention_data["amount"].asString
//                    val status_id = Retention_data["status_id"].asInt
//                    val status = RetentionStatus(context!!, status_id).name
//                    val status_rgb = RetentionStatus(context, status_id).rgb
//                    val img_name =
//                        if (Retention_data["img_name"].isJsonNull) "" else Retention_data["img_name"].asString
//                    val ref_no =
//                        if (Retention_data["ref_no"].isJsonNull) "" else Retention_data["ref_no"].asString
//                    val owner_name =if (owner_data?.get("username")!!.isJsonNull) "" else owner_data["username"].asString
//                    val owner_email =if (owner_data?.get("email")!!.isJsonNull) "" else owner_data["email"].asString
//
//                    val originalFormat: java.text.DateFormat =
//                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
//                    val targetFormat: java.text.DateFormat = SimpleDateFormat("yyyy-MM-dd")
//                    val date: java.util.Date = originalFormat.parse(Retention_data["created_at"].asString)
//                    val created_at: String = targetFormat.format(date)
//                    val company_name_en = if (company_data["company_name_en"].isJsonNull) "" else company_data["company_name_en"].asString
//                    val company_name_zh = if (company_data["company_name_zh"].isJsonNull) "" else company_data["company_name_zh"].asString
//                    val company_address = if (company_data["company_address"].isJsonNull) "" else company_data["company_address"].asString
//                    val Retention_item = PaymentPlan(
//                        id,
//                        project_id,
//                        context.getString(R.string.Retention),
//                        amount,
//                        1,
//                        status_id,
//                        status,
//                        status_rgb,
//                        created_at,
//                        "",
//                        img_name,
//                        ref_no,
//                        company_name_en,
//                        company_name_zh,
//                        company_address,
//                        null,
//                        false,
//                        true,
//                        owner_name,
//                        owner_email,
//
//                    )
//                    return Retention_item
//                }
//                return null
//            } catch (e: Exception) {
//                Log.d("error_jper",e.toString())
//                return null
//            }
//
//        } catch (e: Exception) {
//            Log.d("error_jper",e.toString())
//            return null
//        }
//    }

    fun GetPaymentPlan(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ): ArrayList<PaymentPlan>? {
        val PaymentPlans_list= ArrayList<PaymentPlan>()

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                // .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("PaymentPlan",result.toString())

                val Postdata = if(result["plan"].asJsonArray.size()==0) null else result["plan"].asJsonArray
                val company_data:JsonObject? = if (result["company"].isJsonNull) null else result["company"].asJsonObject
                val owner_data:JsonObject? = if (result["owner"].isJsonNull) null else result["owner"].asJsonObject

                if(Postdata !=null &&company_data!=null) {
                    for (i in 0 until Postdata.size()){
                        val PaymentPlan_list: JsonObject = Postdata.get(i).getAsJsonObject()

                        val id = PaymentPlan_list["id"].asInt
                        val project_id = PaymentPlan_list["project_id"].asInt
                        val title = if (PaymentPlan_list["title"].isJsonNull) "N/A" else PaymentPlan_list["title"].asString
                        val amount = if(PaymentPlan_list["amount"].isJsonNull) "N/A" else PaymentPlan_list["amount"].asString
                        val is_final = if(PaymentPlan_list["is_final"].asInt == 1) true else false
                        var payment_status_id =  PaymentPlan_list["status_id"].asInt
                        var status_id =0
                        var status = ""
                        var status_rgb = 0
                        val phase = PaymentPlan_list["phase"].asInt
                        Log.d("temp", "title: " +title + "phase: " +phase)

                        val due_date =if(PaymentPlan_list["due_date"].isJsonNull) "N/A" else PaymentPlan_list["due_date"].asString
                        var remarks =  if(PaymentPlan_list["remarks"].isJsonNull) "" else PaymentPlan_list["remarks"].asString
                        if (remarks.equals("null")) remarks=""
                        val company_name_en = if (company_data["company_name_en"].isJsonNull) "" else company_data["company_name_en"].asString
                        val company_name_zh = if (company_data["company_name_zh"].isJsonNull) "" else company_data["company_name_zh"].asString
                        val company_address = if (company_data["company_address"].isJsonNull) "" else company_data["company_address"].asString

                        val owner_name =if (owner_data?.get("username")!!.isJsonNull) "" else owner_data["username"].asString
                        val owner_email =if (owner_data?.get("email")!!.isJsonNull) "" else owner_data["email"].asString

                        val notice_data = if (PaymentPlan_list["notice"].isJsonNull) null else PaymentPlan_list["notice"].asJsonObject
                        var notice_id=0
                        if(notice_data !=null)  {
                            notice_id = (if (notice_data["id"].isJsonNull) null else notice_data["id"].asInt)!!
                            status_id = notice_data["status_id"].asInt


                        }
                        if(status_id==3 &&payment_status_id == 2)
                        {
                            payment_status_id = 4
                        }
                        status = PaymentPlanStatus(context!!,payment_status_id).name
                        status_rgb = PaymentPlanStatus(context,payment_status_id).rgb

                        val PaymentPlan_item = PaymentPlan(
                            id,
                            project_id,
                            title,
                            amount,
                            payment_status_id,
                            status_id,
                            status,
                            status_rgb,
                            due_date,
                            remarks,
                            "",
                            "",
                            company_name_en,
                            company_name_zh,
                            company_address,
                            notice_id,
                            is_final,
                            false,
                            owner_name,
                            owner_email,
                            phase
                        )

                        PaymentPlans_list.add(PaymentPlan_item)
                    }

                    return PaymentPlans_list
                }
                return null
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }

    fun GetPaymentPlanEnable(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ):Boolean {
        val PaymentPlans_list= ArrayList<PaymentPlan>()

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                // .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("PaymentPlanEnable",result.toString())

                val payment_enable = if(result["payment_enable"].isJsonNull) false else result["payment_enable"].asBoolean

                return payment_enable
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }
    }
    fun GetRetentionReceipt(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ): RetentionReceipt? {
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //  .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                val issued_date =  if(result["created_at"].isJsonNull) "N/A" else result["created_at"].asString
                val method = if(result["method"].isJsonNull) 0 else result["method"].asInt
                val amount = if(result["amount"].isJsonNull) "N/A" else result["amount"].asString
                val date =  if(result["date"].isJsonNull) "N/A" else result["date"].asString
                val project_id =if(result["project_id"].isJsonNull) "N/A" else result["project_id"].asString
                val channel = if(result["channel"].isJsonNull) "N/A" else result["channel"].asString
                var proof = if(result["proof_m"].isJsonNull) "N/A" else  result["proof_m"].asString
                var username = if(result["username"].isJsonNull) "N/A" else  result["username"].asString
                if(result["doc"].isJsonNull) proof="N/A"

                val item = RetentionReceipt(username,issued_date,"",method,amount,date,project_id,channel,proof)

                return item
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }

    fun GetPaymentPlanReceipt(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?
    ): RetentionReceipt? {   Log.d("result_okok", API_url)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //  .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("PaymentPlanReceipt",result.toString())
                val title = if(result["title"].isJsonNull) "N/A" else result["title"].asString
                val method = if(result["method"].isJsonNull) 0 else result["method"].asInt
                val amount = if(result["amount"].isJsonNull) "N/A" else result["amount"].asString
                val date =  if(result["date"].isJsonNull) "N/A" else result["date"].asString
                val issued_date =  if(result["created_at"].isJsonNull) "N/A" else result["created_at"].asString
                val project_id =if(result["project_id"].isJsonNull) "N/A" else result["project_id"].asString
                val channel = if(result["channel"].isJsonNull) "N/A" else result["channel"].asString

                var proof = "N/A"
                try{
                     proof = if(result["proof_m"].isJsonNull) "N/A" else  result["proof_m"].asString
                }
                catch (e:Exception){
                     proof = "N/A"
                }
                var username = if(result["username"].isJsonNull) "N/A" else  result["username"].asString
                if(result["doc"].isJsonNull) proof="N/A"
                if(method!=1){proof="N/A"}
                val item = RetentionReceipt(username,issued_date,title,method,amount,date,project_id,channel,proof)

                return item
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }
    fun GetQuoteDetail(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?
    ): QuotationDetail? {
        var quotation_detail_list =ArrayList<Add>()
        Log.d("result_okok", API_url)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //  .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("result_quote",result.toString())
                val id = if(result["id"].isJsonNull) 0 else result["id"].asInt
                val status_id = if(result["status_id"].isJsonNull) 0 else result["status_id"].asInt
                val created_at = if(result["created_at"].isJsonNull) "N/A" else result["created_at"].asString
                val available_at = if(result["available_at"].isJsonNull) "N/A" else result["available_at"].asString


                val originalFormat: DateFormat =
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                val date: Date = originalFormat.parse(created_at)
                val created_at_date: String = targetFormat.format(date)

                val max =  if(result["max"].isJsonNull) 0F else result["max"].asString.toFloat()
                val min =if(result["min"].isJsonNull) 0F else result["min"].asString.toFloat()
                var attachment_m =if(result["attachment_m"].isJsonNull) "" else result["attachment_m"].asString
                if(result["attachment_name"].isJsonNull) attachment_m=""
                val details =if(result["detail"].isJsonNull) null else result["detail"].asJsonArray

                if(details!=null){
                    for (index in 0 until details.size()) {
                        val details_obj =if(details.get(index).isJsonNull) null else details.get(index).asJsonObject

                        if(details_obj!=null){

                            val item_id = if(details_obj["id"].isJsonNull) 0 else details_obj["id"].asInt
                            val desc = if(details_obj["description"].isJsonNull) "N/A" else details_obj["description"].asString
                            val min_amount = if(details_obj["min_amount"].isJsonNull) 0F else details_obj["min_amount"].asString.toFloat()
                            val max_amount = if(details_obj["max_amount"].isJsonNull) 0F else details_obj["max_amount"].asString.toFloat()
                            val remarks = if(details_obj["remarks"].isJsonNull) "N/A" else details_obj["remarks"].asString

                            quotation_detail_list.add(Add(item_id,desc,min_amount,max_amount,remarks))
                        }
                    }
                }
                val item = QuotationDetail(id,status_id,max,min,created_at_date,available_at,attachment_m,quotation_detail_list)

                return item
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }
    fun UpdatePassword(
        context: Context?,
        API_url: String?,
        password: String?,
        current_pw: String?,
        header: String?,
        lang: String?
    ): ArrayList<String> {
        val response: ArrayList<String> = ArrayList<String>()

        val json = JsonObject()
        json.addProperty("password", password.toString())
        json.addProperty("password_confirmation", password.toString())
        json.addProperty("current_password", current_pw.toString())

        try {
            val result = Ion.with(context)
                .load(API_url)
                // .setTimeout(10000)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("password_updated",result.toString())

                val success =if (result["result"].isJsonNull) null else result["result"].asString

                if(success!=null) {
                    response.add(success)
                    response.add(result["msg"].asString)
                    return response
                }
                else {
                    response.add("error")
                    response.add("Unknown Error")
                    return response
                }
            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())


                response.add("error")
                response.add("Unknown Error")
                return response
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())

            response.add("error")
            response.add("Unknown Error")
            return response
        }

    }

    fun GetDisbursement(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ): ArrayList<Disbursement>? {
        var disbursement_list =ArrayList<Disbursement>()
        Log.d("result_okok", API_url)
        try {
            val result_array = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //  .setTimeout(5000)
                .asJsonArray()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                for (index in 0 until result_array.size()) {
                    val result = if (result_array.get(index).isJsonNull) null else result_array.get(index).asJsonObject
                    if (result!=null){
                        val id = if (result["id"].isJsonNull) 0 else result["id"].asInt

                        val type = if (result["type"].isJsonNull) 0 else result["type"].asInt
                        val project_id =
                            if (result["project_id"].isJsonNull) 0 else result["project_id"].asInt
                        val status_id =
                            if (result["status_id"].isJsonNull) 0 else result["status_id"].asInt
                        val init_amount =
                            if (result["init_amount"].isJsonNull) "N/A" else result["init_amount"].asString
                        val init_date =
                            if (result["init_date"].isJsonNull) "N/A" else result["init_date"].asString
                        val transaction_fee =
                            if (result["transaction_fee"].isJsonNull) "N/A" else result["transaction_fee"].asString
                        val transaction_fee_deducted =
                            if (result["transaction_fee_deducted"].isJsonNull) "N/A" else result["transaction_fee_deducted"].asString
                        val commission_amount =
                            if (result["commission_amount"].isJsonNull) "N/A" else result["commission_amount"].asString
                        val commission_deducted =
                            if (result["commission_deducted"].isJsonNull) "N/A" else result["commission_deducted"].asString
                        val release_amount =
                            if (result["release_amount"].isJsonNull) "N/A" else result["release_amount"].asString
                        val released_date =
                            if (result["released_date"].isJsonNull) "N/A" else result["released_date"].asString
                        val title = if (result["title"].isJsonNull) "N/A" else result["title"].asString

                        val item = Disbursement(
                            id,
                            type,
                            project_id,
                            status_id,
                            init_amount,
                            init_date,
                            transaction_fee,
                            transaction_fee_deducted,
                            commission_amount,
                            commission_deducted,
                            release_amount,
                            released_date,
                            title
                        )
                        disbursement_list.add(item)
                    }
                }
                return disbursement_list
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }

    fun GetCalendarEvent(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ): ArrayList<Event>? {
        val CalendarEvent = ArrayList<Event>()
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                // .setTimeout(5000)
                .asJsonArray()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("CalendarEvent",result.toString())

                for (i in 0 until result.size()) {

                    val CalendarEvent_data:JsonObject = result[i].asJsonObject

                    val id = CalendarEvent_data["id"].asInt
                    val is_editable = CalendarEvent_data["is_editable"].asBoolean
                    val project_id = CalendarEvent_data["project_id"].asInt
                    val item = CalendarEvent_data["item"].asString

                    val originalFormat: DateFormat =
                        SimpleDateFormat("yyyy-MM-dd")
                    val targetMonthFormat: DateFormat = SimpleDateFormat("MM")
                    val targetDayFormat: DateFormat = SimpleDateFormat("dd")
                    val date: Date = originalFormat.parse(CalendarEvent_data["complete_date"].asString)
                    val month = Month(context!!,  targetMonthFormat.format(date).toInt()).name
                    val day =targetDayFormat.format(date).toString()

                    val fulldate = CalendarEvent_data["complete_date"].asString

                    val content = Event(id,project_id,item,item,day,month,fulldate,is_editable)
                    CalendarEvent.add(content)
                }
                return CalendarEvent
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }

    fun GetProjectEvent(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?

    ): ArrayList<Event>? {
        val CalendarEvent = ArrayList<Event>()
        Log.d("API_url",API_url.toString())
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                // .setTimeout(5000)
                .asJsonArray()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("CalendarEvent",result.toString())

                for (i in 0 until result.size()) {

                    val CalendarEvent_data:JsonObject = result[i].asJsonObject

                    val id = CalendarEvent_data["id"].asInt
                    val is_editable = if(CalendarEvent_data["is_editable"].asInt==1) true else false
                    val project_id = CalendarEvent_data["project_id"].asInt
                    val item = CalendarEvent_data["title"].asString

                    Log.d("CalendarEvent_is_editable",is_editable.toString())
                    val originalFormat: DateFormat =
                        SimpleDateFormat("yyyy-MM-dd")
                    val targetMonthFormat: DateFormat = SimpleDateFormat("MM")
                    val targetDayFormat: DateFormat = SimpleDateFormat("dd")
                    val date: Date = originalFormat.parse(CalendarEvent_data["target_date"].asString)
                    val month = Month(context!!,  targetMonthFormat.format(date).toInt()).name
                    val day =targetDayFormat.format(date).toString()

                    val fulldate = CalendarEvent_data["target_date"].asString

                    val content = Event(id,project_id,item,item,day,month,fulldate,is_editable)
                    CalendarEvent.add(content)
                }
                return CalendarEvent
            } catch (e: Exception) {
                Log.d("error_jper_calendar2",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper_calendar",e.toString())
            return null
        }
    }
    fun GetChatRooms(
        context: Context?,
        API_url: String?,
        header: String?,
        lang: String?
    ): ArrayList<ChatRoomList>? {
        val chatroom_list = ArrayList<ChatRoomList>()
        var provider_item : ChatRoom_Provider?=null
        try {
            val result: JsonArray = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //    .setTimeout(5000)
                .asJsonArray()
                .get()

            try {
                Log.d("Chatroom_list",result.toString())
//                JsonArray BlogFeatured_list = result.get("result").getAsJsonObject().get("data").getAsJsonArray();

                for (i in 0 until result.size()) {

                    val chatroom_list_data = if(result[i].isJsonNull) null else result[i].asJsonObject


                    if(chatroom_list_data!=null) {
                        Log.d("test_result",chatroom_list_data.toString())
                        val chatroom_id = chatroom_list_data["id"].asInt

                        var chatroom_update_date = if(chatroom_list_data["updated_at"].isJsonNull) "" else chatroom_list_data["updated_at"].asString
                        if(!chatroom_update_date.equals("")) {
                            val originalFormat: DateFormat =
                                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                            val targetMonthFormat: DateFormat = SimpleDateFormat("MM")
                            val targetDayFormat: DateFormat = SimpleDateFormat("dd")
                            val targetTimeFormat: DateFormat = SimpleDateFormat("HH:mm")
                            val date: Date = originalFormat.parse(chatroom_update_date)
                            val month = (targetMonthFormat.format(date).toInt()).toString()
                            val day = targetDayFormat.format(date).toString()
                            val time = targetTimeFormat.format(date).toString()
                            chatroom_update_date = day.plus("/").plus(month).plus(" ").plus(time)
                        }

                        val chatroom_project_id = chatroom_list_data["quote"].asJsonObject.get("project_id").asInt
                        val chatroom_quote_id = chatroom_list_data["quote_id"].asInt
                        val chatroom_key = chatroom_list_data["key"].asString
                        val chatroom_unread = chatroom_list_data["unread"].asInt
                        val chatroom_item =
                            ChatRoom(chatroom_id, chatroom_key, chatroom_project_id,chatroom_quote_id, chatroom_unread,chatroom_update_date)

                        Log.d("quote_id1",chatroom_quote_id.toString())

                        val company_id = chatroom_list_data["company"].asJsonObject.get("id").asInt
                        val company_name_en = chatroom_list_data["company"].asJsonObject.get("company_name_en").asString
                        val company_name_zh = chatroom_list_data["company"].asJsonObject.get("company_name_zh").asString
                        val company_logo = if (chatroom_list_data["company"].asJsonObject.get("logo").isJsonNull)"" else chatroom_list_data["company"].asJsonObject.get("logo").asString
                        val company_avg_score = if(chatroom_list_data["company"].asJsonObject.get("avg_score").isJsonNull)"0" else chatroom_list_data["company"].asJsonObject.get("avg_score").asString


                        val provider_list = if(chatroom_list_data["users"].isJsonNull) null else chatroom_list_data["users"].asJsonArray
                        var provider_id = 0
                        var provider_username = ""
                        var provider_icon = ""
                        if(provider_list !=null){

                            for (y in 0 until provider_list.size()) {
                                val provider_list_data = if(provider_list[y].isJsonNull) null else provider_list[y].asJsonObject
                                if(provider_list_data!=null && provider_list_data["pivot"].asJsonObject.get("type").asInt==1){
                                    provider_id = if (provider_list_data["user_id"].isJsonNull) 0 else provider_list_data["user_id"].asInt
                                    provider_username = if (provider_list_data["username"].isJsonNull) "" else provider_list_data["username"].asString
                                    provider_icon = if (provider_list_data["icon"].isJsonNull) "" else provider_list_data["icon"].asString
                                }

                            }
                            provider_item = ChatRoom_Provider(provider_id,provider_username,provider_icon)


                            val chatroomlist_item = ChatRoomList(
                                chatroom_item,
                                provider_item,
                                company_id,
                                company_name_en,
                                company_name_zh,
                                company_logo,
                                company_avg_score)
                            chatroom_list.add(chatroomlist_item)
                        }
                    }
                }

                return chatroom_list

            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
        return null
    }
    fun SendMessage(
        context: Context?,
        API_url: String?,
        header:String?,
        key: String?,
        content:String?,
        lang:String?
    ): Boolean {

        val json = JsonObject()
        json.addProperty("key", key.toString())
        json.addProperty("content", content.toString())
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {
                if(result.get("result").asString.equals("success"))
                    return true
                else if (result.get("result").asString.equals("success"))
                    return false
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }
        return false
    }
    fun ComplaintProvider(
        context: Context?,
        API_url: String?,
        header: String?,
        project_id:Int,
        quote_id:Int,
        comment: String?,
        lang: String?
    ): Boolean{
        Log.d("report_msg",comment)
        val json = JsonObject()
        json.addProperty("project_id", project_id)
        json.addProperty("quote_id", quote_id)
        json.addProperty("comment", comment)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //    .setTimeout(5000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("Profile",result.toString())

                val success =  result["result"].asString.equals("success")

                return success

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }

    }
    fun ReadMessage(
        context: Context?,
        API_url: String?,
        header: String?,
        key:String,
        lang:String?
    ): Boolean{
        val json = JsonObject()
        json.addProperty("key", key)

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //    .setTimeout(5000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("result",result.toString())

                val success =  result["result"].asString.equals("success")

                return success

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }

    }
    fun GetNotification(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?
    ): ArrayList<Activity>? {
        val Notifications = ArrayList<Activity>()
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //     .setTimeout(5000)
                .asJsonArray()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("CalendarEvent",result.toString())

                for (i in 0 until result.size()) {
                    val Notification_data:JsonObject = result[i].asJsonObject

                    val id = Notification_data["id"].asInt
                    val content = Notification_data["content"].asString
                    val originalFormat: DateFormat =
                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")

                    val targetDateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                    val targetTimeFormat: DateFormat = SimpleDateFormat("HH:mm")
                    originalFormat.setTimeZone(TimeZone.getDefault())
                    targetDateFormat.setTimeZone(TimeZone.getDefault())
                    targetTimeFormat.setTimeZone(TimeZone.getDefault())
                    Log.d("TimeZone_http",TimeZone.getDefault().getID().toString())

                    val date: Date = originalFormat.parse(Notification_data["created_at"].asString)
                    val time =targetTimeFormat.format(date).toString()
                    val day =targetDateFormat.format(date).toString()

                    val read = Notification_data["read"].asInt ==1
                    val item = Activity(id,content,day,time,read,"navbar_activity")
                    Notifications.add(item)
                }
                return Notifications
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
    }
    fun GetNotificationCount(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?
    ):Int {

        var unread_count =0
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //   .setTimeout(5000)
                .asJsonArray()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("CalendarEvent",result.toString())

                for (i in 0 until result.size()) {
                    val Notification_data:JsonObject = result[i].asJsonObject


                    if( Notification_data["read"].asInt ==0)
                        unread_count+=1

                }
                return unread_count
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return 0
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return 0
        }
    }

    fun ReadNotification(
        context: Context?,
        API_url: String?,
        header: String?,
        lang: String?
    ): Boolean{

        try {
            val result = Ion.with(context)
                .load("POST",API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //    .setTimeout(5000)
                .asJsonObject()
                .get()
            //  Log.d("success_book",result.get("success").getAsString());

            try {
                Log.d("result",result.toString())

                val success =  result["result"].asString.equals("success")

                return success

            }
            catch (e: Exception) {
                Log.d("error_jper",e.toString())

                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }

    }
    fun GetChatUnread(
        context: Context?,
        API_url: String?,
        header:String?,
        lang:String?
    ): Int {

        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)

                //    .setTimeout(5000)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {
                val user = result["user"].asInt
                val unread = result["unread"].asInt

                val item = ChatUnread(user,unread)

                return unread
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return 0
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return 0
        }
    }
    fun AddProjectEvent(
        context: Context?,
        API_url: String?,
        header:String?,
        project_id: Int,
        title: String,
        target_date:String,
        lang:String?

    ): Boolean {

        val json = JsonObject()
        json.addProperty("project_id", project_id)
        json.addProperty("title", title.toString())
        json.addProperty("target_date", target_date.toString())
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)

                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {
                if(result.get("result").asString.equals("success"))
                    return true
                else if (result.get("result").asString.equals("success"))
                    return false
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }
        return false
    }
    fun EditProjectEvent(
        context: Context?,
        API_url: String?,
        header:String?,
        id: Int,
        project_id: Int,
        title: String,
        target_date:String,
        lang:String?

    ): ArrayList<String>? {
        var result_array = ArrayList<String>()
        val json = JsonObject()
        json.addProperty("id", id)
        json.addProperty("project_id", project_id)
        json.addProperty("title", title.toString())
        json.addProperty("target_date", target_date.toString())
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {

                result_array.add((result.get("result").asString))
                result_array.add((result.get("msg").asString))

                return result_array

            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
        return null
    }

    fun CreatePaymentNotice(
        context: Context?,
        API_url: String?,
        header:String?,
        plan_id: Int,
        project_id: Int,
        due_date:String,
        lang:String?

    ): Boolean {

        val json = JsonObject()
        json.addProperty("plan_id", plan_id)
        json.addProperty("project_id", project_id)
        json.addProperty("due_date", due_date)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {
                if(result.get("result").asString.equals("success"))
                    return true
                else if (!result.get("result").asString.equals("success"))
                    return false
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }
        return false
    }

    fun CreatePaymentPlan(
        context: Context?,
        API_url: String?,
        header:String?,
        project_id: Int,
        title: String,
        amount: Float,
        due_date: String,
        remarks: String,
        is_final:Boolean,
        lang:String?

    ): ArrayList<String>? {
        var result_array = ArrayList<String>()
        var return_msg=""
        val json = JsonObject()
        json.addProperty("project_id", project_id)
        json.addProperty("title", title)
        json.addProperty("amount", amount)
        json.addProperty("due_date", due_date)
        json.addProperty("remarks", remarks)
        json.addProperty("is_final", is_final)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {

                result_array.add((result.get("result").asString))
                result_array.add((result.get("msg").asString))

                return result_array

            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return null
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return null
        }
        return null
    }
    fun EditPaymentPlan(
        context: Context?,
        API_url: String?,
        header:String?,
        plan_id: Int,
        project_id: Int,
        title: String,
        amount: Float,
        due_date: String,
        remarks: String,
        is_final:Boolean,
        lang: String

    ): Boolean {

        val json = JsonObject()
        json.addProperty("id", plan_id)
        json.addProperty("project_id", project_id)
        json.addProperty("title", title)
        json.addProperty("amount", amount)
        json.addProperty("due_date", due_date)
        json.addProperty("remarks", remarks)
        json.addProperty("is_final", is_final)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {
                if(result.get("result").asString.equals("success"))
                    return true
                else if (result.get("result").asString.equals("error"))
                    return false
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }
        return false
    }
    fun DeletePaymentPlan(
        context: Context?,
        API_url: String?,
        header:String?,
        id: Int,
        project_id: Int,
        lang:String?
    ): Boolean {

        val json = JsonObject()
        json.addProperty("id", id)
        json.addProperty("project_id", project_id)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {
                if(result.get("result").asString.equals("success"))
                    return true
                else if (result.get("result").asString.equals("error"))
                    return false
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }
        return false
    }
    fun DeleteProgressImage(
        context: Context?,
        API_url: String?,
        header:String?,
        id: Int,
        project_id: Int,
        lang:String?

    ): Boolean {

        val json = JsonObject()
        json.addProperty("image_id", id)
        json.addProperty("project_id", project_id)
        try {
            val result = Ion.with(context)
                .load(API_url)
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                .setTimeout(30000)
                .setJsonObjectBody(json)
                .asJsonObject()
                .get()

            Log.d("result_okok",result.toString())

            try {
                if(result.get("result").asString.equals("success"))
                    return true
                else if (result.get("result").asString.equals("error"))
                    return false
            } catch (e: Exception) {
                Log.d("error_jper",e.toString())
                return false
            }

        } catch (e: Exception) {
            Log.d("error_jper",e.toString())
            return false
        }
        return false
    }
}