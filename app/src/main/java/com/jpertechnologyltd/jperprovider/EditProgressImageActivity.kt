package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.drjacky.imagepicker.ImagePicker
import com.jpertechnologyltd.jperprovider.item.ProgressPhotoStyle
import com.jpertechnologyltd.jperprovider.adapter.ProgressPhotoAdapter
import com.jpertechnologyltd.jperprovider.item.ContractImage
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.builder.Builders
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class EditProgressImageActivity : AppCompatActivity() {

    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    var progress_image_list=ArrayList<ProgressPhotoStyle>()
    var upload_image_list=ArrayList<File>()
//    var pre_image_list=ArrayList<ContractImage>()
    var pre_image_links=ArrayList<String>()
    var pre_image_dates=ArrayList<String>()
    var image_ids=ArrayList<Int>()
    var image_budget_ids=ArrayList<Int>()
    var project_id:Int?=null
    var contract_id:Int?=null
    var title:String?=null
    var today:String?=null
    var progress_image_adapter : ProgressPhotoAdapter?=null
    private var progress_image_file: File? = null
    private var lang : String? = "en"
    private var header : String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_contract_item_image)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        try {
            project_id = intent.getIntExtra("project_id",0)
            contract_id = intent.getIntExtra("contract_id",0)
            title = intent.getStringExtra("title")
            pre_image_links= intent.getStringArrayListExtra("image_url_links")
            pre_image_dates= intent.getStringArrayListExtra("image_dates")
            image_ids= intent.getIntegerArrayListExtra("image_ids")
            image_budget_ids= intent.getIntegerArrayListExtra("image_budget_ids")
//            pre_image_list= ArrayList(intent.getParcelableArrayListExtra("images[]"))
            Log.d("project_id_get",pre_image_links!!.toString())
        }
        catch (e:java.lang.Exception){
            Log.d("project_id_get",e.toString())
        }
        val contract_item_title = findViewById(R.id.contract_item_title) as TextView
        val add_button = findViewById(R.id.add_button) as Button
        val back_button = findViewById(R.id.back_button) as ImageView
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")


        if (!lang!!.equals("")){
            val locale = Locale(lang)
            val config = resources.configuration
            Locale.setDefault(locale)
            config.locale = locale
            resources.updateConfiguration(config, resources.displayMetrics)
        }


        back_button.setOnClickListener {
            super.onBackPressed()
        }
        add_button.setOnClickListener {
            ImagePicker.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
             //   .compress(1024)			//Final image size will be less than 1 MB(Optional)
              //  .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .createIntentFromDialog { launcher.launch(it) }
        }

        contract_item_title.text= title

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR).toString()
        val month = calendar.get(Calendar.MONTH+1).toString()
        val day = calendar.get(Calendar.DAY_OF_MONTH).toString()

        today = year+"-"+month+"-"+day
        if(pre_image_links!!.size>0){

            for (i in 0 until pre_image_links!!.size) {
                if(contract_id==image_budget_ids.get(i)) {
                    val obj = ProgressPhotoStyle(
                        image_ids.get(i),
                        "",
                        null,
                        pre_image_links.get(i),
                        pre_image_dates.get(i),
                        0,
                        true
                    )
                    progress_image_list!!.add(obj)
                    //Log.d("progress_image_list", progress_image_list[i].toString())
                }
            }
        }
//        val obj = ProgressPhotoStyle("test",null,"https://uat.jper.com.hk/assets/landing/img/jper/promo_fengshui.png","2020-11-29",0,true)
//        val obj2 = ProgressPhotoStyle("test",null,"https://uat.jper.com.hk/assets/landing/img/jper/promo_fengshui.png","2020-11-29",0,true)
//        val obj3 = ProgressPhotoStyle("test",null,"https://uat.jper.com.hk/assets/landing/img/jper/promo_fengshui.png","2020-11-29",0,true)
//        progress_image_list!!.add(obj)
//        progress_image_list!!.add(obj2)
//        progress_image_list!!.add(obj3)
        val progress_image_listview = findViewById<View>(R.id.progress_image_listview) as RecyclerView

        progress_image_adapter = ProgressPhotoAdapter(progress_image_list!!,this,project_id!!)
        progress_image_listview.adapter = progress_image_adapter
        progress_image_listview.layoutManager = LinearLayoutManager (this, RecyclerView.VERTICAL,false)

        val done_button = findViewById(R.id.done_button) as Button
        // set on-click listener
        done_button.setOnClickListener {
            // project_type_value.text.toString()
            // project_district_value.text.toString()
            // min_budget.text.toString()
            // site_address_value.text.toString()
            // description_value.text.toString()
            // name_value.text.toString()
            // phone_value.text.toString()

            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE





                Thread(Runnable {
                    var add_result = ""
                    try {
                        if (progress_image_adapter!!.getFiles()!!.size > 0) {
                            for (i in 0 until progress_image_adapter!!.getFiles()!!.size) {
                                upload_image_list.add(progress_image_adapter!!.getFiles()?.get(i)!!)
                                Log.d("progress_image_adapter", "okga")
                            }
                            var builder: Builders.Any.M = Ion.with(this)
                                .load("https://uat.jper.com.hk/api/company/project/budget/image/upload")
                                .setHeader("Authorization", "Bearer " + header)
                                .setHeader("Content-Language", lang)
                                .setMultipartParameter("project_id", project_id.toString())
                                .setMultipartParameter("budget_id", contract_id.toString())


                            if (upload_image_list!!.size > 0) {

                                for (i in 0 until upload_image_list.size) {
//                val part: Part = FilePart("docs", file_upload.get(i))
//                fileParts.add(part)
                                    try {
                                        builder = builder.setMultipartFile(
                                            "images[]",
                                            "image/jpeg,png",
                                            upload_image_list.get(i)
                                        )
                                        Log.d("images[]", "okga")
                                    } catch (x: Exception) {
                                        Log.d("images[]", x.toString())
                                    }
                                }
                            }

                            try {
                                ////////////////////////Send_data_to_server//////////////////////////
                                builder
                                    .asJsonObject()
                                    .setCallback { e, result ->


                                        if (e != null) {
//                                    showDialog(this, e.toString())
                                            Log.d("result_error", e.toString())
                                        }
                                        if (result["result"].asString.equals("success")) {

                                            Log.d("msg", result["msg"].asString)
                                            add_result = getString(R.string.Success_msg)

                                        } else if (result["result"].asString.equals("error")) {
                                            Log.d("sendinfo_msg", result["msg"].asString)
                                            // showDialog(this, result["msg"].asString)
                                            add_result = result["msg"].asString

                                        }
                                        Log.d("result_okok", result.toString())

                                        runOnUiThread(Runnable {

                                            Loading!!.visibility = View.VISIBLE
                                            progressOverlay!!.visibility = View.VISIBLE
                                            Log.d("add_result", add_result)
                                            if (!add_result.equals("")) {

                                                Loading!!.visibility = View.GONE
                                                progressOverlay!!.visibility = View.GONE
                                                Log.d("add_result", add_result)

                                                val dialog = Dialog(this)
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                                dialog.window!!.setBackgroundDrawable(
                                                    ColorDrawable(
                                                        Color.TRANSPARENT
                                                    )
                                                )
                                                dialog.setCancelable(false)
                                                dialog.setContentView(R.layout.dialog_message)

                                                val dialog_message =
                                                    dialog.findViewById(R.id.dialog_message) as TextView
                                                dialog_message.text = add_result
                                                val confirm_button =
                                                    dialog.findViewById(R.id.confirm_button) as Button

                                                val close_button =
                                                    dialog.findViewById(R.id.close_button) as ImageView
                                                close_button.visibility = View.INVISIBLE
                                                confirm_button.setOnClickListener {
                                                    dialog.dismiss()
                                                    val editor = getSharedPreferences(
                                                        "MyPreferences",
                                                        Context.MODE_PRIVATE
                                                    ).edit()
                                                    editor.putBoolean("refresh_contract", true)
                                                    editor.apply()
                                                    finish()
                                                }
                                                close_button.setOnClickListener {
                                                    dialog.dismiss()
                                                    val editor = getSharedPreferences(
                                                        "MyPreferences",
                                                        Context.MODE_PRIVATE
                                                    ).edit()
                                                    editor.putBoolean("refresh_contract", true)
                                                    editor.apply()
                                                    finish()

                                                }

                                                dialog.setCancelable(true)
                                                dialog.show()
                                            }
                                        })
                                    }
                            } catch (x: Exception) {
                                Log.d("result_error", x.toString())
                                add_result = "Unkown Error"

                                runOnUiThread(Runnable {

                                    Loading!!.visibility = View.VISIBLE
                                    progressOverlay!!.visibility = View.VISIBLE
                                    Log.d("add_result", add_result)
                                    if (!add_result.equals("")) {

                                        Loading!!.visibility = View.GONE
                                        progressOverlay!!.visibility = View.GONE
                                        Log.d("add_result", add_result)

                                        val dialog = Dialog(this)
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                        dialog.setCancelable(false)
                                        dialog.setContentView(R.layout.dialog_message)

                                        val dialog_message =
                                            dialog.findViewById(R.id.dialog_message) as TextView
                                        dialog_message.text = add_result
                                        val confirm_button =
                                            dialog.findViewById(R.id.confirm_button) as Button

                                        val close_button =
                                            dialog.findViewById(R.id.close_button) as ImageView
                                        close_button.visibility = View.INVISIBLE
                                        confirm_button.setOnClickListener {
                                            dialog.dismiss()
                                            val editor = getSharedPreferences(
                                                "MyPreferences",
                                                Context.MODE_PRIVATE
                                            ).edit()
                                            editor.putBoolean("refresh_contract", true)
                                            editor.apply()
                                            finish()
                                        }
                                        close_button.setOnClickListener {
                                            dialog.dismiss()
                                            val editor = getSharedPreferences(
                                                "MyPreferences",
                                                Context.MODE_PRIVATE
                                            ).edit()
                                            editor.putBoolean("refresh_contract", true)
                                            editor.apply()
                                            finish()

                                        }

                                        dialog.setCancelable(true)
                                        dialog.show()
                                    }
                                })
                            }
                        }
                    }
                    catch (e:java.lang.Exception){
                        runOnUiThread(Runnable {
                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE
                            showDialog(this,getString(R.string.progress_photo_upload_warning))
                        })
                    }
                }).start()
            }

    }
    private val launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = it.data?.data
            val fileUri_full_path = fileUri?.path
            progress_image_file = File(fileUri?.path)
            Log.d("fileUri",fileUri.toString())
            //You can get File object from intent
            ImagePicker.getFile(it.data)

            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR).toString()
            val month = (calendar.get(Calendar.MONTH)+1).toString()
            val day = calendar.get(Calendar.DAY_OF_MONTH).toString()

            Log.d("today",today)
            today = year+"-"+month+"-"+day

            val new_photo = ProgressPhotoStyle(0,"",fileUri_full_path,null,today!!,0,false)

//            if(position == photo_position){
//                position +=1
//                photos.add(Photo("1", null,position))
//            }


            progress_image_list.add(new_photo)
            progress_image_adapter!!.refreshDataset()

            //You can also get File Path from intent
//            val filePath: String? = ImagePicker.getFilePath(data)
        } else if (it.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(it.data), Toast.LENGTH_SHORT).show()
        } else {

            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            val fileUri_full_path = fileUri?.path
            progress_image_file = File(fileUri?.path)
            Log.d("fileUri",fileUri.toString())
            //You can get File object from intent
            ImagePicker.getFile(data)

            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR).toString()
            val month = (calendar.get(Calendar.MONTH)+1).toString()
            val day = calendar.get(Calendar.DAY_OF_MONTH).toString()

            Log.d("today",today)
            today = year+"-"+month+"-"+day

            val new_photo = ProgressPhotoStyle(0,"",fileUri_full_path,null,today!!,0,false)

//            if(position == photo_position){
//                position +=1
//                photos.add(Photo("1", null,position))
//            }


            progress_image_list.add(new_photo)
            progress_image_adapter!!.refreshDataset()

            //You can also get File Path from intent
//            val filePath: String? = ImagePicker.getFilePath(data)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {

            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
}