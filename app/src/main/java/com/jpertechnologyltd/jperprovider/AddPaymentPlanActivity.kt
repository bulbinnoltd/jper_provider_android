package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.item.PaymentPlan
import com.jpertechnologyltd.jperprovider.item.ProgressPhotoStyle
import com.jpertechnologyltd.jperprovider.adapter.ProgressPhotoAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService.CreatePaymentPlan
import com.koushikdutta.ion.Ion
import java.util.*
import kotlin.collections.ArrayList


class AddPaymentPlanActivity : AppCompatActivity() {

    val posts: ArrayList<ProgressPhotoStyle> = ArrayList()
    val PaymentPlan: ArrayList<PaymentPlan> = ArrayList()
    var style_adapter = ProgressPhotoAdapter(posts,this,0)
     var project_id=0

    var lang : String? = "en"

    private var header : String? = null

    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_payment_plan)
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        try {
            project_id = intent.getIntExtra("project_id",0)
            Log.d("project_id_get","ok")
        }
        catch (e:java.lang.Exception){
            Log.d("project_id_get",e.toString())
        }

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)


        val add_payment_plan_submit_button = findViewById<Button>(R.id.add_payment_plan_submit_button)
        val add_payment_plan_desc = findViewById<TextInputEditText>(R.id.add_payment_plan_desc)
        val add_payment_plan_amount = findViewById<TextInputEditText>(R.id.add_payment_plan_amount)
        val add_payment_plan_final_pay_spinner_view = findViewById<AutoCompleteTextView>(R.id.add_payment_plan_final_pay_spinner_view)
        val add_payment_plan_due = findViewById<TextInputEditText>(R.id.add_payment_plan_due)
        val add_payment_plan_terms = findViewById<TextInputEditText>(R.id.add_payment_plan_terms)
        val yesno = listOf(
            getString(R.string.No),
            getString(R.string.Yes)
        )
        val adapter = ArrayAdapter(this, R.layout.item_text_option, yesno)
        add_payment_plan_final_pay_spinner_view?.setAdapter(adapter)
        add_payment_plan_due.setText(""+year+"-"+(month+1)+"-"+day)
        add_payment_plan_due.setOnClickListener {
            val dpd = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    add_payment_plan_due.setText(""+mYear+"-"+(mMonth+1)+"-"+mDay)
                },year,month,day)
            dpd.datePicker.minDate = System.currentTimeMillis() -1000
            if(lang.equals("en")) {
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancle_button), dpd)
            }
            else{
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE,  getString(R.string.cancle_button), dpd)
            }
            dpd.show()
        }


        val back_button = findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {
            super.onBackPressed()
        }

        var pp_title=""
        var pp_amount:Float?=null
        var pp_amount_string:String?=null
        var pp_is_final:Boolean?=null
        var pp_remarks=""
        var pp_due_date=""
        add_payment_plan_submit_button.setOnClickListener {
            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE

            try {
                pp_title = add_payment_plan_desc.text.toString()
            } catch (x: Exception) {
            }
            try {
                pp_amount = add_payment_plan_amount.text.toString().toFloat()
                pp_amount_string = add_payment_plan_amount.text.toString()
            } catch (x: Exception) {
            }
            try {
                pp_is_final =
                    if (add_payment_plan_final_pay_spinner_view.text.toString()
                            .equals(yesno[1])
                    ) true
                    else
                        false
            } catch (x: Exception) {
            }
            try {
                pp_remarks = add_payment_plan_terms.text.toString()
            } catch (x: Exception) {
            }
            try {
                pp_due_date = add_payment_plan_due.text.toString()

            } catch (x: Exception) {
            }

            if (pp_title.equals("") ||pp_amount_string.equals("") || pp_due_date.equals("") || pp_is_final==null) {
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
                showDialog(this, getString(R.string.Without_info))
            } else {
                var result: ArrayList<String>? = null
                Thread(Runnable {
                    result = CreatePaymentPlan(
                        this,
                        "https://uat.jper.com.hk/api/company/project/payment/plan/add",
                        header,
                        project_id,
                        pp_title,
                        pp_amount!!,
                        pp_due_date,
                        pp_remarks,
                        pp_is_final!!,
                        lang
                    )
                    runOnUiThread(java.lang.Runnable {
                        if (result != null) {


                            if(result!!.get(0).equals("success")) {
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE
                                val dialog = Dialog(this)
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                dialog.setCanceledOnTouchOutside(false)
                                dialog.setContentView(R.layout.dialog_message)

                                val dialog_message =
                                    dialog.findViewById(R.id.dialog_message) as TextView
                                dialog_message.text = getString(R.string.Success_msg)
                                val confirm_button =
                                    dialog.findViewById(R.id.confirm_button) as Button

                                val close_button =
                                    dialog.findViewById(R.id.close_button) as ImageView
                                close_button.visibility = View.INVISIBLE
                                confirm_button.setOnClickListener {
                                    dialog.dismiss()
                                    val editor = getSharedPreferences(
                                        "MyPreferences",
                                        Context.MODE_PRIVATE
                                    ).edit()
                                    editor.putBoolean("refresh_paymentplan", true)
                                    editor.apply()
                                    finish()
                                }
                                close_button.setOnClickListener {
                                    dialog.dismiss()
                                }

                                dialog.setCancelable(true)
                                dialog.show()
                            }
                            else{
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE
                                val dialog = Dialog(this)
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                dialog.setCanceledOnTouchOutside(false)
                                dialog.setContentView(R.layout.dialog_message)

                                val dialog_message =
                                    dialog.findViewById(R.id.dialog_message) as TextView
                                dialog_message.text = result!!.get(1)
                                val confirm_button =
                                    dialog.findViewById(R.id.confirm_button) as Button

                                val close_button =
                                    dialog.findViewById(R.id.close_button) as ImageView
                                close_button.visibility = View.INVISIBLE
                                confirm_button.setOnClickListener {
                                    dialog.dismiss()
                                }
                                close_button.setOnClickListener {
                                    dialog.dismiss()
                                }

                                dialog.setCancelable(true)
                                dialog.show()
                            }
                        }
                    })
                }).start()
            }
        }
    }

//    override fun onNothingSelected(parent: AdapterView<*>?) {
//        Toast.makeText(applicationContext,"Please Select", Toast.LENGTH_LONG).show()
//    }
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        var items:String = parent?.getItemAtPosition(position) as String
//        Toast.makeText(applicationContext,"$items",Toast.LENGTH_LONG).show()
//    }



    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    fun getItem(position: Int): PaymentPlan {
        return PaymentPlan[position]
    }

}
