package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.component.HttpsService.PostFCMtoken
import com.koushikdutta.ion.Ion
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp

class FirstPageActivity : AppCompatActivity() {
    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    var lang: String?= "en"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_page)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        val login_button = findViewById<Button>(R.id.login_button)
        val first_page_email_value = findViewById<TextInputEditText>(R.id.first_page_email_value)
        val first_page_pw_value = findViewById<TextInputEditText>(R.id.first_page_pw_value)
        // set on-click listener
        login_button.setPaintFlags(login_button.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        login_button.setOnClickListener {
            sendinfo(first_page_email_value.text,first_page_pw_value.text)
//            val intent = Intent(this, MainActivity::class.java)
//            startActivity(intent)
//            finish()
        }
        val forgot_pw_button = findViewById<TextView>(R.id.forgot_pw_button)
        // set on-click listener
        forgot_pw_button.setPaintFlags(forgot_pw_button.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        forgot_pw_button.setOnClickListener {

            val intent = Intent (this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

        val sign_up_button = findViewById<TextView>(R.id.sign_up_button)
        // set on-click listener
        sign_up_button.setOnClickListener {
//
//            val intent = Intent (this, RegisterActivity::class.java)
//            startActivity(intent)
            val intent = Intent(this, WebviewActivity::class.java)
            intent.putExtra("url","https://uat.jper.com.hk/join/us#form")
            startActivity(intent)
        }
    }
    fun sendinfo(email: Editable?, password: Editable? ){

        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE

        val json = JsonObject()
        json.addProperty("email", email.toString())
        json.addProperty("password", password.toString())

        Log.d("register",email.toString().plus(password.toString()))
        try {
            ////////////////////////Send_data_to_server//////////////////////////
            Ion.with(baseContext)
                .load("https://uat.jper.com.hk/api/company/login")
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback { e, result ->
                    if (e != null)
                        Log.d("result_error", e.toString())
                    try {
                        if (result["result"].asString.equals("success")) {

                            Log.d("token", result["token"].asString)
                            //save token
                            val editor =
                                this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                                    .edit()
                            editor.putString("user_token", result["token"].asString)
                            editor.putInt("user_id", result["user"].asJsonObject.get("id").asInt)
                            editor.apply()
                            FirebaseInstanceId.getInstance().instanceId
                                .addOnCompleteListener(OnCompleteListener { task ->
                                    if (!task.isSuccessful) {
                                        Log.d("getInstanceId failed", task.exception.toString())
                                        return@OnCompleteListener
                                    }

                                    // Get new Instance ID token
                                    val token = task.result?.token

                                    // Log and toast
                                    val msg = token.toString()
                                    Log.d("firebase_token", msg)
                                    if (PostFCMtoken(
                                            this,
                                            "https://uat.jper.com.hk/api/messaging/token",
                                            result["token"].asString,
                                            msg,
                                            lang
                                        )
                                    ) {
                                        val editor = this.getSharedPreferences(
                                            "MyPreferences",
                                            Context.MODE_PRIVATE
                                        ).edit()
                                        editor.putString("FCM_token", msg)
                                        editor.apply()

                                        //Log event Firebase Analytics
//                                        try {
//                                            var Bundle = Bundle()
//                                            Bundle.putString(
//                                                FirebaseAnalytics.Param.METHOD,
//                                                "method"
//                                            )
//                                            firebaseAnalytics.logEvent(
//                                                FirebaseAnalytics.Event.LOGIN,
//                                                Bundle
//                                            )
//                                        }
//                                        catch( e: Exception ){}

                                        val intent = Intent(this, MainActivity::class.java)
                                        startActivity(intent)
                                        finish()

                                        Loading!!.visibility = View.GONE
                                        progressOverlay!!.visibility = View.GONE
                                    }
                                })


                        } else if (result["result"].asString.equals("error")) {
                            Log.d("sendinfo_msg", result["msg"].asString)


                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE

                            showDialog(this, result["msg"].asString)
                        }
                        Log.d("result_okok", result.toString())
                    }
                    catch (x: Exception){

                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        Log.d("error_fcm",x.toString())
                    }
                }

        } catch (x: Exception) {


            Loading!!.visibility = View.GONE
            progressOverlay!!.visibility = View.GONE
            Log.d("result_error", x.toString())

        }

    }

    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }

}
