package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.item.ProgressPhotoStyle
import com.jpertechnologyltd.jperprovider.adapter.ProgressPhotoAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.PaymentPlan
import com.koushikdutta.ion.Ion
import java.util.*
import kotlin.collections.ArrayList


class EditPaymentPlanActivity : AppCompatActivity() {
    val posts: ArrayList<ProgressPhotoStyle> = ArrayList()
    val PaymentPlan: ArrayList<PaymentPlan> = ArrayList()
    var style_adapter = ProgressPhotoAdapter(posts,this,0)
    var plan_id=0
    var project_id=0
    var title=""
    var is_final=false
    var pp_is_final:Boolean?=null
    var amount=""
    var remarks=""
    var created_at=""

    private var lang : String? = "en"

    private var header : String? = null

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_payment_plan)
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        try {
            project_id = intent.getIntExtra("project_id",0)
            plan_id = intent.getIntExtra("plan_id",0)
            title = intent.getStringExtra("title")
            remarks = intent.getStringExtra("remarks")
            created_at = intent.getStringExtra("created_at")
            amount = intent.getStringExtra("amount")
            is_final = intent.getBooleanExtra("is_final",false)
            pp_is_final = is_final
            Log.d("project_id_get","ok")
        }
        catch (e:java.lang.Exception){
            Log.d("project_id_get",e.toString())
        }

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)


        val edit_payment_save_button = findViewById<Button>(R.id.edit_payment_save_button)
        val edit_payment_delete_button = findViewById<Button>(R.id.edit_payment_delete_button)
        val edit_payment_plan_desc = findViewById<TextInputEditText>(R.id.edit_payment_plan_desc)
        val edit_payment_plan_amount = findViewById<TextInputEditText>(R.id.edit_payment_plan_amount)
        val edit_payment_plan_final_pay_spinner_view = findViewById<AutoCompleteTextView>(R.id.edit_payment_plan_final_pay_spinner_view)
        val edit_payment_plan_due = findViewById<TextInputEditText>(R.id.edit_payment_plan_due)
        val edit_payment_plan_terms = findViewById<TextInputEditText>(R.id.edit_payment_plan_terms)
        val yesno = listOf(
            getString(R.string.No),
            getString(R.string.Yes)
        )
        val adapter = ArrayAdapter(this, R.layout.item_text_option, yesno)
        if(is_final)
            edit_payment_plan_final_pay_spinner_view.setText(adapter.getItem(1))
        else
            edit_payment_plan_final_pay_spinner_view.setText(adapter.getItem(0))
        edit_payment_plan_final_pay_spinner_view?.setAdapter(adapter)
        edit_payment_plan_final_pay_spinner_view.setOnItemClickListener { parent, view, position, id ->
            Log.d("position",position.toString())
            if(position==0) pp_is_final = false
            else if(position==1) pp_is_final=true
        }



        edit_payment_plan_due.setText(""+year+"-"+(month+1)+"-"+day)
        edit_payment_plan_due.setOnClickListener {
            val dpd = DatePickerDialog(this,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    edit_payment_plan_due.setText(""+mYear+"-"+(mMonth+1)+"-"+mDay)
                },year,month,day)
            dpd.datePicker.minDate = System.currentTimeMillis() -1000
            if(lang.equals("en")) {
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancle_button), dpd)
            }
            else{
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE,  getString(R.string.cancle_button), dpd)
            }
            dpd.show()
        }


        val back_button = findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {
            super.onBackPressed()
        }

        edit_payment_plan_desc.setText(title)
        Log.d("amount",amount.toString())
        edit_payment_plan_amount.setText(amount.toDouble().toInt().toString())

        edit_payment_plan_due.setText(created_at)
        edit_payment_plan_terms.setText(remarks)


        edit_payment_delete_button.setOnClickListener {
            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE


                var result: Boolean? = null
                Thread(Runnable {
                    result = HttpsService.DeletePaymentPlan(
                        this,
                        "https://uat.jper.com.hk/api/company/project/payment/plan/remove",
                        header,
                        plan_id,
                        project_id,
                        lang

                    )
                    runOnUiThread(java.lang.Runnable {
                        if (result != null) {
                            showDialog(this, getString(R.string.Success_msg))
                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE

                        }
                    })
                }).start()
        }


//    override fun onNothingSelected(parent: AdapterView<*>?) {
//        Toast.makeText(applicationContext,"Please Select", Toast.LENGTH_LONG).show()
//    }
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//        var items:String = parent?.getItemAtPosition(position) as String
//        Toast.makeText(applicationContext,"$items",Toast.LENGTH_LONG).show()
//    }

        edit_payment_save_button.setOnClickListener {
            var pp_title=""
            var pp_amount=0F
            var pp_amount_string:String?=null
            var pp_remarks=""
            var pp_due_date=""

            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE
            try {
                pp_title = edit_payment_plan_desc.text.toString()
            } catch (x: Exception) {
            }
            try {
                pp_amount = edit_payment_plan_amount.text.toString().toFloat()
                pp_amount_string = edit_payment_plan_amount.text.toString()
            } catch (x: Exception) {
            }
            try {
                pp_remarks = edit_payment_plan_terms.text.toString()
            } catch (x: Exception) {
            }
            try {
                pp_due_date = edit_payment_plan_due.text.toString()

            } catch (x: Exception) {
            }

            if (pp_title.equals("") || pp_due_date.equals("") || pp_is_final==null||  pp_amount_string.equals("")) {
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
                showDialog(this, getString(R.string.Without_info))
            } else {

                Thread(Runnable {

                    try {
                        val json = JsonObject()
                        json.addProperty("project_id", project_id)
                        json.addProperty("id", plan_id)
                        json.addProperty("title", pp_title)
                        json.addProperty("amount", pp_amount!!.toFloat())
                        json.addProperty("is_final", pp_is_final)
                        json.addProperty("due_date", pp_due_date!!)
                        json.addProperty("remarks", pp_remarks)

                        Ion.with(baseContext)
                            .load("https://uat.jper.com.hk/api/company/project/payment/plan/update")
                            .setHeader("Authorization", "Bearer " + header)
                            .setJsonObjectBody(json)
                            .asJsonObject()
                            .setCallback { e, result ->
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE
                                if (e != null)
                                    Log.d("result_error", e.toString())
                                if (result["result"].asString.equals("success")) {

                                    Log.d("result_updated", "result_updated")

                                    val dialog = Dialog(this)
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                                    dialog.setCanceledOnTouchOutside(false)
                                    dialog.setContentView(R.layout.dialog_message)

                                    val dialog_message =
                                        dialog.findViewById(R.id.dialog_message) as TextView
                                    dialog_message.text = getString(R.string.Success_msg)
                                    val confirm_button =
                                        dialog.findViewById(R.id.confirm_button) as Button

                                    val close_button =
                                        dialog.findViewById(R.id.close_button) as ImageView
                                    close_button.visibility = View.INVISIBLE
                                    confirm_button.setOnClickListener {
                                        dialog.dismiss()
                                        val editor = getSharedPreferences(
                                            "MyPreferences",
                                            Context.MODE_PRIVATE
                                        ).edit()
                                        editor.putBoolean("refresh_paymentplan", true)
                                        editor.apply()
                                        finish()
                                    }
                                    close_button.setOnClickListener {
                                        dialog.dismiss()
                                    }

                                    dialog.setCancelable(true)
                                    dialog.show()


                                } else if (result["result"].asString.equals("error")) {

                                    showDialog(this, result["msg"].asString)
                                }
                                Log.d("result_okok", result.toString())
                            }
                    } catch (x: Exception) {
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        Log.d("result_error", x.toString())
                    }

                }).start()
            }


        }
    edit_payment_delete_button.setOnClickListener {

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_confirm)

        val dialog_message = dialog.findViewById(R.id.message) as TextView
        dialog_message.text = getString(R.string.dialog_delete_info)
        val confirm_button = dialog.findViewById(R.id.save_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        val cancel_button = dialog.findViewById(R.id.cancel_button) as Button
        cancel_button.setOnClickListener{
            dialog.dismiss()
        }
        close_button.visibility = View.INVISIBLE
        confirm_button.setOnClickListener {
            dialog.dismiss()

            var delete_result :String?= null


            Thread(Runnable {

                try {
                    val json = JsonObject()
                    json.addProperty("project_id", project_id)
                    json.addProperty("id", plan_id)

                    Ion.with(this)
                        .load("https://uat.jper.com.hk/api/company/project/payment/plan/remove")
                        .setHeader("Authorization", "Bearer " + header)
                        .setJsonObjectBody(json)
                        .asJsonObject()
                        .setCallback { e, result ->
                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE
                            if (e != null)
                                Log.d("result_error", e.toString())
                            if (result["result"].asString.equals("success")) {

                                Log.d("result_updated1", "result_updated1")


                                delete_result =result["msg"].asString
                                runOnUiThread(java.lang.Runnable {
                                    if(delete_result==null) {

                                    }

                                    else{

                                        dialog.dismiss()
                                        val editor = getSharedPreferences(
                                            "MyPreferences",
                                            Context.MODE_PRIVATE
                                        ).edit()
                                        editor.putBoolean("refresh_paymentplan", true)
                                        editor.apply()
                                        finish()
                                        Log.d("result_updated3", "res3ult_updated")

                                    }
                                })
                            }
                            else if (result["result"].asString.equals("error")) {

                                Log.d("result_updated2", "result_updated2")
                                runOnUiThread(java.lang.Runnable {


                                    dialog.dismiss()
                                    showDialog(this, result["msg"].asString)

                                })
                            }
                            Log.d("result_okok", result.toString())
                        }
                } catch (x: Exception) {
                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE
                    Log.d("result_error", x.toString())
                }

            }).start()
        }
        dialog.setCancelable(true)
        dialog.show()
    }
}

    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    fun getItem(position: Int): PaymentPlan {
        return PaymentPlan[position]
    }

}