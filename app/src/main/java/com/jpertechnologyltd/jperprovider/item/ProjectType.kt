package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R

class ProjectType(var context: Context, var id: Int, var name: String) {
    constructor(context: Context,id: Int) : this(context, id,"")

    companion object {
        var projecttypes : HashMap< Int,String> = HashMap<Int,String> ()

    }

    init {
        this.context=context

        projecttypes.put(1,context.getString(R.string.Project_type1))
        projecttypes.put(2,context.getString(R.string.Project_type2))
        projecttypes.put(3,context.getString(R.string.Project_type3))

        this.id=id
        name = projecttypes[id].toString()

    }
}
