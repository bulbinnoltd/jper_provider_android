package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R

class ProjectCategory(var context: Context, var id: Int, var name: String) {
    constructor(context: Context,id: Int) : this(context, id,"")

    companion object {
        var projectcategory : HashMap< Int,String> = HashMap<Int,String> ()

    }

    init {
        this.context=context

        projectcategory.put(1,context.getString(R.string.Create_partial_cat1))
        projectcategory.put(2,context.getString(R.string.Create_partial_cat2))
        projectcategory.put(3,context.getString(R.string.Create_partial_cat3))
        projectcategory.put(4,context.getString(R.string.Create_partial_cat4))
        projectcategory.put(5,context.getString(R.string.Create_partial_cat5))
        projectcategory.put(6,context.getString(R.string.Create_partial_cat6))
        projectcategory.put(7,context.getString(R.string.Create_partial_cat7))
        projectcategory.put(8,context.getString(R.string.Create_partial_cat8))
        projectcategory.put(9,context.getString(R.string.Create_partial_cat9))
        projectcategory.put(10,context.getString(R.string.Create_partial_cat10))
        projectcategory.put(11,context.getString(R.string.Create_partial_cat11))


        this.id=id
        name = projectcategory[id].toString()

    }
}
