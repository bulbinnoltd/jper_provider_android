package com.jpertechnologyltd.jperprovider.item.myproject

import com.jpertechnologyltd.jperprovider.item.FileDownload

class MyProjectSummary(
    val id: Int,
    val user_id: Int,
    val status_id: Int,
    val district_id: Int,
    val district: String,
    val scope_id: Int,
    val scope: String,
    val type_id: Int,
    val type: String,
    val style_id: Int,
    val style_name_en: String,
    val style_name_zh: String,
    val style_img: String,
    val start_at: String,
    val completed_at: String,
    val matched_date: String,
    val complete_date: String,
    val post_date: String,
    val bg_accepted: Int,
    val description: String,
    val address: String,
    val area: String,
    val budget_max: String,
    val max: String,
    val min: String,
    val contract_m: String,
    val categories: ArrayList<Int>,
    val docs: ArrayList<FileDownload>,
    val notes: ArrayList<String>,
    val contacts: ArrayList<MyProjectContacts>,
    val quote_id:Int,
    val quote_project_id:Int,
    val quote_status_id:Int
){
}