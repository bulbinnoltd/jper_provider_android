package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import android.graphics.Color
import com.jpertechnologyltd.jperprovider.R

class QuoteStatus(var context: Context, var id: Int, var name: String, var rgb: Int) {
    constructor(context: Context,id: Int) : this(context, id,"",Color.rgb(118,203,196))

    companion object {
        var projectscopes : HashMap< Int,String> = HashMap<Int,String> ()
        var projectscopescolor : HashMap< Int,Int> = HashMap<Int,Int> ()

    }

    init {
        this.context=context

        projectscopes.put(1,context.getString(R.string.Quote_status_pending))
        projectscopes.put(2,context.getString(R.string.Quote_status_cancel))
        projectscopes.put(3,context.getString(R.string.Quote_status_expire))
        projectscopes.put(4,context.getString(R.string.Quote_status_match))
        projectscopes.put(5,context.getString(R.string.Quote_status_draft))

        projectscopescolor.put(1,Color.rgb(	143,189,127)) //pending green
        projectscopescolor.put(2,Color.rgb(225,84,84)) //cancel
        projectscopescolor.put(3,Color.rgb(130,128,128)) //expired
        projectscopescolor.put(4,Color.rgb(85,107,47)) //matched //dark green
        projectscopescolor.put(5,Color.rgb(	251, 171, 4)) //draft


        this.id=id
        name = projectscopes[id].toString()
        rgb = projectscopescolor[id]!!

    }
}
