package com.jpertechnologyltd.jperprovider.item

class NewContractItem(
    val id:Int,
    val project_id: Int,
    val item: String,
    val u_price: String,
    val qty :String,
    val amount : String,
    val complete_date: String,
    val remark: String,
    val created_at: String,
    val updated_at: String){


}