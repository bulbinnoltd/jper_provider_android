package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import android.graphics.Color
import com.jpertechnologyltd.jperprovider.R

class ContractItemStatus(var context: Context, var id: Int, var name: String, var rgb: Int) {
    constructor(context: Context,id: Int) : this(context, id,"", Color.rgb(118,203,196))

    companion object {
        var projectscopes : HashMap< Int,String> = HashMap<Int,String> ()
        var projectscopescolor : HashMap< Int,Int> = HashMap<Int,Int> ()

    }

    init {
        this.context=context

        projectscopes.put(1,context.getString(R.string.contract_item_status1))
        projectscopes.put(2,context.getString(R.string.contract_item_status2))
        projectscopes.put(3,context.getString(R.string.contract_item_status3))
        projectscopes.put(4,context.getString(R.string.contract_item_status4))

        projectscopescolor.put(1, Color.rgb(225, 84, 84))
        projectscopescolor.put(2, Color.rgb(230,126,34))
        projectscopescolor.put(3, Color.rgb(247, 130, 5))
        projectscopescolor.put(4,Color.rgb(143, 189, 127))

        this.id=id
        name = projectscopes[id].toString()
        rgb = projectscopescolor[id]!!

    }
}