package com.jpertechnologyltd.jperprovider.item.myproject

class MyProjectQuote(
    val id: Int,
    val company_id: Int,
    val last_update_by: String,
    val status_id: Int,
    val available_at: String,
    val contract_sum: String,
    val max: String,
    val min: String

){
    class MyProjectQuoteDetail(
        val id: Int,
        val quote_id: Int,
        val description: String,
        val min_amount: String,
        val max_amount: String,
        val final_amount: String,
        val remarks: String,
        val status_id: Int
    )
}