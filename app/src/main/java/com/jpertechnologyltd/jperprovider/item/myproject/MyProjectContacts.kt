package com.jpertechnologyltd.jperprovider.item.myproject

class MyProjectContacts(
    val user_id:Int,
    val contact_name: String,
    val contact_no: String,
    val contact_method: Int
){
}