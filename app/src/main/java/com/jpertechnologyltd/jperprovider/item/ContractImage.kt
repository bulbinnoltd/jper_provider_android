package com.jpertechnologyltd.jperprovider.item

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import android.util.Log

class ContractImage(var id: Int, var budget_id: Int, var image_link: String?, var date: String?) :
Parcelable {
    constructor(parcel: Parcel) : this(
    parcel.readInt(),
    parcel.readInt(),
    parcel.readString(),
    parcel.readString()
    ){
    }
    override fun writeToParcel(dest: Parcel?, flags: Int) {
        Log.d("flag",flags.toString())
    }

    override fun describeContents(): Int {
        Log.d("flag","")
        return 0
    }
    private fun readFromParcel(parcel: Parcel){
        id = parcel.readInt()
        image_link = parcel.readString()
        date = parcel.readString()
    }
    companion object CREATOR : Parcelable.Creator<ContractImage> {
        override fun createFromParcel(parcel: Parcel): ContractImage {
            return ContractImage(parcel)
        }

        override fun newArray(size: Int): Array<ContractImage?> {
            return arrayOfNulls(size)
        }
    }
}