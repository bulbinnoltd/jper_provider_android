package com.jpertechnologyltd.jperprovider.item

data class OnBoarding (
    val title: String,
    val description: String,
    val icon: Int
)