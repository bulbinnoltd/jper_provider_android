package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R

class ProjectScope(var context: Context, var id: Int, var name: String) {
    constructor(context: Context,id: Int) : this(context, id,"")

    companion object {
        var projectscopes : HashMap< Int,String> = HashMap<Int,String> ()

    }

    init {
        this.context=context

        projectscopes.put(1,context.getString(R.string.full_details_title))
        projectscopes.put(2,context.getString(R.string.partial_details_title))

        this.id=id
        name = projectscopes[id].toString()

    }
}
