package com.jpertechnologyltd.jperprovider.item

class QuoteProject (
    val id: Int,
    val district: String,
    val scope: String,
    val type: String,
    val style_id: Int,
    val created_at: String,
    val style_name_en: String,
    val style_name_zh: String,
    val style_img:String,
    val area:String,
    val budget_max: String){
}