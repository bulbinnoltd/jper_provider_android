package com.jpertechnologyltd.jperprovider.item.myproject

class PaymentPlanNotice(

    val id: Int,
    val scope: Int,
    val payment_title:String,
    val amount: String,
    val retention_amount: String,
    val plan_amount: String,
    val paid_to_date: String,
    val due_date: String,
    val ref_no: String,
    val issued_date: String,
    val company_name_en: String,
    val company_name_zh : String,
    val company_tel : String,
    val company_email : String,
    val company_website : String,
    val company_address : String,
    val bank_code : String,
    val bank_ac : String,
    val owner_username : String,
    val owner_email : String

)
{

}