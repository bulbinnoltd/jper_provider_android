package com.jper.jperowner.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R

class PaymentChannel_id(var context: Context, var name: String, var id: Int) {
    constructor(context: Context,name: String) : this(context, name,1)

    companion object {
        var paymentchannels : HashMap< String,Int> = HashMap<String,Int> ()

    }

    init {
        this.context=context

        paymentchannels.put(context.getString(R.string.PaymentChannel_1),1)
        paymentchannels.put(context.getString(R.string.PaymentChannel_2),2)
        paymentchannels.put(context.getString(R.string.PaymentChannel_3),3)
        paymentchannels.put(context.getString(R.string.PaymentChannel_4),4)
        paymentchannels.put(context.getString(R.string.PaymentChannel_5),5)
        paymentchannels.put(context.getString(R.string.PaymentChannel_6),6)
        paymentchannels.put(context.getString(R.string.PaymentChannel_7),7)
        paymentchannels.put(context.getString(R.string.PaymentChannel_8),8)
        paymentchannels.put(context.getString(R.string.PaymentChannel_9),9)


        this.name=name
        id = paymentchannels[name]!!

    }
}
