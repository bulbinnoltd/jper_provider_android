package com.jpertechnologyltd.jperprovider.item

class NewPaymentPlan(
    val id:Int,
    val project_id: Int,
    val phase:Int,
    val title: String,
    val percentage: String,
    val amount:String,
    val status_id:Int,
    val receipt_name:String,
    val due_date:String,
    val is_final:Int,
    val remarks: String,
    val created_at: String,
    val updated_at: String,
    val receipt: String,
    val receipt_m: String,
    val notice: ArrayList<NewPaymentPlan>,
    val company: ArrayList<CompanyBriefProfile>,
    val owner: ArrayList<Profile>,
    val owner_email:String
)
{


}