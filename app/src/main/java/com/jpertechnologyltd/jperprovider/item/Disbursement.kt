package com.jpertechnologyltd.jperprovider.item

class Disbursement(
    val id:Int,
    val type: Int,
    val project_id: Int,
    val status_id:Int,
    val init_amount:String,
    val init_date:String,
    val transaction_fee:String,
    val transaction_fee_deducted:String,
    val commission_amount:String,
    val commission_deducted: String,
    val release_amount: String,
    val released_date: String,
    val title: String
) {


}