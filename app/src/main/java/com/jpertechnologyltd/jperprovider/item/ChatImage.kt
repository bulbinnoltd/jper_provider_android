package com.jpertechnologyltd.jperprovider.item
import android.net.Uri

class ChatImage(val type: Int, var path: String?, var fileUri: Uri?) {
    //type=0 ; upload from user while texting
    //type=1 ; crawl from server

}