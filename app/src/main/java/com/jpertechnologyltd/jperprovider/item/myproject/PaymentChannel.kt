package com.jper.jperowner.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R


class PaymentChannel(var context: Context, var name: String, var id: Int) {
    constructor(context: Context,id: Int) : this(context, "",id)

    companion object {
        var paymentchannels_name : HashMap< Int,String> = HashMap<Int,String> ()

    }

    init {
        this.context=context


        paymentchannels_name.put(1,context.getString(R.string.PaymentChannel_1))
        paymentchannels_name.put(2,context.getString(R.string.PaymentChannel_2))
        paymentchannels_name.put(3,context.getString(R.string.PaymentChannel_3))
        paymentchannels_name.put(4,context.getString(R.string.PaymentChannel_4))
        paymentchannels_name.put(5,context.getString(R.string.PaymentChannel_5))
        paymentchannels_name.put(6,context.getString(R.string.PaymentChannel_6))
        paymentchannels_name.put(7,context.getString(R.string.PaymentChannel_7))
        paymentchannels_name.put(8,context.getString(R.string.PaymentChannel_8))
        paymentchannels_name.put(9,context.getString(R.string.PaymentChannel_9))


        this.id = id
        name= paymentchannels_name[id]!!
    }
}
