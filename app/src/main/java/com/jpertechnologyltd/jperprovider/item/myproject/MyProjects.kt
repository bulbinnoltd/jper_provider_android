package com.jpertechnologyltd.jperprovider.item.myproject

import com.jpertechnologyltd.jperprovider.item.mybids.MyProjectsDocs
import com.jpertechnologyltd.jperprovider.item.mybids.MyProjectsNotes
import com.jpertechnologyltd.jperprovider.item.mybids.MyProjectsQuote

class MyProjects (
    val id: Int,
    val project_id: Int,
    val company_id: Int,
    val last_update_by: String,
    val status_id: Int,
    val available_at: String,
    val contract_sum: String,
    val max: String,
    val min: String,
    val user_id: Int,
    val district_id: Int,
    val district: String,
    val scope_id: Int,
    val scope: String,
    val type_id: Int,
    val type: String,
    val style_id: Int,
    val start_at: String,
    val completed_at: String,
    val matched_date: String,
    val complete_date: String,
    val post_date: String,
    val bg_accepted: Int,
    val description: String,
    val address: String,
    val area: String,
    val budget_max: String,
    val contract_m: String,
    val docs: ArrayList<MyProjectsDocs>,
    val notes: ArrayList<MyProjectsNotes>,
    val category: ArrayList<Int>,
    val quote: MyProjectsQuote,
    val owner_name: String
){
}