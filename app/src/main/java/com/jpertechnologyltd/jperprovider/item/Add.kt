package com.jpertechnologyltd.jperprovider.item

import android.os.Parcel
import android.os.Parcelable
import android.util.Log

class Add (var id: Int,var description: String, var min_amount: Float, var max_amount: Float, var remarks: String) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readString()!!
    ){
}
    override fun writeToParcel(dest: Parcel?, flags: Int) {
        Log.d("flag",flags.toString())
    }

    override fun describeContents(): Int {
        Log.d("flag","")
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Add> {
        override fun createFromParcel(parcel: Parcel): Add {
            return Add(parcel)
        }

        override fun newArray(size: Int): Array<Add?> {
            return arrayOfNulls(size)
        }
    }
}