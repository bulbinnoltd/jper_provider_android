package com.jpertechnologyltd.jperprovider.item

class QuoteProjectSummary (
    val project_id: Int,
    val district: String,
    val type: String,
    val scope: String,
    val budget_max: String,
    val area: String,
    val start_at: String?,
    val completed_at: String?,
    val address: String?,
    val description: String?,
    val style_id: Int,
    val style_name_en: String,
    val style_name_zh: String,
    val style_img: String,
    val url:String?,
    val category_array: ArrayList<Int>,
    val note_array: ArrayList<String>,
    val docs_array: ArrayList<FileDownload>,
    val fixed_items_array: ArrayList<Add>

){
}