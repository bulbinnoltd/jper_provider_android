package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import android.graphics.Color
import com.jpertechnologyltd.jperprovider.R

class RetentionStatus(var context: Context, var id: Int, var name: String, var rgb: Int) {
    constructor(context: Context,id: Int) : this(context, id,"",Color.rgb(118,203,196))

    companion object {
        var retentions : HashMap< Int,String> = HashMap<Int,String> ()
        var retention_color : HashMap< Int,Int> = HashMap<Int,Int> ()

    }

    init {
        this.context=context

        retentions.put(1,context.getString(R.string.Retention_status_init))
        retentions.put(2,context.getString(R.string.Retention_status_pending))
        retentions.put(3,context.getString(R.string.Retention_status_paid))
        retentions.put(4,context.getString(R.string.Retention_status_allocted))
        retentions.put(5,context.getString(R.string.Retention_status_released))


        retention_color.put(1,Color.rgb(255,0,0))
        retention_color.put(2,Color.rgb(230,126,34))
        retention_color.put(3,Color.rgb(151, 170, 128))
        retention_color.put(4,Color.rgb(255,152,0))
        retention_color.put(5,Color.rgb(217, 100, 89))


        this.id=id
        name = retentions[id].toString()
        rgb = retention_color[id]!!

    }
}
