package com.jpertechnologyltd.jperprovider.item

class PaymentPlan(
    val id:Int,
    val project_id: Int,
    val title: String,
    val amount:String,
    val payment_status_id:Int,
    val status_id:Int,
    val status:String,
    val status_rgb:Int,
    val created_at:String,
    val remarks: String,
    val img_name: String,
    val ref_no: String,
    val company_name_en: String,
    val company_name_zh: String,
    val company_address: String,
    val notice_id:Int?,
    val is_final:Boolean,
    val is_retention:Boolean,
    val owner_name:String,
    val owner_email:String,
    val phase: Int,     //!!!


    ) {


}