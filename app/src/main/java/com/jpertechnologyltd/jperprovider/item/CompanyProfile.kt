package com.jpertechnologyltd.jperprovider.item

class CompanyProfile(val id: Int?, val company_name_en: String?, val company_name_zh: String?,  val jper_comment: String?, val logo: String?, val about: String?, val address: String?, val website: String?, val rate_count:Int,val avg_rate: Float?, val communication_rate: Float?, val punctuality_rate: Float?,val quality_rate: Float?,val portfolios:ArrayList<Portfolio>,val comments:ArrayList<Comment>) {

}