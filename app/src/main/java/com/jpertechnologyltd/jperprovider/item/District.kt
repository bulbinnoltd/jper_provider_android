package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R

class District(var context: Context, var id: Int, var name: String) {
    constructor(context: Context,id: Int) : this(context, id,"")

    companion object {
        var districts : HashMap< Int,String> = HashMap<Int,String> ()

    }

    init {
        this.context=context

        districts.put(1,context.getString(R.string.dist_1))
        districts.put(2,context.getString(R.string.dist_2))
        districts.put(3,context.getString(R.string.dist_3))
        districts.put(4,context.getString(R.string.dist_4))
        districts.put(5, context.getString(R.string.dist_5))
        districts.put(6,context.getString(R.string.dist_6))
        districts.put(7,context.getString(R.string.dist_7))
        districts.put(8,context.getString(R.string.dist_8))
        districts.put(9, context.getString(R.string.dist_9))
        districts.put(10, context.getString(R.string.dist_10))
        districts.put(11,context.getString(R.string.dist_11))
        districts.put(12,context.getString(R.string.dist_12))
        districts.put(13, context.getString(R.string.dist_13))
        districts.put(14, context.getString(R.string.dist_14))
        districts.put(15,context.getString(R.string.dist_15))
        districts.put(16, context.getString(R.string.dist_16))
        districts.put(17, context.getString(R.string.dist_17))
        districts.put(18, context.getString(R.string.dist_18))

        this.id=id
        name = districts[id].toString()

    }
}
