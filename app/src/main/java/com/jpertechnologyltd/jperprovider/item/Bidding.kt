package com.jpertechnologyltd.jperprovider.item

class Bidding (
    val id: Int,
    val district_id: Int,
    val district: String,
    val scope_id: Int,
    val scope: String,
    val type_id: Int,
    val type: String,
    val style_id: Int,
    val created_at: String,
    val start_at: String,
    val completed_at: String,
    val post_date: String,
    val area: String,
    val budget_max: String,
    val project_id: Int,
    val contract_m: String,
    val style:Style,
    val cat_cover:String,
    val category:ArrayList<Int>?,
    val nominated:Boolean
    ){
}