package com.jpertechnologyltd.jperprovider.item

class ContractItem(
    val id: Int,
    val title: String,
    val status_id: Int,
    val u_price: String,
    val amount: String,
    val qty: String,
    val remark : String,
    val complete_date: String,
    val Images: ArrayList<ContractImage>) {


}