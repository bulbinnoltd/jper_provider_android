package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import android.graphics.Color
import com.jpertechnologyltd.jperprovider.R

class ProjectStatus(var context: Context, var id: Int, var name: String, var rgb: Int) {
    constructor(context: Context,id: Int) : this(context, id,"",Color.rgb(118,203,196))

    companion object {
        var projectscopes : HashMap< Int,String> = HashMap<Int,String> ()
        var projectscopescolor : HashMap< Int,Int> = HashMap<Int,Int> ()

    }

    init {
        this.context=context

        projectscopes.put(1,context.getString(R.string.Project_status_active))
        projectscopes.put(2,context.getString(R.string.Project_status_inprogress))
        projectscopes.put(3,context.getString(R.string.Project_status_completed))
        projectscopes.put(4,context.getString(R.string.Project_status_pending))
        projectscopes.put(5,context.getString(R.string.Project_status_wait_rating))
        projectscopes.put(6,"Removed")
        projectscopes.put(7,context.getString(R.string.Project_status_quote_received))

        projectscopescolor.put(1,Color.rgb(	151, 170, 128))
        projectscopescolor.put(2,Color.rgb(247,129,5))
        projectscopescolor.put(3,Color.rgb(143,189,127))
        projectscopescolor.put(4,Color.rgb(236,112,99))
        projectscopescolor.put(5,Color.rgb(	11, 83, 148))
        projectscopescolor.put(6,Color.rgb(61,133,198))
        projectscopescolor.put(7,Color.rgb(61,133,198))


        this.id=id
        name = projectscopes[id].toString()
        rgb = projectscopescolor[id]!!

    }
}
