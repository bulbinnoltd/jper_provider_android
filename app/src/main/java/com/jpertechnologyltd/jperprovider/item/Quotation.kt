package com.jpertechnologyltd.jperprovider.item

class Quotation(
    val name: String,
    val rating: String,
    val submission_date: String,
    val amount: String,
    val earliest_date: String,
    val item: List<QuotationItem>) {


}