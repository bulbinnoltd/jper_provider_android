package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R

class DisbursementStatus(var context: Context, var id: Int, var name: String) {
    constructor(context: Context,id: Int) : this(context, id,"")

    companion object {
        var projecttypes : HashMap< Int,String> = HashMap<Int,String> ()

    }

    init {
        this.context=context

        projecttypes.put(1,context.getString(R.string.dis_status1))
        projecttypes.put(2,context.getString(R.string.dis_status2))
        projecttypes.put(3,context.getString(R.string.dis_status3))
        projecttypes.put(4,context.getString(R.string.dis_status4))
        projecttypes.put(5,context.getString(R.string.dis_status5))

        this.id=id
        name = projecttypes[id].toString()

    }
}
