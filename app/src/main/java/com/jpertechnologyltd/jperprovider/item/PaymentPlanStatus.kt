package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import android.graphics.Color
import com.jpertechnologyltd.jperprovider.R

class PaymentPlanStatus(var context: Context, var id: Int, var name: String, var rgb: Int) {
    constructor(context: Context,id: Int) : this(context, id,"",Color.rgb(118,203,196))

    companion object {
        var paymentplans : HashMap< Int,String> = HashMap<Int,String> ()
        var paymentplans_color : HashMap< Int,Int> = HashMap<Int,Int> ()

    }

    init {
        this.context=context

        paymentplans.put(1,context.getString(R.string.PaymentPlan_status_active))
        paymentplans.put(2,context.getString(R.string.PaymentPlan_status_issued))
        paymentplans.put(3,context.getString(R.string.PaymentPlan_status_deleted))
        paymentplans.put(4,context.getString(R.string.PaymentPlan_status_paid))

        paymentplans_color.put(1,Color.rgb(225, 84, 84)) //red
        paymentplans_color.put(2,Color.rgb(247, 130, 5)) //orange
        paymentplans_color.put(3,Color.rgb(224,67,30))
        paymentplans_color.put(4,Color.rgb(143, 189, 127))//green

        this.id=id
        name = paymentplans[id].toString()
        rgb = paymentplans_color[id]!!

    }
}
