package com.jpertechnologyltd.jperprovider.item

class QuotationDetail(
    val id: Int,
    val status_id: Int,
    val max: Float,
    val min: Float,
    val created_at: String,
    val available_at: String,
    val attachment_m: String,
    val item: ArrayList<Add>) {


}