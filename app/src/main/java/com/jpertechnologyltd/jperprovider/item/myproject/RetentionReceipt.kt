package com.jpertechnologyltd.jperprovider.item.myproject

class RetentionReceipt(
    val username: String,
    val issued_date: String,
    val title: String,
    val method: Int,
    val amount: String,
    val date: String,
    val project_id: String,
    val channel: String,
    val proof : String)
{

}