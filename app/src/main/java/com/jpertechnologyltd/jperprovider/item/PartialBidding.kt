package com.jpertechnologyltd.jperprovider.item

class PartialBidding (
    val id: Int,
    val district_id: Int,
    val scope: Int,
    val type: Int,
    val style_id: Int,
    val created_at: String,
    val style_name_en: String,
    val style_name_zh: String,
    val style_img:String,
    val area:String,
    val budget_max: String){
}