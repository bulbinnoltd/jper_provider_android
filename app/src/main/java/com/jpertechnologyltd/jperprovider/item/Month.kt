package com.jpertechnologyltd.jperprovider.item

import android.content.Context
import com.jpertechnologyltd.jperprovider.R

class Month(var context: Context, var id: Int, var name: String) {
    constructor(context: Context,id: Int) : this(context, id,"")

    companion object {
        var months : HashMap< Int,String> = HashMap<Int,String> ()

    }

    init {
        this.context=context

        months.put(1,context.getString(R.string.Jan))
        months.put(2,context.getString(R.string.Feb))
        months.put(3,context.getString(R.string.Mar))
        months.put(4,context.getString(R.string.Apr))
        months.put(5, context.getString(R.string.May))
        months.put(6,context.getString(R.string.Jun))
        months.put(7,context.getString(R.string.Jul))
        months.put(8,context.getString(R.string.Aug))
        months.put(9, context.getString(R.string.Sep))
        months.put(10, context.getString(R.string.Oct))
        months.put(11,context.getString(R.string.Nov))
        months.put(12,context.getString(R.string.Dec))

        this.id=id
        name = months[id].toString()

    }
}
