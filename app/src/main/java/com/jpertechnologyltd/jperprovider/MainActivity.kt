package com.jpertechnologyltd.jperprovider

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.ui.AppBarConfiguration
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetChatUnread
import com.jpertechnologyltd.jperprovider.ui.activity.ActivityFragment
import com.jpertechnologyltd.jperprovider.ui.chat.ChatRoomFragment
import com.jpertechnologyltd.jperprovider.ui.mybids.MybidsFragment
import com.jpertechnologyltd.jperprovider.ui.mybids.MybidsQuoteFragment
import com.jpertechnologyltd.jperprovider.ui.myproject.MyProjectsFragment
import com.jpertechnologyltd.jperprovider.ui.myproject.MyProjectsQuotedDetailFragment
import com.jpertechnologyltd.jperprovider.ui.profile.ProfileFragment
import com.jpertechnologyltd.jperprovider.ui.quoteproject.QuoteProjectFragment
import com.jpertechnologyltd.jperprovider.ui.quoteproject.QuoteSubmissionFragment
import org.json.JSONException


class MainActivity : AppCompatActivity() {

    var bg_accepted = 0
    var project_id = 0
    var status_id = 0
    var contract_sum = "0"
    var post_date = "N/A"
    var scope_id = 0
    var contract_m = ""

    companion object {
        const val PARAM_NAVIGATION_ID = "navigation_id"

        fun newInstance(context: Context, navigationId: Int) =  Intent(
            context,
            MainActivity::class.java
        ).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            putExtra(PARAM_NAVIGATION_ID, navigationId)
        }

    }
    var navView: BottomNavigationView?= null
    var lang: String?= "en"

    private lateinit var broadcastReceiver: BroadcastReceiver
    private var header : String? = null
    var navigationId:Int? = 0
    var fragment_id :Int? = null

    lateinit var appBarConfiguration:AppBarConfiguration
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        loadFragment(item.itemId)
        true
    }
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        val extras = intent!!.extras
        if (extras != null) {
            val fragment_id = extras.getInt("fragment_id")
            if (fragment_id == R.id.navigation_myprojects_detail) {
                val bundle = Bundle()

                bundle.putInt("bg_accepted", bg_accepted)
                bundle.putInt("project_id", project_id)
                bundle.putInt("status_id", status_id)
                bundle.putString("contract_sum", contract_sum)
                bundle.putString("post_date", post_date)
                bundle.putString("contract_m", contract_m)
                bundle.putInt("scope_id", scope_id)


                val MyProjectsQuotedDetailFragment = MyProjectsQuotedDetailFragment()
                MyProjectsQuotedDetailFragment.setArguments(bundle)

                Log.d("bg_acceptedinMain2",bg_accepted.toString())
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment, MyProjectsQuotedDetailFragment, fragment_id.toString())
                    .commit()
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "")
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
         navView = findViewById(R.id.nav_view)
//        val badgeCount = 2
//        ShortcutBadger.applyCount(this, badgeCount)

//        val badgeDrawable = navView.getBadge(R.id.navigation_profile)
//        if (badgeDrawable != null) {
//            badgeDrawable.isVisible = false
//            badgeDrawable.clearNumber()
//        }
        fragment_id=intent.getIntExtra("fragment_id", 0)

        navigationId = intent.getIntExtra("navigation_id", R.id.navigation_quoteproject)


        if(fragment_id==R.id.navigation_myprojects_detail){

            try {
                bg_accepted = intent.getIntExtra("bg_accepted", 0)
                project_id = intent.getIntExtra("project_id", 0)
                status_id = intent.getIntExtra("status_id", 0)
                contract_sum = intent.getStringExtra("contract_sum")!!
                post_date =  intent.getStringExtra("post_date")!!
                scope_id =  intent.getIntExtra("scope_id", 0)
                contract_m =intent.getStringExtra("contract_m")!!


            } catch (err: JSONException) {
                Log.d("Error", err.toString())
            }
        }
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_quoteproject,
                R.id.navigation_myprojects,
                R.id.navigation_chat,
                R.id.navigation_myprojects
            )
        )


        val navView2:BottomNavigationView = findViewById(R.id.nav_view)
        navView2.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        // loadFragment(navigation.selectedItemId)
        // loadFragment(R.id.navigation_dashboard)

        // loadFragment(R.id.navigation_dashboard)
        if(navigationId!=null) {
            navView2.selectedItemId = navigationId!!
            if(fragment_id ==0)
                loadFragment(navigationId!!)
            else
                loadFragment(fragment_id!!)
        }

        Thread(Runnable {

            val noti_num = GetChatUnread(
                this,
                "https://uat.jper.com.hk/api/my/message/unread/count",
                header,
                lang
            )

            // try to touch View of UI thread

            navView2.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

            this.runOnUiThread(java.lang.Runnable {
                onFabolousEvent(noti_num)

            })
        }).start()
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1 != null) {
                    handleBroadcastActions(p1)
                }
            }

        }
    }
    //    private fun addFragment(f: Fragment) {
//        val transaction = supportFragmentManager.beginTransaction()
//        transaction.add(R.id.nav_host_fragment, f)
//        transaction.commit()
//    }
//
//    private fun replaceFragment(f : Fragment){
//        val transaction = supportFragmentManager.beginTransaction()
//        transaction.replace(R.id.nav_host_fragment, f)
//        transaction.commit()
//    }
    fun switchContent(id: Int, fragment: Fragment): Unit {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(id, fragment, fragment.toString())
        ft.addToBackStack(null)
        ft.commit()
    }

    private fun loadFragment(itemId: Int) {
        val tag = itemId.toString()
        var fragment = supportFragmentManager.findFragmentByTag(tag) ?: when (itemId) {
            R.id.navigation_quoteproject -> {
                QuoteProjectFragment()
            }
            R.id.navigation_mybids -> {
                MybidsFragment()
            }

            R.id.navigation_chat -> {
                ChatRoomFragment()
            }
            R.id.navigation_myprojects -> {
                MyProjectsFragment()
            }
            R.id.navigation_profile -> {
                ProfileFragment()
            }
            R.id.activity_notification -> {
                val bundle = Bundle()
                var company_name_en = intent.getStringExtra("company_name_en")
                var company_name_zh = intent.getStringExtra("company_name_zh")
                var company_logo = intent.getStringExtra("company_logo")
                var company_avg_score = intent.getFloatExtra("company_avg_score", 0F)
                var company_id = intent.getIntExtra("company_id", 0)
                var key = intent.getStringExtra("key")


                bundle.putInt("company_id", company_id!!)
                bundle.putString("company_name_en", company_name_en!!)
                bundle.putString("company_name_zh", company_name_zh!!)
                bundle.putString("company_logo", company_logo!!)
                bundle.putString("key", key!!)
                bundle.putFloat("company_avg_score", company_avg_score!!)


                val ActivityFragment = ActivityFragment()
                ActivityFragment.setArguments(bundle)
                ActivityFragment
            }

            R.id.navigation_myprojects_detail -> {


//                    Activity activity = (AppCompatActivity) arg0.getContext();
//
//                    Intent intent = new Intent(activity, MyProjectDetailActivity.class);
//                    intent.putExtra("project_id",singleMyProject.getProject_id());
//                    intent.putExtra("status_id",singleMyProject.getStatus_id());
//
//                    context.startActivity(intent);


                val bundle = Bundle()

                bundle.putInt("bg_accepted", bg_accepted)
                bundle.putInt("project_id", project_id)
                bundle.putInt("status_id", status_id)
                bundle.putString("contract_sum", contract_sum)
                bundle.putString("post_date", post_date)
                bundle.putString("contract_m", contract_m)
                bundle.putInt("scope_id", scope_id)


                val MyProjectsQuotedDetailFragment = MyProjectsQuotedDetailFragment()
                MyProjectsQuotedDetailFragment.setArguments(bundle)
                MyProjectsQuotedDetailFragment
            }
            else -> {
                null
            }
        }

        // replace fragment
        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, fragment, tag)
                .commit()
        }
    }
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount != 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
    fun onFabolousEvent(message: Int) {
        val bundle : Bundle? = intent.extras
        val badge = navView?.getOrCreateBadge(R.id.navigation_chat)
        badge?.backgroundColor = getColor(R.color.colorJper)
        badge?.badgeTextColor = getColor(R.color.white)
        badge?.isVisible = false

        if (message > 0) {
            badge?.isVisible = true
        }
        if (badge!= null){
            badge.number= message
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        var fragment: Fragment? = null
        if(requestCode== 0x001)
            fragment= supportFragmentManager.findFragmentById(QuoteSubmissionFragment().id) as Fragment?
        else if(requestCode==(0x001 + 1))
            fragment= supportFragmentManager.findFragmentById(MybidsQuoteFragment().id) as Fragment?

        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, intent)
        }
    }
    private fun handleBroadcastActions(intent: Intent) {
        when (intent.action) {
            "Reload" -> {


                val navView2: BottomNavigationView = findViewById(R.id.nav_view)
                val noti_num = GetChatUnread(
                    this,
                    "https://uat.jper.com.hk/api/my/message/unread/count",
                    header,
                    lang
                )

                // try to touch View of UI thread

                navView2.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

                onFabolousEvent(noti_num)

                val extras = intent.extras
                if (extras != null) {
                    val fragment_id = extras.getInt("fragment_id")
                    if (fragment_id == R.id.navigation_myprojects_detail) {
                        val bundle = Bundle()

                        bundle.putInt("bg_accepted", bg_accepted)
                        bundle.putInt("project_id", project_id)
                        bundle.putInt("status_id", status_id)
                        bundle.putString("contract_sum", contract_sum)
                        bundle.putString("post_date", post_date)
                        bundle.putString("contract_m", contract_m)
                        bundle.putInt("scope_id", scope_id)


                        val MyProjectsQuotedDetailFragment = MyProjectsQuotedDetailFragment()
                        MyProjectsQuotedDetailFragment.setArguments(bundle)

                        Log.d("bg_acceptedinMain",bg_accepted.toString())
                        supportFragmentManager

                            .beginTransaction()
                            .replace(R.id.nav_host_fragment, MyProjectsQuotedDetailFragment, fragment_id.toString())
                            .commit()
                    }
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver, IntentFilter("Reload"))


    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
    }
}
