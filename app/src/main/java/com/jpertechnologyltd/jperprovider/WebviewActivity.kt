package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.DownloadManager
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.webkit.*
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import java.util.*

class WebviewActivity : AppCompatActivity() {

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var header:String? = null
    var lang:String? = "en"
    var project_id:Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        /////////////////////Initiate_loading_anim///////////////////////////


        Loading!!.setVisibility(View.VISIBLE)
        progressOverlay!!.setVisibility(View.VISIBLE)

        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")

        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            super.onBackPressed()
        }

        var url =intent.getStringExtra("url")
        if(!url.contains("https") || !url.contains("http") )
            url = "https://" + url
        try{
            project_id =intent.getIntExtra("project_id",0)
        }
        catch (e:Exception){

        }
        val headers: HashMap<String, String> = HashMap<String, String>()
        headers.put("Authorization", "Bearer " +header)
        headers.put("Content-Language", lang!!)
        val myWebView: WebView = findViewById(R.id.webview)

        try{
            val webViewClient: WebViewClient = object: WebViewClient() {

                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                    view?.loadUrl(request?.url.toString())
                    return super.shouldOverrideUrlLoading(view, request)
                }

                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

                    Loading!!.setVisibility(View.VISIBLE)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                    super.onPageStarted(view, url, favicon)
                }

                override fun onPageFinished(view: WebView?, url: String?) {

                    Loading!!.setVisibility(View.GONE)
                    progressOverlay!!.setVisibility(View.GONE)
                    super.onPageFinished(view, url)
                }
            }
            myWebView.webViewClient = webViewClient

            myWebView.settings.javaScriptEnabled = true
            myWebView.settings.defaultTextEncodingName = "utf-8"
            myWebView.settings.useWideViewPort = true
            myWebView.settings.loadWithOverviewMode = true
            myWebView.settings.setSupportZoom(true)
            myWebView.settings.builtInZoomControls = true
            myWebView.settings.displayZoomControls = false
            myWebView.loadUrl(url,headers)

            Log.d("url_link",url.toString())

            if(url.contains(".pdf")) {
                myWebView.setDownloadListener { url, userAgent, contentDisposition, mimeType, _ ->
                    val request = DownloadManager.Request(Uri.parse(url))
                    request.addRequestHeader("Authorization", "Bearer ".plus(header))
                    request.setMimeType(mimeType)
                    request.addRequestHeader("cookie", CookieManager.getInstance().getCookie(url))
                    request.addRequestHeader("User-Agent", userAgent)
                    request.setDescription("Downloading file...")
                    request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType))
                    request.allowScanningByMediaScanner()
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    request.setDestinationInExternalFilesDir(
                        this,
                        Environment.DIRECTORY_DOWNLOADS,
                        "project_".plus(project_id).plus("contract")
                    )
                    val dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
                    dm.enqueue(request)
                    Toast.makeText(applicationContext, "Downloading File", Toast.LENGTH_LONG).show()
                    super.onBackPressed()
                }
            }
        }
        catch (e : Exception){
            Log.d("url_error",e.toString())
            Loading!!.setVisibility(View.GONE)
            progressOverlay!!.setVisibility(View.GONE)
        }
    }
}

