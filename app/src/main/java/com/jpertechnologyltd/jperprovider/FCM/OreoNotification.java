package com.jpertechnologyltd.jperprovider.FCM;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.os.Build;

import androidx.core.text.HtmlCompat;

import com.jpertechnologyltd.jperprovider.R;

public class OreoNotification extends ContextWrapper {

    private static final String CHANNEL_ID = "Default";
    private static final String CHANNEL_NAME = "Default channel";
    private NotificationManager notificationManager;

    public OreoNotification(Context base) {
        super(base);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        getManager().createNotificationChannel(channel);
//        NotificationChannel channel;
//        channel = new NotificationChannel(CHANNEL_ID,
//                CHANNEL_NAME,  NotificationManager.IMPORTANCE_HIGH);
//        channel.setDescription("Fcm Test channel for app test FCM");
//        channel.enableLights(true);
//        channel.enableVibration(true);
//        channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
//        channel.setShowBadge(false);
//        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
//    getManager().createNotificationChannel(channel);
    }

    public NotificationManager getManager(){
        if(notificationManager == null){
            notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }

        return notificationManager;
    }


    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getOreoNotification(String title, String body, PendingIntent pendingIntent){
        return  new Notification.Builder(getApplicationContext(), CHANNEL_ID)
//                .setAutoCancel(true)
//                .setWhen(System.currentTimeMillis())
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setColor(Color.parseColor("#ffc533"))
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(title)
//                .setContentText(body);
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(Color.parseColor("#8FAD7B"))
                //  .setSmallIcon(R.mipmap.ic_launcher_foreground)
                .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
                        + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                .setContentText(body).setAutoCancel(true).setContentIntent(pendingIntent);
    }
    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getOtherOreoNotification(String title, String body){
        return  new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_ALL)
                .setColor(Color.parseColor("#8FAD7B"))
                .setSmallIcon(R.mipmap.ic_launcher)
                //  .setSmallIcon(R.mipmap.ic_launcher_foreground)
                .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
                        + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                .setContentText(body).setAutoCancel(true);
    }
}
