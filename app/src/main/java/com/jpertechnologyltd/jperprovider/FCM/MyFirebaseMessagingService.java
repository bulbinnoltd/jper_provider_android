package com.jpertechnologyltd.jperprovider.FCM;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.text.HtmlCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jpertechnologyltd.jperprovider.ForgotPasswordActivity;
import com.jpertechnologyltd.jperprovider.MainActivity;
import com.jpertechnologyltd.jperprovider.MyProjectDetailNotiActivity;
import com.jpertechnologyltd.jperprovider.R;
import com.jpertechnologyltd.jperprovider.ui.chat.ChatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(String s) {
        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

// Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("FCM", "Message data payload: " + remoteMessage.getData().toString());

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("FCM", "Message data payload: " +  remoteMessage.getNotification().getBody());
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Log.d("FCM_case1", "Message data payload: " +  remoteMessage.getNotification().getBody());
                sendNotification1(remoteMessage);

            }
            else{
                Log.d("FCM_case2", "Message data payload: " +  remoteMessage.getNotification().getBody());

                sendNotification(remoteMessage);
            }


        }

    }
    @SuppressLint("LongLogTag")
    private void sendNotification(RemoteMessage remoteMessage) {
        String channelId = "Default";
//        if (!isAppIsInBackground(getApplicationContext())) {
//            //foreground app
//
//
//            Log.e("remoteMessage foreground", remoteMessage.getData().toString());
//            String title = remoteMessage.getNotification().getTitle();
//            String body = remoteMessage.getNotification().getBody();
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
//            notificationBuilder.setAutoCancel(true)
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setColor(Color.parseColor("#ffc533"))
//                    .setSmallIcon(R.drawable.ic_push_notice)
//                    .setContentTitle(title)
//                    .setContentText(body).setAutoCancel(true);
//
//            notificationManager.notify(1, notificationBuilder.build());
//        }else{
        Log.e("remoteMessage background", remoteMessage.getData().toString());
        Map data = remoteMessage.getData();
        Log.d("FCM_data",data.toString());
        try {
//                if (remoteMessage.getData().get("action").toString().equals("6")){
//
//                    String title = remoteMessage.getNotification().getTitle();
//                    String body = remoteMessage.getNotification().getBody();
//                    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//
//                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
//                    notificationBuilder.setAutoCancel(true)
//                            .setDefaults(Notification.DEFAULT_ALL)
//                            .setColor(Color.parseColor("#8FAD7B"))
//                           // .setSmallIcon(R.mipmap.ic_launcher_foreground)
//                            .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
//                                    + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
//                            .setContentText(body).setAutoCancel(true);
//                    notificationManager.notify(1, notificationBuilder.build());
//
//                    Intent broadcast = new Intent();
//                    broadcast.setAction("Reload");
//                    sendBroadcast(broadcast);
//
//                }
            if (remoteMessage.getData().get("action").toString().equals("4") || remoteMessage.getData().get("action").toString().equals("8")){ //project status updated // go to my proj

                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();
                String project = remoteMessage.getData().get("project");


                Integer bg_accepted = 0;
                Integer project_id = 0;
                Integer status_id = 0;
                String contract_sum = "0";
                String post_date = "N/A";
                Integer scope_id = 0;
                String contract_m = "";
                try {
                    JSONObject jsonobject = new JSONObject(project);

                    bg_accepted = jsonobject.getInt("bg_accepted");
                    project_id = jsonobject.getInt("id");
                    status_id = jsonobject.getInt("status_id");
                    contract_sum = "0";
                    post_date = jsonobject.getString("start_at");
                    scope_id = jsonobject.getInt("scope");
                    contract_m = jsonobject.getJSONObject("").get("contract_m").toString();
                    if (jsonobject.getJSONObject("detail").isNull("contract_name")) contract_m="";
                    Log.d("contract_name_in_noti",jsonobject.getJSONObject("detail").get("contract_name").toString());
                    Log.d("contract_m_in_noti",contract_m.toString());


                }catch (JSONException err){
                    Log.d("Error", err.toString());
                }
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                intent.putExtra("fragment_id",R.id.navigation_myprojects_detail);
                intent.putExtra("bg_accepted",bg_accepted);
                intent.putExtra("project_id",project_id);
                intent.putExtra("status_id",status_id);
                intent.putExtra("contract_sum",contract_sum);
                intent.putExtra("post_date",post_date);
                intent.putExtra("contract_m",contract_m);
                intent.putExtra("scope_id",scope_id);

                Log.d("FCM_user_data", "Message data payload: " + project_id.toString());

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 1 /* Request code */, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);


                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
                notificationBuilder.setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setColor(Color.parseColor("#8FAD7B"))
                        // .setSmallIcon(R.mipmap.ic_launcher_foreground)
                        .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
                                + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                        .setContentText(body).setAutoCancel(true)
                        .setContentIntent(pendingIntent);

                notificationManager.notify(1, notificationBuilder.build());

                Intent broadcast = new Intent();
                broadcast.setAction("Reload");
                sendBroadcast(broadcast);

            }
            else if (remoteMessage.getData().get("action").toString().equals("5")){ //chat msg noti

                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();
                String user = remoteMessage.getData().get("users");
                String key = remoteMessage.getData().get("key");
                String company_name_en = remoteMessage.getData().get("company_name_en");
                String company_name_zh = remoteMessage.getData().get("company_name_zh");

                String company_logo = "";
                try {
                    JSONArray jsonarray = new JSONArray(user);
                    Integer user_id = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                            .getInt("user_id", 0);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        if(Integer.parseInt(jsonarray.getJSONObject(i).get("user_id").toString())!=user_id){
                            company_logo = jsonarray.getJSONObject(i).get("icon").toString();
                            company_name_en = jsonarray.getJSONObject(i).get("username").toString();
                            company_name_zh = jsonarray.getJSONObject(i).get("username").toString();
                        }
                    }
                    Log.d("jsonarray.chatmsg",jsonarray.getJSONObject(0).get("user_id").toString());
                }catch (JSONException err){
                    Log.d("Error", err.toString());
                }
                Intent intent = new Intent(this, ChatActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("url", remoteMessage.getData().get("url"));
                intent.putExtra("key", key);
                intent.putExtra("company_name_en", company_name_en);
                intent.putExtra("company_name_zh", company_name_zh);
                intent.putExtra("company_logo", company_logo);
                intent.putExtra("navigation_id", R.id.activity_notification);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                try {
                    JSONArray jsonarray = new JSONArray(user);
                    Log.d("jsonarray.chatmsg",jsonarray.getJSONObject(0).get("user_id").toString());
                }catch (JSONException err){
                    Log.d("Error", err.toString());
                }

                Log.d("FCM_user_data", "Message data payload: " + user.toString());
                NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);


                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
                notificationBuilder.setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setColor(Color.parseColor("#8FAD7B"))
                        // .setSmallIcon(R.mipmap.ic_launcher_foreground)
                        .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
                                + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                        .setContentText(body).setAutoCancel(true)
                        .setContentIntent(pendingIntent);

                notificationManager.notify(1, notificationBuilder.build());

                Intent broadcast = new Intent();
                broadcast.setAction("Reload");
                sendBroadcast(broadcast);

            }
            else{


                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();
                NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
                notificationBuilder.setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setColor(Color.parseColor("#8FAD7B"))
                        // .setSmallIcon(R.mipmap.ic_launcher_foreground)
                        .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
                                + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                        .setContentText(body).setAutoCancel(true);
                notificationManager.notify(1, notificationBuilder.build());
            }
            String body = data.get("key").toString();
        }
        catch (Exception e){
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();

            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
            notificationBuilder.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setColor(Color.parseColor("#8FAD7B"))
                    //  .setSmallIcon(R.mipmap.ic_launcher_foreground)
                    .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
                            + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                    .setContentText(body).setAutoCancel(true);
            notificationManager.notify(1, notificationBuilder.build());
            Log.d("error_FCM_action",e.toString());
        }
//            Intent resultIntent = new Intent(getApplicationContext() , Homepage.class);
//            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
//                    0 /* Request code */, resultIntent,
//                    PendingIntent.FLAG_UPDATE_CURRENT);

//        }
    }

//    public static boolean isAppIsInBackground(Context context) {
//        boolean isInBackground = true;
//        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
//            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
//            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
//                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
//                    for (String activeProcess : processInfo.pkgList) {
//                        if (activeProcess.equals(context.getPackageName())) {
//                            isInBackground = false;
//                        }
//                    }
//                }
//            }
//        } else {
//
//            List taskInfo = am.getRunningTasks(1);
//            ComponentName componentInfo = taskInfo.get(0).topActivity;
//            if (componentInfo.getPackageName().equals(context.getPackageName())) {
//                isInBackground = false;
//            }
//        }
//
//        return isInBackground;
//    }

    @SuppressLint("NewApi")
    private void sendNotification1(RemoteMessage remoteMessage) {
//        if (!isAppIsInBackground(getApplicationContext())) {
//            //foreground app
//            Log.e("remoteMessage", remoteMessage.getData().toString());
//            String title = remoteMessage.getNotification().getTitle();
//            String body = remoteMessage.getNotification().getBody();
//
//            OreoNotification oreoNotification = new OreoNotification(this);
//            Notification.Builder builder = oreoNotification.getOreoNotification(title, body);
//
//            int i = 0;
//            oreoNotification.getManager().notify(i, builder.build());
//        }else{
        Log.e("remoteMessage", remoteMessage.getData().toString());

        try {
//            if (remoteMessage.getData().get("action").toString().equals("4") || remoteMessage.getData().get("action").toString().equals("6")){
//
//                String title = remoteMessage.getNotification().getTitle();
//                String body = remoteMessage.getNotification().getBody();
//                Intent broadcast = new Intent();
//                broadcast.setAction("Reload");
//                sendBroadcast(broadcast);
//                OreoNotification oreoNotification = new OreoNotification(this);
//                Notification.Builder builder = oreoNotification.getOreoNotification(title, body);
//                builder.setAutoCancel(true)
//                        .setDefaults(Notification.DEFAULT_ALL)
//                        .setColor(Color.parseColor("#8FAD7B"))
//                        //  .setSmallIcon(R.mipmap.ic_launcher_foreground)
//                        .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
//                                + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
//                        .setContentText(body).setAutoCancel(true);
//                NotificationManager notificationManager =
//                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                int i = 0;
//                oreoNotification.getManager().notify(i, builder.build());
//            }
            if (remoteMessage.getData().get("action").toString().equals("4")|| remoteMessage.getData().get("action").toString().equals("8")){ //project status updated // go to my proj
                int requestID = (int) System.currentTimeMillis();
                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();
                String project = remoteMessage.getData().get("project");


                Integer bg_accepted = 0;
                Integer project_id = 0;
                Integer status_id = 0;
                String contract_sum = "0";
                String post_date = "N/A";
                Integer scope_id = 0;
                String contract_m = "";
                try {
                    JSONObject jsonobject = new JSONObject(project);

                    bg_accepted = Integer.valueOf(jsonobject.get("bg_accepted").toString());
                    project_id =Integer.valueOf( jsonobject.get("id").toString());
                    status_id = Integer.valueOf(jsonobject.get("status_id").toString());
                    contract_sum = "0";
                    post_date = jsonobject.get("start_at").toString();
                    scope_id = Integer.valueOf(jsonobject.get("scope").toString());

                    contract_m = jsonobject.getJSONObject("detail").get("contract_m").toString();
                    if (jsonobject.getJSONObject("detail").isNull("contract_name")) contract_m="";
                    Log.d("contract_name_in_noti",jsonobject.getJSONObject("detail").get("contract_name").toString());
                    Log.d("contract_m_in_noti",contract_m.toString());
                }catch (JSONException err){
                    Log.d("Error", err.toString());
                }
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                intent.putExtra("fragment_id",R.id.navigation_myprojects_detail);
                intent.putExtra("bg_accepted",bg_accepted);
                intent.putExtra("project_id",project_id);
                intent.putExtra("status_id",status_id);
                intent.putExtra("contract_sum",contract_sum);
                intent.putExtra("post_date",post_date);
                intent.putExtra("contract_m",contract_m);
                intent.putExtra("scope_id",scope_id);
//
//                PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID /* Request code */, intent,
//                        PendingIntent.FLAG_ONE_SHOT);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID /* Request code */, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Intent broadcast = new Intent();
                broadcast.setAction("Reload");
                sendBroadcast(broadcast);
                OreoNotification oreoNotification = new OreoNotification(this);
                Notification.Builder builder = oreoNotification.getOreoNotification(title, body,pendingIntent);
//                builder.setAutoCancel(true)
//                        .setDefaults(Notification.DEFAULT_ALL)
//                        .setColor(Color.parseColor("#8FAD7B"))
//                        //  .setSmallIcon(R.mipmap.ic_launcher_foreground)
//                        .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
//                                + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
//                        .setContentText(body).setAutoCancel(true).setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int i = 0;
                oreoNotification.getManager().notify(requestID, builder.build());

            }

            else if (remoteMessage.getData().get("action").toString().equals("5")){

                String user = remoteMessage.getData().get("users");
                String key = remoteMessage.getData().get("key");
                String company_name_en = remoteMessage.getData().get("company_name_en");
                String company_name_zh = remoteMessage.getData().get("company_name_zh");
                String company_logo = "";
                try {
                    JSONArray jsonarray = new JSONArray(user);
                    Integer user_id = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                            .getInt("user_id", 0);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        if(Integer.parseInt(jsonarray.getJSONObject(i).get("user_id").toString())!=user_id){
                            company_logo = jsonarray.getJSONObject(i).get("icon").toString();
                            company_name_en = jsonarray.getJSONObject(i).get("username").toString();
                            company_name_zh = jsonarray.getJSONObject(i).get("username").toString();
                        }
                    }
                    Log.d("jsonarray.chatmsg",jsonarray.getJSONObject(0).get("user_id").toString());
                }catch (JSONException err){
                    Log.d("Error", err.toString());
                }
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("url", remoteMessage.getData().get("url"));
                intent.putExtra("key", key);
                intent.putExtra("company_logo", company_logo);
                intent.putExtra("company_name_en", company_name_en);
                intent.putExtra("company_name_zh", company_name_zh);
                intent.putExtra("navigation_id", R.id.activity_notification);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();
                Intent broadcast = new Intent();
                broadcast.setAction("Reload");
                sendBroadcast(broadcast);
                OreoNotification oreoNotification = new OreoNotification(this);
                Notification.Builder builder = oreoNotification.getOtherOreoNotification(title, body);
                builder.setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setColor(Color.parseColor("#8FAD7B"))
                        //  .setSmallIcon(R.mipmap.ic_launcher_foreground)
                        .setContentTitle( HtmlCompat.fromHtml( "<a><font color=#8FAD7B>" +  title
                                + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY))
                        .setContentText(body).setAutoCancel(true).setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int i = 0;
                oreoNotification.getManager().notify(i, builder.build());
            }
            else{

                String title = remoteMessage.getNotification().getTitle();
                String body = remoteMessage.getNotification().getBody();
                OreoNotification oreoNotification = new OreoNotification(this);
                Notification.Builder builder = oreoNotification.getOtherOreoNotification(title, body);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int i = 0;
                oreoNotification.getManager().notify(i, builder.build());
            }
            String body = remoteMessage.getData().get("key").toString();
        }
        catch (Exception e){

//            String title = remoteMessage.getNotification().getTitle();
//            String body = remoteMessage.getNotification().getBody();
//            OreoNotification oreoNotification = new OreoNotification(this);
//            Notification.Builder builder = oreoNotification.getOtherOreoNotification(title, body);
//
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            int i = 0;
//            oreoNotification.getManager().notify(i, builder.build());
            Log.d("error_FCM_action",e.toString());
        }

//        }

    }

}
