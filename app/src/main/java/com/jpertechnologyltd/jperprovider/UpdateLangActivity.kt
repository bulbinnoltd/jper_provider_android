package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import java.util.*


class UpdateLangActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {

        var Loading:ImageView? = null
        var progressOverlay: FrameLayout? = null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_lang)
        // Create a new Locale object

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        val editor = this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
        val linearLayout = findViewById<LinearLayout>(R.id.linearLayout)
        linearLayout.visibility = View.VISIBLE
        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            super.onBackPressed()
        }

        val en_button = findViewById<Button>(R.id.en_button)
        // set on-click listener

        en_button.setOnClickListener {
            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE
            var changelang = false
            Thread(Runnable {
                val languageCode = "en"
                val config = resources.configuration
                val locale = Locale(languageCode)

                editor.putString("lang", "en")
                editor.apply()

                Locale.setDefault(locale)
                config.locale = locale
                resources.updateConfiguration(config, resources.displayMetrics)



                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                changelang = true
                this.runOnUiThread(java.lang.Runnable {
                    if(changelang){
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                    }
                })
            }).start()
        }

        val zh_button = findViewById<Button>(R.id.zh_button)
        // set on-click listener

        zh_button.setOnClickListener {
            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE
            var changelang = false
            Thread(Runnable {
                val languageCode = "zh"
                val config = resources.configuration
                val locale = Locale(languageCode)

                editor.putString("lang", "zh")
                editor.apply()

                Locale.setDefault(locale)
                config.locale = locale
                resources.updateConfiguration(config, resources.displayMetrics)


                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                changelang = true
                this.runOnUiThread(java.lang.Runnable {
                    if(changelang){
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                    }
                })
            }).start()

        }
    }

}
