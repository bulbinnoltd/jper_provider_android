package com.jpertechnologyltd.jperprovider.ui.myproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.MyProjectsAdapter
import com.jpertechnologyltd.jperprovider.item.myproject.MyProjects
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener
import com.jpertechnologyltd.jperprovider.item.District
import com.jpertechnologyltd.jperprovider.item.ProjectScope
import com.jpertechnologyltd.jperprovider.item.ProjectType
import com.jpertechnologyltd.jperprovider.item.mybids.MyProjectsDocs
import com.jpertechnologyltd.jperprovider.item.mybids.MyProjectsNotes
import com.jpertechnologyltd.jperprovider.item.mybids.MyProjectsQuote
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import java.util.concurrent.Future

class MyProjectsActiveFragment : Fragment() {
    private var header : String? = null
    private var lang : String? = null
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null

    private lateinit var no_project_view: ConstraintLayout

    var projects =ArrayList<MyProjects?>()
    var moreprojects =ArrayList<MyProjects?>()
    lateinit var mMyProjectsAdapter : MyProjectsAdapter
    private lateinit var project_listview: RecyclerView

    var loading: Future<JsonObject>? = null
    private var currentpageurl: String? = null
    lateinit var Postdata : JsonArray

    private var nextpageurl : String? = null

    private lateinit var broadcastReceiver: BroadcastReceiver
    var swipeContainer: SwipeRefreshLayout?=null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_myproject_project, container, false)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////

        header = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang =context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        val context = context!!

        val myprojects_recyclerview =
            root.findViewById<View>(R.id.myprojects_listview) as RecyclerView

        no_project_view = root.findViewById(R.id.no_project_view) as ConstraintLayout

        project_listview = root.findViewById<View>(R.id.myprojects_listview) as RecyclerView

        val mLayoutManager = LinearLayoutManager(activity)

        project_listview.setLayoutManager(mLayoutManager)

        Thread(Runnable {
            try {
                // JsonObject json = new JsonObject();
                Ion.with(activity)
                    .load("https://uat.jper.com.hk/api/company/my/projects/matched")
                    .setHeader("Authorization", "Bearer " +header)
                    .setHeader("Content-Language", lang)
                    //  .setTimeout(10000)
                    .asJsonObject()
                    .setCallback(FutureCallback { e, result ->
                        if (e != null) {
                            //   Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                            return@FutureCallback
                        }
                        Postdata = result.getAsJsonArray("data")
                        nextpageurl = try {
                            if(result.get("next_page_url").isJsonNull) "" else result.getAsJsonPrimitive("next_page_url")
                                .asString
                        } catch (x: Exception) {
                            Log.d("error_jperx",x.toString())
                            null
                        }
                        projects = ArrayList<MyProjects?>()
                        currentpageurl =
                            if(result.get("current_page").isJsonNull) "" else result.getAsJsonPrimitive("current_page").asString

                        if(Postdata.size()>0){
                            Log.d("postdata",Postdata.toString())
                            for (i in 0 until Postdata.size()) {
                                try{
                                    val Project_list: JsonObject = Postdata.get(i).getAsJsonObject()
                                    var notes_list = ArrayList<MyProjectsNotes>()
                                    var docs_list = ArrayList<MyProjectsDocs>()
                                    var category_array = ArrayList<Int>()

                                    val project_obj: JsonObject? = if(Project_list["project"].isJsonNull) null else Project_list["project"].asJsonObject

                                    var id_in_data = 0
                                    var project_id_in_data = 0
                                    var company_id_in_data = 0
                                    var last_update_by_in_data = ""
                                    var status_id_in_data = 0
                                    var available_at_in_data = ""
                                    var contract_sum_in_project = ""
                                    var max_in_project = ""
                                    var min_in_project = ""
                                    var category: JsonArray? = null

                                    id_in_data = if(Project_list["id"].isJsonNull) 0 else Project_list["id"].asInt
                                    project_id_in_data = if(Project_list["project_id"].isJsonNull) 0 else Project_list["project_id"].asInt
                                    company_id_in_data = if(Project_list["company_id"].isJsonNull) 0 else Project_list["company_id"].asInt
                                    last_update_by_in_data = if(Project_list["last_update_by"].isJsonNull) "" else Project_list["last_update_by"].asString
                                    available_at_in_data = if(Project_list["available_at"].isJsonNull) "" else Project_list["available_at"].asString
                                    contract_sum_in_project = if(Project_list["contract_sum"].isJsonNull) "" else Project_list["contract_sum"].asString
                                    max_in_project = if(Project_list["max"].isJsonNull) "" else Project_list["max"].asString
                                    min_in_project = if(Project_list["min"].isJsonNull) "" else Project_list["min"].asString


                                    var id_in_project = 0
                                    var user_id = 0
                                    var district_id = 0
                                    var district = ""
                                    var status_id_in_project = 0
                                    var scope_id_in_project = 0
                                    var scope_in_project =""
                                    var type_id_in_project = 0
                                    var type_in_project = ""
                                    var style_id_in_project = 0
                                    var complete_date_in_project = ""
                                    var bg_accepted = 0
                                    var start_at_in_project = ""
                                    var completed_at_in_project = ""
                                    var matched_date_in_project = ""
                                    var post_date_in_project = ""

                                    var project_id_in_detail = 0
                                    var description_in_detail = ""
                                    var address = ""
                                    var area = ""
                                    var budget_max = ""
                                    var contract_m = ""

                                    var note_id = 0
                                    var project_id_in_note = 0
                                    var item_in_note = ""

                                    var doc_id = 0
                                    var project_id_in_doc = 0
                                    var status_id_in_doc = 0
                                    var url_in_doc =""
                                    var owner_name =""

                                    var docs: JsonArray? =null
                                    var notes: JsonArray? =null
                                    var quote_obj: JsonObject? =null
                                    if(project_obj!=null) {
                                        try{
                                            docs =
                                                if (project_obj["docs"].isJsonNull) null else project_obj["docs"].asJsonArray
                                        }
                                        catch (e:Exception){
                                        }
                                        try{
                                            notes =
                                                if (project_obj["notes"].isJsonNull) null else project_obj["notes"].asJsonArray
                                        }
                                        catch (e:Exception){
                                        }
                                        try {
                                            category =
                                                if (project_obj.get("category").isJsonNull) null else project_obj.get(
                                                    "category"
                                                ).asJsonArray
                                        } catch(e:Exception){}
                                        try{
                                            var user_obj = project_obj["user"].asJsonObject
                                            owner_name =
                                                if (user_obj["username"].isJsonNull) "N/A" else user_obj["username"].asString
                                        }
                                        catch (e:Exception){
                                        }
                                        val detail: JsonObject? =
                                            if (project_obj["detail"].isJsonNull) null else project_obj["detail"].asJsonObject
                                        status_id_in_data = if(project_obj["status_id"].isJsonNull) 0 else project_obj["status_id"].asInt

                                        id_in_project = if (project_obj["id"].isJsonNull) 0 else project_obj["id"].asInt
                                        user_id = if (project_obj["user_id"].isJsonNull) 0 else project_obj["user_id"].asInt
                                        district_id = if (project_obj["district_id"].isJsonNull) 0 else project_obj["district_id"].asInt
                                        district = District(context!!, district_id).name
                                        status_id_in_project = if (project_obj["status_id"].isJsonNull) 0 else project_obj["status_id"].asInt
                                        scope_id_in_project = if (project_obj["scope"].isJsonNull) 0 else project_obj["scope"].asInt
                                        scope_in_project = ProjectScope(context!!, scope_id_in_project).name
                                        type_id_in_project = if (project_obj["type"].isJsonNull) 0 else project_obj["type"].asInt
                                        type_in_project = ProjectType(context!!, type_id_in_project).name
                                        style_id_in_project = if (project_obj["style_id"].isJsonNull) 0 else project_obj["style_id"].asInt
                                        complete_date_in_project = if (project_obj["complete_date"].isJsonNull) "" else project_obj["complete_date"].asString
                                        bg_accepted = if (project_obj["bg_accepted"].isJsonNull) 0 else project_obj["bg_accepted"].asInt
                                        start_at_in_project = if (project_obj["start_at"].isJsonNull) "" else project_obj["start_at"].asString
                                        completed_at_in_project = if (project_obj["completed_at"].isJsonNull) "" else project_obj["completed_at"].asString
                                        matched_date_in_project = if (project_obj["matched_date"].isJsonNull) "" else project_obj["matched_date"].asString
                                        post_date_in_project = if (project_obj["post_date"].isJsonNull) "" else project_obj["post_date"].asString

                                        if(detail!=null){
                                            description_in_detail = if (detail["description"].isJsonNull) "" else detail["description"].asString
                                            address = if (detail["address"].isJsonNull) "" else detail["address"].asString
                                            area = if (detail["area"].isJsonNull) "" else detail["area"].asString.dropLast(3)
                                            budget_max = if (detail["budget_max"].isJsonNull) "" else detail["budget_max"].asString
                                            contract_m = if (detail["contract_m"].isJsonNull) "" else detail["contract_m"].asString
                                            if (detail["contract_name"].isJsonNull) contract_m=""

                                        }
                                        if(notes!=null && notes.size()>0){
                                            for (i in 0 until notes.size()){
                                                val note_obj: JsonObject = notes.get(i).getAsJsonObject()
                                                note_id = if (note_obj["id"].isJsonNull) 0 else note_obj["id"].asInt
                                                project_id_in_note = if (note_obj["project_id"].isJsonNull) 0 else note_obj["project_id"].asInt
                                                item_in_note = if (note_obj["item"].isJsonNull) "" else note_obj["item"].asString
                                                var note = MyProjectsNotes(note_id,project_id_in_note,item_in_note)
                                                notes_list.add(note)
                                            }
                                        }
                                        if(docs!=null && docs.size()>0){
                                            for (i in 0 until docs.size()){
                                                val doc_obj: JsonObject = docs.get(i).getAsJsonObject()

                                                var url_in_doc =""
                                                doc_id = if (doc_obj["id"].isJsonNull) 0 else doc_obj["id"].asInt
                                                project_id_in_doc = if (doc_obj["project_id"].isJsonNull) 0 else doc_obj["project_id"].asInt
                                                status_id_in_doc = if (doc_obj["status_id"].isJsonNull) 0 else doc_obj["status_id"].asInt
                                                url_in_doc = if (doc_obj["url"].isJsonNull) "" else doc_obj["url"].asString
                                                var doc = MyProjectsDocs(doc_id,project_id_in_doc,status_id_in_doc,url_in_doc)
                                                docs_list.add(doc)
                                            }

                                        }
                                        if (category != null && category.size() > 0) {
                                            for (x in 0 until category.size()) {
                                                category_array.add(
                                                    category.get(x).asJsonObject.get(
                                                        "id"
                                                    ).asInt
                                                )
                                            }

                                        }
                                    }

                                    try{
                                        quote_obj = if(Project_list["quote"].isJsonNull) null else Project_list["quote"].asJsonObject
                                    }
                                    catch (e:Exception){
                                    }
                                    var id_in_quote = 0
                                    var company_id_in_quote = 0
                                    var last_update_by_in_quote = ""
                                    var status_id_in_quote = 0
                                    var available_at_in_quote = ""
                                    var contract_sum_in_quote = ""
                                    var max_in_quote = ""
                                    var min_in_quote = ""
                                    var quote = MyProjectsQuote(
                                        id_in_quote,
                                        company_id_in_quote,
                                        last_update_by_in_quote,
                                        status_id_in_quote,
                                        available_at_in_quote,
                                        contract_sum_in_quote,
                                        max_in_quote,
                                        min_in_quote)

                                    if(quote_obj!=null){

                                        id_in_quote = if (quote_obj["id"].isJsonNull) 0 else quote_obj["id"].asInt
                                        company_id_in_quote = if (quote_obj["company_id"].isJsonNull) 0 else quote_obj["company_id"].asInt
                                        last_update_by_in_quote = if (quote_obj["last_update_by"].isJsonNull) "" else quote_obj["last_update_by"].asString
                                        status_id_in_quote = if (quote_obj["status_id"].isJsonNull) 0 else quote_obj["status_id"].asInt
                                        available_at_in_quote = if (quote_obj["available_at"].isJsonNull) "" else quote_obj["available_at"].asString
                                        contract_sum_in_quote = if (quote_obj["contract_sum"].isJsonNull) "" else quote_obj["contract_sum"].asString
                                        max_in_quote = if (quote_obj["max"].isJsonNull) "" else quote_obj["max"].asString
                                        min_in_quote = if (quote_obj["min"].isJsonNull) "" else quote_obj["min"].asString

                                        quote = MyProjectsQuote(
                                            id_in_quote,
                                            company_id_in_quote,
                                            last_update_by_in_quote,
                                            status_id_in_quote,
                                            available_at_in_quote,
                                            contract_sum_in_quote,
                                            max_in_quote,
                                            min_in_quote
                                        )
                                    }
                                    val project = MyProjects(
                                        id_in_data,
                                        project_id_in_data,
                                        company_id_in_data,
                                        last_update_by_in_data,
                                        status_id_in_data,
                                        available_at_in_data,
                                        contract_sum_in_project,
                                        max_in_project,
                                        min_in_project,
                                        user_id,
                                        district_id,
                                        district,
                                        scope_id_in_project,
                                        scope_in_project,
                                        type_id_in_project,
                                        type_in_project,
                                        style_id_in_project,
                                        start_at_in_project,
                                        completed_at_in_project,
                                        matched_date_in_project,
                                        complete_date_in_project,
                                        post_date_in_project,
                                        bg_accepted,
                                        description_in_detail,
                                        address,
                                        area,
                                        budget_max,
                                        contract_m,
                                        docs_list,
                                        notes_list,
                                        category_array,
                                        quote,
                                        owner_name
                                    )
                                    //Log.d("contract_namepjall",contract_name)
                                    projects.add(project)
                                }
                                catch (e:java.lang.Exception){
                                    Log.d("error_getprojects",e.toString())
                                }
                            }
                            mMyProjectsAdapter = MyProjectsAdapter(projects, project_listview)
                            project_listview.setAdapter(mMyProjectsAdapter)

                            activity?.runOnUiThread(Runnable {
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE
                                if(Postdata.size()>0){
                                    Log.d("result+project",Postdata.size().toString())
                                    no_project_view.visibility = View.GONE

                                    swipeContainer!!.visibility=View.VISIBLE
                                    project_listview.visibility=View.VISIBLE

                                }
                                else{
                                    Log.d("result+project",Postdata.size().toString())

                                    no_project_view.visibility = View.VISIBLE
                                    swipeContainer!!.visibility=View.GONE
                                    project_listview.visibility=View.GONE
                                }

                            })
                            //specifying an adapter to access data, create views and replace the content
                            mMyProjectsAdapter.setOnLoadMoreListener(object : OnLoadMoreListener {
                                override fun onLoadMore() {
                                    project_listview.post(Runnable {
                                        Handler().post {
                                            projects.add(null)
                                            mMyProjectsAdapter.notifyItemInserted(projects.size - 1)
                                            Add_more_data_to_list()
                                        }
                                    })
                                }
                            })
                        }
                        else {
                            activity?.runOnUiThread(Runnable {
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE
                                if(Postdata.size()>0){
                                    Log.d("result+project",Postdata.size().toString())
                                    no_project_view.visibility = View.GONE

                                    swipeContainer!!.visibility=View.VISIBLE
                                    project_listview.visibility=View.VISIBLE
                                }
                                else{
                                    Log.d("result+project",Postdata.size().toString())
                                    no_project_view.visibility = View.VISIBLE
                                    swipeContainer!!.visibility=View.GONE
                                    project_listview.visibility=View.GONE
                                }
                            })
                        }

                    })
            } catch (e: Exception) {
            }
        }).start()

        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                // Refresh items
                try {
                    refreshItems()
                } catch (e: java.lang.Exception) {
                }
            }
        })
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1 != null) {
                    handleBroadcastActions(p1)
                }
            }

        }

        return root
    }
    private fun handleBroadcastActions(intent: Intent) {
        when (intent.action) {
            "Reload" -> {
                refreshItems()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(broadcastReceiver, IntentFilter("Reload"))

    }

    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(broadcastReceiver)
    }

    fun refreshItems() {
        // Load items
        // ...
        val handler = Handler()
        handler.postDelayed({
            try {
                onItemsLoadComplete() // Actions to do after 10 seconds
            } catch (e: java.lang.Exception) {
                swipeContainer!!.setRefreshing(false)
            }
        }, 1000)
        //   activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();

        //    onItemsLoadComplete();
    }
    fun onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        getFragmentManager()!!.beginTransaction().detach(this).commitNowAllowingStateLoss()
        getFragmentManager()!!.beginTransaction().attach(this).commitAllowingStateLoss()        // Stop refresh animation
        swipeContainer!!.setRefreshing(false)
    }
    fun isEmptyString(text: String?): Boolean {
        return text == null || text.trim { it <= ' ' } == "null" || text.trim { it <= ' ' }
            .isEmpty()
    }
    fun Add_more_data_to_list()
    {
        try {

            moreprojects = ArrayList<MyProjects?>()
            if (loading != null && !loading!!.isDone() && !loading!!.isCancelled()) {
                projects.removeAt(projects.size - 1)
                mMyProjectsAdapter.notifyItemRemoved(projects.size)
                return
            }
            if (isEmptyString(nextpageurl)) {
                projects.removeAt(projects.size - 1)
                mMyProjectsAdapter.notifyItemRemoved(projects.size)
                return
            }
            Ion.with(activity)
                .load("$nextpageurl")
                .setHeader("Authorization", "Bearer " + header)
                .setHeader("Content-Language", lang)
                //.setTimeout(10000)
                .asJsonObject().setCallback(
                    FutureCallback { e, result ->
                        if (e != null) {
                            projects.removeAt(projects.size - 1)
                            mMyProjectsAdapter.notifyItemRemoved(projects.size)
                            Log.e("MYAPP", "exception", e)
                            //  Toast.makeText(getContext(),e.getMessage()+nextpageurl+"&catid=12", Toast.LENGTH_SHORT).show();
                            return@FutureCallback
                        }

                        projects.removeAt(projects.size - 1)
                        mMyProjectsAdapter.notifyItemRemoved(projects.size)
                        // Toast.makeText(getContext(),nextpageurl, Toast.LENGTH_SHORT).show();
                        Postdata = result.getAsJsonArray("data")
                        if (result.getAsJsonPrimitive("current_page")
                                .asString != currentpageurl
                        ) {
                            currentpageurl =
                                result.getAsJsonPrimitive("current_page")
                                    .asString
                            try {
                                nextpageurl =
                                    result.getAsJsonPrimitive("next_page_url")
                                        .asString
                            } catch (x: java.lang.Exception) {
                            }
                            if (Postdata.size() > 0) {

                                for (i in 0 until Postdata.size()) {
                                    try {
                                        val Project_list: JsonObject =
                                            Postdata.get(i).getAsJsonObject()
                                        var notes_list = ArrayList<MyProjectsNotes>()
                                        var docs_list = ArrayList<MyProjectsDocs>()

                                        var category_array = ArrayList<Int>()

                                        val project_obj: JsonObject? =
                                            if (Project_list["project"].isJsonNull) null else Project_list["project"].asJsonObject

                                        var id_in_data = 0
                                        var project_id_in_data = 0
                                        var company_id_in_data = 0
                                        var last_update_by_in_data = ""
                                        var status_id_in_data = 0
                                        var available_at_in_data = ""
                                        var contract_sum_in_project = ""
                                        var max_in_project = ""
                                        var min_in_project = ""
                                        var category: JsonArray? = null

                                        id_in_data =
                                            if (Project_list["id"].isJsonNull) 0 else Project_list["id"].asInt
                                        project_id_in_data =
                                            if (Project_list["project_id"].isJsonNull) 0 else Project_list["project_id"].asInt
                                        company_id_in_data =
                                            if (Project_list["company_id"].isJsonNull) 0 else Project_list["company_id"].asInt
                                        last_update_by_in_data =
                                            if (Project_list["last_update_by"].isJsonNull) "" else Project_list["last_update_by"].asString
                                        status_id_in_data =
                                            if (Project_list["status_id"].isJsonNull) 0 else Project_list["status_id"].asInt
                                        available_at_in_data =
                                            if (Project_list["available_at"].isJsonNull) "" else Project_list["available_at"].asString
                                        contract_sum_in_project =
                                            if (Project_list["contract_sum"].isJsonNull) "" else Project_list["contract_sum"].asString
                                        max_in_project =
                                            if (Project_list["max"].isJsonNull) "" else Project_list["max"].asString
                                        min_in_project =
                                            if (Project_list["min"].isJsonNull) "" else Project_list["min"].asString


                                        var id_in_project = 0
                                        var user_id = 0
                                        var district_id = 0
                                        var district = ""
                                        var status_id_in_project = 0
                                        var scope_id_in_project = 0
                                        var scope_in_project = ""
                                        var type_id_in_project = 0
                                        var type_in_project = ""
                                        var style_id_in_project = 0
                                        var complete_date_in_project = ""
                                        var bg_accepted = 0
                                        var start_at_in_project = ""
                                        var completed_at_in_project = ""
                                        var matched_date_in_project = ""
                                        var post_date_in_project = ""

                                        var project_id_in_detail = 0
                                        var description_in_detail = ""
                                        var address = ""
                                        var area = ""
                                        var budget_max = ""
                                        var contract_m = ""

                                        var note_id = 0
                                        var project_id_in_note = 0
                                        var item_in_note = ""

                                        var doc_id = 0
                                        var project_id_in_doc = 0
                                        var status_id_in_doc = 0
                                        var url_in_doc = ""
                                        var owner_name = ""

                                        var docs: JsonArray? = null
                                        var notes: JsonArray? = null
                                        var quote_obj: JsonObject? = null
                                        if (project_obj != null) {
                                            try {
                                                docs =
                                                    if (project_obj["docs"].isJsonNull) null else project_obj["docs"].asJsonArray
                                            } catch (e: Exception) {
                                            }
                                            try {
                                                notes =
                                                    if (project_obj["notes"].isJsonNull) null else project_obj["notes"].asJsonArray
                                            } catch (e: Exception) {
                                            }
                                            try{
                                                var user_obj = project_obj["user"].asJsonObject
                                                owner_name =
                                                    if (user_obj["username"].isJsonNull) "N/A" else user_obj["username"].asString
                                            }
                                            catch (e:Exception){
                                            }
                                            try {
                                                category =
                                                    if (project_obj.get("category").isJsonNull) null else project_obj.get(
                                                        "category"
                                                    ).asJsonArray
                                            } catch(e:Exception){}

                                            val detail: JsonObject? =
                                                if (project_obj["detail"].isJsonNull) null else project_obj["detail"].asJsonObject
                                            status_id_in_data = if(project_obj["status_id"].isJsonNull) 0 else project_obj["status_id"].asInt

                                            id_in_project =
                                                if (project_obj["id"].isJsonNull) 0 else project_obj["id"].asInt
                                            user_id =
                                                if (project_obj["user_id"].isJsonNull) 0 else project_obj["user_id"].asInt
                                            district_id =
                                                if (project_obj["district_id"].isJsonNull) 0 else project_obj["district_id"].asInt
                                            district = District(context!!, district_id).name
                                            status_id_in_project =
                                                if (project_obj["status_id"].isJsonNull) 0 else project_obj["status_id"].asInt
                                            scope_id_in_project =
                                                if (project_obj["scope"].isJsonNull) 0 else project_obj["scope"].asInt
                                            scope_in_project =
                                                ProjectScope(context!!, scope_id_in_project).name
                                            type_id_in_project =
                                                if (project_obj["type"].isJsonNull) 0 else project_obj["type"].asInt
                                            type_in_project =
                                                ProjectType(context!!, type_id_in_project).name
                                            style_id_in_project =
                                                if (project_obj["style_id"].isJsonNull) 0 else project_obj["style_id"].asInt
                                            complete_date_in_project =
                                                if (project_obj["complete_date"].isJsonNull) "" else project_obj["complete_date"].asString
                                            bg_accepted =
                                                if (project_obj["bg_accepted"].isJsonNull) 0 else project_obj["bg_accepted"].asInt
                                            start_at_in_project =
                                                if (project_obj["start_at"].isJsonNull) "" else project_obj["start_at"].asString
                                            completed_at_in_project =
                                                if (project_obj["completed_at"].isJsonNull) "" else project_obj["completed_at"].asString
                                            matched_date_in_project =
                                                if (project_obj["matched_date"].isJsonNull) "" else project_obj["matched_date"].asString
                                            post_date_in_project =
                                                if (project_obj["post_date"].isJsonNull) "" else project_obj["post_date"].asString

                                            if (detail != null) {
                                                description_in_detail =
                                                    if (detail["description"].isJsonNull) "" else detail["description"].asString
                                                address =
                                                    if (detail["address"].isJsonNull) "" else detail["address"].asString
                                                area =
                                                    if (detail["area"].isJsonNull) "" else detail["area"].asString.dropLast(3)
                                                budget_max =
                                                    if (detail["budget_max"].isJsonNull) "" else detail["budget_max"].asString
                                                contract_m =
                                                    if (detail["contract_m"].isJsonNull) "" else detail["contract_m"].asString
                                                if (detail["contract_name"].isJsonNull) contract_m=""

                                            }
                                            if (notes != null && notes.size() > 0) {
                                                for (i in 0 until notes.size()) {
                                                    val note_obj: JsonObject =
                                                        notes.get(i).getAsJsonObject()
                                                    note_id =
                                                        if (note_obj["id"].isJsonNull) 0 else note_obj["id"].asInt
                                                    project_id_in_note =
                                                        if (note_obj["project_id"].isJsonNull) 0 else note_obj["project_id"].asInt
                                                    item_in_note =
                                                        if (note_obj["item"].isJsonNull) "" else note_obj["item"].asString
                                                    var note = MyProjectsNotes(
                                                        note_id,
                                                        project_id_in_note,
                                                        item_in_note
                                                    )
                                                    notes_list.add(note)
                                                }
                                            }
                                            if (docs != null && docs.size() > 0) {
                                                for (i in 0 until docs.size()) {
                                                    val doc_obj: JsonObject =
                                                        docs.get(i).getAsJsonObject()

                                                    var url_in_doc = ""
                                                    doc_id =
                                                        if (doc_obj["id"].isJsonNull) 0 else doc_obj["id"].asInt
                                                    project_id_in_doc =
                                                        if (doc_obj["project_id"].isJsonNull) 0 else doc_obj["project_id"].asInt
                                                    status_id_in_doc =
                                                        if (doc_obj["status_id"].isJsonNull) 0 else doc_obj["status_id"].asInt
                                                    url_in_doc =
                                                        if (doc_obj["url"].isJsonNull) "" else doc_obj["url"].asString
                                                    var doc = MyProjectsDocs(
                                                        doc_id,
                                                        project_id_in_doc,
                                                        status_id_in_doc,
                                                        url_in_doc
                                                    )
                                                    docs_list.add(doc)
                                                }

                                            }
                                            if (category != null && category.size() > 0) {
                                                for (x in 0 until category.size()) {
                                                    category_array.add(
                                                        category.get(x).asJsonObject.get(
                                                            "id"
                                                        ).asInt
                                                    )
                                                }

                                            }
                                        }

                                        try {
                                            quote_obj =
                                                if (Project_list["quote"].isJsonNull) null else Project_list["quote"].asJsonObject
                                        } catch (e: Exception) {
                                        }
                                        var id_in_quote = 0
                                        var company_id_in_quote = 0
                                        var last_update_by_in_quote = ""
                                        var status_id_in_quote = 0
                                        var available_at_in_quote = ""
                                        var contract_sum_in_quote = ""
                                        var max_in_quote = ""
                                        var min_in_quote = ""
                                        var quote = MyProjectsQuote(
                                            id_in_quote,
                                            company_id_in_quote,
                                            last_update_by_in_quote,
                                            status_id_in_quote,
                                            available_at_in_quote,
                                            contract_sum_in_quote,
                                            max_in_quote,
                                            min_in_quote
                                        )

                                        if (quote_obj != null) {

                                            id_in_quote =
                                                if (quote_obj["id"].isJsonNull) 0 else quote_obj["id"].asInt
                                            company_id_in_quote =
                                                if (quote_obj["company_id"].isJsonNull) 0 else quote_obj["company_id"].asInt
                                            last_update_by_in_quote =
                                                if (quote_obj["last_update_by"].isJsonNull) "" else quote_obj["last_update_by"].asString
                                            status_id_in_quote =
                                                if (quote_obj["status_id"].isJsonNull) 0 else quote_obj["status_id"].asInt
                                            available_at_in_quote =
                                                if (quote_obj["available_at"].isJsonNull) "" else quote_obj["available_at"].asString
                                            contract_sum_in_quote =
                                                if (quote_obj["contract_sum"].isJsonNull) "" else quote_obj["contract_sum"].asString
                                            max_in_quote =
                                                if (quote_obj["max"].isJsonNull) "" else quote_obj["max"].asString
                                            min_in_quote =
                                                if (quote_obj["min"].isJsonNull) "" else quote_obj["min"].asString

                                            quote = MyProjectsQuote(
                                                id_in_quote,
                                                company_id_in_quote,
                                                last_update_by_in_quote,
                                                status_id_in_quote,
                                                available_at_in_quote,
                                                contract_sum_in_quote,
                                                max_in_quote,
                                                min_in_quote
                                            )
                                        }
                                        val project = MyProjects(
                                            id_in_data,
                                            project_id_in_data,
                                            company_id_in_data,
                                            last_update_by_in_data,
                                            status_id_in_data,
                                            available_at_in_data,
                                            contract_sum_in_project,
                                            max_in_project,
                                            min_in_project,
                                            user_id,
                                            district_id,
                                            district,
                                            scope_id_in_project,
                                            scope_in_project,
                                            type_id_in_project,
                                            type_in_project,
                                            style_id_in_project,
                                            start_at_in_project,
                                            completed_at_in_project,
                                            matched_date_in_project,
                                            complete_date_in_project,
                                            post_date_in_project,
                                            bg_accepted,
                                            description_in_detail,
                                            address,
                                            area,
                                            budget_max,
                                            contract_m,
                                            docs_list,
                                            notes_list,
                                            category_array,
                                            quote,
                                            owner_name
                                        )
                                        //Log.d("contract_namepjall",contract_name)
                                        moreprojects.add(project)
                                    } catch (e: java.lang.Exception) {
                                        Log.d("error_getprojects", e.toString())
                                    }
                                }
                                projects.addAll(moreprojects)
                                moreprojects.clear()
                                mMyProjectsAdapter.notifyDataSetChanged()
                                mMyProjectsAdapter.setLoaded()
                            }
                        }
                    })
        }
        catch (e: Exception){

        }

    }
}

