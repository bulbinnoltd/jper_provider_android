package com.jpertechnologyltd.jperprovider.ui.myproject

import android.content.Context
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.item.ProjectStatus
import com.jpertechnologyltd.jperprovider.ui.calendar.CalendarFragment
import com.jpertechnologyltd.jperprovider.ui.quoteproject.QuoteSubmissionFragment
import java.lang.Exception


class MyProjectsQuotedDetailFragment : Fragment() {

    var project_id : Int=0
    var bg_accepted : Int=0
    var scope_id : Int=0
    var status_id : Int=0
    var post_date : String="N/A"
    var contract_sum_value : String = "N/A"
    var contract_m : String = "N/A"
    var viewpager : ViewPager?=null

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        val args = arguments
//        try {
//            bg_accepted = args!!.getInt("bg_accepted",0)
//            project_id = args!!.getInt("project_id",0)
//            status_id =  args!!.getInt("status_id",0)
//            scope_id =  args!!.getInt("scope_id",0)
//            post_date =  args!!.getString("post_date","")
//            contract_sum_value =   args!!.getString("contract_sum","")
//            contract_m =   args!!.getString("contract_m","")
//
//            Log.d("bg_accepted_in_fragment", bg_accepted.toString())
//        }
//        catch (e: Exception){
//            Log.d("error_insum",e.toString())
//        }
//
//    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_quote_project_detail, container, false)
        val args = arguments
        try {
            bg_accepted = args!!.getInt("bg_accepted",0 )
            project_id = args!!.getInt("project_id",0)
            status_id =  args!!.getInt("status_id",0)
            scope_id =  args!!.getInt("scope_id",0)
            post_date =  args!!.getString("post_date","")
            contract_sum_value =   args!!.getString("contract_sum","")
            contract_m =   args!!.getString("contract_m","")

            Log.d("bg_accepted_in_fragment", bg_accepted.toString())
        }
        catch (e: Exception){
            Log.d("error_insum",e.toString())
        }

        val tabLayout = root.findViewById(R.id.tabLayout) as TabLayout

        tabLayout.addTab(tabLayout.newTab().setText("Project Summary"))

        val viewpager = root.findViewById(R.id.pager) as ViewPager

        setupViewPager(viewpager)
        tabLayout.setupWithViewPager(viewpager)

        val project_submitDate = root.findViewById(R.id.project_submitDate) as TextView
        project_submitDate.text = post_date

        val project_detail_title = root.findViewById<TextView>(R.id.project_detail_title)
        val project_detail_status = root.findViewById<TextView>(R.id.project_detail_status)
        val myprojects_calendar_icon = root.findViewById(R.id.myprojects_calendar_icon) as ImageView

        myprojects_calendar_icon.setOnClickListener {
            val CalendarFragment: Fragment = CalendarFragment()
            val bundle = Bundle()
            bundle.putInt("project_id", project_id!!)
            CalendarFragment.setArguments(bundle)

            var fr = getFragmentManager()?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.add(R.id.nav_host_fragment,
                CalendarFragment
            )
            fr?.commit()
        }
//        val quote_submission = root.findViewById<FloatingActionButton>(R.id.quote_create_button)
//        quote_submission.setOnClickListener{
//            var fr1 = getFragmentManager()?.beginTransaction()
//            fr1?.replace(R.id.nav_host_fragment, QuoteSubmissionFragment())
//            fr1?.commit()
//        }
        val status = ProjectStatus(context!!, status_id!!).name
        val status_rgb = ProjectStatus(context!!, status_id!!).rgb
        val drawable = project_detail_status.getBackground() as GradientDrawable
        drawable.setColor(status_rgb!!)
        project_detail_status.text = ProjectStatus(context!!,status_id!!).name

        project_detail_title.setText(getString(R.string.project_title).plus(" #").plus(project_id.toString()))

        val back_button = root.findViewById(R.id.back_button) as ImageView
        back_button.setOnClickListener {

            var fr = getFragmentManager()?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.add(R.id.nav_host_fragment, MyProjectsFragment())
            fr?.commit()
        }

//        val project_detail_status = root.findViewById(R.id.project_detail_status) as TextView
//        project_detail_status.setText("Pending")
//        val drawable =    project_detail_status.getBackground() as GradientDrawable
//        drawable.setColor(Color.rgb(237,176,173))

        //Remove temp link of specific project's contract
        val editor = activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
        editor.remove("contract_temp_link")
        editor.apply()



        return root
    }


    private fun setupViewPager(viewPager: ViewPager): Unit {
        val adapter =
            Adapter(
                childFragmentManager
            )
        val bundle = Bundle()
        try{
            bundle.putInt("bg_accepted", bg_accepted!!)
            bundle.putInt("project_id", project_id!!)
            bundle.putInt("status_id", status_id!!)
            bundle.putInt("scope_id", scope_id!!)
            bundle.putString("post_date", post_date!!)
            bundle.putString("contract_sum", contract_sum_value!!)
            bundle.putString("contract_m", contract_m!!)
        }
        catch (e:Exception){
            Log.d("errortoput", e.toString())
        }
        val MyProjectsProjectSummaryFragment= MyProjectsProjectSummaryFragment()
        MyProjectsProjectSummaryFragment.setArguments(bundle)


        val MyProjectsQuotedPaymentPlanFragment= MyProjectsQuotedPaymentPlanFragment()
        MyProjectsQuotedPaymentPlanFragment.setArguments(bundle)

        val MyProjectsProjectManageFragment= MyProjectsProjectManageFragment()
        MyProjectsProjectManageFragment.setArguments(bundle)

        val MyProjectsQuotedDisbursementFragment= MyProjectsQuotedDisbursementFragment()
        MyProjectsQuotedDisbursementFragment.setArguments(bundle)

        adapter.addFragment(MyProjectsProjectManageFragment, getString(R.string.Project_detail_tab_contract))
        adapter.addFragment(MyProjectsQuotedPaymentPlanFragment,  getString(R.string.Project_detail_tab_payment))
        adapter.addFragment(MyProjectsQuotedDisbursementFragment,  getString(R.string.Project_detail_tab_disburse))
        adapter.addFragment(MyProjectsProjectSummaryFragment,  getString(R.string.Project_detail_tab_summary))

       viewPager.adapter = adapter
    }

    internal class Adapter(manager:  FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(
            fragment: Fragment,
            title: String
        ) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

}




