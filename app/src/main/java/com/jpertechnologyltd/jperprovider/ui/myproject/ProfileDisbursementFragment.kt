package com.jpertechnologyltd.jperprovider.ui.myproject

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.MyProjectsQuotedAdapter
import com.jpertechnologyltd.jperprovider.adapter.ProfileDisbursementAdapter
import com.jpertechnologyltd.jperprovider.item.Quotation
import com.jpertechnologyltd.jperprovider.item.QuotationItem
import com.jpertechnologyltd.jperprovider.adapter.QuoteQuoteAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.Disbursement


class ProfileDisbursementFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_fragment_profile_disbursement, container, false)
        val disbursement_listview = root.findViewById<View>(R.id.disbursement_listview) as RecyclerView
        val disbursement_items = ArrayList<Disbursement>()

        disbursement_items!!.add(
                Disbursement(
                    1,2,1,3,"20000.00","2020-09-02","300.00","0.00","400.00","","8000.00","2020-12-12","訂"
                )
            )
        disbursement_items!!.add(
            Disbursement(
                2,2,2,3,"20000.00","2020-09-02","300.00","0.00","400.00","","8000.00","2020-12-12","訂"
            )
        )
        Log.d("temp", "disbursement_items: " + disbursement_items.size.toString())

//        val disbursement_adapter = parentFragment?.let { ProfileDisbursementAdapter(disbursement_items,it) }
        val disbursement_adapter = ProfileDisbursementAdapter(disbursement_items,this)
        disbursement_listview.adapter = disbursement_adapter
        disbursement_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)


        return root
    }
}

