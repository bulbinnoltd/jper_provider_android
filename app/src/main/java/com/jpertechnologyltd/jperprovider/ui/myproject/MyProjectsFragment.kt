package com.jpertechnologyltd.jperprovider.ui.myproject

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.MyBiddingAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.mybids.MyProjects
import com.jpertechnologyltd.jperprovider.ui.activity.ActivityFragment
import com.synnapps.carouselview.CarouselView
import java.util.*
import java.util.concurrent.Future
import kotlin.collections.ArrayList


class MyProjectsFragment : Fragment() {

    private var header : String? = null
    var lang : String? = "en"
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null

    var unread_msg_no = 0
    var unreaddot: TextView? = null
    private lateinit var broadcastReceiver: BroadcastReceiver
    private lateinit var no_project_imageView: ConstraintLayout
    private lateinit var no_project_buttonView: LinearLayout

    var biddings =ArrayList<MyProjects?>()
    var morebiddings =ArrayList<MyProjects?>()
    lateinit var mMyBiddingsAdapter : MyBiddingAdapter
    private lateinit var project_listview: RecyclerView

    var loading: Future<JsonObject>? = null
    private var currentpageurl: String? = null
    lateinit var Postdata : JsonArray

    private var nextpageurl : String? = null

    var swipeContainer: SwipeRefreshLayout?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }
    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(broadcastReceiver, IntentFilter("Reload"))
    }


    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(broadcastReceiver)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_myproject, container, false)
        lang =context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        val config = resources.configuration
        val locale = Locale(lang)


        Locale.setDefault(locale)
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)
        val tabLayout = root.findViewById(R.id.tabLayout) as TabLayout
        unreaddot = root.findViewById(R.id.unread_number_total) as TextView


        val viewpager = root.findViewById(R.id.pager) as ViewPager

        setupViewPager(viewpager)
        tabLayout.setupWithViewPager(viewpager)

        val notification_button = root.findViewById(R.id.notification_button) as ImageView
        notification_button.setOnClickListener{
            activity!!.supportFragmentManager.beginTransaction()
                .add(R.id.nav_host_fragment, ActivityFragment())
                .addToBackStack(null)
                .commit()
        }
        Thread(Runnable {
            unread_msg_no = HttpsService.GetNotificationCount(
                context,
                "https://uat.jper.com.hk/api/my/notification",
                header,
                lang
            )


            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {

                if(unread_msg_no>0) {
                    unreaddot!!.visibility = View.VISIBLE
                    unreaddot!!.text = unread_msg_no.toString()
                }
            })
        }).start()
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1 != null) {
                    handleBroadcastActions(p1)
                }
            }

        }

        return root
    }

    private fun handleBroadcastActions(intent: Intent) {
        when (intent.action) {
            "Reload" -> {
                Thread(Runnable {
                    unread_msg_no = HttpsService.GetNotificationCount(
                        context,
                        "https://uat.jper.com.hk/api/my/notification",
                        header,
                        lang
                    )


                    activity?.runOnUiThread(java.lang.Runnable {

                        if(unread_msg_no>0) {
                            unreaddot!!.visibility = View.VISIBLE
                            unreaddot!!.text = unread_msg_no.toString()
                        }
                    })
                }).start()
            }
        }
    }
    private fun setupViewPager(viewPager: ViewPager): Unit {
        val adapter =
            Adapter(
                childFragmentManager
            )
        adapter.addFragment(MyProjectsActiveFragment(), "Active")
       viewPager.adapter = adapter
    }

    internal class Adapter(manager: FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(
            fragment: Fragment,
            title: String
        ) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

}




