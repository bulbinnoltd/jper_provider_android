package com.jpertechnologyltd.jperprovider.ui.myproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.ManageContract
import com.jpertechnologyltd.jperprovider.adapter.ManageContractAdapter


class ManageContractFragment : Fragment() {

    private lateinit var ManageContract: ArrayList<ManageContract>
    private lateinit var no_manage_contract_layout: LinearLayout
    private lateinit var no_manage_contract_buttonView:LinearLayout
    private lateinit var manage_contract_listview: RecyclerView
    private lateinit var manage_contract_two_buttonsView:LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_fragment_manage_contract, container, false)

        no_manage_contract_layout = root.findViewById(R.id.no_manage_contract_layout) as LinearLayout
        no_manage_contract_buttonView = root.findViewById(R.id.no_manage_contract_buttonView) as LinearLayout
        manage_contract_two_buttonsView = root.findViewById(R.id.manage_contract_two_buttonsView) as LinearLayout
        manage_contract_listview = root.findViewById<View>(R.id.manage_contract_listview) as RecyclerView

        ManageContract = ArrayList<ManageContract>()
        ManageContract.add(ManageContract("Signed Contract one.pdf","","","",""))
        ManageContract.add(ManageContract("Signed Contract two.jpg","","","",""))
        ManageContract.add(ManageContract("Signed Contract three.png","","","",""))

        val back_button = root.findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {

            var fr = getFragmentManager()?.beginTransaction()
            fr?.replace(R.id.nav_host_fragment,
                    MyProjectsFragment()
            )

            fr?.commit()
        }

        val manage_contract_adapter = ManageContractAdapter(ManageContract)
        manage_contract_listview.adapter = manage_contract_adapter
        manage_contract_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)

        showMyContracts()
        return root
    }

    private fun showMyContracts() =

        if (ManageContract.size > 0){
            no_manage_contract_layout.visibility = View.GONE
            no_manage_contract_buttonView.visibility = View.INVISIBLE
            manage_contract_listview.visibility= View.VISIBLE
            manage_contract_two_buttonsView.visibility=View.VISIBLE
        }
        else {
            no_manage_contract_layout.visibility = View.VISIBLE
            no_manage_contract_buttonView.visibility = View.VISIBLE
            manage_contract_listview.visibility= View.GONE
            manage_contract_two_buttonsView.visibility=View.GONE
        }

}