package com.jpertechnologyltd.jperprovider.ui.myproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.MyProjects
import com.jpertechnologyltd.jperprovider.adapter.MyProjectsProjectsAdapter

class MyProjectsProjectFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_quote_myprojects, container, false)
        val myprojects_recyclerview =
            root.findViewById<View>(R.id.myprojects_recyclerview) as RecyclerView
        val myprojects = ArrayList<MyProjects>()
        myprojects.add(
            MyProjects(
                "Project #ID1",
                "Pending",
                "Wan Chai",
                "HK\$ 250,000 - 350,000",
                "Home / 600 sqft",
                "Partial Remodel / Repair",
                "1",
                "2020-05-28",
                ""
            )
        )



        val project_myprojects_adapter = parentFragment?.let { MyProjectsProjectsAdapter(myprojects, it) }
        myprojects_recyclerview.adapter = project_myprojects_adapter
        myprojects_recyclerview.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


        return root

    }
}

