package com.jpertechnologyltd.jperprovider.ui.profile

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jper.jperowner.item.*
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.AllReviewAdapter
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener
import com.jpertechnologyltd.jperprovider.item.Comment
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Future
import kotlin.collections.ArrayList

class ViewReviewFragment : Fragment() {
    var commentlist : ArrayList<Comment?>? =null
    var rating_value :String? = null
    var rating_count :String? = null
    var rating :Float? = null
    var rating_item1 :Float? = null
    var rating_item2 :Float? = null
    var rating_item3 :Float? = null
    var company_id :Int? = null
    private var header : String? = null
    var lang : String? = "en"

    lateinit var Postdata : JsonArray
    lateinit var morecomments : ArrayList<Comment?>

    private var nextpageurl : String? = null
    private var currentpageurl: String? = null

    lateinit var  mReviewAdapter : AllReviewAdapter
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var loading: Future<JsonObject>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        //commentlist = args?.getParcelableArrayList<Comment>("data")
        rating_value = args?.getString("rating_value")
        rating_count = args?.getString("rating_count")
        rating = args?.getFloat("rating")
        rating_item1 = args?.getFloat("rating_item1")
        rating_item2 = args?.getFloat("rating_item2")
        rating_item3 = args?.getFloat("rating_item3")
        company_id = args?.getInt("company_id")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_provider_review, container, false)
        val rating_view = root.findViewById<RatingBar>(R.id.rating)
        val rating_value_view = root.findViewById<TextView>(R.id.rating_value)
        val rating_count_view = root.findViewById<TextView>(R.id.review_number)
        val rating_item1_view = root.findViewById<RatingBar>(R.id.rating_item1)
        val rating_item2_view = root.findViewById<RatingBar>(R.id.rating_item2)
        val rating_item3_view = root.findViewById<RatingBar>(R.id.rating_item3)
        val back_button = root.findViewById(R.id.back_button) as ImageView
        header = activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////

        // set on-click listener
        back_button.setOnClickListener {
            activity?.onBackPressed()
        }
        rating_view.rating=rating!!
        rating_item1_view.rating=rating_item1!!
        rating_item2_view.rating=rating_item2!!
        rating_item3_view.rating=rating_item3!!
        rating_value_view.text=rating_value!!
        rating_count_view.text=rating_count!!

        val review_listview = root.findViewById<RecyclerView>(R.id.review_listview)

        val mLayoutManager = LinearLayoutManager(activity)

        review_listview.setLayoutManager(mLayoutManager)
        Thread(Runnable {
            try {
                // JsonObject json = new JsonObject();
                Ion.with(activity)
                    .load("https://uat.jper.com.hk/api/company/".plus(company_id).plus("/rate/comments"))
                    .setHeader("Authorization", "Bearer " +header)
                    .setHeader("Content-Language", lang)
                    // .setTimeout(10000)
                    .asJsonObject()
                    .setCallback(FutureCallback { e, result ->
                        if (e != null) {
                            //   Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                            return@FutureCallback
                        }
                        Postdata = result.getAsJsonArray("data")
                        nextpageurl = try {
                            if(result.get("next_page_url").isJsonNull) "" else result.getAsJsonPrimitive("next_page_url")
                                .asString
                        } catch (x: Exception) {
                            Log.d("error_jperx",x.toString())
                            null
                        }
                        commentlist = ArrayList()
                        currentpageurl =
                            if(result.get("current_page").isJsonNull) "" else result.getAsJsonPrimitive("current_page").asString

                        Log.d("company_id_in_fragement",company_id.toString() )

                        if(Postdata.size()>0){

                            for (i in 0 until Postdata.size()) {

                                Log.d("comments_in_fragement",Postdata.toString() )
                                try{
                                    val Comment_data_list: JsonObject = Postdata.get(i).getAsJsonObject()

                                    val user_data: JsonObject? = if(Comment_data_list.get("owner").isJsonNull) null else Comment_data_list.get("owner").getAsJsonObject()
                                    val id = Comment_data_list["id"].asInt
                                    val commentdetail=  if(Comment_data_list.get("comment").isJsonNull) null else Comment_data_list["comment"].asJsonObject
                                    val rate = Comment_data_list["average"].asString.toFloat()
                                    val originalFormat: DateFormat =
                                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                                    val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                                    val date: Date = originalFormat.parse(Comment_data_list["created_at"].asString)
                                    val created_at: String = targetFormat.format(date)

                                    var user_name = "N/A"

                                    if (user_data!=null) {
                                      user_name= if(user_data.get("username").isJsonNull) "N/A" else user_data.get("username").asString
                                    }
                                    var content = "N/A"
                                    if (commentdetail!=null) {
                                        content= if(commentdetail["comment"].isJsonNull) "N/A" else commentdetail["comment"].asString
                                    }

                                    var comment_data = Comment(id,user_name,rate,content, created_at)
                                    commentlist!!.add(comment_data)
                                }
                                catch (e:java.lang.Exception){
                                    Log.d("error_getcomments",e.toString())
                                }
                            }



                                    mReviewAdapter =
                                        AllReviewAdapter(commentlist!!,review_listview)
                                    review_listview.adapter = mReviewAdapter

                                    mReviewAdapter.setOnLoadMoreListener(object :
                                        OnLoadMoreListener {
                                        override fun onLoadMore() {
                                            review_listview.post(Runnable {
                                                Handler().post {

                                                    Log.d("comments_in_more","comments_in_more")
                                                    commentlist!!.add(null)
                                                    mReviewAdapter.notifyItemInserted(commentlist!!.size - 1)
                                                    Add_more_data_to_list()
                                                }
                                            })
                                        }
                                    })


                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE

                            //specifying an adapter to access data, create views and replace the content

                        }
                        else {
                            activity?.runOnUiThread(Runnable {
                                Loading!!.visibility = View.GONE
                                progressOverlay!!.visibility = View.GONE

                            })
                        }

                    })
            } catch (e: Exception) {
            }
        }).start()


//        if (commentlist !=null) {
//            val review_adapter = ReviewAdapter(commentlist!!, commentlist!!.size)
//            review_listview.adapter = review_adapter
//            review_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)
//        }

        return root
    }
    fun resetprocess() {
        loading?.cancel(true)
        loading = null
    }

    fun isEmptyString(text: String?): Boolean {
        return text == null || text.trim { it <= ' ' } == "null" || text.trim { it <= ' ' }
            .isEmpty()
    }
    fun Add_more_data_to_list()
    {
        try {
            Log.d("comments_in_more","comments_in_more")

            morecomments = ArrayList()
            if (loading != null && !loading!!.isDone() && !loading!!.isCancelled()) {
                commentlist!!.removeAt(commentlist!!.size - 1)
                mReviewAdapter.notifyItemRemoved(commentlist!!.size)
                return
            }
            if (isEmptyString(nextpageurl)) {
                commentlist!!.removeAt(commentlist!!.size - 1)
                mReviewAdapter.notifyItemRemoved(commentlist!!.size)
                return
            }
            Ion.with(activity)
                .load("$nextpageurl")
                .setHeader("Authorization", "Bearer " +header)
                .setHeader("Content-Language", lang)
                //.setTimeout(10000)
                .asJsonObject().setCallback(
                    FutureCallback { e, result ->
                        if (e != null) {
                            commentlist!!.removeAt(commentlist!!.size - 1)
                            mReviewAdapter.notifyItemRemoved(commentlist!!.size)
                            Log.e("MYAPP", "exception", e)
                            //  Toast.makeText(getContext(),e.getMessage()+nextpageurl+"&catid=12", Toast.LENGTH_SHORT).show();
                            return@FutureCallback
                        }

                        commentlist!!.removeAt(commentlist!!.size - 1)
                        mReviewAdapter.notifyItemRemoved(commentlist!!.size)
                        // Toast.makeText(getContext(),nextpageurl, Toast.LENGTH_SHORT).show();
                        Postdata = result.getAsJsonArray("data")
                        if (result.getAsJsonPrimitive("current_page")
                                .asString != currentpageurl
                        ) {
                            currentpageurl =
                                result.getAsJsonPrimitive("current_page")
                                    .asString
                            try {
                                nextpageurl =
                                    result.getAsJsonPrimitive("next_page_url")
                                        .asString
                            } catch (x: java.lang.Exception) {
                            }
                            for (i in 0 until Postdata.size()) {
                                try{
                                    val Comment_data_list: JsonObject = Postdata.get(i).getAsJsonObject()

                                    val user_data: JsonObject? = if(Comment_data_list.get("owner").isJsonNull) null else Comment_data_list.get("owner").getAsJsonObject()
                                    val id = Comment_data_list["id"].asInt
                                    val commentdetail=  if(Comment_data_list.get("comment").isJsonNull) null else Comment_data_list["comment"].asJsonObject
                                    val rate = Comment_data_list["average"].asString.toFloat()
                                    val originalFormat: DateFormat =
                                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                                    val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                                    val date: Date = originalFormat.parse(Comment_data_list["created_at"].asString)
                                    val created_at: String = targetFormat.format(date)

                                    var user_name = "N/A"

                                    if (user_data!=null) {
                                        user_name= if(user_data.get("username").isJsonNull) "N/A" else user_data.get("username").asString
                                    }
                                    var content = "N/A"
                                    if (commentdetail!=null) {
                                        content= if(commentdetail["comment"].isJsonNull) "N/A" else commentdetail["comment"].asString
                                    }

                                    var comment_data = Comment(id,user_name,rate,content, created_at)
                                    morecomments.add(comment_data)
                                    //     NewsAdapter.notifyItemInserted(Lists.size());
                                }
                                catch (e:java.lang.Exception){}
                            }
                            commentlist!!.addAll(morecomments)
                            morecomments.clear()
                            mReviewAdapter.notifyDataSetChanged()
                            mReviewAdapter.setLoaded()
                            //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                        }
                        resetprocess()
                    }
                )
        } catch (e: java.lang.Exception) {
        }

    }
}

