package com.jpertechnologyltd.jperprovider.ui.myproject.notice

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.jper.jperowner.item.PaymentChannel
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetRetentionReceipt
import java.text.DecimalFormat


class DepositReceiptFragment : Fragment() {
    var project_id :Int? = null

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    private var header : String? = null
    var lang : String? = "en"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

        project_id = args?.getInt("project_id")

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_deposit_receipt, container, false)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////

        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")

        val pr_project_owner_name = root.findViewById(R.id.pr_project_owner_name) as TextView
        val pr_issue_date = root.findViewById(R.id.pr_issue_date) as TextView
        val pn_project_title = root.findViewById(R.id.pn_project_title) as TextView
        val pn_amount_paid_value = root.findViewById(R.id.pn_amount_paid_value) as TextView
        val pn_payment_date_value = root.findViewById(R.id.pn_payment_date_value) as TextView
        val pn_proof_of_payment_value = root.findViewById(R.id.pn_proof_of_payment_value) as TextView
        val pn_payment_channel_value = root.findViewById(R.id.pn_payment_channel_value) as TextView
        val pn_payment_method_value = root.findViewById(R.id.pn_payment_method_value) as TextView
        Thread(Runnable {
            val receipt = GetRetentionReceipt(
                context,
                "https://uat.jper.com.hk/api/company/project/".plus(project_id)
                    .plus("/retention/receipt"),
                header,
                lang
            )
            activity?.runOnUiThread(java.lang.Runnable {
                if (receipt != null) {
                    pn_project_title.text =
                        context!!.getString(R.string.project_title).plus(" # ").plus(project_id)
                    if (!receipt.amount.equals("N/A")) {
                        try {
                            val number: String = receipt.amount
                            val amount = number.toDouble()
                            val formatter = DecimalFormat("#,###.##")
                            var formatted = formatter.format(amount)
                            if (formatted.contains(".00")) formatted =
                                formatted.substring(0, formatted.length - 3)
                            pn_amount_paid_value.text = "HK $" + formatted
                        } catch (e: Exception) {
                            e.printStackTrace()
                            pn_amount_paid_value.text = "HK$ ".plus(receipt.amount)
                        }
                    }
                    else pn_amount_paid_value.text = receipt.amount
                    pn_payment_date_value.text = receipt.date

                    pr_project_owner_name.text= receipt.username
                    pr_issue_date.text= receipt.issued_date
                    if (receipt.method ==1)pn_payment_method_value.text = getString(R.string.pn_method1)
                    else if (receipt.method ==2)pn_payment_method_value.text = getString(R.string.pn_method2)
                    else if (receipt.method ==3)pn_payment_method_value.text = getString(R.string.pn_method3)
                    else if (receipt.method ==4)pn_payment_method_value.text = getString(R.string.pn_method4)
                    else pn_payment_method_value.text ="N/A"


                    if (!receipt.channel.equals("N/A")) pn_payment_channel_value.text =
                        PaymentChannel(context!!, receipt.channel.toInt()).name
                    else pn_payment_channel_value.text = receipt.channel
                    if(!receipt.proof.equals("N/A")) {
                            pn_proof_of_payment_value.setOnClickListener {

                                val intent = Intent(activity, WebviewActivity::class.java)
                                intent.putExtra("url", receipt.proof)
                                Log.d("url", receipt.proof)
                                context!!.startActivity(intent)


                            }
                    }
                    else{
                        pn_proof_of_payment_value.text = "N/A"
                    }
                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE
                } else {
                    pn_project_title.text =
                        context!!.getString(R.string.project_title).plus(" # ").plus(project_id)
                    pn_amount_paid_value.text = "N/A"
                    pn_payment_date_value.text = "N/A"
                    pn_payment_channel_value.text = "N/A"
                    pn_proof_of_payment_value.text = "N/A"
                    pn_payment_method_value.text ="N/A"

                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE
                }
            })
        }).start()
        val back_button = root.findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            activity?.onBackPressed()
////            val intent = Intent (getActivity(), BlogActivity::class.java)
////            startActivity(intent)
//            var fr = getFragmentManager()?.beginTransaction()
//            fr?.addToBackStack(null)
//            fr?.add(R.id.nav_host_fragment, MyProjectsDetailFragment())
//            //fr?.replace(R.id.nav_host_fragment, MyProjectsDetailFragment())
//            fr?.commit()
//            // your code to perform when the user clicks on the ImageView
        }
        return root
    }
}