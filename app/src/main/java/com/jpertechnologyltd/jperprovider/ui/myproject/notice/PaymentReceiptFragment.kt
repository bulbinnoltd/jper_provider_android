package com.jpertechnologyltd.jperprovider.ui.myproject.notice

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.jper.jperowner.item.PaymentChannel
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetPaymentPlanReceipt
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat


class PaymentReceiptFragment : Fragment() {
    var project_id :Int? = null
    var plan_id :Int? = null

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    private var header : String? = null
    var lang : String? = "en"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

        project_id = args?.getInt("project_id")
        plan_id = args?.getInt("plan_id")
        Log.d("proj_plan_id",plan_id.toString())

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
     

        val root = inflater.inflate(R.layout.fragment_payment_receipt, container, false)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////

        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        val back_button = root.findViewById(R.id.back_button) as ImageView
        // set on-click listener

        val pr_project_owner_name = root.findViewById(R.id.pr_project_owner_name) as TextView
        val pr_issue_date = root.findViewById(R.id.pr_issue_date) as TextView
        val pr_project_title = root.findViewById(R.id.pr_project_title) as TextView
        val pr_description_value = root.findViewById(R.id.pr_description_value) as TextView
        val pr_amount_paid_value = root.findViewById(R.id.pr_amount_paid_value) as TextView
        val pr_payment_date_value = root.findViewById(R.id.pr_payment_date_value) as TextView
        val pr_proof_of_payment_value = root.findViewById(R.id.pr_proof_of_payment_value) as TextView
        val pr_payment_channel_value = root.findViewById(R.id.pr_payment_channel_value) as TextView
        val pr_payment_method_value = root.findViewById(R.id.pr_payment_method_value) as TextView
        Thread(Runnable {
            val receipt = GetPaymentPlanReceipt(
                context,
                "https://uat.jper.com.hk/api/company/project/".plus(project_id).plus("/plan/")
                    .plus(plan_id).plus("/receipt"),
                header,
                lang
            )
            activity?.runOnUiThread(java.lang.Runnable {
                if (receipt != null) {
                    Log.d("proj_receipt",receipt.title)
                    pr_project_owner_name.text= receipt.username
                    pr_issue_date.text= receipt.issued_date
                    pr_description_value.text = receipt.title
                    pr_project_title.text =
                        context!!.getString(R.string.project_title).plus(" # ").plus(project_id)
                    if (!receipt.amount.equals("N/A")) {
                        try {
                            val number: String = receipt.amount
                            val amount = number.toDouble()
                            val formatter = DecimalFormat("#,###.##")
                            var formatted = formatter.format(amount)
                            if (formatted.contains(".00")) formatted =
                                formatted.substring(0, formatted.length - 3)
                            pr_amount_paid_value.text = "HK $" + formatted
                        }catch (e: Exception) {
                        e.printStackTrace()
                        pr_amount_paid_value.text = "HK$ ".plus(

                            receipt.amount
                        ) }
                    }
                    else pr_amount_paid_value.text = "N/A"

                    if (receipt.method ==1)pr_payment_method_value.text = getString(R.string.pn_method1)
                    else if (receipt.method ==2)pr_payment_method_value.text = getString(R.string.pn_method2)
                    else if (receipt.method ==3)pr_payment_method_value.text = getString(R.string.pn_method3)
                    else if (receipt.method ==4)pr_payment_method_value.text = getString(R.string.pn_method4)
                    else pr_payment_method_value.text ="N/A"

//            val originalFormat: DateFormat =
//                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
//            val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
//            val date: Date = originalFormat.parse(receipt.date)
//            val due_date: String = targetFormat.format(date)
                    var format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    try {
                        val newDate = format.parse( receipt.date)
                        format = SimpleDateFormat("yyyy-MM-dd")
                        val date = format.format(newDate)
                        pr_payment_date_value.text = date
                    } catch (e: ParseException) {
                        e.printStackTrace()
                        pr_payment_date_value.text =  receipt.date
                    }

                    if (!receipt.channel.equals("N/A")) pr_payment_channel_value.text =
                        PaymentChannel(context!!, receipt.channel.toInt()).name
                    else pr_payment_channel_value.text = "N/A"

                    if(!receipt.proof.equals("N/A") && !receipt.proof.equals("")){
                        Log.d("receipt.proof",receipt.proof)
                        pr_proof_of_payment_value.text = getString(R.string.pn_proof_file)
                        pr_proof_of_payment_value.setOnClickListener {
                            val intent = Intent(activity, WebviewActivity::class.java)
                            intent.putExtra("url", receipt.proof!!)
                            Log.d("proof_url", receipt.proof)
                            context!!.startActivity(intent)
//                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(receipt.proof))
//                startActivity(browserIntent)
                        }
                    }
                    else{ pr_proof_of_payment_value.text = "N/A"}
                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE
                } else {

                    Log.d("proj_receipt","N/A")
                    pr_project_title.text =
                        context!!.getString(R.string.project_title).plus(" # ").plus(project_id)
                    pr_amount_paid_value.text = "N/A"
                    pr_payment_date_value.text = "N/A"
                    pr_payment_channel_value.text = "N/A"
                    pr_proof_of_payment_value.text = "N/A"
                    pr_payment_method_value.text ="N/A"

                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE
                }

            })
        }).start()
        back_button.setOnClickListener {
            activity?.onBackPressed()
        }
        return root
    }
}