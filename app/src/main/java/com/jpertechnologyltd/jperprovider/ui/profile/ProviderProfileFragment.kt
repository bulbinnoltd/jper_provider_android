package com.jpertechnologyltd.jperprovider.ui.profile

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.adapter.GalleryAdapter
import com.jpertechnologyltd.jperprovider.adapter.ReviewAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.CompanyProfile
import com.jpertechnologyltd.jperprovider.item.District
import com.stfalcon.imageviewer.StfalconImageViewer


class ProviderProfileFragment : Fragment() {


    var lang: String? = null
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var company_id :Int? = null
    var companyfile : CompanyProfile? = null

    var project_id  = 0
    var company_name_en=""
    var company_name_zh=""
    var company_logo=""
    var company_avg_score=0F
    var company_district_id=0
    var quote_id=0
    var key=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

        company_id = args?.getInt("company_id")
        Log.d("company_id",company_id.toString())
        try{
            company_name_en= args!!.getString("company_name_en", company_name_en)
            company_name_zh= args.getString("company_name_zh", company_name_zh)
            company_logo= args.getString("company_logo", company_logo)
            company_avg_score = args.getFloat("company_avg_score", company_avg_score)
            company_district_id = args.getInt("company_district_id", company_district_id)
        }
        catch (e: Exception){

        }

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_provider_profile, container, false)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE


        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")

        val provider_address_value = root.findViewById<TextView>(R.id.provider_address_value)
        val profile_title = root.findViewById<TextView>(R.id.review_title)
        val profile_about = root.findViewById<TextView>(R.id.provider_desc)
        val provider_address = root.findViewById<TextView>(R.id.provider_address)
        val provider_website = root.findViewById<TextView>(R.id.provider_website)
        val jper_comment = root.findViewById<TextView>(R.id.jper_comment)
        val jper_comment_layout = root.findViewById<ConstraintLayout>(R.id.jper_comment_layout)
        val rating = root.findViewById<RatingBar>(R.id.rating)
        val rating_value = root.findViewById<TextView>(R.id.rating_value)
        val rating_count = root.findViewById<TextView>(R.id.review_number)
        val rating_item1 = root.findViewById<RatingBar>(R.id.rating_item1)
        val rating_item2 = root.findViewById<RatingBar>(R.id.rating_item2)
        val rating_item3 = root.findViewById<RatingBar>(R.id.rating_item3)
        val profile_image = root.findViewById<ImageView>(R.id.profile_image)

        val available_date_title = root.findViewById<TextView>(R.id.available_date_title)

        val gallery_listview = root.findViewById<View>(R.id.gallery_listview) as RecyclerView


        val review_listview = root.findViewById<View>(R.id.review_listview) as RecyclerView

        val review_button = root.findViewById(R.id.review_button) as Button

        var header =  activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
             ?.getString("user_token", "")

        val back_button = root.findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {

                activity!!.onBackPressed()

    }

        Log.d("provider_company_id",company_id.toString())

        val district =  District(context!!,company_district_id).name
        provider_address_value.text =district

        Thread(Runnable {
            companyfile = HttpsService.GetMyCompanyInfo(
                context,
                "https://uat.jper.com.hk/api/company/",
                company_id!!,
                lang
            )

            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {
                if (companyfile != null) {
                    Log.d("jper_comment_in_provider",companyfile!!.jper_comment)
                    if (lang!!.equals("zh")) profile_title.text =
                        companyfile!!.company_name_zh else profile_title.text =
                        companyfile!!.company_name_en
                    if (!companyfile!!.about.equals("N/A")) {
                      profile_about.text = companyfile!!.about
                    }
                    else{
                        profile_about.visibility=View.GONE
                    }
                    if (!companyfile!!.jper_comment.equals("N/A")) {
                        jper_comment.text = companyfile!!.jper_comment
                        jper_comment_layout.visibility=View.GONE
                    }
                    else{
                        jper_comment_layout.visibility=View.GONE
                    }
                    if (!companyfile!!.address.equals("N/A")) {
                        provider_address.text = companyfile!!.address
                        provider_address.visibility = View.GONE
                    }
                    Log.d("companyfile!!.website",companyfile!!.website)

                    if (!companyfile!!.website.equals("N/A")) {
                        provider_website.text = HtmlCompat.fromHtml(
                            "<a><font color=#8FAD7B>" + companyfile!!.website
                                    + "</font></a>", HtmlCompat.FROM_HTML_MODE_LEGACY

                        )
                        provider_website.setOnClickListener{arg0 ->
                            val intent = Intent(activity, WebviewActivity::class.java)
                            intent.putExtra("url",companyfile!!.website)
                            context!!.startActivity(intent)
                        }
                        provider_website.visibility = View.VISIBLE
                    }
                    else{ provider_website.visibility = View.GONE}
                    rating.rating = companyfile!!.avg_rate!!
                    rating_item1.rating =
                        String.format("%.1f", companyfile!!.quality_rate).toFloat()
                    rating_value.text =
                        String.format("%.1f", companyfile!!.avg_rate).toDouble().toString()
                    rating_item2.rating =
                        String.format("%.1f", companyfile!!.communication_rate).toFloat()
                    rating_item3.rating =
                        String.format("%.1f", companyfile!!.punctuality_rate).toFloat()
                    rating_count.text = "(".plus(companyfile!!.rate_count.toString())
                }
                try {
                    val glideUrl = GlideUrl(
                        companyfile!!.logo,
                        LazyHeaders.Builder()
                            .addHeader("Authorization", "Bearer ".plus(header))
                            .build()
                    )

                    Glide.with(context!!).load(glideUrl)
                        .placeholder(R.drawable.profile_pic_placeholder).into(
                            profile_image
                        )

                } catch (e: Exception) {

                    Glide.with(context!!).load(R.drawable.profile_pic_placeholder).into(
                        profile_image
                    )

                }


                profile_image.setOnClickListener {

                    //ToDo
                    val listimages =

                        arrayOf<String?>(
                            "https://uat.jper.com.hk/style/image/7",
                            "https://uat.jper.com.hk/style/image/7"
                        )
                    Log.d("profile?.icon", companyfile!!.logo.toString())
                    if (!companyfile!!.logo.toString().equals("")) {
                        try {
                            val glideUrl = GlideUrl(
                                companyfile!!.logo,
                                LazyHeaders.Builder()
                                    .addHeader("Authorization", "Bearer ".plus(header))
                                    .build()
                            )
                            val profileimage =

                                arrayOf<String?>(companyfile!!.logo.toString())

                            StfalconImageViewer.Builder<String>(
                                context,
                                profileimage
                            ) { view, image ->

                                Glide.with(context!!).load(glideUrl)
                                    .placeholder(R.drawable.profile_pic_placeholder).into(
                                        view
                                    )

                            }.show()

                        } catch (e: Exception) {

                            val profileimage =

                                arrayOf<String?>(companyfile!!.logo.toString())

                            StfalconImageViewer.Builder<String>(
                                context,
                                profileimage
                            ) { view, image ->

                                Glide.with(context!!).load(R.drawable.profile_pic_placeholder).into(
                                    view
                                )

                            }.show()
                        }

                    } else {
                        val profileimage =

                            arrayOf<String?>(companyfile!!.logo.toString())

                        StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->

                            Glide.with(context!!).load(R.drawable.profile_pic_placeholder)
                                .into(view)

                        }.show()
                    }

                }
                val gallery_adapter = companyfile?.portfolios?.let { GalleryAdapter(it,activity!!) }
                gallery_listview.adapter = gallery_adapter
                gallery_listview.layoutManager = LinearLayoutManager(
                    activity,
                    RecyclerView.HORIZONTAL,
                    false
                )

                val review_adapter = companyfile?.comments?.let { ReviewAdapter(it, 3) }
                review_listview.adapter = review_adapter
                review_listview.layoutManager = LinearLayoutManager(
                    activity,
                    RecyclerView.VERTICAL,
                    false
                )

                if (companyfile != null) {

                    if (companyfile!!.comments.size > 0) {

                        val viewcommentFragment = ViewReviewFragment()

                        review_button.setOnClickListener {

                            val bundle = Bundle()
                            if (companyfile != null) {
                                bundle.putParcelableArrayList("data", companyfile!!.comments)
                                bundle.putString(
                                    "rating_value", String.format(
                                        "%.1f",
                                        companyfile!!.avg_rate
                                    ).toDouble().toString()
                                )
                                bundle.putString(
                                    "rating_count",
                                    "(".plus(companyfile!!.rate_count.toString())
                                )
                                bundle.putFloat("rating", companyfile!!.avg_rate!!)
                                bundle.putFloat("rating_item1", companyfile!!.quality_rate!!)
                                bundle.putFloat("rating_item2", companyfile!!.communication_rate!!)
                                bundle.putFloat("rating_item3", companyfile!!.punctuality_rate!!)
                                bundle.putInt("company_id", company_id!!)


                                viewcommentFragment.setArguments(bundle)

                            }
                            var fr = getFragmentManager()?.beginTransaction()
                            fr?.addToBackStack(null)
                            fr?.add(R.id.nav_host_fragment, viewcommentFragment)
                            //fr?.replace(R.id.nav_host_fragment, PortfolioFragment())
                            fr?.commit()
                        }
                    } else {
                        review_button.visibility = View.GONE
                        available_date_title.visibility = View.GONE
                    }
                }

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()


        return root
    }
}

