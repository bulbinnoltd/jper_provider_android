package com.jpertechnologyltd.jperprovider.ui.myproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.fragment.app.Fragment
//import com.asiapay.sdk.PaySDK
//import com.asiapay.sdk.integration.*
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.component.HttpsService.CreatePaymentNotice
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetPaymentPlanNotice
import com.jpertechnologyltd.jperprovider.item.myproject.PaymentPlanNotice
import java.text.DecimalFormat
import java.util.*


class PaymentNoticeFragment : Fragment() {

    private lateinit var paymentIntentClientSecret: String
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var plan_id :Int? = null
    var project_id :Int? = null
    var notice_id :Int? = null
    var is_final :Boolean = false
    var header : String? = null
    var lang : String? = null
    var owner_name : String? = null
    var owner_email : String? = null
    var company_address : String? = null
    var company_name_zh : String? = null
    var company_name_en : String? = null
    var paymentplan_data:PaymentPlanNotice?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

        plan_id = args?.getInt("plan_id")
        notice_id = args?.getInt("notice_id")
        project_id = args?.getInt("project_id")
        owner_name = args?.getString("owner_name")
        owner_email = args?.getString("owner_email")
        company_address = args?.getString("company_address")
        company_name_en = args?.getString("company_name_en")
        company_name_zh = args?.getString("company_name_zh")
        try {
            is_final = args!!.getBoolean("is_final")
        }
        catch (e:Exception){
            is_final = false
        }


    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_payment_notice, container, false)
        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val not_final_view = root.findViewById(R.id.not_final_view) as LinearLayout
        val not_final_space = root.findViewById(R.id.not_final_space) as View
        val pn_provider_company_name = root.findViewById(R.id.pn_provider_company_name) as TextView
        val pn_provider_address = root.findViewById(R.id.pn_provider_address) as TextView
        val pn_owner_username = root.findViewById(R.id.pn_owner_username) as TextView
        val pn_owener_email = root.findViewById(R.id.pn_owener_email) as TextView
        val pn_project_title = root.findViewById(R.id.pn_project_title) as TextView
        val pn_payment_name_value = root.findViewById(R.id.pn_payment_name_value) as TextView
        val pn_invoice_amount_value = root.findViewById(R.id.pn_invoice_amount_value) as TextView
        val pn_amount_due_value = root.findViewById(R.id.pn_amount_due_value) as TextView
        val pn_due_by = root.findViewById(R.id.pn_due_by) as TextInputEditText
        val pn_retention_applied_value = root.findViewById(R.id.pn_retention_applied_value) as TextView
        val pn_create_button = root.findViewById(R.id.pn_create_button) as Button

        pn_due_by.setText(""+year+"-"+(month+1)+"-"+day)
        pn_owner_username.setText(owner_name)
        pn_owener_email.setText(owner_email)
        pn_provider_address.text = company_address
        if(lang.equals("en")){
            pn_provider_company_name.setText(company_name_en)
        }
        else{
            pn_provider_company_name.setText(company_name_zh)

        }
        Thread(Runnable {
            Log.d("planid_project_id",plan_id.toString() +"  "+ project_id.toString())
            paymentplan_data = GetPaymentPlanNotice(context!!,"https://uat.jper.com.hk/api/company/project/".plus(project_id).plus("/payment/").plus(plan_id).plus("/notice"),header,lang)
            val staffprofile = HttpsService.GetUserProfile(context,"https://uat.jper.com.hk/api/company/staff/profile",header,lang)
            val companyfile = HttpsService.GetCompanyInfo(context,"https://uat.jper.com.hk/api/company/profile",header,lang)
            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {

                if(paymentplan_data!=null){
                    if(lang.equals("zh")) pn_provider_company_name.text = paymentplan_data!!.company_name_zh else pn_provider_company_name.text =  paymentplan_data!!.company_name_en

                    pn_project_title.text = context!!.getString(R.string.project_title).plus(" # ").plus(project_id)
                    pn_payment_name_value.text = paymentplan_data!!.payment_title
                    try {
                        val number: String = paymentplan_data!!.plan_amount
                        val amount = number.toDouble()
                        val formatter = DecimalFormat("#,###.##")
                        var formatted = formatter.format(amount)
                        if (formatted.contains(".00")) formatted =
                            formatted.substring(0, formatted.length - 3)
                        pn_invoice_amount_value.text = "HK $" + formatted
                    } catch (e: Exception) {
                        e.printStackTrace()
                        pn_invoice_amount_value.text = "HK$ ".plus(paymentplan_data!!.plan_amount)
                    }
                    try {
                        val number: String = paymentplan_data!!.retention_amount
                        val amount = number.toDouble()
                        val formatter = DecimalFormat("#,###.##")
                        var formatted = formatter.format(amount)
                        if (formatted.contains(".00")) formatted =
                            formatted.substring(0, formatted.length - 3)
                        pn_retention_applied_value.text = "HK $" + formatted
                    } catch (e: Exception) {
                        e.printStackTrace()
                        pn_retention_applied_value.text = "HK$ ".plus(paymentplan_data!!.retention_amount)
                    }
                    try {
                        val number: String = paymentplan_data!!.plan_amount
                        val amount = number.toDouble()
                        val formatter = DecimalFormat("#,###.##")
                        var formatted = formatter.format(amount)
                        if (formatted.contains(".00")) formatted =
                            formatted.substring(0, formatted.length - 3)
                        pn_amount_due_value.text = "HK $" + formatted
                    } catch (e: Exception) {
                        e.printStackTrace()
                        pn_amount_due_value.text = "HK$ ".plus(paymentplan_data!!.plan_amount)
                    }


                    if(is_final!! && paymentplan_data!!.scope ==1){
                        not_final_view.visibility = View.VISIBLE
                        not_final_space.visibility = View.VISIBLE
                    }
                }
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()


        pn_create_button.setOnClickListener{
            var result:Boolean?=null
            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE
            Thread(Runnable {
                result = CreatePaymentNotice(context,"https://uat.jper.com.hk/api/company/project/payment/notice/create",header,plan_id!!,project_id!!,pn_due_by.text.toString(),lang)
                activity?.runOnUiThread(java.lang.Runnable {

                    if (result != null) {

                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        val dialog = Dialog(context!!)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        dialog.setCanceledOnTouchOutside(false)
                        dialog.setContentView(R.layout.dialog_message)
                        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
                        dialog_message.text = getString(R.string.Success_msg)
                        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

                        val close_button = dialog.findViewById(R.id.close_button) as ImageView
                        close_button.visibility = View.INVISIBLE
                        confirm_button.setOnClickListener {
                            dialog.dismiss()
                            val editor = activity!!.getSharedPreferences(
                                "MyPreferences",
                                Context.MODE_PRIVATE
                            ).edit()
                            editor.putBoolean("refresh_paymentplan", true)
                            editor.apply()
                            activity!!.onBackPressed()
                        }

                        close_button.setOnClickListener {
                            dialog.dismiss()
                        }

                        dialog.setCancelable(true)
                        dialog.show()

                    }
                })
            }).start()
        }
        pn_due_by.setOnClickListener{
            val dpd = DatePickerDialog(context!!,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    pn_due_by.setText(""+mYear+"-"+(mMonth+1)+"-"+mDay)
                },year,month,day)
            dpd.datePicker.minDate = System.currentTimeMillis() -1000
            if(lang.equals("en")) {
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancle_button), dpd)
            }
            else{
                dpd.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm_button), dpd)
                dpd.setButton(DialogInterface.BUTTON_NEGATIVE,  getString(R.string.cancle_button), dpd)
            }
            dpd.show()
        }
        val back_button = root.findViewById(R.id.back_button) as ImageView
        back_button.setOnClickListener {
            activity!!.onBackPressed()
        }

        return root
    }
    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
}