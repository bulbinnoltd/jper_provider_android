package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.adapter.QuoteProjectAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetAdvertisment
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetNotificationCount
import com.jpertechnologyltd.jperprovider.item.Advertisment
import com.jpertechnologyltd.jperprovider.item.QuoteProject
import com.jpertechnologyltd.jperprovider.ui.activity.ActivityFragment
import com.jpertechnologyltd.jperprovider.ui.calendar.CalendarFragment
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import java.util.*
import java.util.concurrent.Future
import kotlin.collections.ArrayList


class QuoteProjectFragment : Fragment() {
    var loading: Future<JsonObject>? = null
    var unreaddot: TextView? = null

    private lateinit var broadcastReceiver: BroadcastReceiver
    lateinit var  mQuoteProjectAdapter : QuoteProjectAdapter
    lateinit var Postdata : JsonArray
    lateinit var morequoteprojects : ArrayList<QuoteProject?>
    lateinit var quoteprojects : ArrayList<QuoteProject?>
    private var nextpageurl : String? = null
    private var end = false
    private var currentpageurl: String? = null
    var checked_checkbox=""
    private var selected_tab = 0
    lateinit var layoutManager: LinearLayoutManager
    var checked_checkbox_array = ArrayList<String>()
    var ticked_radiobutton_array = ArrayList<String>()
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    private var header : String? = null
    var unread_msg_no = 0

    private var lang : String? = null
    private var Jper_ads : ArrayList<Advertisment>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       val root = inflater.inflate(R.layout.fragment_quoteproject, container, false)
        header = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
        lang =context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE

        val config = resources.configuration
        val locale = Locale(lang)


        Locale.setDefault(locale)
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)


        val editor = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
        editor.remove("filter_request_url")
        editor.apply()

        val checkbox = arrayOfNulls<CheckBox>(18)

        val calendar_button = root.findViewById(R.id.calendar_button) as ImageView
        calendar_button.setOnClickListener{
            activity!!.supportFragmentManager.beginTransaction()
            .add(R.id.nav_host_fragment, CalendarFragment())
            .addToBackStack(null)
            .commit()
        }
        val notification_button = root.findViewById(R.id.notification_button) as ImageView
        notification_button.setOnClickListener{
            activity!!.supportFragmentManager.beginTransaction()
                .add(R.id.nav_host_fragment, ActivityFragment())
                .addToBackStack(null)
                .commit()
        }
        val tabLayout = root.findViewById(R.id.tabLayout) as TabLayout
        unreaddot = root.findViewById(R.id.unread_number_total) as TextView

        Thread(Runnable {
            unread_msg_no = GetNotificationCount(context, "https://uat.jper.com.hk/api/my/notification",header, lang)
            try {

                if (lang.equals("zh")) {
                    Jper_ads = GetAdvertisment(
                        context!!,
                        "https://uat.jper.com.hk/api/zh_hk/dynamic/board/provider",
                        header,
                        lang
                    )
                } else {
                    Jper_ads = GetAdvertisment(
                        context!!,
                        "https://uat.jper.com.hk/api/en/dynamic/board/provider",
                        header,
                        lang
                    )

                }

            }
            catch (e:Exception){

            }
            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {

                if(unread_msg_no>0) {
                    unreaddot!!.visibility = View.VISIBLE
                    unreaddot!!.text = unread_msg_no.toString()
                }
                if (Jper_ads != null) {
                    val carouselView: CarouselView
                    carouselView = root.findViewById(R.id.carouselView)
                    carouselView.setImageListener(JperimageListener)
                    carouselView.setPageCount(Jper_ads!!.size)
                }

                val viewpager = root.findViewById(R.id.pager) as ViewPager
                setupViewPager(viewpager)
                tabLayout.setupWithViewPager(viewpager)


                val onTabSelectedListener: TabLayout.OnTabSelectedListener = object :
                    TabLayout.OnTabSelectedListener {
                    override fun onTabSelected(tab: TabLayout.Tab) {
                        selected_tab = tabLayout.selectedTabPosition
                        Log.d("selectedTabPosition", tabLayout.selectedTabPosition.toString())
                        var filter_request_url = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                            ?.getString("filter_request_url", null)
                        if(filter_request_url!=null){
                            val editor = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
                            editor.putString("filter_request_url", filter_request_url)
                            editor.apply()
                            Log.d("filter_request_url_tab_selected",filter_request_url.toString())
                        }
                    }

                    override fun onTabUnselected(tab: TabLayout.Tab) {


                    }

                    override fun onTabReselected(tab: TabLayout.Tab) {}
                }
                tabLayout.addOnTabSelectedListener(onTabSelectedListener)
                Log.d("selectedTabPosition_init", tabLayout.selectedTabPosition.toString())

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()
        val filter_button = root.findViewById(R.id.filter_button) as ImageView
        // set on-click listener
        filter_button.setOnClickListener {

            val dialog = activity?.let { it1 -> BottomSheetDialog(
                it1,
                R.style.AppBottomSheetDialogTheme
            ) }
            dialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setOnShowListener {
                val dialog = it as BottomSheetDialog
                val bottomSheet = dialog.findViewById<View>(R.id.design_bottom_sheet)
                bottomSheet?.let { sheet ->
                    dialog.behavior.peekHeight = sheet.height
                    sheet.parent.parent.requestLayout()
                }
            }
            val view = layoutInflater.inflate(R.layout.fragment_fragment_quotefilter, null)
            if (dialog != null) {
                dialog.setContentView(view)
            }
            if (dialog != null) {
                dialog.show()
            }
            val close = view.findViewById<ImageView>(R.id.back_button)
            close.setOnClickListener{
                if (dialog != null) {
                    dialog.dismiss()
                }
            }
            val dist1_checkBox = view.findViewById(R.id.dist1_checkBox) as CheckBox
            val dist2_checkBox = view.findViewById(R.id.dist2_checkBox) as CheckBox
            val dist3_checkBox = view.findViewById(R.id.dist3_checkBox) as CheckBox
            val dist4_checkBox = view.findViewById(R.id.dist4_checkBox) as CheckBox
            val dist5_checkBox = view.findViewById(R.id.dist5_checkBox) as CheckBox
            val dist6_checkBox = view.findViewById(R.id.dist6_checkBox) as CheckBox
            val dist7_checkBox = view.findViewById(R.id.dist7_checkBox) as CheckBox
            val dist8_checkBox = view.findViewById(R.id.dist8_checkBox) as CheckBox
            val dist9_checkBox = view.findViewById(R.id.dist9_checkBox) as CheckBox
            val dist10_checkBox = view.findViewById(R.id.dist10_checkBox) as CheckBox
            val dist11_checkBox = view.findViewById(R.id.dist11_checkBox) as CheckBox
            val dist12_checkBox = view.findViewById(R.id.dist12_checkBox) as CheckBox
            val dist13_checkBox = view.findViewById(R.id.dist13_checkBox) as CheckBox
            val dist14_checkBox = view.findViewById(R.id.dist14_checkBox) as CheckBox
            val dist15_checkBox = view.findViewById(R.id.dist15_checkBox) as CheckBox
            val dist16_checkBox = view.findViewById(R.id.dist16_checkBox) as CheckBox
            val dist17_checkBox = view.findViewById(R.id.dist17_checkBox) as CheckBox
            val dist18_checkBox = view.findViewById(R.id.dist18_checkBox) as CheckBox

            val radioButton1 = view.findViewById(R.id.scope1_radiobutton) as RadioButton
            val radioButton2 = view.findViewById(R.id.scope2_radiobutton) as RadioButton

            val resetBtn = view.findViewById<TextView>(R.id.filter_reset)
            Log.d("array_size", checked_checkbox_array.size.toString())
            if(checked_checkbox_array.size>0){
                for(x in 0 until checked_checkbox_array.size) {

                    val id_checked_checkbox= checked_checkbox_array.get(x)
                    Log.d("check_box", x.toString() + " value:" + id_checked_checkbox)
                    val id :Int = getResources().getIdentifier(
                        "dist" + id_checked_checkbox + "_checkBox",
                        "id",
                        context!!.getPackageName()
                    )
                    view.findViewById<CheckBox>(id).isChecked = true
                }
            }
            if(ticked_radiobutton_array.size>0){
                val id_ticked_radiobutton= ticked_radiobutton_array.get(0)
                ticked_radiobutton_array.get(0)
                Log.d(
                    "radio_button",
                    id_ticked_radiobutton.toString() + " value:" + id_ticked_radiobutton
                )
                val id :Int = getResources().getIdentifier(
                    "scope" + id_ticked_radiobutton + "_radiobutton",
                    "id",
                    context!!.getPackageName()
                )
                view.findViewById<RadioButton>(id).isChecked = true
            }
            radioButton1.setOnClickListener {radioButton2.isChecked=false}
            radioButton2.setOnClickListener {radioButton1.isChecked=false}

            resetBtn.setOnClickListener {
                dist1_checkBox.isChecked=false
                dist2_checkBox.isChecked=false
                dist3_checkBox.isChecked=false
                dist4_checkBox.isChecked=false
                dist5_checkBox.isChecked=false
                dist6_checkBox.isChecked=false
                dist7_checkBox.isChecked=false
                dist8_checkBox.isChecked=false
                dist9_checkBox.isChecked=false
                dist10_checkBox.isChecked=false
                dist11_checkBox.isChecked=false
                dist12_checkBox.isChecked=false
                dist13_checkBox.isChecked=false
                dist14_checkBox.isChecked=false
                dist15_checkBox.isChecked=false
                dist16_checkBox.isChecked=false
                dist17_checkBox.isChecked=false
                dist18_checkBox.isChecked=false

                radioButton1.isChecked=false
                radioButton2.isChecked=false
            }
            val apply_button = view.findViewById<Button>(R.id.apply_button)
            apply_button.setOnClickListener {

                checked_checkbox = ""
                checked_checkbox_array = ArrayList<String>()
                var ticked_radiobutton = "scope=".plus(selected_tab + 1)
                ticked_radiobutton_array = ArrayList<String>()
                var filter_request = ""
                for(i in 0..17) {
                    val id :Int = getResources().getIdentifier(
                        "dist" + (i + 1) + "_checkBox",
                        "id",
                        context!!.getPackageName()
                    )
                    checkbox[i] = view.findViewById<CheckBox>(id)
                    Log.d("checked", checkbox[i].toString())

                    try{
                        if(checkbox[i]!!.isChecked) {
                            //no check box hv added b4
                            if(checked_checkbox.equals("")){
//                                checked_checkbox ="districts="+ ((i+1).toString())
                                checked_checkbox ="&districts="
                                checked_checkbox_array.add(((i + 1).toString()))
                                Log.d("checked", (i + 1).toString())

                            }
                            else{
                                //hv check box hv added b4
//                                checked_checkbox += ","+((i+1).toString())
                                checked_checkbox_array.add(((i + 1).toString()))
                                Log.d("checked", (i + 1).toString())
                            }
                        }
                    }
                    catch (e: Exception){
//
//                        Loading!!.visibility = View.GONE
//                        progressOverlay!!.visibility = View.GONE

                    }

                }
                checked_checkbox=checked_checkbox.plus(
                    checked_checkbox_array.toString().replace(
                        "\\s".toRegex(),
                        ""
                    )
                )
                Log.d("checked_checkbox_request", checked_checkbox)


                if(checked_checkbox.equals("[]")) checked_checkbox =""

                if(radioButton1.isChecked) {
                    ticked_radiobutton = "scope=1"
                    ticked_radiobutton_array.add("1")
                }
                else if(radioButton2.isChecked) {
                    ticked_radiobutton = "scope=2"
                    ticked_radiobutton_array.add("2")
                }

                //form the rquest of filtering

                //save filter request url
                editor.putString("filter_request_url", checked_checkbox)
                editor.apply()

                val viewpager = root.findViewById(R.id.pager) as ViewPager
                setupViewPager(viewpager)
                tabLayout.setupWithViewPager(viewpager)
                Log.d("ticked_radiobutton", ticked_radiobutton)
                if(ticked_radiobutton.equals("scope=1")){
//
//                    val bundle = Bundle()
//
//                    bundle.putString("filter_request_url", checked_checkbox)
//                    val FullBidding = FullBiddingFragment()
//                    FullBidding.setArguments(bundle)
//                    Log.d("FullBidding", "FullBidding")
                    val tab = tabLayout.getTabAt(0)
                    tab!!.select()


                }
                else if (ticked_radiobutton.equals("scope=2")){
                    val PartialBidding = PartialBiddingFragment()
                    Log.d("PartialBidding", "PartialBidding")

                    val tab = tabLayout.getTabAt(1)
                    tab!!.select()

                }
                else {

                    val tab = tabLayout.getTabAt(selected_tab)

                    Log.d("selectedTabPosition", selected_tab.toString())
                    tab!!.select()
                }
                Log.d("filter_request", filter_request)
                dialog.dismiss()

            }
        }
//        val carouselView: CarouselView
//        carouselView = root.findViewById(R.id.carouselView)
//        carouselView.setPageCount(sampleImages.size)
//        carouselView.setImageListener(imageListener)
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1 != null) {
                    handleBroadcastActions(p1)
                }
            }

        }

        return root
    }
    val JperimageListener: ImageListener = object : ImageListener {
        override fun setImageForPosition(position: Int, imageView: ImageView) {
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            val glideUrl = GlideUrl(
                Jper_ads!!.get(position).cover_url,
                LazyHeaders.Builder()
                    .addHeader("Authorization", "Bearer ".plus(header))
                    .build()
            )
            Glide.with(context!!).load(glideUrl).into(imageView)
            imageView.setOnClickListener {

                val intent = Intent(activity, WebviewActivity::class.java)
                intent.putExtra("url", Jper_ads!!.get(position).destination)
                context!!.startActivity(intent)
                Log.d("destination", Jper_ads!!.get(position).destination.toString())
            }
        }
    }
    fun isEmptyString(text: String?): Boolean {
        return text == null || text.trim { it <= ' ' } ==    "null" || text.trim { it <= ' ' }
            .isEmpty()
    }
    fun getRequest():String{
        return checked_checkbox
    }

    private fun setupViewPager(viewPager: ViewPager): Unit {
        val adapter = Adapter(childFragmentManager)

        val Quoteproject_full_tab: String = getString(R.string.Quoteproject_full_tab)
        val Quoteproject_partial_tab: String = getString(R.string.Quoteproject_partial_tab)

        val FullBidding = FullBiddingFragment()
        val PartialBidding =PartialBiddingFragment()


        adapter.addFragment(FullBidding, Quoteproject_full_tab)
        adapter.addFragment(PartialBidding, Quoteproject_partial_tab)

        viewPager.adapter = adapter
    }

    private fun handleBroadcastActions(intent: Intent) {
        when (intent.action) {
            "Reload" -> {
                Thread(Runnable {
                    unread_msg_no = GetNotificationCount(context, "https://uat.jper.com.hk/api/my/notification",header, lang)


                    activity?.runOnUiThread(java.lang.Runnable {

                        if(unread_msg_no>0) {
                            unreaddot!!.visibility = View.VISIBLE
                            unreaddot!!.text = unread_msg_no.toString()
                        }
                    })
                }).start()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(broadcastReceiver, IntentFilter("Reload"))

    }

    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(broadcastReceiver)
    }
    internal class Adapter(manager: FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(
            fragment: Fragment,
            title: String
        ) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

}