package com.jpertechnologyltd.jperprovider.ui.chat

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.item.Chat
import com.jpertechnologyltd.jperprovider.item.ChatImage
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.ChatAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService.ReadMessage
import com.jpertechnologyltd.jperprovider.component.HttpsService.SendMessage
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.builder.Builders
import java.io.File
import java.net.URLDecoder
import java.net.URLEncoder
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Future
import kotlin.collections.ArrayList

class ChatActivity : AppCompatActivity() {
    var Postdata : JsonArray? = null
    var chat_adapter : ChatAdapter?=null
    lateinit var morechats : ArrayList<Chat?>
    var chats : ArrayList<Chat?> =ArrayList<Chat?>()
    private var nextpageurl : String? = null
    private var end = false
    private var currentpageurl: String? = null
    lateinit var layoutManager: LinearLayoutManager
    private var lang : String? = null
    private var user_id : Int? = null
    private var key : String? = null
    private var header : String? = null
    private var message_listview : RecyclerView? = null
    lateinit var moreportchats : ArrayList<Chat?>
    var loading: Future<JsonObject>? = null
    var msg ="N/A"
    var  project_id:Int = 0
    var  quote_id:Int= 0
    var  company_id:Int= 0
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var result:Boolean? = null
    private var attach_image_file: File? = null

    val FileUploadArray: java.util.ArrayList<File> = java.util.ArrayList()

    private lateinit var broadcastReceiver: BroadcastReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        user_id = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getInt("user_id", 0)
        company_id=intent.getIntExtra("company_id", 0)
        val company_name_en=intent.getStringExtra("company_name_en")
        val company_name_zh=intent.getStringExtra("company_name_zh")
        val company_logo=intent.getStringExtra("company_logo")
        val company_avg_score=intent.getFloatExtra("company_avg_score", 0F)
        quote_id=intent.getIntExtra("quotation_id", 0)
        project_id=intent.getIntExtra("project_id", 0)
        key=intent.getStringExtra("key")

        val profile_image  = findViewById(R.id.profile_image) as ImageView
        val company_title = findViewById(R.id.company_title) as TextView
        val rating = findViewById(R.id.rating) as RatingBar
        val message_textview = findViewById(R.id.message_textview) as TextInputEditText
        val header_content = findViewById(R.id.header_content) as ConstraintLayout

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        /////////////////////Initiate_loading_anim///////////////////////////

        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE


//        header_content.setOnClickListener {
//
//
//            val intent = Intent(this, MainActivity::class.java)
//            intent.putExtra("navigation_id", R.id.navigation_myprojects)
//            intent.putExtra("fragment_id", R.id.ProviderProfileFragment)
//
//            Log.d("company_id_inchatact", company_id.toString())
//            intent.putExtra("company_id", company_id)
//
//            intent.putExtra("company_id", company_id)
//            intent.putExtra("company_name_en", company_name_en)
//            intent.putExtra("company_name_zh", company_name_zh)
//            intent.putExtra("company_logo", company_logo)
//            intent.putExtra("company_avg_score", company_avg_score)
//            intent.putExtra("key", key)
//            intent.putExtra("quotation_id", quote_id)
//            intent.putExtra("project_id", project_id)
//            intent.putExtra("back_activity", "ChatActivity")
//
//            startActivity(intent)
//        }

        if(lang!!.equals("zh")){
            if(isEnglish(company_name_zh) && company_name_zh.length>18) {
                var company_name_splited=company_name_zh.chunked(18)
                company_title.text = company_name_splited[0].plus("…")
            }
            else if (!isEnglish(company_name_zh) && company_name_zh.length>8){

                var company_name_splited=company_name_zh.chunked(8)
                company_title.text = company_name_splited[0].plus("…")
            }
            else
                company_title.text = company_name_zh
        }
        else {
            if(isEnglish(company_name_en) && company_name_en.length>18) {
                var company_name_splited=company_name_en.chunked(18)
                company_title.text = company_name_splited[0].plus("…")
            }
            else if (!isEnglish(company_name_en) && company_name_en.length>8){

                var company_name_splited=company_name_en.chunked(8)
                company_title.text = company_name_splited[0].plus("…")
            }
            else
                company_title.text = company_name_en
        }

        rating.rating= company_avg_score.toFloat()

//        val report_button = findViewById(R.id.report_button) as Button
//        report_button.setOnClickListener {
//            var result =false
//
//            val dialog = Dialog(this)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//            dialog.setCanceledOnTouchOutside(false)
//            dialog.setContentView(R.layout.dialog_report_message)
//
//
//            val dialog_title = dialog.findViewById(R.id.dialog_title) as TextView
//            dialog_title.text = getString(R.string.choose_action)
//
//            val close_button = dialog.findViewById(R.id.close_button) as ImageView
//
//            close_button.setOnClickListener {
//                dialog.dismiss()
//            }
//
//            val delete_button = dialog.findViewById(R.id.delete_button) as Button
//            delete_button.text = getString(R.string.cancle_button)
//            delete_button.setOnClickListener {
//                dialog.dismiss()
//            }
//
//            val comment = dialog.findViewById(R.id.comment) as TextInputEditText
//
//
//            val save_button = dialog.findViewById(R.id.save_button) as Button
//            save_button.text = getString(R.string.report_button)
//            save_button.setOnClickListener {
//                dialog.dismiss()
//                try {
//                    msg = comment.text.toString()
//
//                    Log.d("getstring", msg)
//                }
//                catch (e: Exception){
//                    Log.d("getstring", e.toString())
//                    msg ="N/A"
//                }
//                Loading!!.visibility = View.VISIBLE
//                progressOverlay!!.visibility = View.VISIBLE
//                Thread(Runnable {
//
//                    result = ComplaintProvider(
//                        this,
//                        "https://uat.jper.com.hk/api/owner/complaint/project",
//                        header,
//                        project_id,
//                        quote_id,
//                        msg
//                    )
//
//                    this.runOnUiThread(java.lang.Runnable {
//                        if (result) {
//
//                            Loading!!.visibility = View.GONE
//                            progressOverlay!!.visibility = View.GONE
//                            showDialog(getString(R.string.General_status_completed))
//                            Log.d("report", "ok")
//                        } else {
//                            Log.d("report", "fail")
//
//                            showDialog("Error")
//                            Loading!!.visibility = View.GONE
//                            progressOverlay!!.visibility = View.GONE
//                        }
//                    })
//                }).start()
//            }
//
//            dialog.setCancelable(true)
//            dialog.show()
//
//
//        }

//        val view_quote_button = findViewById(R.id.view_quote_button) as Button
//        view_quote_button.setOnClickListener {
//            if(chats.size>0)
//                message_listview!!.scrollToPosition(chats.size - 1)
//            val intent = Intent(this, MainActivity::class.java)
//            intent.putExtra("navigation_id", R.id.navigation_myprojects)
//            intent.putExtra("back_activity", "ChatActivity")
//            intent.putExtra("fragment_id", R.id.QuotationDetailFragment)
//            intent.putExtra("project_id", project_id)
//            intent.putExtra("quotation_id", quote_id)
//            intent.putExtra("company_id", company_id)
//            intent.putExtra("company_name_en", company_name_en)
//            intent.putExtra("company_name_zh", company_name_zh)
//            intent.putExtra("company_logo", company_logo)
//            intent.putExtra("company_avg_score", company_avg_score)
//            intent.putExtra("key", key)
//
//
//            startActivity(intent)
//        }
        Log.d("provider icon 3", company_logo)

        Glide.with(this).load(company_logo).placeholder(R.drawable.profile_pic_placeholder).into(
            profile_image
        )


        val send_button = findViewById(R.id.send_button) as ImageView
        // set on-click listener
        send_button.setOnClickListener {

            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE

            Thread(Runnable {
                var new_msg = URLEncoder.encode(message_textview.text.toString(), "UTF-8")
                new_msg = new_msg.replace("+", "%20")

                try {

                    result = SendMessage(
                        this,
                        "https://uat.jper.com.hk/api/chatroom/send",
                        header,
                        key,
                        new_msg,
                        lang
                    )
                    Log.d("send_msg_result", result.toString())
                    this.runOnUiThread(java.lang.Runnable {
                        if (result != null && result!!) {


                            var images: ArrayList<String> = ArrayList()
                            val calendar = Calendar.getInstance(TimeZone.getDefault())


                            val thisMonth = (calendar[Calendar.MONTH] + 1).toString()
                            val thisDate = calendar[Calendar.DATE].toString()
                            val thisHour =
                                if (calendar[Calendar.HOUR_OF_DAY] < 10) "0".plus(calendar[Calendar.HOUR_OF_DAY]) else calendar[Calendar.HOUR_OF_DAY].toString()
                            val thisMin =
                                if (calendar[Calendar.MINUTE] < 10) "0".plus(calendar[Calendar.MINUTE]) else calendar[Calendar.MINUTE].toString()
                            val created_at = thisDate.plus("/").plus(thisMonth).plus("  ").plus(
                                thisHour
                            ).plus(":").plus(thisMin)
                            message_textview.setText("")
                            new_msg =
                                if (new_msg == null) "" else URLDecoder.decode(new_msg, "UTF-8")
                            chats.add(Chat(0, new_msg, 0, true, created_at, ChatImage(1, "", null)))
                            chat_adapter?.refresh()



                            message_listview!!.scrollToPosition(chat_adapter!!.itemCount - 1)
                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE

                        } else {

                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE
                            message_listview!!.scrollToPosition(chat_adapter!!.itemCount - 1)
                            showDialog("Error")
                        }
                    })

                } catch (e: Exception) {
                    Log.d("error", e.toString())

                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE
                }

            }).start()



//            images = ArrayList()
//            images.add("https://www.hypesphere.com/files/news/5f27eacc34b81.jpg")
//            images.add("https://www.hypesphere.com/files/news/5f27eacc34b81.jpg")
//            images.add("https://www.hypesphere.com/files/news/5f27eacc34b81.jpg")
//            val chat_item = Chat(1, "", 1, false,"23/9 13:23",images)
//            chats.add(chat_item)
//            chat_adapter?.notifyDataSetChanged()
        }

//        val attach_button  = findViewById<ImageView>(R.id.attach_button)
//        attach_button.setOnClickListener {
//
//            val intent = Intent(this, ChatAttachmentActivity::class.java)
//            intent.putExtra("company_id",company_id)
//            intent.putExtra("company_name_en",company_name_en)
//            intent.putExtra("company_name_zh", company_name_zh)
//            intent.putExtra("company_logo",company_logo)
//            intent.putExtra("company_avg_score", company_avg_score)
//            intent.putExtra("key",key)
//            intent.putExtra("quotation_id", quote_id)
//            intent.putExtra("project_id",project_id)
//            startActivity(intent)
//
////            val intent = Intent(this, ChatAttachmentActivity::class.java)
////            startActivity(intent)
//        }
//
        val attach_button  = findViewById<ImageView>(R.id.attach_image_button)
        attach_button.setOnClickListener {

            ImagePicker.with(this)
                .crop() //Crop image(Optional), Check Customization for more option
                .createIntentFromDialog { launcher.launch(it) }

        }

        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            super.onBackPressed()
        }


        LoadData()

        message_listview = findViewById<View>(R.id.messages_view) as RecyclerView

        val autoMeasureEnabled :Boolean = false
        val mLayoutManager  = object : LinearLayoutManager(this) {
            override fun isAutoMeasureEnabled(): Boolean {
                return autoMeasureEnabled
            }
        }
        mLayoutManager.isAutoMeasureEnabled = false
        message_listview!!.setHasFixedSize(true)
        mLayoutManager.stackFromEnd = true
        //       mLayoutManager.reverseLayout = true
        mLayoutManager.isSmoothScrollbarEnabled = true

        message_listview!!.setLayoutManager(mLayoutManager)
        Log.d("user_id", user_id.toString())

        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1 != null) {
                    handleBroadcastActions(p1)
                }
            }

        }

    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount != 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
    private fun handleBroadcastActions(intent: Intent) {
        Log.d("incoming_call",intent.action!!.toString())
        when (intent.action) {
            "Reload" -> {
                LoadData()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver, IntentFilter("Reload"))

    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
    }
    //To-do get type and media
    fun LoadData(){try {
        // JsonObject json = new JsonObject();
        Ion.with(this)
            .load("https://uat.jper.com.hk/api/chatroom/".plus(key).plus("/messages"))
            .setHeader("Authorization", "Bearer " + header)
            //     .setTimeout(3000)
            .asJsonObject()
            .setCallback(FutureCallback { e, result ->
                if (e != null) {
                    //   Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                    return@FutureCallback
                }
                Postdata =
                    if (result.get("data").isJsonNull) null else result.getAsJsonArray("data")
                nextpageurl = try {
                    result.getAsJsonPrimitive("next_page_url")
                        .asString
                } catch (x: Exception) {
                    Log.d("error_jperx", x.toString())
                    null
                }

                chats = ArrayList<Chat?>()
                currentpageurl =
                    result.getAsJsonPrimitive("current_page").asString
                if (Postdata != null) {
                    for (i in 0 until Postdata!!.size()) {

                        Log.d("chat_result", Postdata.toString())
                        val data: JsonObject = Postdata!!.get(i).getAsJsonObject()
                        //   Toast.makeText(getContext(),data.get("title").getAsString() , Toast.LENGTH_SHORT).show();

                        val id = data["id"].asInt

                        val sender_id = data["sender_id"].asInt

                        var isSender = false
                        if (user_id == sender_id) isSender = true

                        val msg_body = if (data["msg_body"].isJsonNull) "" else URLDecoder.decode(
                            data["msg_body"].asString,
                            "UTF-8"
                        )
                        val originalFormat: DateFormat =
                            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")
                        val targetMonthFormat: DateFormat = SimpleDateFormat("MM")
                        val targetDayFormat: DateFormat = SimpleDateFormat("dd")
                        val targetTimeFormat: DateFormat = SimpleDateFormat("HH:mm")
                        val date: Date = originalFormat.parse(data["created_at"].asString)
                        val month = (targetMonthFormat.format(date).toInt()).toString()
                        val day = targetDayFormat.format(date).toString()
                        val time = targetTimeFormat.format(date).toString()
                        val created_date = day.plus("/").plus(month).plus(" ").plus(time)
                        //val url = data["url"].asString
                        //  Toast.makeText(getContext(),Avatar_icon , Toast.LENGTH_SHORT).show();


                        val media =
                            if (data.get("media").isJsonNull) null else data.get("media").asJsonArray

                        var type = 0
                        var chat_image: ChatImage? = null

                        if (media != null) {
                            if (media.size() > 0) {
                                if (isSender) type = 3
                                else type = 4
//                                for (i in 0 until media.size()) {
                                val media_data = media.get(0).asJsonObject
                                val image_url =
                                    if (media_data.get("urlm").isJsonNull) "" else media_data.get("urlm").asString
                                Log.d("urlmurlm", image_url)
                                chat_image = ChatImage(1, image_url, null)
//                                }
                            }

                        } else {
                            chat_image = ChatImage(1, "", null)
                        }
                        val chat_item = Chat(id, msg_body, type, isSender, created_date, chat_image)
                        chats.add(0, chat_item)
                        Log.d("isSender", isSender.toString())
                    }

                    chat_adapter = ChatAdapter(chats, message_listview)
                    message_listview!!.adapter = chat_adapter


                    try {
                        val readMessage = ReadMessage(
                            this,
                            "https://uat.jper.com.hk/api/chatroom/message/update/read",
                            header,
                            key!!,
                            lang
                        )
                        Log.d("readMessage", readMessage.toString())
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                    } catch (e: java.lang.Exception) {

                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                    }

                    message_listview!!.scrollToPosition(chat_adapter!!.itemCount - 1)
//                    message_listview!!.scrollToPosition(chats.size - 1)
                    //specifying an adapter to access data, create views and replace the content

                    chat_adapter!!.setOnLoadMoreListener(object : OnLoadMoreListener {
                        override fun onLoadMore() {
                            message_listview!!.post(Runnable {
                                Handler().post {
                                    chats.add(0, null)
                                    chat_adapter!!.notifyItemInserted(0)
                                    Add_more_data_to_list()
                                    Log.d("scroll", "scroll")
                                }
                            })
                        }
                    })
                }
            })
    }
    catch (e: Exception){
        Log.d("chat_error", e.toString())
    }
    }
    fun resetprocess() {
        loading?.cancel(true)
        loading = null
    }

    fun isEmptyString(text: String?): Boolean {
        return text == null || text.trim { it <= ' ' } == "null" || text.trim { it <= ' ' }
            .isEmpty()
    }
    fun Add_more_data_to_list()
    {
        try {

            morechats = ArrayList<Chat?>()
            if (loading != null && !loading!!.isDone() && !loading!!.isCancelled()) {
                chats.removeAt(0)
                chat_adapter?.notifyItemRemoved(0)
                Log.d("get_message", "here1")
                return
            }
            if (isEmptyString(nextpageurl)) {
                chats.removeAt(0)
                chat_adapter?.notifyItemRemoved(0)

                Log.d("get_message", "here2")
                return
            }
            Ion.with(this)
                .load("$nextpageurl")
                //  .setTimeout(3000)
                .setHeader("Authorization", "Bearer " + header)
                .asJsonObject().setCallback(
                    FutureCallback { e, result ->
                        if (e != null) {
                            chats.removeAt(0)
                            chat_adapter?.notifyItemRemoved(0)
                            Log.e("MYAPP", "exception", e)
                            //  Toast.makeText(getContext(),e.getMessage()+nextpageurl+"&catid=12", Toast.LENGTH_SHORT).show();
                            return@FutureCallback
                        }

                        chats.removeAt(0)
                        chat_adapter?.notifyItemRemoved(0)

                        // Toast.makeText(getContext(),nextpageurl, Toast.LENGTH_SHORT).show();
                        Postdata = result.getAsJsonArray("data")
                        Log.d("get_message", Postdata.toString())
                        try {
                            if (result.getAsJsonPrimitive("current_page")
                                    .asString != currentpageurl
                            ) {
                                currentpageurl =
                                    result.getAsJsonPrimitive("current_page")
                                        .asString
                                try {
                                    nextpageurl =
                                        result.getAsJsonPrimitive("next_page_url")
                                            .asString
                                } catch (x: java.lang.Exception) {
                                }
                                if (Postdata != null) {
                                    for (i in 0 until Postdata!!.size()) {

                                        Log.d("chat_result", Postdata.toString())
                                        val data: JsonObject = Postdata!!.get(i).getAsJsonObject()
                                        //   Toast.makeText(getContext(),data.get("title").getAsString() , Toast.LENGTH_SHORT).show();

                                        val id = data["id"].asInt

                                        val sender_id = data["sender_id"].asInt

                                        var isSender = false
                                        if (user_id == sender_id) isSender = true

                                        val msg_body =
                                            if (data["msg_body"].isJsonNull) "" else URLDecoder.decode(
                                                data["msg_body"].asString,
                                                "UTF-8"
                                            )
                                        val originalFormat: DateFormat =
                                            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSX")

                                        val targetMonthFormat: DateFormat = SimpleDateFormat("MM")
                                        val targetDayFormat: DateFormat = SimpleDateFormat("dd")
                                        val targetTimeFormat: DateFormat = SimpleDateFormat("HH:mm")
                                        val date: Date =
                                            originalFormat.parse(data["created_at"].asString)
                                        val month =
                                            (targetMonthFormat.format(date).toInt()).toString()
                                        val day = targetDayFormat.format(date).toString()
                                        val time = targetTimeFormat.format(date).toString()
                                        val created_date = day.plus("/").plus(month).plus(" ").plus(
                                            time
                                        )

                                        val media =
                                            if (data.get("media").isJsonNull) null else data.get(
                                                "media"
                                            ).asJsonArray


                                        var type = 0
                                        var chat_image: ChatImage? = null

                                        if (media != null) {
                                            if (media.size() > 0) {
                                                if (isSender) type = 3
                                                else type = 4
//                                for (i in 0 until media.size()) {
                                                val media_data = media.get(0).asJsonObject
                                                val image_url =
                                                    if (media_data.get("urlm").isJsonNull) "" else media_data.get(
                                                        "urlm"
                                                    ).asString
                                                chat_image = ChatImage(1, image_url, null)
//                                }
                                            }

                                        } else {
                                            chat_image = ChatImage(1, "", null)
                                        }
                                        val chat_item = Chat(
                                            id,
                                            msg_body,
                                            type,
                                            isSender,
                                            created_date,
                                            chat_image
                                        )
                                        morechats.add(0, chat_item)
                                        //     NewsAdapter.notifyItemInserted(Lists.size());
                                    }
                                    chats.addAll(0, morechats)

                                    chat_adapter?.notifyItemRangeInserted(0, morechats.size)
                                    morechats.clear()
                                    //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                                }
                            }
                        } catch (e: java.lang.Exception) {

                        }
                        resetprocess()
                    }
                )
        } catch (e: java.lang.Exception) {
        }

    }
    private fun showDialog(msg: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)


        val dialog_title = dialog.findViewById(R.id.dialog_title) as TextView
        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = msg

        val close_button = dialog.findViewById(R.id.close_button) as ImageView

        close_button.setOnClickListener {
            dialog.dismiss()
        }

        val save_button = dialog.findViewById(R.id.confirm_button) as Button
        save_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    private val launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {

            //Image Uri will not be null for RESULT_OK
            val fileUri = it.data?.data
            attach_image_file = File(fileUri?.path)
//            profile_image.setImageURI(fileUri)
            uploadImage(fileUri?.path, fileUri, attach_image_file)


            //You can get File object from intent
            ImagePicker.getFile(it.data)
            Log.d("fileUri", fileUri.toString())
            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(it.data)
        } else if (it.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(it.data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            attach_image_file = File(fileUri?.path)
//            profile_image.setImageURI(fileUri)
            uploadImage(fileUri?.path, fileUri, attach_image_file)


            //You can get File object from intent
            ImagePicker.getFile(data)
            Log.d("fileUri", fileUri.toString())
            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(data)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    private fun uploadImage(attach_image_file: String?, fileUri: Uri?, image_file: File?){

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_loading)

        /////////////////////Initiate_loading_anim///////////////////////////
        var dialog_Loading =  dialog.findViewById(R.id.loading_progress_xml) as ImageView
        var dialog_progressOverlay =  dialog.findViewById(R.id.progress_overlay) as FrameLayout
        Glide.with(this).load(R.drawable.jper_loading).into(dialog_Loading!!)
        dialog_progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    dialog_progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        dialog_Loading!!.setVisibility(View.VISIBLE)
        dialog_progressOverlay!!.setVisibility(View.VISIBLE)
        /////////////////////Initiate_loading_anim///////////////////////////

        dialog.setCancelable(false)
        dialog.show()

        val calendar = Calendar.getInstance(TimeZone.getDefault())


        val thisMonth = (calendar[Calendar.MONTH] + 1).toString()
        val thisDate = calendar[Calendar.DATE].toString()
        val thisHour =
            if (calendar[Calendar.HOUR_OF_DAY] < 10) "0".plus(calendar[Calendar.HOUR_OF_DAY]) else calendar[Calendar.HOUR_OF_DAY].toString()
        val thisMin =
            if (calendar[Calendar.MINUTE] < 10) "0".plus(calendar[Calendar.MINUTE]) else calendar[Calendar.MINUTE].toString()
        val created_at = thisDate.plus("/").plus(thisMonth).plus("  ").plus(
            thisHour
        ).plus(":").plus(thisMin)

        var file_upload_tmp = FileUploadArray
        file_upload_tmp.clear()
        file_upload_tmp.add(File(fileUri?.path))

        var builder: Builders.Any.M = Ion.with(baseContext)
            .load("https://uat.jper.com.hk/api/chatroom/send")
            .setHeader("Authorization", "Bearer " + header)
            .setMultipartParameter("key", key)
            .setMultipartParameter("content", "")

        if(file_upload_tmp!!.size>0) {
            try{
                builder =builder.setMultipartFile(
                    "media[]",
                    "image/jpeg,png",
                    file_upload_tmp.get(file_upload_tmp.lastIndex)
                )
            }
            catch (e: java.lang.Exception){}

        }
        try {
            ////////////////////////Send_data_to_server//////////////////////////
            builder
                .asJsonObject()
                .setCallback { e, result ->

                    if (e != null){
                        showDialog("Error")
                        Log.d("result_error", e.toString())
                        dialog.dismiss()
                    }
                    if (result["result"].asString.equals("success")) {
                        Log.d("upload_imageinACT", fileUri.toString())
                        Log.d("msg", result["msg"].asString)
                        //save token
                        chats.add(
                            Chat(
                                0, "", 3, true, created_at, ChatImage(
                                    0,
                                    "",
                                    fileUri
                                )
                            )
                        )


                        chat_adapter!!.notifyDataSetChanged()

                        message_listview!!.scrollToPosition(chats.size - 1)

                        dialog.dismiss()
                    }
                    else if (result["result"].asString.equals("error")) {
                        Log.d("sendinfo_msg", result["msg"].asString)
                        // showDialog(this, result["msg"].asString)
                        showDialog("Error")

                        dialog.dismiss()
                    }
                    Log.d("result_okok", result.toString())
                }
        } catch (x: Exception) {
            Log.d("result_error", x.toString())

            showDialog("Error")
            dialog.dismiss()
        }


        Loading!!.visibility = View.GONE
        progressOverlay!!.visibility = View.GONE

    }
    fun isEnglish(text: String): Boolean {
        var onlyEnglish = false
        for (character in text.toCharArray()) {
            onlyEnglish =
                if (Character.UnicodeBlock.of(character) === Character.UnicodeBlock.BASIC_LATIN || Character.UnicodeBlock.of(
                        character
                    ) === Character.UnicodeBlock.LATIN_1_SUPPLEMENT || Character.UnicodeBlock.of(
                        character
                    ) === Character.UnicodeBlock.LATIN_EXTENDED_A || Character.UnicodeBlock.of(
                        character
                    ) === Character.UnicodeBlock.GENERAL_PUNCTUATION
                ) {
                    true
                } else {
                    false
                }
        }
        return onlyEnglish
    }
}