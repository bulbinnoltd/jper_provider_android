package com.jpertechnologyltd.jperprovider.ui.myproject

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.jpertechnologyltd.jperprovider.R

class ViewPaymentReceiptFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_view_payment_receipt, container, false)

        val proof_payment_text = root.findViewById<TextView>(R.id.proof_payment_text)
        proof_payment_text.setPaintFlags(proof_payment_text.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        proof_payment_text.setOnClickListener{}

        val back_button = root.findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {

            var fr = getFragmentManager()?.beginTransaction()
            fr?.replace(R.id.nav_host_fragment,
                MyProjectsProjectDetailFragment()
            )

            fr?.commit()
        }


        return root
    }
}