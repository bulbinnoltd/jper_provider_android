package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Add


class EditItemFragment : Fragment() {
    var Quotelist : ArrayList<Add>? = null
    var itemPosition :Int? = null
    var project_id :Int? = null
    var post_date :String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        Quotelist = args?.getParcelableArrayList<Add>("data")
        itemPosition = args?.getInt("itemPosition")
        project_id = args?.getInt("project_id")
        post_date = args?.getString("post_date")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_edit_item, container, false)
        val edit_item_name_edit = root.findViewById<TextInputEditText>(R.id.edit_item_name_edit)
        val edit_item_min_edit = root.findViewById<TextInputEditText>(R.id.edit_item_min_edit)
        val edit_item_max_edit = root.findViewById<TextInputEditText>(R.id.edit_item_max_edit)
        val edit_item_remarks_edit = root.findViewById<TextInputEditText>(R.id.edit_item_remarks_edit)
//        val EditItem = ArrayList<EditItem>()
//        EditItem.add(EditItem("", "","", "",""))

        var position = itemPosition
        edit_item_name_edit.setText(Quotelist!![position!!].description)

        edit_item_remarks_edit.setText(Quotelist!![position!!].remarks.toString())
        Log.d("submit item", Quotelist!![itemPosition!!].max_amount.toString())
//        this!!.Quotelist!![this!!.itemPosition!!].minvalue = edit_item_min_edit.text.toString()
//        this!!.Quotelist!![this!!.itemPosition!!].maxvalue = edit_item_max_edit.text.toString()

        val back_button = root.findViewById(R.id.edit_back_button) as ImageView
        val QuoteSubmissionFragment: Fragment = QuoteSubmissionFragment()
        val bundle = Bundle()

        back_button.setOnClickListener {

            bundle.putParcelableArrayList("data", Quotelist)
            bundle.putInt("project_id", project_id!!)
            bundle.putString("post_date", post_date!!)
            QuoteSubmissionFragment.setArguments(bundle)
            var fr = getFragmentManager()?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.add(R.id.nav_host_fragment, QuoteSubmissionFragment)
            fr?.commit()
        }

        val delete_button = root.findViewById(R.id.edititem_delete_button) as Button
        if(Quotelist!![position!!].id!=0){ //fixed item
            delete_button.visibility=View.GONE
            edit_item_name_edit.isFocusable=false
            edit_item_name_edit.isClickable=false
        }
        else{

            edit_item_min_edit.setText(Quotelist!![position!!].min_amount.toDouble().toInt().toString())
            edit_item_max_edit.setText(Quotelist!![position!!].max_amount.toDouble().toInt().toString())
        }

        delete_button.setOnClickListener {
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.dialog_confirm)

            val dialog_message = dialog.findViewById(R.id.message) as TextView
            dialog_message.text = getString(R.string.dialog_delete_info)
            val confirm_button = dialog.findViewById(R.id.save_button) as Button

            val close_button = dialog.findViewById(R.id.close_button) as ImageView
            val cancel_button = dialog.findViewById(R.id.cancel_button) as Button
            cancel_button.setOnClickListener {
                dialog.dismiss()
            }
            close_button.visibility = View.INVISIBLE
            confirm_button.setOnClickListener {
                dialog.dismiss()
                Quotelist?.removeAt(position)

                bundle.putInt("project_id", project_id!!)
                bundle.putParcelableArrayList("data", Quotelist)
                bundle.putString("post_date", post_date!!)
                QuoteSubmissionFragment.setArguments(bundle)

                var fr = getFragmentManager()?.beginTransaction()
                getFragmentManager()?.popBackStack()
                fr?.addToBackStack(null)
                fr?.replace(R.id.nav_host_fragment, QuoteSubmissionFragment)
                fr?.commit()
            }
            dialog.setCancelable(true)
            dialog.show()
        }

        val save_button = root.findViewById(R.id.edititem_save_button) as Button

        save_button.setOnClickListener {
            var edit_item_min:String?=null
            var edit_item_max:String?=null
            var description_value:String?=null
            try { edit_item_min=  edit_item_min_edit.text.toString()}catch (x: Exception) { }
            try { edit_item_max=  edit_item_max_edit.text.toString()}catch (x: Exception) { }
            try { description_value=  edit_item_name_edit.text.toString()}catch (x: Exception) { }
            if(edit_item_min_edit.text.toString().equals("0")){
                showDialog(activity!!,getString(R.string.zero_amount_warning))
            }
            else if(edit_item_max_edit.text.toString().equals("0")){
                showDialog(activity!!, context!!.getString(R.string.warn_max_min))
            }
            else  if(edit_item_min.equals("") ||edit_item_max.equals("") || description_value.equals(""))
            {
                showDialog(activity!!,getString(R.string.Without_info))
            }
            else {
                try {
                    if (edit_item_min_edit.text.toString()
                            .toDouble() <= edit_item_max_edit.text.toString().toDouble()
                    ) {
                        Quotelist!![position].description = edit_item_name_edit.text.toString()
                        Quotelist!![position].remarks = edit_item_remarks_edit.text.toString()
                        Quotelist!![position].min_amount =
                            edit_item_min_edit.text.toString().toFloat()
                        Quotelist!![position].max_amount =
                            edit_item_max_edit.text.toString().toFloat()

                        //  sendinfo(edit_item_name_edit.text,edit_item_min_edit.text,edit_item_max_edit.text)
                        Log.d("change item title button", edit_item_name_edit.text.toString())

                        bundle.putInt("project_id", project_id!!)
                        bundle.putParcelableArrayList("data", Quotelist)
                        bundle.putString("post_date", post_date!!)
                        QuoteSubmissionFragment.setArguments(bundle)

                        var fr = getFragmentManager()?.beginTransaction()
                        getFragmentManager()?.popBackStack()
                        fr?.addToBackStack(null)
                        fr?.replace(R.id.nav_host_fragment, QuoteSubmissionFragment)
                        fr?.commit()
                    } else {
                        showDialog(activity!!, context!!.getString(R.string.warn_max_min))
                    }
                }
                catch (e: Exception) {
                    showDialog(activity!!, context!!.getString(R.string.Without_info))
                }
            }
        }



//        val contract_adapter = parentFragment?.let { ContractItemAdapter(contractitems, it) }
//        contract_listview.adapter = contract_adapter
//        contract_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)

        return root
    }
    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    fun sendinfo(name: Editable?, min: Editable?, max: Editable? ){

//        val msg = HttpsService.UpdateTime(this,"https://uat.jper.com.hk/api/owner/change/password",password.toString(),header)
//        if (msg!=null){
//            showDialog(this ,msg)
//        }
    }
}

