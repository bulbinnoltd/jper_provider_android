package com.jpertechnologyltd.jperprovider.ui.myproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jpertechnologyltd.jperprovider.AddPaymentPlanActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.PaymentPlanAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetPaymentPlan
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetPaymentPlanEnable
import com.jpertechnologyltd.jperprovider.item.ContractItem
import com.jpertechnologyltd.jperprovider.item.PaymentPlan
import java.util.*
import kotlin.collections.ArrayList

class MyProjectsQuotedPaymentPlanFragment : Fragment() {
    private lateinit var paymentplan_listview: RecyclerView
    private var contractitems: ArrayList<ContractItem>? = null

    var swipeContainer: SwipeRefreshLayout?=null
    private lateinit var no_paymentplan_view: ConstraintLayout
    private var header : String? = null

    var contain_retention: Boolean = false
    var project_id : Int? = null
    var status_id : Int? = null
    var post_date : String? = null
    var contract_sum_value : String? = null

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null

    var lang:String?= "en"

    var paymentplans_data:ArrayList<PaymentPlan> ?=null

    var retention_id :Int? = null
    var is_final:Boolean? = null
    var bg_accepted :Int? = null
    var contractsum: String? = null
    var amount: String? = null
    var company_name_en: String? = null
    var company_name_zh: String? = null
    var payment_enable = false
    var phase:Int? = null


    private lateinit var broadcastReceiver: BroadcastReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        try {
            project_id = args?.getInt("project_id")
        }
        catch (e: Exception){
            Log.d("error_insum", e.toString())
        }

    }
    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(broadcastReceiver, IntentFilter("Reload"))
        var refresh= activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getBoolean("refresh_paymentplan", false)
        Log.d("refresh_paymentplan", refresh.toString())

        if(refresh!!) {
            val editor = activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
            editor.putBoolean("refresh_paymentplan", false)
            editor.apply()

            refreshItems()
            Log.d("refresh_paymentplan", refresh.toString())

        }

    }


    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(broadcastReceiver)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_manage_paymentplan, container, false)
//        no_contractitem = root.findViewById(R.id.no_contractitem) as TextView
//        no_paymentitem = root.findViewById(R.id.no_paymentitem) as TextView
//        payment_sum_view2 = root.findViewById(R.id.payment_sum_view2) as MaterialCardView


        no_paymentplan_view = root.findViewById(R.id.no_paymentplans_view) as ConstraintLayout

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////


        paymentplan_listview = root.findViewById<View>(R.id.paymentplan_listview) as RecyclerView
        val mLayoutManager = LinearLayoutManager(activity)
        paymentplan_listview!!.setLayoutManager(mLayoutManager)

        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")

        val add_paymentplan_button = root.findViewById<Button>(R.id.add_paymentplan_button) as FloatingActionButton

        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        val config = resources.configuration
        val locale = Locale(lang)


        Locale.setDefault(locale)
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)

        val paymentplan_listview = root.findViewById<View>(R.id.paymentplan_listview) as RecyclerView


        Thread(Runnable {

            val paymentplans = ArrayList<PaymentPlan>()
            val retention_data = null
            //  val retention_data = GetRetention(context,"https://uat.jper.com.hk/api/company/project/".plus(project_id.toString()).plus("/retention"),header)
            if (retention_data != null) {
                //     paymentplans.add(retention_data)
                contain_retention = true
            }
            paymentplans_data = GetPaymentPlan(
                context, "https://uat.jper.com.hk/api/company/project/".plus(
                    project_id.toString()
                ).plus("/payment/plan"), header, lang
            )
            payment_enable = GetPaymentPlanEnable(
                context, "https://uat.jper.com.hk/api/company/project/".plus(
                    project_id.toString()
                ).plus("/payment/plan"), header, lang
            )
            if (paymentplans_data != null) {

                is_final = paymentplans_data!!.get(0).is_final
                retention_id = paymentplans_data!!.get(0).id
                amount = paymentplans_data!!.get(0).amount
                company_name_zh = paymentplans_data!!.get(0).company_name_zh
                company_name_en = paymentplans_data!!.get(0).company_name_en
                phase = paymentplans_data!!.get(0).phase

                paymentplans.addAll(paymentplans_data!!)

                Log.d("temp","paymentplans phase"+  phase.toString())
            }

            activity?.runOnUiThread(java.lang.Runnable {

                val no_paymentplans_view =
                    root.findViewById<View>(R.id.no_paymentplans_view) as ConstraintLayout

                if (payment_enable) {
                    add_paymentplan_button.setOnClickListener {
                        val intent = Intent(activity, AddPaymentPlanActivity::class.java)
                        intent.putExtra("project_id", project_id)
                        context!!.startActivity(intent)
                    }
                }

                if (paymentplans.size > 0) {
                    Log.d("paymentPlan", "ok")
                } else {

                    no_paymentplans_view.visibility = View.VISIBLE
                }

                if (paymentplans.size > 0) {
                    val paymentplan_adapter = parentFragment?.let {
                        PaymentPlanAdapter(
                            paymentplans,
                            it,
                            this
                        )
                    }
                    paymentplan_listview.adapter = paymentplan_adapter
                    paymentplan_listview.layoutManager =
                        LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                }

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()

        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                // Refresh items
                try {
                    refreshItems()
                } catch (e: java.lang.Exception) {
                }
            }
        })
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1 != null) {
                    handleBroadcastActions(p1)
                }
            }

        }
        return root
    }
    private fun handleBroadcastActions(intent: Intent) {
        when (intent.action) {
            "Reload" -> {
                refreshItems()
            }
        }
    }


    fun refreshItems() {
        // Load items
        // ...
        val handler = Handler()
        handler.postDelayed({
            try {
                onItemsLoadComplete() // Actions to do after 10 seconds
            } catch (e: java.lang.Exception) {
                swipeContainer!!.setRefreshing(false)
            }
        }, 1000)
        //   activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();

        //    onItemsLoadComplete();
    }
    fun onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        getFragmentManager()!!.beginTransaction().detach(this).commitNowAllowingStateLoss()
        getFragmentManager()!!.beginTransaction().attach(this).commitAllowingStateLoss()        // Stop refresh animation
        swipeContainer!!.setRefreshing(false)
    }
}

