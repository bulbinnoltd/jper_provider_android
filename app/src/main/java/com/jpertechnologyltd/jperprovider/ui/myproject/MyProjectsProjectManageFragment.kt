package com.jpertechnologyltd.jperprovider.ui.myproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jpertechnologyltd.jperprovider.AddContractItemActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.adapter.ContractItemAdapter
import com.jpertechnologyltd.jperprovider.adapter.NewContractItemAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService.ConfirmBudget
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetContractSum
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetMyNewProjectBudget
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetMyProjectBgAccepted
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetMyProjectBudget

import com.jpertechnologyltd.jperprovider.item.ContractItem
import com.jpertechnologyltd.jperprovider.item.NewContractItem

class MyProjectsProjectManageFragment : Fragment() {
    private lateinit var no_contractitem: TextView
    private lateinit var no_paymentitem: TextView
    private lateinit var payment_sum_view2: MaterialCardView
    private lateinit var contract_listview: RecyclerView
    private var contractitems: ArrayList<ContractItem>? = null

    var swipeContainer: SwipeRefreshLayout?=null
    private lateinit var no_contract_view: ConstraintLayout
    private lateinit var contract_sum: ConstraintLayout
    private lateinit var contract_sum_textview: TextView
    private var header : String? = null
    var lang : String? = "en"

    var bg_accepted =0
    var project_id : Int? = null
    var status_id : Int? = null
    var post_date : String? = null
    var contract_sum_value : String? = null
    var contract_m : String? = null
    var contract_temp_link : String? = null
    var layoutState:String? = ""
    var scope_id:Int? = null //!!!


    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        try {
            project_id = args?.getInt("project_id")
            status_id = args?.getInt("status_id")
            post_date = args?.getString("post_date")
//          contract_sum_value = args?.getString("contract_sum")
            contract_m = args?.getString("contract_m")
            scope_id = args?.getInt("scope_id") //!!!

        }
        catch (e: Exception){
            Log.d("error_insum", e.toString())
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_quote_manage, container, false)
        Log.d("temp", "ContractItemsFragement: " + scope_id.toString())

//        no_contractitem = root.findViewById(R.id.no_contractitem) as TextView
//        no_paymentitem = root.findViewById(R.id.no_paymentitem) as TextView
//        payment_sum_view2 = root.findViewById(R.id.payment_sum_view2) as MaterialCardView


        no_contract_view = root.findViewById(R.id.no_contract_view) as ConstraintLayout
        contract_sum = root.findViewById(R.id.contract_sum) as ConstraintLayout
        contract_sum_textview = root.findViewById(R.id.contract_sum_value) as TextView

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////

        val toggleButton = root.findViewById(R.id.toggleButton) as ToggleButton

        contract_listview = root.findViewById<View>(R.id.contract_listview) as RecyclerView
        val mLayoutManager = LinearLayoutManager(activity)
        contract_listview!!.setLayoutManager(mLayoutManager)

        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        contract_temp_link = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("contract_temp_link_" + project_id, "")
        Log.d("contract_temp_link2", contract_temp_link)

        //


        //

        val add_contractItem_button = root.findViewById<FloatingActionButton>(R.id.add_contractItem_button)
        add_contractItem_button.setOnClickListener {
            val intent = Intent(activity, AddContractItemActivity::class.java)
            intent.putExtra("project_id", project_id)
            context!!.startActivity(intent)
        }

        val quote_create_button = root.findViewById<FloatingActionButton>(R.id.quote_create_button)
        quote_create_button.setOnClickListener {

            val intent = Intent(context, AttachNewQuoteFileActivity::class.java)
            intent.putExtra("project_id", project_id)
//            Log.d("url", contract_m)
            startActivity(intent)
        }
        val signed_contractItem_button = root.findViewById<FloatingActionButton>(R.id.signed_contractItem_button)



        // set on-click listener
        if(!contract_m.equals("")) {
            val editor =
                activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
            editor.remove("contract_temp_link_" + project_id)
            editor.apply()
            editor.commit()
            contract_temp_link = ""
        }
            if(contract_m.equals("")&&contract_temp_link.equals("")){signed_contractItem_button.visibility=View.GONE}
            else if(!contract_m.equals("")&&contract_temp_link.equals("")){
                signed_contractItem_button.visibility=View.VISIBLE
                signed_contractItem_button.setOnClickListener {


//
//            val intent = Intent (this, RegisterActivity::class.java)
//            startActivity(intent)
                    val intent = Intent(context, WebviewActivity::class.java)
                    intent.putExtra("url", contract_m)
                    Log.d("url", contract_m)
                    startActivity(intent)
                }
            }
            else if(contract_m.equals("")&&!contract_temp_link.equals("")){
                signed_contractItem_button.visibility=View.VISIBLE

                signed_contractItem_button.setOnClickListener {
//
//            val intent = Intent (this, RegisterActivity::class.java)
//            startActivity(intent)
                    val intent = Intent(context, WebviewActivity::class.java)
                    intent.putExtra("url", contract_temp_link)
                    Log.d("url", contract_temp_link)
                    startActivity(intent)
                }
            }
            else if(!contract_m.equals("")&&!contract_temp_link.equals("")){
                signed_contractItem_button.visibility=View.VISIBLE

                signed_contractItem_button.setOnClickListener {
//
//            val intent = Intent (this, RegisterActivity::class.java)
//            startActivity(intent)
                    val intent = Intent(context, WebviewActivity::class.java)
                    intent.putExtra("url", contract_temp_link)
                    Log.d("url", contract_temp_link)
                    startActivity(intent)
                }
            }
        fun load_contract_item() {
            Thread(Runnable {
                try {
                    contractitems = GetMyProjectBudget(
                        context,
                        "https://uat.jper.com.hk/api/company/project/".plus(project_id.toString())
                            .plus(
                                "/budget"
                            ),
                        header,
                        lang
                    )
                    contract_sum_value = GetContractSum(
                        context,
                        "https://uat.jper.com.hk/api/company/project/".plus(project_id.toString())
                            .plus(
                                "/summarized/amount"
                            ),
                        header,
                        lang
                    )


                    bg_accepted = GetMyProjectBgAccepted(
                        context,
                        "https://uat.jper.com.hk/api/company/my/project/".plus(project_id.toString()),
                        header,
                        lang
                    )

                } catch (e: Exception) {
                    Log.d("error_getprojectsum", e.toString())
                }
                activity?.runOnUiThread(java.lang.Runnable {
                    if (contractitems != null) {
                        Log.d("result_contractitems", contractitems!!.size.toString())
                        val mContractItemAdapter = ContractItemAdapter(
                            contractitems!!,
                            this,
                            project_id!!
                        )
                        contract_listview.setAdapter(mContractItemAdapter)

                        quote_create_button.visibility = View.VISIBLE

                        if (contractitems!!.size > 0) {
                            no_contract_view.visibility = View.GONE
//
//                    swipeContainer!!.visibility=View.VISIBLE
                            contract_listview.visibility = View.VISIBLE
                            contract_sum.visibility = View.VISIBLE

                            try {
                                var number: String = contract_sum_value!!

                                if (number.contains(".00")) number =
                                    number.substring(0, number.length - 3)
                                contract_sum_textview.text = "HK $" + number
                            } catch (e: Exception) {
                                Log.d("quote_error", e.toString())
                                contract_sum_textview.setText("HK $" + contract_sum_value)
                            }

                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE
                            layoutState = "current"

                            if (bg_accepted == 1) {


                                val ContractItem_button =
                                    root.findViewById<View>(R.id.ContractItem_button) as Button
                                ContractItem_button.visibility = View.VISIBLE


                                ContractItem_button.setOnClickListener {
                                    Loading!!.visibility = View.VISIBLE
                                    progressOverlay!!.visibility = View.VISIBLE
                                    ContractItem_button.visibility = View.GONE
                                    quote_create_button.visibility = View.GONE
                                    Thread(Runnable {
                                        val confirmation = ConfirmBudget(
                                            context!!,
                                            "https://uat.jper.com.hk/api/company/project/budget/submit",
                                            header,
                                            project_id!!,
                                            lang
                                        )


                                        if (confirmation) {

                                            activity!!.runOnUiThread(java.lang.Runnable {
                                                val dialog = Dialog(context!!)
                                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                                                dialog.window!!.setBackgroundDrawable(
                                                    ColorDrawable(
                                                        Color.TRANSPARENT
                                                    )
                                                )
                                                dialog.setCancelable(false)
                                                dialog.setContentView(R.layout.dialog_message)

                                                val dialog_message =
                                                    dialog.findViewById(R.id.dialog_message) as TextView
                                                dialog_message.text =
                                                    getString(R.string.Success_msg)
                                                val confirm_button =
                                                    dialog.findViewById(R.id.confirm_button) as Button

                                                val close_button =
                                                    dialog.findViewById(R.id.close_button) as ImageView
                                                close_button.visibility = View.INVISIBLE
                                                confirm_button.setOnClickListener {
                                                    dialog.dismiss()
                                                    refreshItems(root, contract_listview)

                                                }
                                                dialog.setCancelable(false)
                                                dialog.show()
                                            })
                                        } else {
                                            activity!!.runOnUiThread(java.lang.Runnable {

                                                Loading!!.visibility = View.GONE
                                                progressOverlay!!.visibility = View.GONE

                                                ContractItem_button.visibility = View.VISIBLE
                                                quote_create_button.visibility = View.VISIBLE
                                                showDialog(activity!!, "Error")
                                            })
                                        }
                                    }).start()
                                }
                            } else if (bg_accepted == 3 && scope_id == 1) {
                                toggleButton.visibility = View.VISIBLE

                            }

                        } else {

                            no_contract_view.visibility = View.VISIBLE
//                       swipeContainer!!.visibility=View.GONE
                            contract_listview.visibility = View.GONE
                            contract_sum.visibility = View.GONE
                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE
                            quote_create_button.visibility = View.VISIBLE
                        }
                    } else {
                        no_contract_view.visibility = View.VISIBLE
//                    swipeContainer!!.visibility=View.GONE
                        contract_listview.visibility = View.GONE
                        contract_sum.visibility = View.GONE
                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        quote_create_button.visibility = View.VISIBLE
                    }
                })
            }).start()
        }
        load_contract_item()

        toggleButton.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                layoutState = "current"
                Log.d("toggle", "current")
                LoadDefaultBudget(root,contract_listview)
            } else {
                layoutState = "initial"
                Log.d("toggle", "initial")
                LoadInitialBudget(root, contract_listview)

            }

        }

        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                // Refresh items
                try {
                    refreshItems(root, contract_listview)
                } catch (e: java.lang.Exception) {
                }
            }
        })

        return root
    }

    override fun onResume() {
        super.onResume()

        var refresh= activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getBoolean("refresh_contract", false)
        Log.d("refresh_contract", refresh.toString())

        if(refresh!!) {
//            refreshItems(root, contract_listview)
            val editor = activity!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
            editor.putBoolean("refresh_contract", false)
            editor.apply()

        }

    }

    fun refreshItems(root:View ,contract_listview:RecyclerView) {
        // Load items
        // ...
        val handler = Handler()
        handler.postDelayed({
            try {
                onItemsLoadComplete() // Actions to do after 10 seconds
                if(layoutState?.compareTo("current") ==0)
                    LoadDefaultBudget(root,contract_listview)
                else
                    LoadInitialBudget(root,contract_listview)
                swipeContainer!!.setRefreshing(false)

            } catch (e: java.lang.Exception) {
                swipeContainer!!.setRefreshing(false)
                Log.d("refresh_error",e.toString())
            }
        }, 1000)
        //   activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();

        //    onItemsLoadComplete();
    }

    //!!!
    fun LoadDefaultBudget(root:View , contract_listview:RecyclerView) {
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE

        Thread(Runnable {
            contract_sum_value = GetContractSum(
                context,
                "https://uat.jper.com.hk/api/company/project/".plus(project_id.toString()).plus(
                    "/summarized/amount"
                ),
                header,
                lang
            )

            var contractitems2 = GetMyProjectBudget(
                context,
                "https://uat.jper.com.hk/api/company/project/".plus(project_id.toString()).plus(
                    "/budget"
                ),
                header,
                lang
            )

            Log.d("contractsum", contract_sum_value)
            Log.d("contract_no", contractitems?.size.toString())


            if (contractitems2 == null)
                contractitems2 = ArrayList<ContractItem>()

            activity?.runOnUiThread(java.lang.Runnable {

                if (!contract_sum_value.equals("N/A"))
                    contract_sum_textview.text = " HK$ ".plus(contract_sum_value)
                else contract_sum_textview.text = contract_sum_value

                if (contractitems2.size > 0) {
                    no_contract_view.visibility = View.GONE
                    contract_sum_textview.visibility = View.VISIBLE
                }
//                } else {
//                    no_item_imageView.visibility = View.VISIBLE
//                    no_item_instruction.visibility = View.VISIBLE
//                }


                val contract_adapter2 =
                    parentFragment?.let { ContractItemAdapter(contractitems2!!,this,project_id!!) }

                contract_listview.adapter = contract_adapter2
                contract_listview.layoutManager =
                    LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()
    }

    fun LoadInitialBudget(root:View , contract_listview:RecyclerView){
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        Thread(Runnable {
            if (!contract_sum_value.equals("N/A"))
                contract_sum_textview.text = " HK$ ".plus(contract_sum_value)
            else contract_sum_textview.text = contract_sum_value

            var newcontractitems : ArrayList<NewContractItem>?=null
            try {
                newcontractitems = GetMyNewProjectBudget(
                    context!!,
                    "https://uat.jper.com.hk/api/company/project/".plus(project_id!!.toString())
                        .plus("/initial_budget"),
                    header,
                    project_id!!
                )

            }
            catch (e:Exception){
                Log.d("error_jper",e.toString())

            }
            if (newcontractitems == null)
                newcontractitems = ArrayList<NewContractItem>()

            activity?.runOnUiThread(java.lang.Runnable {

                if (!contract_sum_value.equals("N/A"))
                    contract_sum_textview.text = " HK$ ".plus(contract_sum_value)
                else contract_sum_textview.text = contract_sum_value

                if(newcontractitems.size>0){
                    no_contract_view.visibility = View.GONE
                    contract_sum_textview.visibility = View.VISIBLE
                    Log.d("temp", "newcontractitems.size > 0")
                }
                else{
                    no_contract_view.visibility=View.VISIBLE
                    Log.d("temp", "newcontractitems.size < 0")

                }


                val newcontract_adapter = parentFragment?.let { NewContractItemAdapter(newcontractitems!!,this,project_id!!)}
                contract_listview.adapter = newcontract_adapter
                contract_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()
    }
    //!!!
    fun onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        getFragmentManager()!!.beginTransaction().detach(this).commitNowAllowingStateLoss()
        getFragmentManager()!!.beginTransaction().attach(this).commitAllowingStateLoss()
//        val t: FragmentTransaction = activity!!.getSupportFragmentManager().beginTransaction()
//        activity!!.getSupportFragmentManager().executePendingTransactions()
//        if (Build.VERSION.SDK_INT >= 26) {
//            t.setReorderingAllowed(true);
//        }
//        t.detach(this).attach(this).commit()
        // Stop refresh animation


        Log.d("refresh_error","refresh_error")
        swipeContainer!!.setRefreshing(false)
    }
    private fun showDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }


}

