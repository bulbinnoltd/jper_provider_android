package com.jpertechnologyltd.jperprovider.ui.myproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.FileDownloadAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetMyProjectSummary
import com.jpertechnologyltd.jperprovider.item.FileDownload
import com.jpertechnologyltd.jperprovider.item.ProjectCategory
import com.jpertechnologyltd.jperprovider.item.myproject.MyProjectSummary
import com.jpertechnologyltd.jperprovider.ui.mybids.MybidsQuoteFragment
import com.stfalcon.imageviewer.StfalconImageViewer
import java.text.DecimalFormat

class MyProjectsProjectSummaryFragment : Fragment() {
    private var header : String? = null
    private var lang : String? = null
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var filesdownload: ArrayList<FileDownload>? = null
    var project_category_array = ArrayList<String>()

    var attach_listview : RecyclerView? = null
    var myprojectsummary : MyProjectSummary? = null
    var project_id : Int? = null
    var status_id : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        try {
            project_id = args?.getInt("project_id")
            status_id = args?.getInt("status_id")
        }
        catch (e: Exception){
            Log.d("error_insum", e.toString())
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(
            R.layout.fragment_fragment_myprojects_quoted_summary,
            container,
            false
        )

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////


        header = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")

        lang = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "zh")

        val context = context!!


        val projects_district_value = root.findViewById<View>(R.id.projects_district_value) as TextView
        val project_image = root.findViewById<View>(R.id.project_image) as ImageView
        val project_project_owner_value = root.findViewById<View>(R.id.project_project_owner_value) as TextView
        val project_contact_no_value = root.findViewById<View>(R.id.project_contact_no_value) as TextView
        val project_site_address_value = root.findViewById<View>(R.id.project_site_address_value) as TextView
        val project_type = root.findViewById<View>(R.id.project_type) as TextView
        val project_type_space = root.findViewById<View>(R.id.project_type_space) as TextView
        val project_area = root.findViewById<View>(R.id.project_area) as TextView
        val project_area_unit = root.findViewById<View>(R.id.project_area_unit) as TextView
        val project_scope = root.findViewById<View>(R.id.project_scope) as TextView
        val project_timeframe = root.findViewById<View>(R.id.project_timeframe) as TextView
        val project_budget_value = root.findViewById<View>(R.id.project_budget_value) as TextView
        val project_desc = root.findViewById<View>(R.id.project_desc) as TextView
        val separator = root.findViewById<View>(R.id.separator) as View
        val project_attach_title = root.findViewById<View>(R.id.project_attach_title) as TextView
        val download_button = root.findViewById<View>(R.id.download_button) as FloatingActionButton
        val chipgroup = root.findViewById<ChipGroup>(R.id.chipgroup)



        attach_listview = root.findViewById<View>(R.id.attach_listview) as RecyclerView

        val mLayoutManager = LinearLayoutManager(activity)

        attach_listview!!.setLayoutManager(mLayoutManager)

        Thread(Runnable {
            try {
                myprojectsummary = GetMyProjectSummary(
                    context, "https://uat.jper.com.hk/api/company/my/project/".plus(
                        project_id.toString()
                    ), header,lang
                )
            } catch (e: Exception) {
                Log.d("error_getprojectsum", e.toString())
            }
            activity?.runOnUiThread(java.lang.Runnable {
                if (myprojectsummary != null) {
                    projects_district_value.text = myprojectsummary!!.district
                    if (myprojectsummary!!.contacts.size > 0) {
                        project_project_owner_value.text =
                            myprojectsummary!!.contacts.get(0).contact_name
                        project_contact_no_value.text =
                            myprojectsummary!!.contacts.get(0).contact_no
                    }
                    project_site_address_value.text = myprojectsummary!!.address
                    project_type.text = myprojectsummary!!.type
                    project_scope.text = myprojectsummary!!.scope
                    project_timeframe.text = myprojectsummary!!.start_at.plus(" - ").plus(
                        myprojectsummary!!.completed_at
                    )
                    project_timeframe.text = myprojectsummary!!.start_at.plus(" - ").plus(
                        myprojectsummary!!.completed_at
                    )
                    if (!myprojectsummary!!.area.equals("")) {
                        project_area.text = myprojectsummary!!.area
                    } else {
                        project_type_space.visibility = View.GONE
                        project_area.visibility = View.GONE
                        project_area_unit.visibility = View.GONE
                    }

                    try {
                        val number: String = myprojectsummary!!.budget_max
                        val amount = number.toDouble()
                        val formatter = DecimalFormat("#,###.##")
                        var formatted = formatter.format(amount)

                        if (formatted.contains(".00")) formatted =
                            formatted.substring(0, formatted.length - 3)

                        project_budget_value.text =  "HK$ ".plus(formatted)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        project_budget_value.text =
                            "HK$ ".plus(myprojectsummary!!.budget_max)

                    }



                    if (!myprojectsummary!!.description.equals("")) {
                        Log.d("desc_proj", myprojectsummary!!.description)
                        project_desc.text = " • ".plus(myprojectsummary!!.description)
                    }
                    if (myprojectsummary!!.notes.size > 0) {
                        var desc_notes: String = ""
                        for (index in myprojectsummary!!.notes.indices) {
                            desc_notes = desc_notes.plus(" • ").plus(
                                myprojectsummary!!.notes.get(
                                    index
                                )
                            ).plus("\n")
                        }
                        if (!myprojectsummary!!.description.equals(""))
                            project_desc.text =
                                " • ".plus(myprojectsummary!!.description).plus("\n").plus(
                                    desc_notes
                                )
                        else project_desc.text = desc_notes
                    }

                    if (myprojectsummary!!.docs != null && myprojectsummary!!.docs.size > 0) {

                        filesdownload = ArrayList()
                        filesdownload!!.addAll(myprojectsummary!!.docs)
                        val filedownload_adapter = FileDownloadAdapter(filesdownload!!)
                        attach_listview!!.adapter = filedownload_adapter
                        attach_listview!!.layoutManager =
                            LinearLayoutManager(context, RecyclerView.VERTICAL, false)

                    } else {
                        separator.visibility = View.VISIBLE
                        project_attach_title.visibility = View.VISIBLE
                    }
                    if(myprojectsummary!!.categories.size>0){
                        project_category_array  = ArrayList<String>()
                        for (index in myprojectsummary!!.categories.indices) {
                            project_category_array.add(ProjectCategory( context!!,myprojectsummary!!.categories[index]).name)
                        }

                        for (index in project_category_array.indices) {
                            val chip = Chip(chipgroup.context)
                            chip.text= project_category_array.get(index)
                            chipgroup.addView(chip)
                        }
                    }
//                    if(!myprojectsummary!!.contract_m.equals("")) {
//                        download_button.setOnClickListener {
//
//                            val intent = Intent(activity, WebviewActivity::class.java)
//                            intent.putExtra("url", myprojectsummary!!.contract_m)
//                            context!!.startActivity(intent)
//
//                        }
//
//                    }
//                    else{
//                        download_button.visibility = View.GONE
//                    }
                    download_button.setOnClickListener {

                        val nextFrag: Fragment = MybidsQuoteFragment()

                        val bundle = Bundle()

                        bundle.putInt("quote_id", myprojectsummary!!.quote_id)
                        bundle.putInt("status_id", myprojectsummary!!.quote_status_id)
                        bundle.putInt("project_id", myprojectsummary!!.quote_project_id)
                        bundle.putString("post_date", myprojectsummary!!.post_date)
                        bundle.putBoolean("from_summary",true)
                        nextFrag.arguments = bundle

                        (context as AppCompatActivity).supportFragmentManager.beginTransaction()
                            .replace(R.id.nav_host_fragment, nextFrag)
                            .addToBackStack(null)
                            .commit()

                    }
                    if (!myprojectsummary!!.style_img.equals("")) {
                        try {

                            val glideUrl = GlideUrl(
                                myprojectsummary!!.style_img,
                                LazyHeaders.Builder()
                                    .addHeader("Authorization", "Bearer ".plus(header))
                                    .build()
                            )

                            Glide.with(context!!).load(glideUrl)
                                .placeholder(R.drawable.project_image_placeholder)
                                .into(project_image)

                            project_image.setOnClickListener {
                                //ToDO

                                val profileimage =
                                    arrayOf<String?>(glideUrl.toString())
                                StfalconImageViewer.Builder<String>(
                                    context,
                                    profileimage
                                ) { view, image ->
                                    Glide.with(this).load(image).into(view)
                                    //Picasso.get().load(image).into(view)
                                }.show()

                            }
                        } catch (e: Exception) {

                            Glide.with(context!!).load(R.drawable.project_image_placeholder)
                                .into(project_image)
                        }
                    }
                }

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE

            })
        }).start()
        return root
    }
}