package com.jpertechnologyltd.jperprovider.ui.myproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.R


class MyProjectsProjectDetailFragment : Fragment() {
    var itemPosition :Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        itemPosition = args?.getInt("project_id")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_quote_project_detail, container, false)

        val tabLayout = root.findViewById(R.id.tabLayout) as TabLayout

        tabLayout.addTab(tabLayout.newTab().setText("Project Summary"))

        val viewpager = root.findViewById(R.id.pager) as ViewPager

        setupViewPager(viewpager)
        tabLayout.setupWithViewPager(viewpager)


        val project_detail_title = root.findViewById<TextView>(R.id.project_detail_title)
        val edit_item_min_edit = root.findViewById<TextInputEditText>(R.id.edit_item_min_edit)


        var position = itemPosition
        project_detail_title.setText("Project #".plus(position.toString()))


//        val quote_submission = root.findViewById<FloatingActionButton>(R.id.quote_create_button)
//        quote_submission.setOnClickListener{
//            var fr1 = getFragmentManager()?.beginTransaction()
//            fr1?.replace(R.id.nav_host_fragment, QuoteSubmissionFragment())
//            fr1?.commit()
//        }



        val back_button = root.findViewById(R.id.back_button) as ImageView
        back_button.setOnClickListener {

            var fr = getFragmentManager()?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.replace(R.id.nav_host_fragment, MyProjectsFragment())
            fr?.commit()
        }

//        val project_detail_status = root.findViewById(R.id.project_detail_status) as TextView
//        project_detail_status.setText("Pending")
//        val drawable =    project_detail_status.getBackground() as GradientDrawable
//        drawable.setColor(Color.rgb(237,176,173))

        return root
    }


    private fun setupViewPager(viewPager: ViewPager): Unit {
        val adapter =
            Adapter(
                childFragmentManager
            )
//        adapter.addFragment(QuoteQuoteFragment(), "Quote")
//        adapter.addFragment(QuoteManageFragment(), "Manage")
//        adapter.addFragment(QuoteSummaryFragment(), "Summary")
        adapter.addFragment(MyProjectsProjectManageFragment(), "Manage")
        adapter.addFragment(MyProjectsQuotedQuotesFragment(), "Quotes")
        adapter.addFragment(MyProjectsProjectSummaryFragment(), "Summary")
       viewPager.adapter = adapter
    }

    internal class Adapter(manager: FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(
            fragment: Fragment,
            title: String
        ) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

}




