package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Attachments
import com.jpertechnologyltd.jperprovider.adapter.AttachmentsAdapter


class AttachmentsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_fragment_attachments, container, false)

        val attachment_listview = root.findViewById<View>(R.id.attachments_listview) as RecyclerView

        val attachments = ArrayList<Attachments>()
        attachments.add(Attachments("Attachment one.pdf","",""))
        attachments.add(Attachments("Attachment two.jpg","",""))
        attachments.add(Attachments("Attachment three.png","",""))

        val back_button = root.findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {
            var fr = getFragmentManager()?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.add(R.id.nav_host_fragment, QuoteProjectDetailFragment())
            fr?.commit()
        }

        val attachments_adapter = AttachmentsAdapter(attachments)
        attachment_listview.adapter = attachments_adapter
        attachment_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)

        return root
    }


}