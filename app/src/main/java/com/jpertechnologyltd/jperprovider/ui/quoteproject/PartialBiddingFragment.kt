package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.PartialBiddingAdapter
import com.jpertechnologyltd.jperprovider.component.OnLoadMoreListener
import com.jpertechnologyltd.jperprovider.item.*
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion

import java.util.*
import java.util.concurrent.Future
import kotlin.collections.ArrayList

class PartialBiddingFragment : Fragment() {
    var lang:String?="en"
    private var header : String? = null
    private var filter_request_url : String? = null
    var project_id :Int? = null
    var status_id :Int? = null

    var edit_contract_save_button : Button? = null

    var no_biddings_view: NestedScrollView?=null
    var swipeContainer: SwipeRefreshLayout?=null
    var swipeContainer_empty: SwipeRefreshLayout?=null
    private var last_page : Int? = null
    private var current_page_no =1
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var biddings_listview:RecyclerView? =null
    var loading: Future<JsonObject>? = null
    var Postdata = JsonArray()
    lateinit var morebiddings : ArrayList<Bidding?>
    lateinit var biddings : ArrayList<Bidding?>
    private var nextpageurl : String? = null
    private var currentpageurl: String? = null

    private var current_page =1
    lateinit var mPartialBiddingAdapter: PartialBiddingAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        project_id = args?.getInt("project_id")
        status_id = args?.getInt("project_status")

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_partialbidding, container, false)
        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        filter_request_url = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("filter_request_url", "https://uat.jper.com.hk/api/company/available/projects?scope=2")

        if(!filter_request_url.equals("https://uat.jper.com.hk/api/company/available/projects?scope=2")){
            val editor = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
            editor.putString("filter_request_url", filter_request_url)
            editor.apply()
            filter_request_url="https://uat.jper.com.hk/api/company/available/projects?scope=2".plus(filter_request_url)
        }
        Log.d("filter_request_url_in_part",filter_request_url)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE


        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        val config = resources.configuration
        val locale = Locale(lang)


        Locale.setDefault(locale)
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)


//        /////////////////////Initiate_loading_anim///////////////////////////
//        val no_item_imageView = root.findViewById<View>(R.id.no_item_imageView) as ImageView
//        val no_item_instruction = root.findViewById<View>(R.id.no_item_instruction) as TextView

        biddings_listview = root.findViewById<View>(R.id.biddings_listview) as RecyclerView

        Thread(Runnable {
//            var partial_biddings = ArrayList<PartialBidding>()
//            partial_biddings.add(PartialBidding(1,2,2,2,2,"10-10-2020","12","123","123","123","HK$ 222"))
//            partial_biddings.add(PartialBidding(1,2,2,2,2,"10-10-2020","12","123","123","123","HK$ 222"))
//            partial_biddings.add(PartialBidding(1,2,2,2,2,"10-10-2020","12","123","123","123","HK$ 222"))
//            partial_biddings.add(PartialBidding(1,2,2,2,2,"10-10-2020","12","123","123","123","HK$ 222"))
//                if (partial_biddings==null)
//                quotations = ArrayList<Quotation>()



            try {
                // JsonObject json = new JsonObject();
                Ion.with(activity)
                    .load(filter_request_url)
                    .setHeader("Authorization", "Bearer " + header)
                    .setHeader("Content-Language", lang)
                    //  .setTimeout(10000)
                    .asJsonObject()
                    .setCallback(FutureCallback { e, result ->
                        if (e != null) {
                            //   Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                            return@FutureCallback
                        }

                        Postdata = try {
                            if (result.get("data").isJsonNull) JsonArray() else result.getAsJsonArray(
                                "data"
                            )
                        } catch (x: Exception) {
                            Log.d("error_jperx", x.toString())
                            JsonArray()
                        }
                        nextpageurl = try {
                            if (result.get("next_page_url").isJsonNull) "" else result.getAsJsonPrimitive(
                                "next_page_url"
                            )
                                .asString
                        } catch (x: Exception) {
                            Log.d("error_jperx", x.toString())
                            null
                        }
                        last_page = try {
                            if (result.get("last_page").isJsonNull) 1 else result.getAsJsonPrimitive(
                                "last_page"
                            )
                                .asString.toInt()
                        } catch (x: Exception) {
                            Log.d("error_jperx", x.toString())
                            null
                        }
                        biddings = ArrayList<Bidding?>()
                        currentpageurl  = try {
                            if (result.get("current_page").isJsonNull) "" else result.getAsJsonPrimitive(
                                "current_page"
                            ).asString

                        } catch (x: Exception) {
                            Log.d("error_jperx", x.toString())
                            ""
                        }

                        Log.d("myprojects", currentpageurl)
                        if (Postdata.size() > 0) {
                            activity?.runOnUiThread(java.lang.Runnable {
                               no_biddings_view!!.visibility = View.GONE
                            })
                            current_page_no = 1

                            for (i in 0 until Postdata.size()) {
                                try {
                                    val Bidding_list: JsonObject = Postdata.get(i).getAsJsonObject()
                                    var scope_id = Bidding_list["scope"].asInt
//                                    if (scope_id == 2) {
                                        var id =
                                            if (Bidding_list["id"].isJsonNull) 0 else Bidding_list["id"].asInt
                                        var district_id =
                                            if (Bidding_list["district_id"].isJsonNull) 0 else Bidding_list["district_id"].asInt
                                        val district = District(context!!, district_id).name
                                        val scope = ProjectScope(context!!, scope_id).name
                                        var type_id =
                                            if (Bidding_list["type"].isJsonNull) 0 else Bidding_list["type"].asInt
                                        val type = ProjectType(context!!, type_id).name
                                        var style_id =
                                            if (Bidding_list["style_id"].isJsonNull) 0 else Bidding_list["style_id"].asInt
                                        var created_at =
                                            if (Bidding_list["created_at"].isJsonNull) "" else Bidding_list["created_at"].asString
                                        var start_at =
                                            if (Bidding_list["start_at"].isJsonNull) "" else Bidding_list["start_at"].asString
                                        var completed_at =
                                            if (Bidding_list["completed_at"].isJsonNull) "" else Bidding_list["completed_at"].asString
                                        var post_date =
                                            if (Bidding_list["post_date"].isJsonNull) "" else Bidding_list["post_date"].asString
                                        val category_obj: JsonArray? = if(Bidding_list["category"].isJsonNull) null else Bidding_list["category"].asJsonArray
                                        var category_array =ArrayList<Int> ()
                                        var cat_cover = ""
                                        if(category_obj!=null && category_obj.size()>0){

                                            for(x in 0 until category_obj.size()) {
                                                if(x==0) cat_cover = category_obj.get(0).asJsonObject.get("cover").asString
                                                category_array.add(category_obj.get(x).asJsonObject.get("id").asInt)
                                            }

                                        }
                                        // var post_date =""
//                                        if(!post_date_org.equals("")) {
//                                            val originalFormat: DateFormat =
//                                                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
//                                            val targetFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
//                                            val date: Date =
//                                                originalFormat.parse(post_date_org)
//                                            post_date: String = targetFormat.format(date)
//                                        }
                                        var area = ""
                                        var budget_max = ""
                                        var project_id = 0
                                        var contract_m = ""
                                        val detail: JsonObject? =
                                            if (Bidding_list["detail"].isJsonNull) null else Bidding_list["detail"].asJsonObject
                                        if (detail != null) {
                                            area =
                                                if (detail["area"].isJsonNull) "" else detail["area"].asString.dropLast(3)
                                            budget_max =
                                                if (detail["budget_max"].isJsonNull) "" else detail["budget_max"].asString
                                            project_id =
                                                if (detail["project_id"].isJsonNull) 0 else detail["project_id"].asInt
                                            contract_m =
                                                if (detail["contract_m"].isJsonNull) "" else detail["contract_m"].asString
                                        }
                                        val style: JsonObject? =
                                            if (Bidding_list["style"].isJsonNull) null else Bidding_list["style"].asJsonObject
                                        var style_name_en = ""
                                        var style_name_zh = ""
                                        var cover = ""
                                        if (style != null) {
                                            style_name_en =
                                                if (style["name_en"].isJsonNull) "" else style["name_en"].asString
                                            style_name_zh =
                                                if (style["name_zh"].isJsonNull) "" else style["name_zh"].asString
                                            cover =
                                                if (style["img"].isJsonNull) "" else style["img"].asString
                                        }
                                        val bidding_style = Style(
                                            style_id,
                                            style_name_en,
                                            style_name_zh,
                                            cover
                                        )
                                        val bidding = Bidding(
                                            id,
                                            district_id,
                                            district,
                                            scope_id,
                                            scope,
                                            type_id,
                                            type,
                                            style_id,
                                            created_at,
                                            start_at,
                                            completed_at,
                                            post_date,
                                            area,
                                            budget_max,
                                            project_id,
                                            contract_m,
                                            bidding_style,
                                            cat_cover,
                                            category_array,
                                            false
                                        )
                                        biddings.add(bidding)
//                                    }

                                } catch (e: Exception) {
                                    Log.d("error_jperx",e.toString())
                                }
                            }
                            mPartialBiddingAdapter = PartialBiddingAdapter(biddings, biddings_listview)
                            biddings_listview!!.adapter = mPartialBiddingAdapter
                            mPartialBiddingAdapter.setLoaded()

                        mPartialBiddingAdapter.setOnLoadMoreListener(object : OnLoadMoreListener {
                            override fun onLoadMore() {
                                if(current_page_no<last_page!!){
                                    biddings_listview!!.post(Runnable {
                                        Log.d("filter_requestlast_page",last_page.toString())
                                        current_page_no++
                                        Handler().post {
                                            biddings.add(null)
                                            mPartialBiddingAdapter.notifyItemInserted(biddings.size - 1)
                                            Log.d("filter_request_url_new",filter_request_url.toString())
                                            Add_more_data_to_list(filter_request_url,current_page_no,last_page)
                                        }
                                    })
                                }
                            }
                        })
                        }
                        else{
                            activity?.runOnUiThread(java.lang.Runnable {
                                no_biddings_view!!.visibility = View.VISIBLE
                            })
                        }
                    })
            } catch (e: Exception) {
                Log.d("error_data",e.toString())
            }
            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {
                no_biddings_view= root.findViewById(R.id.no_biddings_view) as NestedScrollView

                biddings_listview!!.layoutManager = LinearLayoutManager(
                    context,
                    RecyclerView.VERTICAL,
                    false
                )
                biddings_listview!!.setNestedScrollingEnabled(true)
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE

            })
        }).start()
//

        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                // Refresh items
                try {
                    refreshItems()
                } catch (e: java.lang.Exception) {
                }
            }
        })
        edit_contract_save_button = root.findViewById(R.id.edit_contract_save_button) as Button
        edit_contract_save_button!!.setOnClickListener {

            try {
                refreshItems()
            } catch (e: java.lang.Exception) {
            }

        }




        return root
    }
    fun refreshItems() {
        // Load items
        // ...
        val handler = Handler()
        handler.postDelayed({
            try {
                onItemsLoadComplete() // Actions to do after 10 seconds
            } catch (e: java.lang.Exception) {
                swipeContainer!!.setRefreshing(false)
            }
        }, 1000)
        //   activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();

        //    onItemsLoadComplete();
    }
    fun isEmptyString(text: String?): Boolean {
        return text == null || text.trim { it <= ' ' } == "null" || text.trim { it <= ' ' }
            .isEmpty()
    }
    fun onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        getFragmentManager()!!.beginTransaction().detach(this).commitNowAllowingStateLoss()
        getFragmentManager()!!.beginTransaction().attach(this).commitAllowingStateLoss()        // Stop refresh animation
        swipeContainer!!.setRefreshing(false)
    }
    fun Add_more_data_to_list(filter_request_url:String?,current_page:Int, last_page: Int?)
    {

        var new_url = filter_request_url.plus("&page=").plus(current_page)
        Log.d("new_url",new_url)
        try {
            morebiddings = ArrayList<Bidding?>()
            if (loading != null && !loading!!.isDone() && !loading!!.isCancelled()) {
                biddings.removeAt(biddings.size - 1)

                mPartialBiddingAdapter.setLoaded()
                mPartialBiddingAdapter.notifyItemRemoved(biddings.size)
                return
            }
            if (current_page>last_page!!) {
                biddings.removeAt(biddings.size - 1)

                mPartialBiddingAdapter.setLoaded()
                mPartialBiddingAdapter.notifyItemRemoved(biddings.size)
                return
            }
            Ion.with(activity)
                .load(new_url)
                .setHeader("Authorization", "Bearer " + header)
                .setHeader("Content-Language", lang)
                //.setTimeout(10000)
                .asJsonObject().setCallback(
                    FutureCallback { e, result ->
                        if (e != null) {
                            biddings.removeAt(biddings.size - 1)

                            mPartialBiddingAdapter.setLoaded()
                            mPartialBiddingAdapter.notifyItemRemoved(biddings.size)
                            Log.e("MYAPP", "exception", e)
                            //  Toast.makeText(getContext(),e.getMessage()+nextpageurl+"&catid=12", Toast.LENGTH_SHORT).show();
                            return@FutureCallback
                        }

                        biddings.removeAt(biddings.size - 1)
                        mPartialBiddingAdapter.setLoaded()

                        mPartialBiddingAdapter.notifyItemRemoved(biddings.size)
                        // Toast.makeText(getContext(),nextpageurl, Toast.LENGTH_SHORT).show();
                        Postdata = try {
                            if (result.get("data").isJsonNull) JsonArray() else result.getAsJsonArray(
                                "data"
                            )
                        } catch (x: Exception) {
                            Log.d("error_jperx", x.toString())
                            JsonArray()
                        }

                        currentpageurl =
                                result.getAsJsonPrimitive("current_page")
                                    .asString
                        if(Postdata.size()>0) {
                            for (i in 0 until Postdata.size()) {
                                try {
                                    val Bidding_list: JsonObject = Postdata.get(i).getAsJsonObject()
                                    var scope_id = Bidding_list["scope"].asInt
                                    var id =
                                        if (Bidding_list["id"].isJsonNull) 0 else Bidding_list["id"].asInt
                                    var district_id =
                                        if (Bidding_list["district_id"].isJsonNull) 0 else Bidding_list["district_id"].asInt
                                    val district = District(context!!, district_id).name
                                    val scope = ProjectScope(context!!, scope_id).name
                                    var type_id =
                                        if (Bidding_list["type"].isJsonNull) 0 else Bidding_list["type"].asInt
                                    val type = ProjectType(context!!, type_id).name
                                    var style_id =
                                        if (Bidding_list["style_id"].isJsonNull) 0 else Bidding_list["style_id"].asInt
                                    var created_at =
                                        if (Bidding_list["created_at"].isJsonNull) "" else Bidding_list["created_at"].asString
                                    var start_at =
                                        if (Bidding_list["start_at"].isJsonNull) "" else Bidding_list["start_at"].asString
                                    var completed_at =
                                        if (Bidding_list["completed_at"].isJsonNull) "" else Bidding_list["completed_at"].asString
                                    var post_date =
                                        if (Bidding_list["post_date"].isJsonNull) "" else Bidding_list["post_date"].asString
                                    val category_obj: JsonArray? =
                                        if (Bidding_list["category"].isJsonNull) null else Bidding_list["category"].asJsonArray
                                    var category_array = ArrayList<Int>()
                                    var cat_cover = ""

                                    if (category_obj != null && category_obj.size() > 0) {
                                        for (x in 0 until category_obj.size()) {
                                            if(x==0) cat_cover = category_obj.get(0).asJsonObject.get("cover").asString
                                            category_array.add(
                                                category_obj.get(x).asJsonObject.get(
                                                    "id"
                                                ).asInt
                                            )
                                        }

                                    }
                                    var area = ""
                                    var budget_max = ""
                                    var project_id = 0
                                    var contract_m = ""
                                    val detail: JsonObject? =
                                        if (Bidding_list["detail"].isJsonNull) null else Bidding_list["detail"].asJsonObject
                                    if (detail != null) {
                                        area =
                                            if (detail["area"].isJsonNull) "" else detail["area"].asString.dropLast(
                                                3
                                            )
                                        budget_max =
                                            if (detail["budget_max"].isJsonNull) "" else detail["budget_max"].asString
                                        project_id =
                                            if (detail["project_id"].isJsonNull) 0 else detail["project_id"].asInt
                                        contract_m =
                                            if (detail["contract_m"].isJsonNull) "" else detail["contract_m"].asString
                                    }
                                    val style: JsonObject? =
                                        if (Bidding_list["style"].isJsonNull) null else Bidding_list["style"].asJsonObject
                                    var style_name_en = ""
                                    var style_name_zh = ""
                                    var cover = ""
                                    if (style != null) {
                                        style_name_en =
                                            if (style["name_en"].isJsonNull) "" else style["name_en"].asString
                                        style_name_zh =
                                            if (style["name_zh"].isJsonNull) "" else style["name_zh"].asString
                                        cover =
                                            if (style["img"].isJsonNull) "" else style["img"].asString
                                    }
                                    val bidding_style = Style(
                                        style_id,
                                        style_name_en,
                                        style_name_zh,
                                        cover
                                    )
                                    val bidding = Bidding(
                                        id,
                                        district_id,
                                        district,
                                        scope_id,
                                        scope,
                                        type_id,
                                        type,
                                        style_id,
                                        created_at,
                                        start_at,
                                        completed_at,
                                        post_date,
                                        area,
                                        budget_max,
                                        project_id,
                                        contract_m,
                                        bidding_style,
                                        cat_cover,
                                        category_array,
                                        false
                                    )
                                    morebiddings.add(bidding)

                                } catch (e: Exception) {

                                    mPartialBiddingAdapter.setLoaded()
                                    Log.d("error_jperxx", e.toString())
                                }
                            }
                        }
                            biddings.addAll(morebiddings)
                            morebiddings.clear()
                            mPartialBiddingAdapter.notifyDataSetChanged()
                            mPartialBiddingAdapter.setLoaded()

                    })
        }
        catch (e: Exception){

            mPartialBiddingAdapter.setLoaded()
        }
    }
}

