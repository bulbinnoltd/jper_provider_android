package com.jpertechnologyltd.jperprovider.ui.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Activity
import com.jpertechnologyltd.jperprovider.adapter.ActivityAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetNotification
import com.jpertechnologyltd.jperprovider.component.HttpsService.ReadNotification

class ActivityFragment : Fragment() {

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null

    private var header : String? = null
    private var lang : String? = "en"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_activity, container, false)
        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","")

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility =View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////



        val activity_listview = root.findViewById<View>(R.id.activity_listview) as RecyclerView
        //val Activity_item_title : String = getString(R.string.Activity_item_title)

        Thread(Runnable {
            var activities = GetNotification(context!!,"https://uat.jper.com.hk/api/my/notification",header,lang)
            if(activities==null)
                activities = ArrayList<Activity>()
            val read = ReadNotification(context!!,"https://uat.jper.com.hk/api/my/notification/read",header,lang)
            Log.d("ReadNotification",read.toString())

            activity?.runOnUiThread(java.lang.Runnable {

                val activity_adapter = activities?.let { ActivityAdapter(it) }
                activity_listview.adapter = activity_adapter
                activity_listview.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)


                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility =View.GONE
            })

        }).start()

        return root
    }
    fun BadgeNumber(position: Int): Int {
        Log.d("fragment", position.toString())
        (activity as MainActivity).onFabolousEvent(position)
        return position
    }


}