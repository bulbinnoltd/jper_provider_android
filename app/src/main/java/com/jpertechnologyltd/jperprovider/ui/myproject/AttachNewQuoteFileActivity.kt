package com.jpertechnologyltd.jperprovider.ui.myproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.FileUploadAdapter
import com.jpertechnologyltd.jperprovider.component.FileUtil
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.FileUpload
import java.io.File
class AttachNewQuoteFileActivity : AppCompatActivity() {

    lateinit var fileupload_adapter : FileUploadAdapter
    var header:String?=""
    var lang:String?="en"
    var project_id : Int? = null
    var filesupload = java.util.ArrayList<FileUpload>()
    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attach)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        try {
            project_id = intent.getIntExtra("project_id",0)

        }
        catch (e:java.lang.Exception){
            Log.d("project_id_get",e.toString())
        }

        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        val attach_button = findViewById<TextView>(R.id.attach_button)
        attach_button.setPaintFlags(attach_button.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        attach_button.setOnClickListener {
            showFileChooser()
        }
        val fileupload_listview = findViewById<View>(R.id.fileupload_listview) as RecyclerView
        filesupload = java.util.ArrayList<FileUpload>()

        fileupload_adapter = FileUploadAdapter(filesupload)
        fileupload_listview.adapter = fileupload_adapter
        fileupload_listview.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )

        val save_button = findViewById(R.id.forgot_pw_submit_button) as Button
        save_button.setOnClickListener {

            var file: File? = null
            var file_format = ""
            try {
                file = filesupload.get(0).fileUri
                if (filesupload.get(0).title.contains(".pdf")) {
                    file_format = "application/pdf"
                }
                else if (filesupload.get(0).title.contains(".png")) {
                    file_format = "image/png"
                } else if (filesupload.get(0).title.contains(".jpeg")) {
                    file_format = "image/jpeg"
                } else if (filesupload.get(0).title.contains(".jpg")) {
                    file_format = "image/jpeg"
                    file_format = ".jpg"
                } else if (filesupload.get(0).title.contains(".doc")) {
                    file_format = "application/msword"
                } else if (filesupload.get(0).title.contains(".docx")) {
                    file_format =
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                } else if (filesupload.get(0).title.contains(".xlsx")) {
                    file_format =
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                }
                var save_result = ""
                Thread(Runnable {
                    save_result = HttpsService.UpdateContract(
                        this,
                        "https://uat.jper.com.hk/api/company/project/contract/upload",
                        project_id,
                        file,
                        file_format,
                        header,
                        lang
                    )
                    runOnUiThread(Runnable {

                        Loading!!.visibility = View.VISIBLE
                        progressOverlay!!.visibility = View.VISIBLE
                        if (!save_result.equals("")) {

                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE


                            val dialog = Dialog(this)
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.setCancelable(false)
                            dialog.setContentView(R.layout.dialog_message)

                            val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
                            dialog_message.text = save_result
                            val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

                            val close_button = dialog.findViewById(R.id.close_button) as ImageView
                            close_button.visibility=View.INVISIBLE
                            confirm_button.setOnClickListener {
                                dialog.dismiss()
                                val editor = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE).edit()
                                editor.putBoolean("refresh_contract",true)
                                editor.putString("contract_temp_link_"+project_id,"https://uat.jper.com.hk/api/company/project/"+project_id+"/contract.pdf")
                                Log.d("contract_temp_link","https://uat.jper.com.hk/api/company/project/"+project_id+"/contract.pdf")
                                editor.apply()
                                editor.commit()
                                finish()
                            }
                            close_button.setOnClickListener {
                                dialog.dismiss()

                                finish()

                            }

                            dialog.setCancelable(true)
                            dialog.show()

                        }
                    })
                }).start()
            } catch (e: Exception) {
                showDialog(this, getString(R.string.General_upload_file_warning))
            }


            //sendinfo(Quotelist)
            //Log.d("submit list",Quotelist.toString())
        }


        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            super.onBackPressed()
        }


    }
    fun showFileChooser() {

        val intent = Intent(Intent.ACTION_GET_CONTENT)

        // Update with mime types
        intent.type = "*/*"
        val mimetypes: Array<String> = arrayOf(
//            "image/jpeg",
//            "image/png",
            "application/pdf"
//            "application/msword",
//            "application/vnd.ms-excel",
//            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
//            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        )
        // Update with additional mime types here using a String[].
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)

        // Only pick openable and local files. Theoretically we could pull files from google drive
        // or other applications that have networked files, but that's unnecessary for this example.
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        intent.setAction(Intent.ACTION_GET_CONTENT)
        // REQUEST_CODE = <some-integer>
        startActivityForResult(intent, (0x001 + 1))
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != (0x001 + 1) || resultCode != AppCompatActivity.RESULT_OK) {
            return
        }
        val fileUri =data?.data
        try{
            val file: File = FileUtil.from(this, fileUri)
            importFile(fileUri!!, file)
        }
        catch (x: Exception){
            Log.d("error", x.toString())
        }

//        val file = File(real_uri)

        //val file = File(fileUri?.path)
        // Import the file

//        var docPaths = ArrayList<Any>()

//        if (fileUri != null) {
//                importFile(fileUri,File(fileUri.toString()))
//                Log.d("fileUri",fileUri.toString())
//
//        }
    }
    fun importFile(fileUri: Uri, file: File) {

        //val selectedFilePath: String? = fileUri.path
        val fileName : String? = getFileName(fileUri)
        filesupload.clear()
        filesupload.add(FileUpload(fileName!!, file))
        fileupload_adapter.refreshDataset()
        // The temp file could be whatever you want

        Log.d("uploaded", fileName.toString())
        Log.d("uploaded_file", fileUri.path?.toString())
        //val fileCopy = copyToTempFile(fileUri, tempFile )

        // Done!
    }

    fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor: Cursor? = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                if (cursor != null) {
                    cursor.close()
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result!!.substring(cut + 1)
            }
        }
        return result
    }
    private fun showDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
}
