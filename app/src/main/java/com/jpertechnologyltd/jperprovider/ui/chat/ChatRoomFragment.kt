package com.jpertechnologyltd.jperprovider.ui.chat

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.ChatRoomAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.Add


class ChatRoomFragment : Fragment() {
    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    var header:String?= null
    var lang:String?= "en"
    var swipeContainer: SwipeRefreshLayout?=null

    private lateinit var broadcastReceiver: BroadcastReceiver
    var this_activity: Activity? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.activity_chatroom, container, false)

        this_activity = activity
        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////


        val chatroom_listview = root.findViewById<View>(R.id.chatroom_listview) as RecyclerView

        Thread(Runnable {

            val chatroom_list = HttpsService.GetChatRooms(context, "https://uat.jper.com.hk/api/chatroom/all",header,lang)


            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {
                if (chatroom_list!=null) {
                    val chatroom_adapter = ChatRoomAdapter(chatroom_list)
                    chatroom_listview.adapter = chatroom_adapter
                    chatroom_listview.layoutManager =
                        LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                }

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()
        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        // Setup refresh listener which triggers new data loading
        // Setup refresh listener which triggers new data loading
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {


                val handler = Handler()
                handler.postDelayed({
                    try {
                        try {

                            Thread(Runnable {

                                val chatroom_list = HttpsService.GetChatRooms(this_activity, "https://uat.jper.com.hk/api/chatroom/all",header,lang)


                                // try to touch View of UI thread
                                activity?.runOnUiThread(java.lang.Runnable {
                                    chatroom_listview.adapter=null
                                    if (chatroom_list!=null) {
                                        val chatroom_adapter = ChatRoomAdapter(chatroom_list)
                                        chatroom_listview.adapter = chatroom_adapter
                                        chatroom_listview.layoutManager =
                                            LinearLayoutManager(this_activity, RecyclerView.VERTICAL, false)
                                    }

                                    Loading!!.visibility = View.GONE
                                    progressOverlay!!.visibility = View.GONE
                                })
                            }).start()
                        } catch (e: java.lang.Exception) {
                        }
                        swipeContainer!!.setRefreshing(false)// Actions to do after 10 seconds
                    } catch (e: java.lang.Exception) {
                        swipeContainer!!.setRefreshing(false)
                    }
                }, 1000)
                // Refresh items

            }
        })
        val back_button = root.findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            activity?.onBackPressed()
        }
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                if (p1 != null) {
                    handleBroadcastActions(p1)
                }
            }

        }

        return root
    }
    private fun handleBroadcastActions(intent: Intent) {
        when (intent.action) {
            "Reload" -> {
                refreshItems()
            }
        }
    }

    fun refreshItems() {
        // Load items
        // ...
        val handler = Handler()
        handler.postDelayed({
            try {
                onItemsLoadComplete() // Actions to do after 10 seconds
            } catch (e: java.lang.Exception) {
                swipeContainer!!.setRefreshing(false)
            }
        }, 1000)
        //   activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();

        //    onItemsLoadComplete();
    }
    fun onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        getFragmentManager()!!.beginTransaction().detach(this).commitNowAllowingStateLoss()
        getFragmentManager()!!.beginTransaction().attach(this).commitAllowingStateLoss()        // Stop refresh animation
        swipeContainer!!.setRefreshing(false)
    }
    override fun onResume() {
        super.onResume()
        activity!!.registerReceiver(broadcastReceiver, IntentFilter("Reload"))

    }

    override fun onPause() {
        super.onPause()
        activity!!.unregisterReceiver(broadcastReceiver)
    }

    fun sendinfo(name:Editable?, min: Editable?, max: Editable? ){

//        val msg = HttpsService.UpdateTime(this,"https://uat.jper.com.hk/api/owner/change/password",password.toString(),header)
//        if (msg!=null){
//            showDialog(this ,msg)
//        }
}
    fun printQuote( quote:ArrayList<Add>){
        for(i in 0 until quote.size){
            Log.d("printquote",quote[i].description)
        }
    }
}

