package com.jpertechnologyltd.jperprovider.ui.myproject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.item.Quotation
import com.jpertechnologyltd.jperprovider.item.QuotationItem
import com.jpertechnologyltd.jperprovider.adapter.QuoteQuoteAdapter


class MyProjectsQuotedQuotesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val Context = context
        val item_list: MutableList<QuotationItem> = ArrayList()
        val root = inflater.inflate(R.layout.fragment_fragment_myprojects_quoted_quotes, container, false)
        val quote_quote_listview = root.findViewById<View>(R.id.quote_quote_listview) as RecyclerView
        val quote_quote_items = ArrayList<Quotation>()
        quote_quote_items.add(Quotation("Provider Name 1", "","2020-5-28", "HKD\$ 60,000 - 920,000","2020-06-02",item_list))
//        quote_quote_items.add(Quotation("ENTER ITEM A TITLE", "","Description details or additional remarks.", "2020-05-06","HKD$300,000","HKD$300,000","1"))
//        quote_quote_items.add(Quotation("ENTER ITEM A TITLE", "","Description details or additional remarks.", "2020-05-06","HKD$300,000","HKD$300,000","1"))
//        quote_quote_items.add(Quotation("ENTER ITEM A TITLE", "","Description details or additional remarks.", "2020-05-06","HKD$300,000","HKD$300,000","1"))
//        quote_quote_items.add(Quotation("ENTER ITEM A TITLE", "","Description details or additional remarks.", "2020-05-06","HKD$300,000","HKD$300,000","1"))

        val contract_adapter = parentFragment?.let { it.context?.let { it1 -> QuoteQuoteAdapter(it1,quote_quote_items) } }
        quote_quote_listview.adapter = contract_adapter
        quote_quote_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)

        item_list.add(QuotationItem("Item A","HKD$ 100,000","HKD$ 100,000"))
        item_list.add(QuotationItem("Item A","HKD$ 100,000","HKD$ 100,000"))
        item_list.add(QuotationItem("Item A","HKD$ 100,000","HKD$ 100,000"))

        return root
    }
}

