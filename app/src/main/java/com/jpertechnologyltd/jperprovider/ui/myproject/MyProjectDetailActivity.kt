package com.jpertechnologyltd.jperprovider.ui.myproject

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.item.ProjectStatus


class MyProjectDetailActivity : AppCompatActivity() {
    var project_id :Int? = null
    var status_id :Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_quote_project_detail)

        val tabLayout = findViewById(R.id.tabLayout) as TabLayout
        val viewpager = findViewById(R.id.pager) as ViewPager
        project_id = intent.getIntExtra("project_id",0)
        status_id = intent.getIntExtra("status_id",0)

        val project_detail_title = findViewById<TextView>(R.id.project_detail_title)
        val project_detail_status = findViewById<TextView>(R.id.project_detail_status)

//        setupViewPager(viewpager)
//        tabLayout.setupWithViewPager(viewpager)
        project_detail_title.setText(getString(R.string.project_title).plus(" #").plus(project_id.toString()))

        val status = ProjectStatus(this, status_id!!).name
        val status_rgb = ProjectStatus(this, status_id!!).rgb
        val drawable = project_detail_status.getBackground() as GradientDrawable
        drawable.setColor(status_rgb!!)
        project_detail_status.text = ProjectStatus(this,status_id!!).name

    }
    private fun setupViewPager(viewPager: ViewPager): Unit {
        val adapter =
            Adapter(
                supportFragmentManager
            )
        val bundle = Bundle()

        bundle.putInt("project_id", project_id!!)
        bundle.putInt("status_id", status_id!!)
        val MyProjectsProjectSummaryFragment= MyProjectsProjectSummaryFragment()
        MyProjectsProjectSummaryFragment.setArguments(bundle)


        val MyProjectsProjectManageFragment= MyProjectsProjectManageFragment()
        MyProjectsProjectManageFragment.setArguments(bundle)

        adapter.addFragment(MyProjectsProjectManageFragment, getString(R.string.Project_detail_tab_contract))
        adapter.addFragment(MyProjectsQuotedQuotesFragment(),  getString(R.string.Project_detail_tab_payment))
//        adapter.addFragment(MyProjectsProjectManageFragment,  getString(R.string.Project_detail_tab_disburse))
        adapter.addFragment(MyProjectsProjectSummaryFragment,  getString(R.string.Project_detail_tab_summary))
        viewPager.adapter = adapter
    }

    internal class Adapter(manager: FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(
            fragment: Fragment,
            title: String
        ) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }
}
