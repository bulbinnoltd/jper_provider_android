package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.FileUploadAdapter
import com.jpertechnologyltd.jperprovider.item.Add
import com.jpertechnologyltd.jperprovider.item.FileUpload
import java.io.File

import com.jpertechnologyltd.jperprovider.component.FileUtil

class AddItemFragment : Fragment() {
    private var header : String? = null
    private var post_date : String? = null
    var Quotelist : ArrayList<Add>? =null

    var project_id :Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        Quotelist = args?.getParcelableArrayList<Add>("data")
        project_id = args?.getInt("project_id")
        post_date = args?.getString("post_date")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_add_item, container, false)
        val description = root.findViewById<TextInputEditText>(R.id.add_item_name_edit)
        val remarks = root.findViewById<TextInputEditText>(R.id.add_item_remarks_edit)
        val add_item_min_edit = root.findViewById<TextInputEditText>(R.id.add_item_min_edit)
        val add_item_max_edit = root.findViewById<TextInputEditText>(R.id.add_item_max_edit)

        Quotelist?.let { printQuote(it)}

//        val AddItem = ArrayList<AddItem>()
//        AddItem.add(AddItem(description.toString(), add_item_min_edit.toString(),add_item_max_edit.toString(), ""))

        val back_button = root.findViewById(R.id.back_button) as ImageView
        back_button.setOnClickListener {
            activity!!.onBackPressed()
        }

//        back_button.setOnClickListener {
//
//            val QuoteSubmissionFragment: Fragment = QuoteSubmissionFragment()
//            val bundle = Bundle()
//            bundle.putParcelableArrayList("data", Quotelist)
//            bundle.putInt("project_id", project_id!!)
//            bundle.putString("post_date", post_date!!)
//            QuoteSubmissionFragment.setArguments(bundle)
//
//            var fr = getFragmentManager()?.beginTransaction()
//            fr?.addToBackStack(null)
//            fr?.add(R.id.nav_host_fragment,
//                QuoteSubmissionFragment
//            )
//            fr?.commit()
//
//        }

        val add_another_button = root.findViewById(R.id.add_another_button) as Button

        add_another_button.setOnClickListener {
            description.text.toString()
            remarks.text.toString()
            add_item_min_edit.text.toString()
            add_item_max_edit.text.toString()
            sendinfo(description.text,add_item_min_edit.text,add_item_max_edit.text)

            val add_item_min_float: Float = java.lang.Float.valueOf(add_item_min_edit.text.toString())
            val add_item_max_float: Float = java.lang.Float.valueOf(add_item_max_edit.text.toString())
            Quotelist?.add(0,Add(0,description.text.toString(), add_item_min_float ,add_item_max_float, remarks.text.toString()))
            Log.d("submit item", Quotelist!![0].max_amount.toString())

            val AddItemFragment: Fragment = AddItemFragment()
            val bundle = Bundle()
            if (Quotelist != null) {
                bundle.putParcelableArrayList("data", Quotelist)
                bundle.putInt("project_id", project_id!!)
                bundle.putString("post_date", post_date!!)
                AddItemFragment.setArguments(bundle)
            }
            var fr = getFragmentManager()?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.replace(R.id.nav_host_fragment,AddItemFragment)
            fr?.commit()
        }
        val save_button = root.findViewById(R.id.done_button) as Button

        save_button.setOnClickListener {
            var add_item_min:String?=null
            var add_item_max:String?=null
            var description_value:String?=null
            try { add_item_min=  add_item_min_edit.text.toString()}catch (x: Exception) { }
            try { add_item_max=  add_item_max_edit.text.toString()}catch (x: Exception) { }
            try { description_value=  description.text.toString()}catch (x: Exception) { }
            if(add_item_min.equals("") ||add_item_max.equals("") || description_value.equals(""))
            {
                showDialog(activity!!,getString(R.string.Without_info))
            }
            else {
                try {
                    if(add_item_min_edit.text.toString().equals("0")){
                        showDialog(activity!!,getString(R.string.zero_amount_warning))
                    }
                    else if(add_item_max_edit.text.toString().equals("0")){
                        showDialog(activity!!, context!!.getString(R.string.warn_max_min))
                    }
                    else if (add_item_min_edit.text.toString()
                            .toDouble() <= add_item_max_edit.text.toString().toDouble()
                    ) {
                        description.text.toString()
                        add_item_min_edit.text.toString()
                        add_item_max_edit.text.toString()
                        sendinfo(description.text, add_item_min_edit.text, add_item_max_edit.text)

                        val add_item_min_float: Float =
                            java.lang.Float.valueOf(add_item_min_edit.text.toString())
                        val add_item_max_float: Float =
                            java.lang.Float.valueOf(add_item_max_edit.text.toString())
                        Quotelist?.add(
                            Quotelist!!.size - 2,
                            Add(
                                0,
                                description.text.toString(),
                                add_item_min_float,
                                add_item_max_float,
                                remarks.text.toString()
                            )
                        )
                        Log.d("submit item", Quotelist!![2].max_amount.toString())

                        val QuoteSubmissionFragment: Fragment = QuoteSubmissionFragment()
                        val bundle = Bundle()
                        if (Quotelist != null) {
                            bundle.putParcelableArrayList("data", Quotelist)
                            bundle.putInt("project_id", project_id!!)
                            bundle.putString("post_date", post_date!!)
                            QuoteSubmissionFragment.setArguments(bundle)
                        }
                        var fr = getFragmentManager()?.beginTransaction()
                        getFragmentManager()?.popBackStack()
                        fr?.addToBackStack(null)
                        fr?.replace(R.id.nav_host_fragment, QuoteSubmissionFragment)
                        fr?.commit()
                    } else {
                        showDialog(activity!!, context!!.getString(R.string.warn_max_min))
                    }
                }
                catch (e: Exception) {
                    showDialog(activity!!, context!!.getString(R.string.Without_info))
                }
            }
        }

//        val contract_adapter = parentFragment?.let { ContractItemAdapter(contractitems, it) }
//        contract_listview.adapter = contract_adapter
//        contract_listview.layoutManager = LinearLayoutManager (activity, RecyclerView.VERTICAL,false)

        return root
    }
    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    fun sendinfo(name:Editable?, min: Editable?, max: Editable? ){

//        val msg = HttpsService.UpdateTime(this,"https://uat.jper.com.hk/api/owner/change/password",password.toString(),header)
//        if (msg!=null){
//            showDialog(this ,msg)
//        }
}
    fun printQuote( quote:ArrayList<Add>){
        for(i in 0 until quote.size){
            Log.d("printquote",quote[i].description)
        }
    }

}


