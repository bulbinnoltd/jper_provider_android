package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.FullBiddingAdapter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class QuoteProjectDetailFragment : Fragment() {
    var itemPosition :Int? = null
    var post_date :String? = null
    var project_image :String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        itemPosition = args?.getInt("project_id")
        post_date = args?.getString("post_date")
        project_image = args?.getString("project_image")

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_quote_project_detail, container, false)
        val lang =context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        val config = resources.configuration
        val locale = Locale(lang)


        Locale.setDefault(locale)
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)
        val project_submitDate = root.findViewById<TextView>(R.id.project_submitDate)

        var format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try {
            val newDate = format.parse(post_date)
            format = SimpleDateFormat("yyyy-MM-dd")
            val date = format.format(newDate)
            project_submitDate.text = date
        } catch (e: ParseException) {
            e.printStackTrace()
            project_submitDate.text= post_date
        }
        val tabLayout = root.findViewById(R.id.tabLayout) as TabLayout
        tabLayout.visibility = View.GONE
        tabLayout.addTab(tabLayout.newTab().setText("Project Summary"))
        val view = root.findViewById(R.id.view) as View
        view.visibility = View.GONE
        val viewpager = root.findViewById(R.id.pager) as ViewPager

        setupViewPager(viewpager)
        tabLayout.setupWithViewPager(viewpager)

        val project_detail_title = root.findViewById<TextView>(R.id.project_detail_title)

        var position = itemPosition
        project_detail_title.setText("Project #".plus(position.toString()))

        val myprojects_calendar_icon: ImageView = root.findViewById<ImageView>(R.id.myprojects_calendar_icon)
        myprojects_calendar_icon.visibility=View.GONE


//        val quote_submission = root.findViewById<FloatingActionButton>(R.id.quote_create_button)
//        quote_submission.setOnClickListener{
//            var fr1 = getFragmentManager()?.beginTransaction()
//            fr1?.replace(R.id.nav_host_fragment, QuoteSubmissionFragment())
//            fr1?.commit()
//        }

        val back_button = root.findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {

            var fr = getFragmentManager()?.beginTransaction()
            fr?.replace(R.id.nav_host_fragment, QuoteProjectFragment())

            fr?.commit()

        }
        val project_submitDate_title = root.findViewById(R.id.project_submitDate_title) as TextView

        project_submitDate_title.visibility=View.VISIBLE
        project_submitDate.visibility=View.VISIBLE

        val project_detail_status = root.findViewById(R.id.project_detail_status) as TextView
        project_detail_status.visibility=View.GONE

//        val drawable = project_detail_status.getBackground() as GradientDrawable
//        drawable.setColor(Color.rgb(118,203,196))

        return root
    }


    private fun setupViewPager(viewPager: ViewPager): Unit {
        val adapter = Adapter(childFragmentManager)
        //adapter.addFragment(QuoteQuoteFragment(), "Quote")
        val nextFrag: Fragment = QuoteProjectSummaryFragment()
        val bundle = Bundle()
        bundle.putInt("project_id", itemPosition!!)
        bundle.putString("post_date", post_date!!)
        bundle.putString("project_image", project_image!!)
        nextFrag.arguments = bundle
        adapter.addFragment(nextFrag, "Summary")
       viewPager.adapter = adapter
    }

    internal class Adapter(manager: FragmentManager?) : FragmentPagerAdapter(manager!!) {
        private val mFragmentList: MutableList<Fragment> =
            ArrayList()
        private val mFragmentTitleList: MutableList<String> = ArrayList()
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(
            fragment: Fragment,
            title: String
        ) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

}




