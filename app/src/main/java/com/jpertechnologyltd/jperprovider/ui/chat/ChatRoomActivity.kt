package com.jpertechnologyltd.jperprovider.ui.chat

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.ChatRoomAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService

class ChatRoomActivity : AppCompatActivity() {

    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null
    var header:String?= null
    var lang:String?= "en"
    var swipeContainer:SwipeRefreshLayout?=null

    var this_activity: Activity? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chatroom)
        this_activity = this
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////

        val chatroom_listview = findViewById<View>(R.id.chatroom_listview) as RecyclerView
        Thread(Runnable {

            val chatroom_list = HttpsService.GetChatRooms(this, "https://uat.jper.com.hk/api/chatroom/all",header,lang)


            // try to touch View of UI thread
            this.runOnUiThread(java.lang.Runnable {
                if (chatroom_list!=null) {
                    val chatroom_adapter = ChatRoomAdapter(chatroom_list)
                    chatroom_listview.adapter = chatroom_adapter
                    chatroom_listview.layoutManager =
                        LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                }

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()

        swipeContainer = findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        // Setup refresh listener which triggers new data loading
        // Setup refresh listener which triggers new data loading
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {


                val handler = Handler()
                handler.postDelayed({
                    try {
                        try {

                            Thread(Runnable {

                                val chatroom_list = HttpsService.GetChatRooms(this_activity, "https://uat.jper.com.hk/api/chatroom/all",header,lang)


                                // try to touch View of UI thread
                                runOnUiThread(java.lang.Runnable {
                                    chatroom_listview.adapter=null
                                    if (chatroom_list!=null) {
                                        val chatroom_adapter = ChatRoomAdapter(chatroom_list)
                                        chatroom_listview.adapter = chatroom_adapter
                                        chatroom_listview.layoutManager =
                                            LinearLayoutManager(this_activity, RecyclerView.VERTICAL, false)
                                    }

                                    Loading!!.visibility = View.GONE
                                    progressOverlay!!.visibility = View.GONE
                                })
                            }).start()
                        } catch (e: java.lang.Exception) {
                        }
                        swipeContainer!!.setRefreshing(false)// Actions to do after 10 seconds
                    } catch (e: java.lang.Exception) {
                        swipeContainer!!.setRefreshing(false)
                    }
                }, 1000)
                // Refresh items

            }
        })

        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount != 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
