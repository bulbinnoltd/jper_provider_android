package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.drjacky.imagepicker.ImagePicker
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.FileUploadAdapter
import com.jpertechnologyltd.jperprovider.adapter.FullBiddingAdapter
import com.jpertechnologyltd.jperprovider.adapter.QuoteSubmissionAdapter
import com.jpertechnologyltd.jperprovider.component.FileUtil
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.Add
import com.jpertechnologyltd.jperprovider.item.FileUpload
import com.koushikdutta.ion.Ion
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat


class QuoteSubmissionFragment : Fragment() {

    var Quotelist : ArrayList<Add>? =null
    var title :String? = null
    private var header : String? = null
    var min_amount :String? = null
    var max_amount :String? = null
    var post_date :String? = null
    var project_id :Int? = null
    var fixed_items :ArrayList<Add>? = null
    var Loading:ImageView? = null
    var progressOverlay: FrameLayout? = null

    var filesupload = java.util.ArrayList<FileUpload>()
    lateinit var fileupload_adapter : FileUploadAdapter
    var lang : String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments

        Quotelist = args?.getParcelableArrayList<Add>("data")
        project_id = args?.getInt("project_id")
        post_date = args?.getString("post_date")
        fixed_items = args?.getParcelableArrayList<Add>("fixed_items")
        try{if (fixed_items!!.size==0) fixed_items = null}
        catch (e: Exception){}
        header = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_quote_submission, container, false)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        /////////////////////Initiate_loading_anim///////////////////////////

        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "")
//        val tabLayout = root.findViewById(R.id.tabLayout) as TabLayout
//
//        tabLayout.addTab(tabLayout.newTab().setText("Quote Submission"))

        val project_submitDate = root.findViewById<TextView>(R.id.project_submitDate)
        var format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        try {
            val newDate = format.parse(post_date)
            format = SimpleDateFormat("yyyy-MM-dd")
            val date = format.format(newDate)
            project_submitDate.text = date
        } catch (e: ParseException) {
            e.printStackTrace()
            project_submitDate.text = post_date
        }


        val project_detail_title = root.findViewById<TextView>(R.id.project_detail_title)
        project_detail_title.text = context!!.getString(R.string.project_title).plus(" #").plus(
            project_id.toString()
        )

        val submission_listview = root.findViewById<View>(R.id.submission_itemlist) as RecyclerView

        //Never add item before and no fixed items
        if (Quotelist == null && fixed_items == null) {
            Quotelist = ArrayList<Add>()
            try {if(fixed_items!!.size==0) {

                Quotelist!!.add(Add(0, context!!.getString(R.string.total), 0F, 0F, ""))
                Quotelist!!.add(
                    Add(
                        0,
                        context!!.getString(R.string.Create_bid_Earliest_Availability_with_star),
                        0F,
                        0F,
                        ""
                    )
                )
            }
            }
            catch (e: Exception){
                Log.d("fixed_items_error", e.toString())
                Quotelist!!.add(Add(0, context!!.getString(R.string.total), 0F, 0F, ""))
                Quotelist!!.add(
                    Add(
                        0,
                        context!!.getString(R.string.Create_bid_Earliest_Availability_with_star),
                        0F,
                        0F,
                        ""
                    )
                )
            }
        }

        //Never add item before but hv fixed items
        else if (fixed_items!=null){
            if(Quotelist == null && fixed_items!!.size>0){
                Quotelist = ArrayList<Add>()
                for (i in 0 until fixed_items!!.size) {
                    Quotelist!!.add(
                        Add(
                            fixed_items!!.get(i).id,
                            fixed_items!!.get(i).description,
                            0F,
                            0F,
                            ""
                        )
                    )
                }
                Quotelist!!.add(Add(0, context!!.getString(R.string.total), 0F, 0F, ""))
                Quotelist!!.add(
                    Add(
                        0,
                        context!!.getString(R.string.Create_bid_Earliest_Availability_with_star),
                        0F,
                        0F,
                        ""
                    )
                )
            }
        }
        //Add item before
        else{
            var total_min = 0.00
            var total_max = 0.00

            for (index in 0 until Quotelist!!.size-2) {
                total_min += Quotelist!!.get(index).min_amount.toDouble()
                total_max += Quotelist!!.get(index).max_amount.toDouble()
            }
            Quotelist!!.get(Quotelist!!.size - 2).min_amount= total_min.toFloat()
            Quotelist!!.get(Quotelist!!.size - 2).max_amount= total_max.toFloat()
        }
//        quote.add(QuoteSubmission("Item A details", "HK$ 10,000", "HKS 30,000",""))
//        quote.add(QuoteSubmission("Item B details", "HK$ 10,000", "HKS 30,000",""))
//        quote.add(QuoteSubmission("Item C details", "HK$ 10,000", "HKS 30,000",""))
//        quote.add(QuoteSubmission("Item D details", "HK$ 10,000", "HKS 30,000",""))
//        quote.add(QuoteSubmission(title.toString(), min_amount.toString(), max_amount.toString(),""))

        if (Quotelist!=null)
        Log.d("llallalalla", Quotelist?.get(0)?.description)

        if (Quotelist !=null) {
            val submission_adapter = QuoteSubmissionAdapter(
                Quotelist!!,
                Quotelist!!.size,
                lang!!,
                project_id!!,
                post_date!!
            )
            submission_listview.adapter = submission_adapter
            submission_listview.layoutManager = LinearLayoutManager(
                activity,
                RecyclerView.VERTICAL,
                false
            )
        }
        val submission_adapter = Quotelist?.let { QuoteSubmissionAdapter(
            it,
            Quotelist!!.size,
            lang!!,
            project_id!!,
            post_date!!
        ) }
        submission_listview.adapter = submission_adapter
        submission_listview.layoutManager = LinearLayoutManager(
            activity,
            RecyclerView.VERTICAL,
            false
        )


        val item_add_icon = root.findViewById(R.id.Add_Item) as TextView
//        item_add_icon.setPaintFlags(item_add_icon.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        item_add_icon.setOnClickListener {
            val AddItemFragment: Fragment = AddItemFragment()
            val bundle = Bundle()
            bundle.putParcelableArrayList("data", Quotelist)
            bundle.putInt("project_id", project_id!!)
            bundle.putString("post_date", post_date!!)
            AddItemFragment.setArguments(bundle)

            var fr = getFragmentManager()?.beginTransaction()
            fr?.addToBackStack(null)
            fr?.add(R.id.nav_host_fragment, AddItemFragment)
            fr?.commit()
        }

        val submit_button = root.findViewById(R.id.submit_button) as Button
        submit_button.setOnClickListener {
            var result = ""


            Loading!!.visibility=View.VISIBLE
            progressOverlay!!.visibility=View.VISIBLE
            Thread(Runnable {
                if (!Quotelist!!.get(Quotelist!!.size - 1).remarks.equals("")) {
                    var file: File? = null
                    var file_format = ""
                    try {
                        file = filesupload.get(0).fileUri
                        if (filesupload.get(0).title.contains(".pdf")) {
                            file_format = "application/pdf"
                        } else if (filesupload.get(0).title.contains(".png")) {
                            file_format = "image/png"
                        } else if (filesupload.get(0).title.contains(".jpeg")) {
                            file_format = "image/jpeg"
                        } else if (filesupload.get(0).title.contains(".jpg")) {
                            file_format = "image/jpeg"
                            file_format = ".jpg"
                        } else if (filesupload.get(0).title.contains(".doc")) {
                            file_format = "application/msword"
                        } else if (filesupload.get(0).title.contains(".docx")) {
                            file_format =
                                "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                        } else if (filesupload.get(0).title.contains(".xlsx")) {
                            file_format =
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        }
                    } catch (e: Exception) {
                    }
                    result = HttpsService.SubmitQuote(
                        context!!,
                        "https://uat.jper.com.hk/api/company/submit/quote",
                        project_id,
                        Quotelist,
                        Quotelist!!.get(Quotelist!!.size - 1).remarks,
                        file,
                        file_format,
                        header,
                        lang
                    )
                    activity!!.runOnUiThread(java.lang.Runnable {


                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        if (!result.equals("")) {
                            val dialog = Dialog(activity!!)
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.setCancelable(false)
                            dialog.setContentView(R.layout.dialog_message)

                            val dialog_message =
                                dialog.findViewById(R.id.dialog_message) as TextView
                            dialog_message.text = result
                            val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

                            val close_button = dialog.findViewById(R.id.close_button) as ImageView
                            confirm_button.setOnClickListener {
                                dialog.dismiss()
                                val intent = Intent(activity, MainActivity::class.java)
                                if (result.equals(context!!.getString(R.string.Success_msg))) {
                                    context!!.startActivity(intent)
                                    activity!!.finish()
                                }
                            }
                            close_button.setOnClickListener {
                                dialog.dismiss()
                                val intent = Intent(activity, MainActivity::class.java)
                                if (result.equals(context!!.getString(R.string.Success_msg))) {
                                    context!!.startActivity(intent)
                                    activity!!.finish()
                                }
                            }

                            dialog.setCancelable(true)
                            dialog.show()
                        }
                    })

                } else {
                    activity!!.runOnUiThread(java.lang.Runnable {


                        Loading!!.visibility = View.GONE
                        progressOverlay!!.visibility = View.GONE
                        showDialog(activity!!, getString(R.string.General_all_info))
                    })

                }

            }).start()

            //sendinfo(Quotelist)
            //Log.d("submit list",Quotelist.toString())
        }

        val back_button = root.findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {
           activity!!.onBackPressed()
        }
//        val project_detail_status = root.findViewById(R.id.project_detail_status) as TextView
//        val drawable = project_detail_status.getBackground() as GradientDrawable
//        drawable.setColor(Color.rgb(118,203,196))
        val fileupload_listview = root.findViewById<View>(R.id.fileupload_listview) as RecyclerView

        filesupload = java.util.ArrayList<FileUpload>()

        fileupload_adapter = FileUploadAdapter(filesupload)
        fileupload_listview.adapter = fileupload_adapter
        fileupload_listview.layoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )

        val attach_button = root.findViewById<View>(R.id.attach_button) as TextView
        attach_button.setPaintFlags(attach_button.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        attach_button.setOnClickListener {
            showFileChooser()
        }
        return root
    }



//    fun onBackPressed() {
//        if (supportFragmentManager.backStackEntryCount != 0) {
//            supportFragmentManager.popBackStack()
//        } else {
//            super.onBackPressed()
//        }
//    }

    fun sendinfo(
        arrayList: ArrayList<Add>?
    ) {
        try {

            val gson = Gson()
            val data = gson.toJson(arrayList)
            val jsonArray = JsonParser().parse(data).asJsonArray

            Log.d("result", jsonArray.toString())
            //array.add(arrayList)
            //val jsArray = JSONArray(arrayList)
            ////////////////////////Send_data_to_server//////////////////////////
            val json = JsonObject()
            json.addProperty("project_id", project_id)
            json.addProperty("add", jsonArray.toString())
            Ion.with(context)
                .load("https://uat.jper.com.hk/api/company/submit/quote")
                .setHeader("Authorization", "Bearer " + header)
                .setHeader("Content-Language", lang)
//                .setMultipartFile("icon", "image/jpeg,png", profile_image_file)
//                .setMultipartParameter("username",  URLEncoder.encode(username.toString(), "UTF-8"))
//                .setMultipartParameter("phone", phone.toString())
//                .setJsonArrayBody(jsonArray)
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback { e, result ->
                    if (e != null)
                        Log.d("result_error1", e.toString())
                    if (result["result"].asString.equals("success")) {

                       // Log.d("result_updated", "result_updated")
                       // showDialog(this, "成功")
                        // Log.d("result_update",result["result"].asString)
                    }
                    else if (result["result"].asString.equals("error")) {

                        Log.d("result_okok", result["msg"].asString)
                      //  showDialog(this, result["msg"].asString)
                    }
                    Log.d("result_okok2", result["msg"].asString)
                }
        } catch (x: Exception) {
            Log.d("result_error3", x.toString())
        }
    }


    private fun showDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    fun showFileChooser() {

        val intent = Intent(Intent.ACTION_GET_CONTENT)

        // Update with mime types
        intent.type = "*/*"
        val mimetypes: Array<String> = arrayOf(
            "image/jpeg",
            "image/png",
            "application/pdf",
            "application/msword",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        )
        // Update with additional mime types here using a String[].
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)

        // Only pick openable and local files. Theoretically we could pull files from google drive
        // or other applications that have networked files, but that's unnecessary for this example.
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        intent.setAction(Intent.ACTION_GET_CONTENT)
        // REQUEST_CODE = <some-integer>
        startActivityForResult(intent, 0x001)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != 0x001 || resultCode != AppCompatActivity.RESULT_OK) {
            return
        }
        val fileUri =data?.data
        try{
            val file: File = FileUtil.from(context, fileUri)
            importFile(fileUri!!, file)
        }
        catch (x: Exception){
            Log.d("error", x.toString())
        }

//        val file = File(real_uri)

        //val file = File(fileUri?.path)
        // Import the file

//        var docPaths = ArrayList<Any>()

//        if (fileUri != null) {
//                importFile(fileUri,File(fileUri.toString()))
//                Log.d("fileUri",fileUri.toString())
//
//        }
    }
    fun importFile(fileUri: Uri, file: File) {

        //val selectedFilePath: String? = fileUri.path
        val fileName : String? = getFileName(fileUri)
        filesupload.clear()
        filesupload.add(FileUpload(fileName!!, file))
        fileupload_adapter.refreshDataset()
        // The temp file could be whatever you want

        Log.d("uploaded", fileName.toString())
        Log.d("uploaded_file", fileUri.path?.toString())
        //val fileCopy = copyToTempFile(fileUri, tempFile )

        // Done!
    }

    fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor: Cursor? = context!!.contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                if (cursor != null) {
                    cursor.close()
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result!!.substring(cut + 1)
            }
        }
        return result
    }
}
