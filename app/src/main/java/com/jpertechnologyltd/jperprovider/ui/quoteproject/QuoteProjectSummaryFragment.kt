package com.jpertechnologyltd.jperprovider.ui.quoteproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.FileDownloadAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.Add
import com.jpertechnologyltd.jperprovider.item.FileDownload
import com.jpertechnologyltd.jperprovider.item.FixedItem
import com.jpertechnologyltd.jperprovider.item.ProjectCategory
import com.stfalcon.imageviewer.StfalconImageViewer
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class QuoteProjectSummaryFragment : Fragment() {
    var lang:String?="en"
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    private var header : String? = null
    var project_id :Int? = null
    var post_date :String? = null
    var project_image_value :String? = null
    var filesdownload = ArrayList<FileDownload>()
    var fixed_items_array = ArrayList<Add>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        header = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        val args = arguments
        project_id = args?.getInt("project_id")
        post_date = args?.getString("post_date")
        project_image_value = args?.getString("project_image")
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_quoted_project_summary, container, false)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE

        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        val config = resources.configuration
        val locale = Locale(lang)
        Locale.setDefault(locale)
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)

        val projects_district_value = root.findViewById<TextView>(R.id.projects_district_value)
        val project_type_space = root.findViewById<TextView>(R.id.project_type_space)
        val detail_budget_max = root.findViewById<TextView>(R.id.project_budget_value)
        val detail_budget_icon = root.findViewById<ImageView>(R.id.project_budget_icon)
        val project_area = root.findViewById<TextView>(R.id.project_area)
        val project_style = root.findViewById<TextView>(R.id.project_style)
        val project_timeframe = root.findViewById<TextView>(R.id.project_timeframe)
//        val project_address = root.findViewById<TextView>(R.id.project_address)
        val project_desc = root.findViewById<TextView>(R.id.project_desc)
        val project_style_ref = root.findViewById<TextView>(R.id.project_style_ref)
        val project_image = root.findViewById<ImageView>(R.id.project_image)
        val project_type = root.findViewById<TextView>(R.id.project_type)
        val chipgroup = root.findViewById<ChipGroup>(R.id.chipgroup)
        var project_category_array = ArrayList<String>()
        var filedownload_listview = root.findViewById<View>(R.id.attach_listview) as RecyclerView


        Thread(Runnable {
            val QuoteProjectSummary = HttpsService.GetProjectDetail(context,"https://uat.jper.com.hk/api/company/available/project/".plus(project_id),header,lang)

            activity?.runOnUiThread(java.lang.Runnable {
                if (QuoteProjectSummary != null) {
                    if(QuoteProjectSummary.docs_array !=null) {
                        if(QuoteProjectSummary.docs_array.size>0) {
                            filesdownload = ArrayList()
                            filesdownload.addAll(QuoteProjectSummary.docs_array)
                            val filedownload_adapter = FileDownloadAdapter(filesdownload)
                            filedownload_listview.adapter = filedownload_adapter
                            filedownload_listview.layoutManager =
                                LinearLayoutManager(context, RecyclerView.VERTICAL, false)

                        }

                    }

                    if(QuoteProjectSummary.fixed_items_array !=null) {
                        if(QuoteProjectSummary.fixed_items_array.size>0) {
                            fixed_items_array = QuoteProjectSummary.fixed_items_array
                        }
                    }
                    projects_district_value.text = QuoteProjectSummary.district
                    if(!QuoteProjectSummary.budget_max.equals(""))detail_budget_max.text ="HK$ ".plus(
                        NumberFormat.getNumberInstance(Locale.US).format(QuoteProjectSummary.budget_max.toInt()))
                    else {

                    }
                    project_type.text = QuoteProjectSummary.type
                    project_area.text = QuoteProjectSummary.area.plus(" ").plus(context!!.getString(R.string.sqft))
                    if (QuoteProjectSummary.area.equals("")){
                        project_type_space.visibility = View.GONE
                        project_area.visibility = View.GONE
                    }
                    project_style.text= QuoteProjectSummary.scope
                    if (lang.equals("en")) {
                        project_style_ref.text = QuoteProjectSummary.style_name_en
                    }
                    else{
                        project_style_ref.text = QuoteProjectSummary.style_name_zh
                    }
                    project_timeframe.text = QuoteProjectSummary.start_at?.plus(" - ")?.plus(QuoteProjectSummary.completed_at)
                    if(QuoteProjectSummary.address.equals("")){
//                        project_address.visibility = View.GONE
//                        project_address_icon.visibility = View.GONE
                    }
//                    project_address.text = QuoteProjectSummary.address

                    if(QuoteProjectSummary.note_array.size>0){
                        var desc_notes : String =""
                        for(index in QuoteProjectSummary.note_array.indices) {
                            desc_notes =desc_notes.plus(" • ").plus(QuoteProjectSummary.note_array.get(index)).plus("\n")
                        }
                        if(!QuoteProjectSummary.description.equals(""))
                            project_desc.text = " • " .plus(QuoteProjectSummary.description).plus("\n").plus(desc_notes)
                        else  project_desc.text = desc_notes
                    }
                    else{
                        project_desc.text = " • " .plus(QuoteProjectSummary.description)
                    }

                    try {

                        val glideUrl = GlideUrl(
                            project_image_value,
                            LazyHeaders.Builder()
                                .addHeader("Authorization", "Bearer ".plus(header))
                                .build()
                        )

                        Glide.with(context!!).load(glideUrl).placeholder(R.drawable.project_image_placeholder)
                            .into(project_image)

                        project_image.setOnClickListener{
                            //ToDO
                            val listimages =
                                arrayOf<String?>("https://uat.jper.com.hk/style/image/7","https://uat.jper.com.hk/style/image/7")
                            val profileimage =
                                arrayOf<String?>(glideUrl.toString())
                            StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->
                                Glide.with(this).load(image).into(view)
                                //Picasso.get().load(image).into(view)
                            }.show()

                        }
                    }
                    catch (e:Exception){

                        Glide.with(context!!).load(R.drawable.project_image_placeholder)
                            .into(project_image)
                    }



                    if(QuoteProjectSummary.category_array.size>0){
                        for (index in QuoteProjectSummary.category_array.indices) {
                            project_category_array.add(ProjectCategory( context!!,QuoteProjectSummary.category_array[index]).name)
                        }

                        for (index in project_category_array.indices) {
                            val chip = Chip(chipgroup.context)
                            chip.text= project_category_array.get(index)
                            chipgroup.addView(chip)
                        }
                    }
                }

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()


        val bid_button = root.findViewById<Button>(R.id.bid_button)
        bid_button.setOnClickListener{
            val QuoteSubmissionFragment: Fragment = QuoteSubmissionFragment()
            val bundle = Bundle()
            bundle.putInt("project_id", project_id!!)
            bundle.putString("post_date", post_date!!)
            bundle.putParcelableArrayList("fixed_items", fixed_items_array)

            bundle.putString("project_image", project_image_value!!)

            QuoteSubmissionFragment.setArguments(bundle)
            (parentFragment?.activity as MainActivity?)?.supportFragmentManager?.beginTransaction()
                ?.addToBackStack(null)
                ?.add(R.id.nav_host_fragment, QuoteSubmissionFragment)
                ?.commit()
        }

//
//        val QuoteProjectSummary = HttpsService.GetProjectDetail(context,"https://uat.jper.com.hk/api/company/project/".plus(project_id),header)
//
//        val projects_district_value = root.findViewById<TextView>(R.id.projects_district_value)
//        val detail_budget_max = root.findViewById<TextView>(R.id.project_budget_value)
//        val project_area = root.findViewById<TextView>(R.id.project_type)
//        val project_style = root.findViewById<TextView>(R.id.project_style)
//        val project_timeframe = root.findViewById<TextView>(R.id.project_timeframe)
//        val project_address = root.findViewById<TextView>(R.id.project_address)
//        val project_desc = root.findViewById<TextView>(R.id.project_desc)
//        val project_image = root.findViewById<ImageView>(R.id.project_image)
//        Log.d("summary1","outside")
//
//        if (QuoteProjectSummary != null) {
//            projects_district_value.setText(QuoteProjectSummary.district)
//            detail_budget_max.setText(QuoteProjectSummary.budget_max)
//            project_area.setText(QuoteProjectSummary.area)
//            project_style.setText(QuoteProjectSummary.style_id)
//            project_timeframe.setText(QuoteProjectSummary.start_at?.plus("-")?.plus(QuoteProjectSummary.completed_at))
//            project_address.setText(QuoteProjectSummary.address)
//            project_desc.setText(QuoteProjectSummary.description)
//            Log.d("summary",QuoteProjectSummary.description.toString())
//            Log.d("summary1","inside")
//
//            //Glide.with(context!!).load(QuoteProjectSummary.url).into(project_image)
//        }

        return root

    }


}