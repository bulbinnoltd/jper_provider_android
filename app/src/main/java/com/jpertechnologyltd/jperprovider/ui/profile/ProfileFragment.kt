package com.jpertechnologyltd.jperprovider.ui.profile

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.jpertechnologyltd.jperprovider.FirstPageActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.SettingAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.*
import com.stfalcon.imageviewer.StfalconImageViewer
import de.hdodenhof.circleimageview.CircleImageView

class ProfileFragment : Fragment() {

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var staffprofile : Profile? =null
    var companyfile : CompanyBriefProfile? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // Handle the back button event
            }
        }
        requireActivity().getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE

        val header = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")

        val lang = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        val device_id = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("device_id", "")
        val setting_listview = root.findViewById<View>(R.id.setting_listview) as RecyclerView

        val profile_username = root.findViewById<TextView>(R.id.profile_username)
        val profile_phone = root.findViewById<TextView>(R.id.profile_phone)
        val profile_email = root.findViewById<TextView>(R.id.profile_email)
        val company_address = root.findViewById<TextView>(R.id.company_address)
        val rating_value = root.findViewById<TextView>(R.id.rating_value)
        val profile_image = root.findViewById<CircleImageView>(R.id.profile_image)
        val provider_profile_button = root.findViewById<ImageView>(R.id.provider_profile_button2)
        val provider_profile_button3 = root.findViewById<ImageView>(R.id.provider_profile_button3)


        val profile_title = root.findViewById<TextView>(R.id.vendor_title)
        val rating = root.findViewById<RatingBar>(R.id.rating)
        //val rating_value = root.findViewById<TextView>(R.id.rating_value)
        val company_profile_image = root.findViewById<CircleImageView>(R.id.vendor_image)

        Thread(Runnable {

            staffprofile = HttpsService.GetUserProfile(context,"https://uat.jper.com.hk/api/company/staff/profile",header,lang)
            companyfile = HttpsService.GetCompanyInfo(context,"https://uat.jper.com.hk/api/company/profile",header,lang)

            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {

                if (staffprofile != null) {
                    profile_username.setText(staffprofile!!.username)
                    profile_phone.setText(staffprofile!!.phone)
                    profile_email.setText(staffprofile!!.email)
                    val glideUrl = GlideUrl(
                        staffprofile?.icon,
                        LazyHeaders.Builder()
                            .addHeader("Authorization", "Bearer ".plus(header))
                            .build()
                    )
                    Glide.with(context!!).load(glideUrl)
                        .placeholder(R.drawable.profile_pic_placeholder).into(profile_image)

                }

                if(companyfile!=null){
                    if(lang.equals("zh"))
                        profile_title.text= companyfile!!.company_name_zh
                    else
                        profile_title.text= companyfile!!.company_name_en
                    rating.rating= companyfile!!.avg_rate!!
                    rating_value.text="(".plus(String.format("%.1f", companyfile!!.avg_rate)).plus(")")
                    val district =  District(context!!,companyfile!!.district_id!!).name
//                    company_address.text= companyfile!!.company_address!!
                    company_address.text=  district
                    val glideUrl = GlideUrl(
                        companyfile?.logo,
                        LazyHeaders.Builder()
                            .addHeader("Authorization", "Bearer ".plus(header))
                            .build()
                    )
                    Glide.with(context!!).load(glideUrl)
                        .placeholder(R.drawable.profile_pic_placeholder).into(company_profile_image)

                    provider_profile_button.setOnClickListener{
                        val bundle = Bundle()
                        bundle.putInt("company_id", companyfile!!.id!!)
                        bundle.putString("company_name_en",companyfile!!.company_name_en!!)
                        bundle.putString("company_name_zh", companyfile!!.company_name_zh!!)
                        bundle.putString("company_logo", companyfile!!.logo)
                        bundle.putFloat("company_avg_score", companyfile!!.avg_rate!!)
                        bundle.putInt("company_district_id", companyfile!!.district_id!!)

                        val ProviderProfileFragment= ProviderProfileFragment()
                        ProviderProfileFragment.setArguments(bundle)
                        var fr = getFragmentManager()?.beginTransaction()
                        fr?.addToBackStack(null)
                        fr?.add(R.id.nav_host_fragment, ProviderProfileFragment)
                        fr?.commit()
                    }
                    provider_profile_button3.setOnClickListener{
                        val bundle = Bundle()
                        bundle.putInt("company_id", companyfile!!.id!!)
                        bundle.putString("company_name_en",companyfile!!.company_name_en!!)
                        bundle.putString("company_name_zh", companyfile!!.company_name_zh!!)
                        bundle.putString("company_logo", companyfile!!.logo)
                        bundle.putFloat("company_avg_score", companyfile!!.avg_rate!!)
                        bundle.putInt("company_district_id", companyfile!!.district_id!!)

                        val ProviderProfileFragment= ProviderProfileFragment()
                        ProviderProfileFragment.setArguments(bundle)
                        var fr = getFragmentManager()?.beginTransaction()
                        fr?.addToBackStack(null)
                        fr?.add(R.id.nav_host_fragment, ProviderProfileFragment)
                        fr?.commit()
                    }
                    //Glide.with(context!!).load(company_profile_image.icon).into(profile_image)
                }
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()

        profile_image.setOnClickListener{
//            val intent = Intent(context, MultiPhotoViewerActivity::class.java)
//            intent.putExtra("photo_url",profile?.icon)
//            startActivity(intent)
            val listimages =
                arrayOf<String?>("https://uat.jper.com.hk/style/image/7","https://uat.jper.com.hk/style/image/7")

            val profileimage =
                arrayOf<String?>(staffprofile?.icon)
            Log.d("profile?.icon",staffprofile?.icon)
            if(!staffprofile?.icon.equals("") && !staffprofile?.icon.equals("https://uat.jper.com.hk/user/icon")) {

                StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->

                    Glide.with(context!!).load(image).into(view)

                }.show()
            }
            else{

                StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->

                    Glide.with(context!!).load(R.drawable.profile_pic_placeholder).into(view)

                }.show()
            }
        }
        company_profile_image.setOnClickListener{

            val profileimage =
                arrayOf<String?>(companyfile?.logo)
            Log.d("profile?.icon",companyfile?.logo)
            if(!companyfile?.logo.equals("") && !companyfile?.logo.equals("https://uat.jper.com.hk/user/icon")) {

                StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->

                    Glide.with(context!!).load(image).into(view)

                }.show()
            }
            else{

                StfalconImageViewer.Builder<String>(context, profileimage) { view, image ->

                    Glide.with(context!!).load(R.drawable.profile_pic_placeholder).into(view)

                }.show()
            }
        }
        val Profile_edit: String = getString(R.string.Profile_edit)
        val Profile_changepw: String = getString(R.string.Profile_changepw)
        val Profile_lang: String = getString(R.string.Profile_lang)
        val settings = ArrayList<Setting>()
        settings.add(Setting(Profile_edit, "Quotation Received","profile_edit_icon"))
        settings.add(Setting(Profile_changepw, "Quotation Received","profile_change_pw_icon"))
        settings.add(Setting(Profile_lang, "Quotation Received","profile_lang_icon"))
        val setting_adapter = SettingAdapter(settings,this)
        setting_listview.adapter = setting_adapter
        setting_listview.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        val more_listview = root.findViewById<View>(R.id.more_listview) as RecyclerView

        val Profile_notification: String = getString(R.string.Profile_notification)
        val Profile_center: String = getString(R.string.Profile_center)
        val Profile_terms: String = getString(R.string.Profile_terms)
        val Profile_privacy: String = getString(R.string.Profile_privacy)
        val Profile_disbursement : String = getString(R.string.Profile_disbursement)

        val mores = ArrayList<Setting>()
        mores.add(Setting(Profile_notification, "Quotation Received","navbar_activity"))
        mores.add(Setting(Profile_center, "https://uat.jper.com.hk/guide/provider","profile_help_icon"))
        mores.add(Setting(Profile_terms, "https://uat.jper.com.hk/terms","profile_terms_icon"))
        mores.add(Setting(Profile_privacy, "https://uat.jper.com.hk/privacy","profile_privacy_icon"))
        mores.add(Setting(Profile_disbursement, "Quotation Received","profile_disbursement_icon"))

        val more_adapter = SettingAdapter(mores,this)
        more_listview.adapter = more_adapter
        more_listview.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        val logout_button = root.findViewById<Button>(R.id.logout_button)
        logout_button.setOnClickListener {
            if(HttpsService.Logout(context,"https://uat.jper.com.hk/api/company/logout",header,lang,device_id)){
                val editor = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                    ?.edit()
                editor?.putString("user_token", "")
                editor?.apply()

                val intent = Intent (context, FirstPageActivity::class.java).also{
                    startActivity(it)
                    activity?.finish()

                }
            }
        }
        return root
    }
    fun getProfie(): Profile? {
        return staffprofile
    }
}