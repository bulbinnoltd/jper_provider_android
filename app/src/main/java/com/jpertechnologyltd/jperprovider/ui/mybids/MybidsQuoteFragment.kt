package com.jpertechnologyltd.jperprovider.ui.mybids

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.github.drjacky.imagepicker.ImagePicker
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.WebviewActivity
import com.jpertechnologyltd.jperprovider.adapter.FileUploadAdapter
import com.jpertechnologyltd.jperprovider.adapter.MyBiddingAdapter
import com.jpertechnologyltd.jperprovider.adapter.ViewQuoteAdapter
import com.jpertechnologyltd.jperprovider.component.FileUtil
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.item.Add
import com.jpertechnologyltd.jperprovider.item.FileUpload
import com.jpertechnologyltd.jperprovider.item.QuotationDetail
import com.jpertechnologyltd.jperprovider.item.QuoteStatus
import java.io.File
import java.text.ParseException
import java.text.SimpleDateFormat


class MybidsQuoteFragment : Fragment() {
    var quote_result:QuotationDetail?=null
    var Quotelist : ArrayList<Add>? =null
    var title :String? = null
    private var header : String? = null
    var min_amount :String? = null
    var max_amount :String? = null
    var post_date :String? = null
    var project_id :Int? = null
    var quote_id :Int? = null
    var status_id :Int? = null
    var from_server :Boolean? = false


    var filesupload = java.util.ArrayList<FileUpload>()
    lateinit var fileupload_adapter : FileUploadAdapter
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var lang : String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments


        project_id = args?.getInt("project_id")
        post_date = args?.getString("post_date")
        quote_id = args?.getInt("quote_id")
        status_id = args?.getInt("status_id")
        from_server = args?.getBoolean("from_summary", false)

        Log.d("quote_id", quote_id.toString())
        header = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_mybid_quote, container, false)

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////
        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "")
//        val tabLayout = root.findViewById(R.id.tabLayout) as TabLayout
//
//        tabLayout.addTab(tabLayout.newTab().setText("Quote Submission"))
        val project_detail_status = root.findViewById(R.id.project_detail_status) as TextView
        val download_file_title = root.findViewById(R.id.download_file_title) as TextView
        val download_file_value = root.findViewById(R.id.download_file_value) as TextView
        val paper_clip_icon = root.findViewById(R.id.paper_clip_icon) as ImageView
        val review_buttonView = root.findViewById(R.id.review_buttonView) as LinearLayout
        val buttonview = root.findViewById(R.id.buttonview) as View

        val project_submitDate = root.findViewById<TextView>(R.id.project_submitDate)



        val project_detail_title = root.findViewById<TextView>(R.id.project_detail_title)
        project_detail_title.text = context!!.getString(R.string.project_title).plus(" #").plus(
            project_id.toString()
        )

        val submission_listview = root.findViewById<View>(R.id.submission_itemlist) as RecyclerView

        //Never add item before
        Log.d("proj_quote_id",quote_id.toString())

        Thread(Runnable {
            quote_result = HttpsService.GetQuoteDetail(
                context!!,
                "https://uat.jper.com.hk/api/company/quote/".plus(quote_id),
                header,
                lang
            )
            if (quote_result != null) {
                Quotelist = ArrayList<Add>()
                for (index in 0 until quote_result!!.item.size) {
                    Quotelist!!.add(quote_result!!.item.get(index))
                }
                Quotelist!!.add(Add(0, context!!.getString(R.string.total), quote_result!!.min, quote_result!!.max, ""))
                Quotelist!!.add(
                    Add(
                        0,
                        context!!.getString(R.string.Create_bid_Earliest_Availability),
                        0F,
                        0F,
                        quote_result!!.available_at
                    )
                )
            }

            activity?.runOnUiThread(Runnable {

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
                if (quote_result != null) {
                    project_submitDate.text = quote_result!!.created_at
                    if(!quote_result?.attachment_m.equals("")) {
                        Log.d("attachment_m",quote_result?.attachment_m.toString())
                        download_file_value.setOnClickListener {
                            val intent = Intent(context, WebviewActivity::class.java)
                            intent.putExtra("url", quote_result?.attachment_m)
                            startActivity(intent)
                        }
                    }
                    else{
                        paper_clip_icon.visibility = View.INVISIBLE
                        download_file_value.text = "N/A"
                        download_file_value.setTextColor(ContextCompat.getColor(activity!!,R.color.colorSubTitle))
                    }
                }

                val status = QuoteStatus(context!!, status_id!!).name
                project_detail_status.text = status
                val status_rgb = QuoteStatus(context!!, status_id!!).rgb
                val drawable =
                    project_detail_status.background as GradientDrawable
                drawable.setColor(status_rgb)

                val submission_adapter = Quotelist?.let {
                    ViewQuoteAdapter(
                        it
//                        Quotelist!!.size,
//                        lang!!,
//                        project_id!!,
//                        post_date!!
                    )
                }
                submission_listview.adapter = submission_adapter
                submission_listview.layoutManager = LinearLayoutManager(
                    activity,
                    RecyclerView.VERTICAL,
                    false
                )

            })
        }).start()
        //sendinfo(Quotelist)
        //Log.d("submit list",Quotelist.toString())


        val cancel_button = root.findViewById(R.id.cancel_button) as Button
        if (status_id != 1) cancel_button.visibility = View.GONE

        cancel_button.setOnClickListener {


            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.dialog_confirm)

            val dialog_message = dialog.findViewById(R.id.message) as TextView
            dialog_message.text = getString(R.string.quote_delete_msg)
            val confirm_button = dialog.findViewById(R.id.save_button) as Button

            val close_button = dialog.findViewById(R.id.close_button) as ImageView
            val cancel_button = dialog.findViewById(R.id.cancel_button) as Button
            cancel_button.setOnClickListener {
                dialog.dismiss()
            }
            close_button.visibility = View.INVISIBLE
            confirm_button.setOnClickListener {
                dialog.dismiss()

                var delete_result: String? = null


                var cancel_result = ""
                Thread(Runnable {
                    cancel_result = HttpsService.CancelQuote(
                        context!!,
                        "https://uat.jper.com.hk/api/company/quote/cancel",
                        quote_id,
                        header,
                        lang
                    )
                    activity?.runOnUiThread(Runnable {

                        Loading!!.visibility = View.VISIBLE
                        progressOverlay!!.visibility = View.VISIBLE
                        if (!cancel_result.equals("")) {

                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE

                            val dialog = Dialog(context!!)
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.setCancelable(false)
                            dialog.setContentView(R.layout.dialog_message)

                            val dialog_message =
                                dialog.findViewById(R.id.dialog_message) as TextView
                            dialog_message.text = cancel_result
                            val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

                            val close_button = dialog.findViewById(R.id.close_button) as ImageView
                            close_button.visibility = View.INVISIBLE
                            confirm_button.setOnClickListener {
                                dialog.dismiss()
                                val editor = activity!!.getSharedPreferences(
                                    "MyPreferences",
                                    Context.MODE_PRIVATE
                                ).edit()
                                editor.putBoolean("refresh", true)
                                editor.apply()
                                activity!!.getSupportFragmentManager().popBackStack()
                            }
                            close_button.setOnClickListener {
                                dialog.dismiss()
                                val editor = activity!!.getSharedPreferences(
                                    "MyPreferences",
                                    Context.MODE_PRIVATE
                                ).edit()
                                editor.putBoolean("refresh_bids", true)
                                editor.apply()
                                activity!!.getSupportFragmentManager().popBackStack()

                            }

                            dialog.setCancelable(true)
                            dialog.show()
                        }
                    })
                }).start()
            }
            dialog.setCancelable(true)
            dialog.show()
        }








                //sendinfo(Quotelist)
            //Log.d("submit list",Quotelist.toString())

      val save_button = root.findViewById(R.id.save_button) as Button

        if (status_id == 2) save_button.visibility = View.GONE
        save_button.setOnClickListener {

            var file: File? = null
            var file_format = ""
            try {
                file = filesupload.get(0).fileUri
                if (filesupload.get(0).title.contains(".pdf")) {
                    file_format = "application/pdf"
                } else if (filesupload.get(0).title.contains(".png")) {
                    file_format = "image/png"
                } else if (filesupload.get(0).title.contains(".jpeg")) {
                    file_format = "image/jpeg"
                } else if (filesupload.get(0).title.contains(".jpg")) {
                    file_format = "image/jpeg"
                    file_format = ".jpg"
                } else if (filesupload.get(0).title.contains(".doc")) {
                    file_format = "application/msword"
                } else if (filesupload.get(0).title.contains(".docx")) {
                    file_format =
                        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                } else if (filesupload.get(0).title.contains(".xlsx")) {
                    file_format =
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                }
                var save_result = ""
                Thread(Runnable {
                    save_result = HttpsService.UpdateQuoteAttachment(
                        context!!,
                        "https://uat.jper.com.hk/api/company/quote/update/attachment",
                        quote_id,
                        file,
                        file_format,
                        header,
                        lang
                    )
                    activity?.runOnUiThread(Runnable {

                        Loading!!.visibility = View.VISIBLE
                        progressOverlay!!.visibility = View.VISIBLE
                        if (!save_result.equals("")) {

                            Loading!!.visibility = View.GONE
                            progressOverlay!!.visibility = View.GONE


                            val dialog = Dialog(activity!!)
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

                            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            dialog.setCancelable(false)
                            dialog.setContentView(R.layout.dialog_message)

                            val dialog_message =
                                dialog.findViewById(R.id.dialog_message) as TextView
                            dialog_message.text = save_result
                            val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

                            val close_button = dialog.findViewById(R.id.close_button) as ImageView
                            close_button.visibility = View.INVISIBLE
                            confirm_button.setOnClickListener {
                                dialog.dismiss()
                                val editor = activity!!.getSharedPreferences(
                                    "MyPreferences",
                                    Context.MODE_PRIVATE
                                ).edit()
                                editor.putBoolean("refresh", true)
                                editor.apply()
                                activity!!.getSupportFragmentManager().popBackStack()
                            }
                            close_button.setOnClickListener {
                                dialog.dismiss()

                                val editor = activity!!.getSharedPreferences(
                                    "MyPreferences",
                                    Context.MODE_PRIVATE
                                ).edit()
                                editor.putBoolean("refresh", true)
                                editor.apply()
                                activity!!.getSupportFragmentManager().popBackStack()

                            }

                            dialog.setCancelable(true)
                            dialog.show()

                        }
                    })
                }).start()
            } catch (e: Exception) {
                showDialog(activity!!, getString(R.string.General_upload_quote_warning))
            }


                    //sendinfo(Quotelist)
                //Log.d("submit list",Quotelist.toString())
            }

        val back_button = root.findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {
            activity?.onBackPressed()
        }
//        val project_detail_status = root.findViewById(R.id.project_detail_status) as TextView
//        val drawable = project_detail_status.getBackground() as GradientDrawable
//        drawable.setColor(Color.rgb(118,203,196))


        val fileupload_listview = root.findViewById<View>(R.id.fileupload_listview) as RecyclerView

        filesupload = java.util.ArrayList<FileUpload>()

        fileupload_adapter = FileUploadAdapter(filesupload)
        fileupload_listview.adapter = fileupload_adapter
        fileupload_listview.layoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )

        val view = root.findViewById<View>(R.id.buttonview) as View
        val attach_button = root.findViewById<View>(R.id.attach_button) as TextView
        attach_button.setPaintFlags(attach_button.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        attach_button.setOnClickListener {
            showFileChooser()
        }
        if (status_id == 3){
            attach_button.visibility = View.GONE
            save_button.visibility = View.GONE
            cancel_button.visibility = View.GONE
            view.visibility = View.GONE
        }
        if(from_server!!){
            attach_button.visibility = View.GONE
            fileupload_listview.visibility = View.GONE
            review_buttonView.visibility = View.GONE
            buttonview.visibility = View.GONE
        }
        return root
    }



//    fun onBackPressed() {
//        if (supportFragmentManager.backStackEntryCount != 0) {
//            supportFragmentManager.popBackStack()
//        } else {
//            super.onBackPressed()
//        }
//    }


    private fun showDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    fun showFileChooser() {

        val intent = Intent(Intent.ACTION_GET_CONTENT)

        // Update with mime types
        intent.type = "*/*"
        val mimetypes: Array<String> = arrayOf(
            "image/jpeg",
            "image/png",
            "application/pdf",
            "application/msword",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        )
        // Update with additional mime types here using a String[].
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)

        // Only pick openable and local files. Theoretically we could pull files from google drive
        // or other applications that have networked files, but that's unnecessary for this example.
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
        intent.setAction(Intent.ACTION_GET_CONTENT)
        // REQUEST_CODE = <some-integer>
        startActivityForResult(intent, 0x001 + 1)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != (0x001 + 1) || resultCode != AppCompatActivity.RESULT_OK) {
            return
        }
        val fileUri =data?.data
        try{
            val file: File = FileUtil.from(context, fileUri)
            importFile(fileUri!!, file)
        }
        catch (x: Exception){
            Log.d("error", x.toString())
        }

//        val file = File(real_uri)

        //val file = File(fileUri?.path)
        // Import the file

//        var docPaths = ArrayList<Any>()

//        if (fileUri != null) {
//                importFile(fileUri,File(fileUri.toString()))
//                Log.d("fileUri",fileUri.toString())
//
//        }
    }
    fun importFile(fileUri: Uri, file: File) {

        //val selectedFilePath: String? = fileUri.path
        val fileName : String? = getFileName(fileUri)
        filesupload.clear()
        filesupload.add(FileUpload(fileName!!, file))
        fileupload_adapter.refreshDataset()
        // The temp file could be whatever you want

        Log.d("uploaded", fileName.toString())
        Log.d("uploaded_file", fileUri.path?.toString())
        //val fileCopy = copyToTempFile(fileUri, tempFile )

        // Done!
    }

    fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor: Cursor? = context!!.contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                if (cursor != null) {
                    cursor.close()
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result!!.substring(cut + 1)
            }
        }
        return result
    }
}
