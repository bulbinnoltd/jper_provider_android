package com.jpertechnologyltd.jperprovider.ui.profile

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.MainActivity
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.component.HttpsService

class UpdatePasswordActivity : AppCompatActivity() {
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    private var header : String? = null
    var lang : String? = "en"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pw)
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")


        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })


        val old_pw_value = findViewById<TextInputEditText>(R.id.old_pw_value)
        val new_pw_value = findViewById<TextInputEditText>(R.id.new_pw_value)

        val new_re_pw_value = findViewById<TextInputEditText>(R.id.new_re_pw_value)

        val new_pw_submit_button = findViewById<Button>(R.id.new_pw_submit_button)
        // set on-click listener
        new_pw_submit_button.setOnClickListener {
            var new_pw:String?=null
            var new_re_pw:String?=null
            var old_pw:String?=null
            try { old_pw=  old_pw_value.text.toString()}catch (x: Exception) { }
            try { new_pw= new_pw_value.text.toString()}catch (x: Exception) { }
            try { new_re_pw=  new_re_pw_value.text.toString()}catch (x: Exception) { }
            if(old_pw.equals("") ||new_pw.equals("")||new_re_pw.equals("") )
            {
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
                showDialog(this,"error",getString(R.string.Without_info))
            }
            else {
                if (new_pw_value.text.toString().equals(new_re_pw_value.text.toString())) {

                    Loading!!.visibility = View.VISIBLE
                    progressOverlay!!.visibility = View.VISIBLE
                    sendinfo(new_pw_value.text, old_pw_value.text)

                    Loading!!.visibility = View.GONE
                    progressOverlay!!.visibility = View.GONE


                } else
                    showDialog(this, "error", getString(R.string.Update_pw_page_warning))
            }
        }

        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            onBackPressed()
        }
    }

    fun sendinfo(password: Editable?,old_password: Editable? ){

        val msg = HttpsService.UpdatePassword(this,"https://uat.jper.com.hk/api/change/password",password.toString(),old_password.toString(),header,lang)

        showDialog(this,msg[0],msg[1])


    }
    private fun showDialog(activity : Activity,success:String, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
            if(!success.equals("error")){
                val intent = Intent (this, MainActivity::class.java)
                startActivity(intent)
            }
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
}
