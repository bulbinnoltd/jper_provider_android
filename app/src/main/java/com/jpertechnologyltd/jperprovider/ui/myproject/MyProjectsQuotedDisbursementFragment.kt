package com.jpertechnologyltd.jperprovider.ui.myproject

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.jpertechnologyltd.jperprovider.*
import com.jpertechnologyltd.jperprovider.adapter.*
import com.jpertechnologyltd.jperprovider.item.ContractItem
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetDisbursement
import com.jpertechnologyltd.jperprovider.item.Disbursement
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class MyProjectsQuotedDisbursementFragment : Fragment() {
    private lateinit var disbursement_listview: RecyclerView
    private var contractitems: ArrayList<ContractItem>? = null

    var swipeContainer: SwipeRefreshLayout?=null
    private lateinit var no_disbursement_view: ConstraintLayout
    private var header : String? = null

    var project_id : Int? = null
    var status_id : Int? = null
    var scope_id : Int? = null
    var post_date : String? = null
    var contract_sum_value : String? = null

    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null

    var lang:String?= "en"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments
        try {
            project_id = args?.getInt("project_id")
        }
        catch (e: Exception){
            Log.d("error_insum",e.toString())
        }
        try {
            scope_id = args?.getInt("scope_id")
        }
        catch (e: Exception){
            Log.d("error_scope_id",e.toString())
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_fragment_manage_disbursement, container, false)

        no_disbursement_view = root.findViewById(R.id.no_disbursement_view) as ConstraintLayout

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////


        disbursement_listview = root.findViewById<View>(R.id.disbursement_listview) as RecyclerView
        val mLayoutManager = LinearLayoutManager(activity)
        disbursement_listview!!.setLayoutManager(mLayoutManager)

        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")

        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang","en")
        val config = resources.configuration
        val locale = Locale(lang)


        Locale.setDefault(locale)
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)

        val disbursement_listview = root.findViewById<View>(R.id.disbursement_listview) as RecyclerView
        val disbursement_fullscope_subtitle = root.findViewById<View>(R.id.disbursement_fullscope_subtitle) as TextView

        Thread(Runnable {


            val disbursement_list = GetDisbursement(context,"https://uat.jper.com.hk/api/company/project/".plus(project_id.toString()).plus("/disbursement/all"),header,lang)
//            disbursement_list!!.add(
//                Disbursement(
//                    1,2,2,3,"20000.00","2020-09-02","300.00","0.00","400.00","","8000.00","2020-12-12","訂"
//                )
//            )
//            disbursement_list!!.add(
//                Disbursement(
//                    1,1,2,3,"20000.00","2020-09-02","300.00","0.00","400.00","","8000.00","2020-12-12","訂"
//                )
//            )

            activity?.runOnUiThread(java.lang.Runnable {

                val no_disbursement_view = root.findViewById<View>(R.id.no_disbursement_view) as ConstraintLayout


                if (disbursement_list!=null)
                {
                    if(disbursement_list.size>0) {
                        val disbursement_listview_adapter = parentFragment?.let { DisbursementAdapter(disbursement_list, it) }
                        disbursement_listview.adapter = disbursement_listview_adapter
                        disbursement_listview.layoutManager =
                            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                    }
                    else
                        no_disbursement_view.visibility=View.VISIBLE

                }
                Log.d("scope_id",scope_id.toString())
                if(scope_id == 1){
                    disbursement_fullscope_subtitle.visibility = View.VISIBLE
                }
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()

        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                // Refresh items
                try {
                    refreshItems()
                } catch (e: java.lang.Exception) {
                }
            }
        })

        return root
    }
    fun refreshItems() {
        // Load items
        // ...
        val handler = Handler()
        handler.postDelayed({
            try {
                onItemsLoadComplete() // Actions to do after 10 seconds
            } catch (e: java.lang.Exception) {
                swipeContainer!!.setRefreshing(false)
            }
        }, 1000)
        //   activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();

        //    onItemsLoadComplete();
    }
    fun onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        getFragmentManager()!!.beginTransaction().detach(this).commitNowAllowingStateLoss()
        getFragmentManager()!!.beginTransaction().attach(this).commitAllowingStateLoss()        // Stop refresh animation
        swipeContainer!!.setRefreshing(false)
    }
}

