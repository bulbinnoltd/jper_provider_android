package com.jpertechnologyltd.jperprovider.ui.calendar

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.applandeo.materialcalendarview.CalendarView
import com.applandeo.materialcalendarview.EventDay
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener
import com.applandeo.materialcalendarview.listeners.OnDayClickListener
import com.bumptech.glide.Glide
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.jpertechnologyltd.jperprovider.R
import com.jpertechnologyltd.jperprovider.adapter.EventAdapter
import com.jpertechnologyltd.jperprovider.component.HttpsService
import com.jpertechnologyltd.jperprovider.component.HttpsService.GetProjectEvent
import com.jpertechnologyltd.jperprovider.item.Event
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CalendarFragment : Fragment() {
    private var project_id : Int? = 0
    private var header : String? = null
    private var lang : String? = "en"
    private var this_context : Context? = null

    var calendar_event : ArrayList<Event>? = null
    var upcoming_adapter :  EventAdapter? =null
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    var Loading_trans: ImageView? = null
    var progressOverlay_trans: FrameLayout? = null

    var swipeContainer: SwipeRefreshLayout?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = arguments


        project_id = args?.getInt("project_id", 0)

        Log.d("project_id", project_id.toString())

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_calendar, container, false)
        this_context= context
        header = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")

        lang = context!!.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "en")
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = root.findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = root.findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })
        Loading!!.visibility = View.VISIBLE
        progressOverlay!!.visibility = View.VISIBLE
        /////////////////////Initiate_loading_anim///////////////////////////

        /////////////////////Initiate_loading_anim///////////////////////////
        Loading_trans = root.findViewById(R.id.loading_progress_calendar) as ImageView
        progressOverlay_trans = root.findViewById(R.id.progress_overlay_calendar)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading_trans!!.setVisibility(View.GONE)
        progressOverlay_trans!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay_trans!!.setVisibility(View.VISIBLE)
                }
            })

        /////////////////////Initiate_loading_anim///////////////////////////


        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout

        val events: MutableList<EventDay> = ArrayList()

        val calendar: Calendar = Calendar.getInstance()

        val thisMonth = calendar[Calendar.MONTH] + 1
        val thisYear = calendar[Calendar.YEAR]

//        events.add(EventDay(calendar, R.drawable.calendar_event_mark))
        events.add(EventDay(calendar, R.drawable.calendar_today_mark))

        val calendarView: CalendarView =
            root.findViewById(R.id.calendarView) as CalendarView
        calendarView.setEvents(events)
        calendarView.setHighlightedDays(listOf(calendar))

        Thread(Runnable {
            Log.d("thisMonth", thisMonth.toString() + " " + thisYear.toString())
            calendar_event = GetProjectEvent(
                context,
//                "https://uat.jper.com.hk/api/company/project/".plus(project_id).plus("/events/").plus(thisYear).plus("/")
//                    .plus(thisMonth),
                "https://uat.jper.com.hk/api/company/my/event/task/".plus(thisYear).plus("/")
                    .plus(thisMonth),
                header,
                lang
            )
            val upcoming_listview = root.findViewById<View>(R.id.upcoming_listview) as RecyclerView


            if (calendar_event == null) {
                calendar_event = ArrayList<Event>()

            } else {

                if (calendar_event!!.size > 0) {

                    for (i in 0 until calendar_event!!.size) {
                        val originalFormat: DateFormat =
                            SimpleDateFormat("yyyy-MM-dd")

                        val date: Date = originalFormat.parse(
                            calendar_event!!.get(i).fulldate
                        )
                        var event_calendar: Calendar = Calendar.getInstance()
                        event_calendar.time = date
                        events.add(EventDay(event_calendar, R.drawable.calendar_event_mark))

                    }
                }
            }
            // try to touch View of UI thread
            activity?.runOnUiThread(java.lang.Runnable {

                if (calendar_event != null) {
                    calendarView.setEvents(events)
                }
                upcoming_adapter = EventAdapter(
                    calendar_event!!,
                    this,
                    project_id!!,
                    header!!,
                    Loading!!,
                    progressOverlay!!
                )
                upcoming_listview.adapter = upcoming_adapter
                upcoming_listview.layoutManager =
                    LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            })
        }).start()



//        Log.d("currentmonth" ,(calendarView.currentPageDate[MONTH]+1).toString())
//        Log.d("currentyear" ,(calendarView.currentPageDate[YEAR]).toString())

        //val selectedDate = calendarView.firstSelectedDate

//        val today_listview = root.findViewById<View>(R.id.today_listview) as RecyclerView
//
//        val today_events = ArrayList<Event>()
//        today_events.add(Event(1,1,"Task 1", "Project #002", "18", "JUN"))
//
//        val today_adapter = EventAdapter(today_events)
//        today_listview.adapter = today_adapter
//        today_listview.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)



        calendarView.setOnPreviousPageChangeListener(object : OnCalendarPageChangeListener {
            override fun onChange() {

                Loading_trans!!.visibility = View.VISIBLE
                progressOverlay_trans!!.visibility = View.VISIBLE
                Thread(Runnable {

                    val currentmonth = (calendarView.currentPageDate[Calendar.MONTH] + 1).toString()
                    val currentyear = calendarView.currentPageDate[Calendar.YEAR].toString()

                    calendar_event!!.clear()

                    val new_event = GetProjectEvent(
                        context,
//                        "https://uat.jper.com.hk/api/company/project/".plus(project_id).plus("/events/").plus(currentyear).plus("/")
//                            .plus(currentmonth),
                        "https://uat.jper.com.hk/api/company/my/event/task/".plus(currentyear).plus(
                            "/"
                        )
                            .plus(currentmonth),
                        header,
                        lang
                    )
                    if (new_event != null) {
                        if (new_event.size > 0) {
                            calendar_event!!.addAll(new_event)

                            for (i in 0 until calendar_event!!.size) {
                                val originalFormat: DateFormat =
                                    SimpleDateFormat("yyyy-MM-dd")

                                val date: Date = originalFormat.parse(
                                    calendar_event!!.get(i).fulldate
                                )
                                var event_calendar: Calendar = Calendar.getInstance()
                                event_calendar.time = date
                                events.add(EventDay(event_calendar, R.drawable.calendar_event_mark))

                            }
                        }
                    }


                    activity?.runOnUiThread(java.lang.Runnable {
                        if (calendar_event != null) {
                            calendarView.setEvents(events)
                            upcoming_adapter!!.refreshDataset()
                        }

                        Loading_trans!!.visibility = View.GONE
                        progressOverlay_trans!!.visibility = View.GONE
                    })
                }).start()
            }
        })
        calendarView.setOnForwardPageChangeListener(object : OnCalendarPageChangeListener {
            override fun onChange() {

                Loading_trans!!.visibility = View.VISIBLE
                progressOverlay_trans!!.visibility = View.VISIBLE
                Thread(Runnable {

                    val currentmonth = (calendarView.currentPageDate[Calendar.MONTH] + 1).toString()
                    val currentyear = calendarView.currentPageDate[Calendar.YEAR].toString()

                    calendar_event!!.clear()

                    val new_event = GetProjectEvent(
                        context,
//                        "https://uat.jper.com.hk/api/company/project/".plus(project_id).plus("/events/").plus(currentyear).plus("/")
//                            .plus(currentmonth),
                        "https://uat.jper.com.hk/api/company/my/event/task/".plus(currentyear).plus(
                            "/"
                        )
                            .plus(currentmonth),
                        header,
                        lang
                    )
                    if (new_event != null) {
                        if (new_event.size > 0) {
                            calendar_event!!.addAll(new_event)

                            for (i in 0 until calendar_event!!.size) {
                                val originalFormat: DateFormat =
                                    SimpleDateFormat("yyyy-MM-dd")

                                val date: Date = originalFormat.parse(
                                    calendar_event!!.get(i).fulldate
                                )
                                var event_calendar: Calendar = Calendar.getInstance()
                                event_calendar.time = date
                                events.add(EventDay(event_calendar, R.drawable.calendar_event_mark))

                            }
                        }
                    }

                    activity?.runOnUiThread(java.lang.Runnable {
                        if (calendar_event != null) {
                            calendarView.setEvents(events)
                            upcoming_adapter!!.refreshDataset()
                        }

                        Loading_trans!!.visibility = View.GONE
                        progressOverlay_trans!!.visibility = View.GONE
                    })
                }).start()
            }
        })
        calendarView.setOnDayClickListener(object :
            OnDayClickListener {
            override fun onDayClick(eventDay: EventDay) {

                Loading_trans!!.visibility = View.VISIBLE
                progressOverlay_trans!!.visibility = View.VISIBLE
                Thread(Runnable {
                    val clickedDayCalendar = eventDay.calendar
                    val currentmonth = (clickedDayCalendar[Calendar.MONTH] + 1).toString()
                    val currentyear = clickedDayCalendar[Calendar.YEAR].toString()

                    calendar_event!!.clear()

                    val new_event = GetProjectEvent(
                        context,
//                        "https://uat.jper.com.hk/api/company/project/".plus(project_id).plus("/events/").plus(currentyear).plus("/")
//                            .plus(currentmonth),
                        "https://uat.jper.com.hk/api/company/my/event/task/".plus(currentyear).plus(
                            "/"
                        )
                            .plus(currentmonth),
                        header,
                        lang
                    )
                    if (new_event != null) {
                        if (new_event.size > 0) calendar_event!!.addAll(new_event)


                        for (i in 0 until calendar_event!!.size) {
                            val originalFormat: DateFormat =
                                SimpleDateFormat("yyyy-MM-dd")

                            val date: Date = originalFormat.parse(
                                calendar_event!!.get(i).fulldate
                            )
                            var event_calendar: Calendar = Calendar.getInstance()
                            event_calendar.time = date
                            events.add(EventDay(event_calendar, R.drawable.calendar_event_mark))

                        }
                    }
                    activity?.runOnUiThread(java.lang.Runnable {
                        if (calendar_event != null) {
                            calendarView.setEvents(events)

                            upcoming_adapter!!.refreshDataset()
                            calendarView.setDate(clickedDayCalendar)
                        }

                        Loading_trans!!.visibility = View.GONE
                        progressOverlay_trans!!.visibility = View.GONE
                    })
                }).start()
            }
        })
        val event_create_button = root.findViewById<FloatingActionButton>(R.id.event_create_button)
        event_create_button.setOnClickListener{
            //showDialog(project_id!!)
            val dialog = Dialog(activity!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.dialog_calendar_create)
//        val body = dialog.findViewById(R.id.body) as TextView
//        body.text = title
            val close_button = dialog.findViewById(R.id.close_button) as ImageView
//        val noBtn = dialog.findViewById(R.id.noBtn) as TextView
            close_button.setOnClickListener {
                dialog.dismiss()
            }

            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)
            val edit_title = dialog.findViewById(R.id.edit_title) as TextView
            val edit_date = dialog.findViewById(R.id.edit_date) as TextInputEditText
            edit_date.setOnClickListener{
                val dpd = DatePickerDialog(context!!,
                    DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                        edit_date.setText("" + mYear + "-" + (mMonth + 1) + "-" + mDay)
                    }, year, month, day
                )

                dpd.datePicker.minDate = System.currentTimeMillis() -1000
                if(lang.equals("en")) {
                    dpd.setButton(
                        DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.confirm_button),
                        dpd
                    )
                    dpd.setButton(
                        DialogInterface.BUTTON_NEGATIVE,
                        getString(R.string.cancle_button),
                        dpd
                    )
                }
                else{
                    dpd.setButton(
                        DialogInterface.BUTTON_POSITIVE,
                        getString(R.string.confirm_button),
                        dpd
                    )
                    dpd.setButton(
                        DialogInterface.BUTTON_NEGATIVE,
                        getString(R.string.cancle_button),
                        dpd
                    )
                }
                dpd.show()
            }
            val save_button = dialog.findViewById(R.id.save_button) as Button
            save_button.setOnClickListener {
                var result:Boolean? = null
                if(!edit_date.text!!.equals("")&&!edit_title.text.equals("")) {
                    Loading_trans!!.visibility = View.VISIBLE
                    progressOverlay_trans!!.visibility = View.VISIBLE
                    Thread(Runnable {

                        try {
                            result = HttpsService.AddProjectEvent(
                                context,
                                "https://uat.jper.com.hk/api/company/project/event/task/add",
                                header,
                                project_id!!,
                                edit_title.text.toString(),
                                edit_date.text.toString(),
                                lang
                            )

                        } catch (e: Exception) {
                            Log.d("error_add", e.toString())
                        }
                        activity?.runOnUiThread(java.lang.Runnable {
                            if (result != null) {
                                Log.d("success_add", "ok")

                                Loading_trans!!.visibility = View.GONE
                                progressOverlay_trans!!.visibility = View.GONE

                                dialog.dismiss()
                                refreshItems()
                            } else {
                                Loading_trans!!.visibility = View.GONE
                                progressOverlay_trans!!.visibility = View.GONE

                                showWarningDialog(activity!!, "Unknown Error")
                            }
                        })
                    }).start()
                }
                else{
                    showWarningDialog(activity!!, context!!.getString(R.string.Without_info))

                }

            }
            dialog.setCancelable(true)
            dialog.show()

        }
        swipeContainer = root.findViewById(R.id.swipeContainer) as SwipeRefreshLayout
        swipeContainer!!.setColorSchemeResources(R.color.colorJper)
        swipeContainer!!.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                // Refresh items
                try {
                    refreshItems()
                } catch (e: java.lang.Exception) {
                }
            }
        })
        return root
    }
    fun refreshItems() {
        // Load items
        // ...
        val handler = Handler()
        handler.postDelayed({
            try {
                onItemsLoadComplete() // Actions to do after 10 seconds
            } catch (e: java.lang.Exception) {
                swipeContainer!!.setRefreshing(false)
            }
        }, 1000)
        //   activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();

        //    onItemsLoadComplete();
    }
    fun onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
//        activity!!.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit()

        getFragmentManager()!!.beginTransaction().detach(this).commitNowAllowingStateLoss()
        getFragmentManager()!!.beginTransaction().attach(this).commitAllowingStateLoss()
        // Stop refresh animation
        swipeContainer!!.setRefreshing(false)
    }
    private fun showDialog(project_id: Int) {
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_calendar_create)
//        val body = dialog.findViewById(R.id.body) as TextView
//        body.text = title
        val close_button = dialog.findViewById(R.id.close_button) as ImageView
//        val noBtn = dialog.findViewById(R.id.noBtn) as TextView
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val edit_title = dialog.findViewById(R.id.edit_title) as TextView
        val edit_date = dialog.findViewById(R.id.edit_date) as TextInputEditText
        edit_date.setOnClickListener{
            val dpd = DatePickerDialog(context!!,
                DatePickerDialog.OnDateSetListener { _, mYear, mMonth, mDay ->
                    edit_date.setText("" + mYear + "-" + (mMonth + 1) + "-" + mDay)
                }, year, month, day
            )

            dpd.datePicker.minDate = System.currentTimeMillis() -1000
            if(lang.equals("en")) {
                dpd.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.confirm_button),
                    dpd
                )
                dpd.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.cancle_button),
                    dpd
                )
            }
            else{
                dpd.setButton(
                    DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.confirm_button),
                    dpd
                )
                dpd.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.cancle_button),
                    dpd
                )
            }
            dpd.show()
        }
        val save_button = dialog.findViewById(R.id.save_button) as Button
        save_button.setOnClickListener {
            if(HttpsService.AddProjectEvent(
                    context,
                    "https://uat.jper.com.hk/api/company/project/event/task/add",
                    header,
                    project_id,
                    edit_title.text.toString(),
                    edit_date.text.toString(),
                    lang
                )
            )
            {
                Log.d("success_add", "ok")
            }
            dialog.dismiss()
        }
        dialog.setCancelable(true)
        dialog.show()

    }

    private fun showWarningDialog(activity: Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
        }
        close_button.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }

}
