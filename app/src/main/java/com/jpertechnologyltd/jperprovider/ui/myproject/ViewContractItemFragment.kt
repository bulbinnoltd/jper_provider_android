package com.jpertechnologyltd.jperprovider.ui.myproject

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jpertechnologyltd.jperprovider.EditContractItemActivity
import com.jpertechnologyltd.jperprovider.R

class ViewContractItemFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_view_contract_item, container, false)

        val edit_contract_item_button = root.findViewById<FloatingActionButton>(R.id.edit_contract_item_button)
        edit_contract_item_button.setOnClickListener(View.OnClickListener {
            Intent(context, EditContractItemActivity::class.java).also{
                startActivity(it)
            }})

        val back_button = root.findViewById(R.id.back_button) as ImageView

        back_button.setOnClickListener {

            var fr = getFragmentManager()?.beginTransaction()
            fr?.replace(R.id.nav_host_fragment,
                MyProjectsProjectDetailFragment()
            )

            fr?.commit()
        }


        return root
    }
}