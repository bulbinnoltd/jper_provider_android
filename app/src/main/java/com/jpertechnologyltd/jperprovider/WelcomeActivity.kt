package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.jpertechnologyltd.jperprovider.ui.chat.ChatActivity
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
//import com.jper.jperowner.component.HttpsService.CheckTokenExpire
import java.util.*
class WelcomeActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT:Long = 3000 // 1 sec
    var action = 0
    var Loading: ImageView? = null
    var header : String? = null
    var token_expire : Boolean = true
    var lang : String? = null


    var bg_accepted = 0
    var project_id = 0
    var status_id = 0
    var contract_sum = "0"
    var post_date = "N/A"
    var scope_id = 0
    var contract_m = ""
    var contract_name = ""

    //chat msg noti
    var title: String = " "
    var body: String = " "
    var user: String = " "
    var key: String = " "
    var company_name_en = " "
    var company_name_zh = " "
    var company_logo = " "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        if(intent.extras!=null){
            for(key in getIntent().getExtras()!!.keySet()){
                var  value = getIntent().getExtras()!!.getString(key);
                Log.d("extras_key", key + "     " + value)

            }
            action = Integer.valueOf(intent.extras?.get("action").toString())
            if(action ==4 || action==8){
                val project = intent.extras?.get("project").toString()
                val jsonobject = JSONObject(project)

                bg_accepted = jsonobject.getInt("bg_accepted")
                project_id = jsonobject.getInt("id")
                status_id = jsonobject.getInt("status_id")
                contract_sum = "0"
                post_date = jsonobject.getString("start_at")
                scope_id = jsonobject.getInt("scope")
//                contract_m = if(jsonobject.getJSONObject("detail").isNull("contract_name")) "" else jsonobject.getJSONObject(
//                    "detail"
//                ).getString("contract_m")

                contract_m = jsonobject.getJSONObject("detail")["contract_m"].toString()
                if (jsonobject.getJSONObject("detail").isNull("contract_name")) contract_m = ""
                Log.d(
                    "contract_name_in_noti",
                    jsonobject.getJSONObject("detail")["contract_name"].toString()
                )
                Log.d("contract_m_in_noti", contract_m)

                Log.d("extras_key_project_id", project_id.toString())

            }
            else if(action ==5){



                val user = intent.extras?.get("users").toString()
                key = intent.extras?.get("key").toString()
                company_name_en = intent.extras?.get("company_name_en").toString()
                company_name_zh = intent.extras?.get("company_name_zh").toString()

                try {
                    val jsonarray = JSONArray(user)
                    val user_id = getSharedPreferences("MyPreferences", MODE_PRIVATE)
                        .getInt("user_id", 0)
                    for (i in 0 until jsonarray.length()) {
                        if (jsonarray.getJSONObject(i)["user_id"].toString().toInt() != user_id) {
                            company_logo = jsonarray.getJSONObject(i)["icon"].toString()
                            company_name_en = jsonarray.getJSONObject(i)["username"].toString()
                            company_name_zh = jsonarray.getJSONObject(i)["username"].toString()
                        }
                    }
                    Log.d("jsonarray.chatmsg", jsonarray.getJSONObject(0)["user_id"].toString())
                } catch (err: JSONException) {
                    Log.d("Error", err.toString())
                }

                Log.d("extras_key_project_id", project_id.toString())

            }
        }
        try {
            Log.d("extra_action", intent.extras?.get("quote").toString())
        } catch (err: JSONException) {
            Log.d("Error", err.toString())
        }
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading =findViewById(R.id.loading_progress_xml) as ImageView
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.VISIBLE)
        Loading!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    Loading!!.setVisibility(View.VISIBLE)
                }
            })
//        val device_id = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
//            ?.getString("device_id","")
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token", "")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("lang", "")

        val device_id = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("device_id", "")
        Log.d("user_token", header)

        if (!header!!.equals("")){
            //token_expire = CheckTokenExpire(this,"https://uat.jper.com.hk/api/token/check",header)
            //token_expire =true
            token_expire =false
        }

        if (!lang!!.equals("")){
            val locale = Locale(lang)
            val config = resources.configuration
            Locale.setDefault(locale)
            config.locale = locale
            resources.updateConfiguration(config, resources.displayMetrics)
        }

        Handler().postDelayed({
            // This method will be executed once the timer is over
//            Start your app main activity
            if (!header!!.equals("") && !lang!!.equals("") && !token_expire && !device_id.equals("")) {
                //if(!header!!.equals("") && !lang!!.equals("") && !token_expire ) {
                if (action == 0) {
                    startActivity(Intent(this, MainActivity::class.java))
                } else if (action == 8 || action == 4) {
                    val intent = Intent(this, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.putExtra("fragment_id", R.id.navigation_myprojects_detail)
                    intent.putExtra("bg_accepted", bg_accepted)
                    intent.putExtra("project_id", project_id)
                    intent.putExtra("status_id", status_id)
                    intent.putExtra("contract_sum", contract_sum)
                    intent.putExtra("post_date", post_date)
                    intent.putExtra("contract_m", contract_m)
                    intent.putExtra("scope_id", scope_id)
                    Log.d("okokokok_contractm", contract_m.toString())
                    startActivity(intent)
                } else if (action == 5) {
                    val intent = Intent(this, ChatActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.putExtra("key", key)
                    intent.putExtra("company_name_en", company_name_en)
                    intent.putExtra("company_name_zh", company_name_zh)
                    intent.putExtra("company_logo", company_logo)
                    intent.putExtra("navigation_id", R.id.activity_notification)
                    Log.d("okokokok", "okokokok")
                    startActivity(intent)
                }
            } else if ((header!!.equals("") || token_expire || device_id.equals("")) && !lang!!.equals(
                    ""
                )
            ) startActivity(Intent(this, FirstPageActivity::class.java))
            //  else if( (header!!.equals("") || token_expire ) && !lang!!.equals("")) startActivity(Intent(this, FirstPageActivity::class.java))
            else if ((header!!.equals("") || token_expire || device_id.equals("")) && lang!!.equals(
                    ""
                )
            ) startActivity(Intent(this, ChooseLangActivity::class.java))
            //    else if( (header!!.equals("") || token_expire ) && lang!!.equals("")) startActivity(Intent(this, ChooseLangActivity::class.java))
            // close this activity
            finish()
        }, SPLASH_TIME_OUT)
    }

}
