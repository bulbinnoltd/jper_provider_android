package com.jpertechnologyltd.jperprovider

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.viewpager2.widget.ViewPager2
import com.jpertechnologyltd.jperprovider.item.OnBoarding
import com.jpertechnologyltd.jperprovider.adapter.OnBoardingAdapter
import kotlinx.android.synthetic.main.activity_onboarding.*


class OnBoardingActivity :AppCompatActivity(){
    private val introSliderAdapter = OnBoardingAdapter(
        listOf(
            OnBoarding(
                "Connect & Build Your Space With Ease",
                "Do you get paranoid from walking shop to shop just to compare pricing for your upcoming renovation?",
                R.drawable.onboard_msg1
            ),
            OnBoarding(
                "Connect & Build Your Space With Ease",
                "How do you keep track of all these items if they are mostly hidden behind walls? ",
                R.drawable.onboard_msg2
            ),
            OnBoarding(
                "Connect & Build Your Space With Ease",
                "Don't fret! Jper allows project owners to easily compare, match, track, monitor, and pay their service providers anywhere on the go.",
                R.drawable.onboard_msg3
            )
        )
    )

    override fun onCreate(savedInstantState: Bundle?){
        super.onCreate(savedInstantState)
        setContentView(R.layout.activity_onboarding)

        introSliderViewPager.adapter = introSliderAdapter
        setupIndicators()
        setCurrentIndicator(0)
        introSliderViewPager.registerOnPageChangeCallback(object:
            ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentIndicator(position)
            }
        })
        textSkipIntro.setOnClickListener{

            Log.d("badge","badge")
//            Intent(applicationContext, FirstPageActivity::class.java).also{
//                startActivity(it)
//                finish()
//        }
            Intent(applicationContext, FirstPageActivity::class.java).also{
                startActivity(it)
                finish()
            }
        }
    }

    private fun setupIndicators(){
        val indicators = arrayOfNulls<ImageView>(introSliderAdapter.itemCount)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        layoutParams.setMargins(8,0,8,0)
        for(i in indicators.indices){
            indicators[i] = ImageView(applicationContext)
            indicators[i].apply {
                this?.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )
                this?.layoutParams = layoutParams
            }
            indicatorsContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicator(index: Int){
        val childCount = indicatorsContainer.childCount
        for (i in 0 until childCount){
            val imageView = indicatorsContainer[i] as ImageView
            if(i == index){
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_active
                    )
                )
            }else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.indicator_inactive
                    )
                )
            }
        }
    }
}