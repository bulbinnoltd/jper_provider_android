package com.jpertechnologyltd.jperprovider

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.material.textfield.TextInputEditText
import de.hdodenhof.circleimageview.CircleImageView
import java.io.File

class RegisterActivity : AppCompatActivity() {
    lateinit var  profile_image : CircleImageView
    private var mProfileFile: File? = null
    private var profile_image_file: File? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        profile_image = findViewById<CircleImageView>(R.id.profile_image)
        val sign_up_button = findViewById<Button>(R.id.sign_up_button)
        val register_username_value = findViewById<TextInputEditText>(R.id.register_username_value)
        val register_email_value = findViewById<TextInputEditText>(R.id.register_email_value)
        val register_password_value = findViewById<TextInputEditText>(R.id.register_password_value)
        val register_re_password_value = findViewById<TextInputEditText>(R.id.register_re_password_value)
        val register_phone_no_value = findViewById<TextInputEditText>(R.id.register_phone_no_value)

        sign_up_button.setOnClickListener {
            //check password and re-enter password
            if (register_password_value.text.toString().equals(register_re_password_value.text.toString())) {
                //sendinfo(register_username_value.text, register_email_value.text,register_password_value.text,register_phone_no_value.text,profile_image_file)
                val intent = Intent (this, MainActivity::class.java)
                startActivity(intent)
            }
            else{

            }
        }

        profile_image.setOnClickListener {

                ImagePicker.with(this)
                    .crop()	    			//Crop image(Optional), Check Customization for more option
//                    .compress(1024)			//Final image size will be less than 1 MB(Optional)
                    .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                    .createIntentFromDialog { launcher.launch(it) }

        }

        val login_button = findViewById<TextView>(R.id.login_button)
        // set on-click listener
        login_button.setOnClickListener {

            val intent = Intent (this, FirstPageActivity::class.java)
            startActivity(intent)
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            profile_image_file = File(fileUri?.path)
            profile_image.setImageURI(fileUri)

            //You can get File object from intent
            ImagePicker.getFile(data)

            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(data)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    private val launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = it.data?.data
            profile_image_file = File(fileUri?.path)
            profile_image.setImageURI(fileUri)

            //You can get File object from intent
            ImagePicker.getFile(it.data)

            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(it.data)
        } else if (it.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(it.data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    fun sendinfo(username: Editable?, email: Editable?, password: Editable?, phone:Editable?, profile_image_file: File?){

        Log.d("register",username.toString().plus(email.toString()).plus(password.toString()).plus(phone.toString().plus(profile_image_file.toString())))
    }
//    private fun showDialog(activity : Activity, message: String) {
//        val dialog = Dialog(activity)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog.setCancelable(false)
//        dialog.setContentView(R.layout.dialog_message)
//
//        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
//        dialog_message.text = message
//        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button
//
//        val close_button = dialog.findViewById(R.id.close_button) as ImageView
//        confirm_button.setOnClickListener {
//            dialog.dismiss()
//        }
//        close_button.setOnClickListener {
//            dialog.dismiss()
//        }
//
//        dialog.setCancelable(true)
//        dialog.show()
//
//    }
}



