package com.jpertechnologyltd.jperprovider

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.material.textfield.TextInputEditText
import com.koushikdutta.ion.Ion
import com.koushikdutta.ion.builder.Builders
import de.hdodenhof.circleimageview.CircleImageView
import java.io.File


class UpdateProfileActivity : AppCompatActivity() {
    var Loading: ImageView? = null
    var progressOverlay: FrameLayout? = null
    lateinit var profile_image: CircleImageView
    private var mProfileFile: File? = null
    private var profile_image_file: File? = null
    private var profile_image_url: String? = null
    private var header : String? = null
    var lang:String? ="en"
    var icon= ""
    var fileUri : Uri? = null
    var activity:Activity?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_updateprofile)
        activity = this
        header = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("user_token","")
        lang = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)?.getString("lang","en")
        profile_image_url = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
            ?.getString("profile_image","")

        profile_image = findViewById<CircleImageView>(R.id.profile_image)
        /////////////////////Initiate_loading_anim///////////////////////////
        Loading = findViewById(R.id.loading_progress_xml) as ImageView
        progressOverlay = findViewById(R.id.progress_overlay)
        Glide.with(this).load(R.drawable.jper_loading).into(Loading!!)
        Loading!!.setVisibility(View.GONE)
        progressOverlay!!.animate()
            .setDuration(3000)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    progressOverlay!!.setVisibility(View.VISIBLE)
                }
            })

        icon=intent.getStringExtra("icon")


        val username=intent.getStringExtra("username")
        val phone=intent.getStringExtra("phone")

        val sign_up_button = findViewById<Button>(R.id.sign_up_button)
        val register_username_value = findViewById<TextInputEditText>(R.id.register_username_value)
        val register_phone_no_value = findViewById<TextInputEditText>(R.id.register_phone_no_value)

        register_username_value.setText(username)
        register_phone_no_value.setText(phone)



        Log.d("goshgosh",icon.toString())
        try {
            val glideUrl = GlideUrl(
                icon,
                LazyHeaders.Builder()
                    .addHeader("Authorization", "Bearer ".plus(header))
                    .build()
            )
            if((icon.equals("https://uat.jper.com.hk/user/icon") ||icon.equals("")) && profile_image_file==null)
                Glide.with(this).load(R.drawable.profile_pic_placeholder).into(profile_image)
            else if(!icon.equals("https://uat.jper.com.hk/user/icon") && profile_image_file==null)
                Glide.with(this).load(glideUrl).into(profile_image)
            else if(profile_image_file!=null)
                profile_image.setImageURI(Uri.parse( fileUri?.path))

        }
        catch (e:Exception){
            Log.d("eeeeeeeeeeee",e.toString())
            Glide.with(this).load(R.drawable.profile_pic_placeholder).into(profile_image)

        }

        val back_button = findViewById(R.id.back_button) as ImageView
        // set on-click listener
        back_button.setOnClickListener {
            onBackPressed()
        }
        sign_up_button.setOnClickListener {
            //check password and re-enter password

            Loading!!.visibility = View.VISIBLE
            progressOverlay!!.visibility = View.VISIBLE

            var username:String?=null
            var phone_no:String?=null
            try { username=  register_username_value.text.toString()}catch (x: Exception) { }
            try { phone_no=  register_phone_no_value.text.toString()}catch (x: Exception) { }
            if(username.equals("") ||phone_no.equals("") )
            {
                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
                showDialog(this,getString(R.string.Without_info))
            }
            else {
                sendinfo(
                    register_username_value.text,
                    register_phone_no_value.text,
                    profile_image_file
                )

                Loading!!.visibility = View.GONE
                progressOverlay!!.visibility = View.GONE
            }
        }

        profile_image.setOnClickListener {

            ImagePicker.with(this)
                .crop() //Crop image(Optional), Check Customization for more option
//                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .createIntentFromDialog { launcher.launch(it) }

        }

    }
    private val launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            fileUri = it.data?.data
            profile_image_file = File(fileUri?.path)

            //You can get File object from intent
            ImagePicker.getFile(it.data)

            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(it.data)

            profile_image.setImageURI(Uri.parse( fileUri?.path))

            val editor =
                this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                    .edit()
            editor.putString("profile_image", fileUri?.path)
            editor.apply()

        } else if (it.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(it.data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            fileUri = data?.data
            profile_image_file = File(fileUri?.path)

            //You can get File object from intent
            ImagePicker.getFile(data)

            //You can also get File Path from intent
            val filePath: String? = ImagePicker.getFilePath(data)

            profile_image.setImageURI(Uri.parse( fileUri?.path))

            val editor =
                this.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
                    .edit()
            editor.putString("profile_image", fileUri?.path)
            editor.apply()

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    fun sendinfo(
        username: Editable?,
        phone: Editable?,
        profile_image_file: File?
    ) {

        try {
            var builder: Builders.Any.M =
                Ion.with(baseContext)
                    .load("https://uat.jper.com.hk/api/company/staff/profile/update")
                    .setHeader("Authorization", "Bearer " +header)
                    .setHeader("Content-Language", lang)
                    .setMultipartParameter("username",  username.toString())
                    .setMultipartParameter("phone", phone.toString())

            if (profile_image_file!=null) {
                builder =
                    builder.setMultipartFile("icon", "image/jpeg,png", profile_image_file)
            }

            builder.asJsonObject()
                .setCallback { e, result ->
                    if (e != null)
                        Log.d("result_error", e.toString())
                    if (result["result"].asString.equals("success")) {

                        Log.d("result_updated", "result_updated")
                        showDialog(this, getString(R.string.Successfully_changed_msg))

                    }
                    else if (result["result"].asString.equals("error")) {

                        showDialog(this, result["msg"].asString)
                    }
                    Log.d("result_okok", result.toString())
                }
        } catch (x: Exception) {
            Log.d("result_error", x.toString())
        }
    }


    private fun showDialog(activity : Activity, message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_message)

        val dialog_message = dialog.findViewById(R.id.dialog_message) as TextView
        dialog_message.text = message
        val confirm_button = dialog.findViewById(R.id.confirm_button) as Button

        val close_button = dialog.findViewById(R.id.close_button) as ImageView
        confirm_button.setOnClickListener {
            dialog.dismiss()
            if(message.equals(getString(R.string.Successfully_changed_msg))){
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()

            }
        }
        close_button.setOnClickListener {

            dialog.dismiss()
        }

        dialog.setCancelable(true)
        dialog.show()

    }
    fun isEmptyString(text: String?): Boolean {
        return text == null || text.trim { it <= ' ' } == "null" || text.trim { it <= ' ' }
            .isEmpty()
    }
}

